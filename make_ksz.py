#+
# This is ABYSS (./make_ksz.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
#!/usr/bin/python
import healpy as hp
import numpy as np
import os

cmd="../genksz  --power=-1.8 --norm=0.1 --Nside=%d --Lmax=%d --component=%d --seed=%d"
cmd2="../generateRandomMap %d %d"

SEED=3023423420
V=[10,-30,90]
Nside=128
Lmax=128
noiseLevel=1e-6

os.system(cmd % (Nside,Lmax,0,SEED))
os.rename("outmap.fits","ksz_vx.fits")
os.system(cmd % (Nside,Lmax,1,SEED))
os.rename("outmap.fits","ksz_vy.fits")
os.system(cmd % (Nside,Lmax,2,SEED))
os.rename("outmap.fits","ksz_vz.fits")

mtot = np.zeros(12*Nside*Nside, dtype=np.float64)
j=0
for m in ["x","y","z"]:
  mtot = mtot  + V[j]*hp.read_map("ksz_v%s.fits" % m)
  j = j +1

hp.write_map("ksz_tot.fits", mtot, dtype=np.float64)

os.system(cmd2 % (Lmax,Nside))

hp.write_map("ref_plus_ksz.fits", hp.read_map("ref.fits") + mtot)

try:
  os.unlink("mycmb.fits")
except OSError:
  pass

os.system("../../main/add_noise ref_plus_ksz.fits mycmb.fits %g" % noiseLevel) 
