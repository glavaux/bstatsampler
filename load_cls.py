#+
# This is ABYSS (./load_cls.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import numpy as np
from Scientific.IO.NetCDF import NetCDFFile

def load_all_cls():

    state_id = int(file("current_state.txt").readline())

    s_cls = NetCDFFile("out/state_0.nc").variables["cls_sampler_cls"].shape

    print "Lmax=" + str(s_cls[0])
    lmax = s_cls[0]

    all_cls = np.zeros((lmax, state_id),dtype=np.float64)
    all_ksz = np.zeros((3, state_id),dtype=np.float64)
    all_dipole = np.zeros((3,2,state_id),dtype=np.float64)

    for i in range(state_id):
        f = NetCDFFile("out/state_%d.nc" % i)
        print i

        all_cls[:,i] = f.variables["cls_sampler_cls"][:]
        all_ksz[0,i] = f.variables["ksz_sampler_1_alpha"][:]
        all_ksz[1,i] = f.variables["ksz_sampler_2_alpha"][:]
        all_ksz[2,i] = f.variables["ksz_sampler_3_alpha"][:]
        all_dipole[:,:,i] = f.variables["dipole_sampler_dipole"][:]
        f.close()

    return all_cls,all_ksz,all_dipole


def plot_all_cls(from_iter=0):

    import healpy as hp
    from matplotlib.pylab import loglog, clf

    ref = np.array(hp.read_cl("spectrum.fits"))['Temperature C_l']

    all_cl=load_all_cls()[0]

    clf()
    for i in range(from_iter,all_cl.shape[1]):
        loglog(all_cl[:,i])

    loglog(ref)
