/*+
This is ABYSS (./libsampler/tools/cg_relax.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <fstream>
#include <iostream>
#include <boost/function.hpp>
#include <boost/format.hpp>
#include "cmb_data.hpp"
#include "cg_relax.hpp"
#include "noise_weighing.hpp"
#include "conjugateGradient.hpp"
#include <fitshandle.h>
#include <alm_fitsio.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <boost/chrono.hpp>
#include <CosmoTool/algo.hpp>

using namespace std;

using CosmoTool::square;
using boost::format;
using boost::str;
using boost::chrono::system_clock;
using boost::chrono::duration;

using namespace CMB;

void CMB::conjugateGradient_relax_solve_auto_scale
                      (RelaxInfo& rinfo,
                      ALM_Generator generate_b,
                      ALM_Map& c_map_hat,
                      CMB_Data& data,
                      MapSignalNoiseSignal_weighing *matrix,
                      Logger *logger)
{
  ALM_Map curInput, save_cmap;
  long Lmax = data.Lmax;
  long l_iter, l_iter_done;
  std::vector<double> base_alpha;
  system_clock::time_point start;
  double alphaN = data.channels[0].Npix()/(4*M_PI);
  ALM_Map c_map_hat_new;
  double alpha_mul;

  double min_noise = std::numeric_limits<double>::max();
  for (int ch = 0; ch < data.numChannels; ch++)
    min_noise = min(min_noise, data.noise[ch]->as<CMB_NN_NoiseMap>()->min_noise);

  clear(c_map_hat);

  base_alpha.resize(data.numChannels);
  for (int ich = 0; ich < data.numChannels; ich++)
    base_alpha[ich] = data.noise[ich]->as<CMB_NN_NoiseMap>()->alpha;

  int iter = 0;

  l_iter = 20;
  l_iter_done = -1;

  cout << "alphaN = " << alphaN << endl;

  start = system_clock::now();
  do
    {
      alpha_mul = std::max(double(1),alphaN*square((matrix->sqrt_cls)[l_iter])/min_noise);
      alpha_mul = sqrt(alpha_mul);

      if (alpha_mul > 1 && l_iter_done == data.Lmax)
        alpha_mul = 1;

      for (int ich = 0; ich < data.numChannels; ich++)
        data.noise[ich]->SetFlexScaling(base_alpha[ich]*alpha_mul);

      curInput = (generate_b(alpha_mul, l_iter));
      if (curInput.Lmax() != l_iter)
        {
          ALM_Map reduced_input(l_iter, l_iter);
          for (long m = 0; m <= min(long(curInput.Lmax()),l_iter); m++)
           {
             for (long l = m; l <= min(long(curInput.Lmax()), l_iter); l++)
              reduced_input(l,m) = curInput(l,m);
           }
          curInput = reduced_input;
        }

      c_map_hat_new.Set(l_iter, l_iter);
      c_map_hat_new.SetToZero();
      // Upgrade the resolution of the solution
      for (long m = 0; m <= l_iter_done; m++)
        {
          for (long l = m; l <= l_iter_done; l++)
            c_map_hat_new(l,m) = c_map_hat(l,m);
        }

      if (logger) {
        logger->enter("RELAXAUTO");
        logger->line() << format("Inverting (Lmax=%d, scale=%g)...") % l_iter % (alpha_mul);
      }
      
      conjugateGradient_pre(curInput, c_map_hat_new, *matrix,
                            *data.preconditioner, rinfo.epsilonCG, 50,
                            -1, false, (ALM_Map*)0, logger);

      if (logger) {
        logger->leave();
      }
      
      c_map_hat = c_map_hat_new;
#if 0
      {
        ALM_Map tmp_alm = c_map_hat;
        CMB_Map tmp_map(data.Nside, RING, SET_NSIDE);
        tmp_alm.ScaleL(info.sqcls);
        alm2map(tmp_alm, tmp_map);

        fitshandle h;
        h.create(str(format("!relax_%d.fits") % iter));
        write_Healpix_map_to_fits(h, tmp_map, planckType<double>());
      }
#endif
      iter++;

      l_iter_done = l_iter;
      l_iter = std::min(data.Lmax, std::min(l_iter+500, 2*l_iter));
    }
  while (alpha_mul > 1 || l_iter_done < data.Lmax);
  duration<double> sec = system_clock::now() - start;
  ofstream f("cg_bench.txt", ios::app);
  f << format("%20.10e") % sec << endl;
}

static void save_cg_progression(int iter,
               ALM_Map& sol)
{
  fitshandle h;
  h.create(str(format("!debug/cgrelax_%d.fits") % iter));
  write_Alm_to_fits(h, sol, sol.Lmax(), sol.Mmax(), planckType<double>());
}


void CMB::conjugateGradient_relax_solve
                  (RelaxInfo& info,
                  ALM_Generator generate_b,
                  ALM_Map& c_map_hat,
                  CMB_Data& data,
                  MapSignalNoiseSignal_weighing *matrix,
                  Logger *logger,
                  ALM_Map *delta_solution)
{
  ALM_Map curInput, save_cmap;
  long Lmax = data.Lmax;
  std::vector<double> base_alpha;
  system_clock::time_point start;

  clear(c_map_hat);

  base_alpha.resize(data.numChannels);
  for (int ich = 0; ich < data.numChannels; ich++)
    base_alpha[ich] = data.noise[ich]->as<CMB_NN_NoiseMap>()->alpha;

  double alpha_mul = info.relaxation_start*info.relaxation_reduction;
  int iter = 0;

  curInput.Set(Lmax, Lmax);

  start = system_clock::now();
  
  logger->enter("RELAX");
  do
    {
      alpha_mul = std::max(double(1),alpha_mul/info.relaxation_reduction);

      for (int ich = 0; ich < data.numChannels; ich++)
        data.noise[ich]->SetFlexScaling(base_alpha[ich]*alpha_mul);

      curInput = (generate_b(alpha_mul, data.Lmax));

      if (logger != 0)
        logger->line() << format("Inverting (Lmax=%d, scale=%g)...") % Lmax % alpha_mul << endl;

      conjugateGradient_pre(curInput, c_map_hat, *matrix,
			    *data.preconditioner, info.epsilonCG*alpha_mul, 50,
			    -1, false, delta_solution, logger, boost::function2<void,int,ALM_Map&>(save_cg_progression));

      iter++;
    }
  while (alpha_mul > 1);
  logger->leave();
  
  duration<double> sec = system_clock::now() - start;
  ofstream f("cg_bench.txt", ios::app);
  f << format("%20.10e") % sec << endl;
}
