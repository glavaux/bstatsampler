/*+
This is ABYSS (./libsampler/tools/quadEstimator.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <sstream>
#include <fstream>
#include <cassert>
#include <cxxutils.h>
#include <alm.h>
#include <xcomplex.h>
#include <alm_healpix_tools.h>
#include <healpix_map.h>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "quadEstimator.hpp"
#include "wigner3j.hpp"
#include "noise_weighing.hpp"
#include <string>
#include <omp.h>
#include <unistd.h>

#define BEAM_LEVEL 1e-9

using namespace std;
using namespace CMB;

QuadraticEstimator::QuadraticEstimator(long inNside, long nL, long nL_internal, const arr<double>& w, const std::string precomputed)
  : nL_CMB(nL), nL_compute(nL_internal), noisePerPixel(0.), A_TT(nL_internal+1), 
    Cls(max(nL,nL_internal)+1), Cls_lensed(max(nL, nL_internal)+1), NoisyCls(max(nL,nL_internal)+1), Nside(inNside), weights(w), Beam(nL_internal+1)
{  
  long lmax = nL_compute;
  long nSide_compute = nL_compute;

  planck_assert(weights.size() == 2*inNside, "Invalid HEALPix weights");
  
  for (int i = 0; i < 2; i++)
    {
      gradient_T[i].SetNside(nSide_compute, RING);
      gradient_TT[i].SetNside(nSide_compute, RING);
    }
  laplacian_T.SetNside(nSide_compute, RING);
  tmp_map.SetNside(nSide_compute, RING);
  alms_input.Set(lmax, lmax);
  alms_TT.Set(nL_CMB, nL_CMB);
  alms_T.Set(lmax, lmax);
  weights_internal.alloc(2*nSide_compute);
  weights_internal.fill(1);
  laplacian_alms.Set(lmax, lmax);
  Beam.fill(1);
  
  Npix = 12*Nside*Nside;
  cerr << "** Initialized quadratic estimator (Lmax_CMB=" << nL_CMB << ", Lmax=" << nL_compute << ", Nside=" << Nside << ")" << endl;
  
  // If some precomputed data exists on hard drive, load it now.
  if (precomputed != "")
    {
    }
 }


QuadraticEstimator::~QuadraticEstimator()
{
}


void QuadraticEstimator::set_Cl_TT(const arr<double>& Cls, const arr<double>& Cls_lensed)
{
  double alpha = 4*M_PI/Npix;

  planck_assert(this->Cls.size() >= Cls.size(), "invalid spectrum size");
  for (long l = 0; l < Cls.size(); l++)
    {
      double b = max(BEAM_LEVEL, Beam[l]);

      this->Cls[l] = Cls[l];
      this->Cls_lensed[l] = Cls_lensed[l];
      NoisyCls[l] = Cls_lensed[l] + noisePerPixel*alpha/(b*b);
    }
  for (long l = Cls.size(); l < this->Cls.size(); l++)
    {
      double b = max(BEAM_LEVEL, Beam[l]);

      this->Cls_lensed[l] = this->Cls[l] = 0;
      NoisyCls[l] = noisePerPixel*alpha/(b*b);
    }
  recompute_A_TT();

}
 
void QuadraticEstimator::setNoiseLevelPerPixel(double noise)
{
  noisePerPixel = noise*noise;
}

void QuadraticEstimator::setBeam(const arr<double>& newBeam)
{
  Beam = newBeam;
}


void QuadraticEstimator::wienerFilter(const ALM_Map& input_d, ALM_Map& filtered)
{
  long lmax_out = filtered.Lmax(), lmax_in = input_d.Lmax();

  filtered.SetToZero();

#pragma omp parallel for schedule(dynamic)
  for (long l = 0; l <= min(lmax_in, lmax_out); l++)
    {
      double ratio = Cls[l]/NoisyCls[l];
      for (long m = 0; m <= l; m++)
        {
          // regulated temperature
          filtered(l,m) = input_d(l,m)*ratio;
        }      
    }
}

void QuadraticEstimator::computeEstimatedPotential(const ALM_Map& input_d,
						   const ALM_Map& input_T,
						   ALM_Map& potential_alm,
						   arr<double>& alm_variance)
{
  long lmax = nL_compute;
  long lmaxTT = nL_CMB;

  planck_assert(input_d.Lmax() <= lmax, "Input data a_lms have too many coefficients");
  planck_assert(input_T.Lmax() <= lmaxTT, "Input temperature a_lms have too many coefficients");

  alms_input.SetToZero();
  alms_TT.SetToZero();

#pragma omp parallel for schedule(dynamic)
  for (long l = 0; l <= input_d.Lmax(); l++)
    for (long m = 0; m <= l; m++)
      alms_input(l,m) = input_d(l,m);

#pragma omp parallel for schedule(dynamic)
  for (long l = 0; l <= input_T.Lmax(); l++)
    for (long m = 0; m <= l; m++)
      alms_TT(l,m) = input_T(l,m);

  computeEstimatedPotential(potential_alm, alm_variance);
}

void QuadraticEstimator::computeEstimatedPotential(const CMB_Map& input_d,
						   ALM_Map& potential_alm,
						   arr<double>& alm_variance)
{
  HEALPIX_method(input_d, alms_input, weights);
  wienerFilter(alms_input, alms_TT);

  computeEstimatedPotential(potential_alm, alm_variance);
}

void QuadraticEstimator::computeEstimatedPotential(const CMB_Map& input_d,
						   const CMB_Map& input_T,
						   ALM_Map& potential_alm,
						   arr<double>& alm_variance)
{
  HEALPIX_method(input_d, alms_input, weights);
  HEALPIX_method(input_T, alms_TT, weights);

  computeEstimatedPotential(potential_alm, alm_variance);
}

void QuadraticEstimator::computeEstimatedPotential(ALM_Map& potential_alm,
						   arr<double>& alm_variance)
{
  long lmax = nL_compute;
  long lmaxTT = nL_CMB;
   
  // Compute the regulated gradient of temperature  and its laplacian
#pragma omp parallel for schedule(dynamic)
  for (long l = 0; l <= lmax; l++)
    {
      double inv_NCl = 1.0/NoisyCls[l];
      for (long m = 0; m <= l; m++)
	{
	  // weighed temperature
	  alms_T(l,m) = alms_input(l,m)*inv_NCl;
        }
    }

   for (long l = 0; l <= lmax; l++)
    {
      for (long m = 0; m <= l; m++)
        {
	  // laplacian of regulated temperature
	  if (l <= lmaxTT)
	    laplacian_alms(l,m) = -alms_TT(l,m)*l*(l+1.);
	  else
	    laplacian_alms(l,m) = 0;
	}
    }
  alms_TT(0,0) = alms_TT(1,0) = alms_TT(1,1) = 0;

  alm2map_der1(alms_TT, tmp_map, gradient_TT[0], gradient_TT[1]);
  alm2map_der1(alms_T, tmp_map, gradient_T[0], gradient_T[1]);
  alm2map(laplacian_alms, laplacian_T);

  long Npix_internal = laplacian_T.Npix();
  for (long i = 0; i < Npix_internal; i++)
    tmp_map[i] = (gradient_T[0][i]*gradient_TT[0][i] + gradient_T[1][i]*gradient_TT[1][i]) + tmp_map[i] * laplacian_T[i];
  
  HEALPIX_method(tmp_map, potential_alm, weights_internal);
  alm_variance.fill(-1);
  for (long l = 1; l <= min(nL_compute,(long)potential_alm.Lmax()); l++)
    {
      for (long m = 0; m <= l; m++)
	{
	  potential_alm(l,m) *= -A_TT[l];
	}
      alm_variance[l] = A_TT[l];
    }
}

static
void compute_F(int l1, long& Lmin, long& Lmax, int l2, int s, arr<double>& coefs)
{
  double alpha, beta;

  compute3j(l2, l1, -s, s, Lmin, Lmax, coefs);

  for (int L = Lmin,i = 0; L <= Lmax; L++, i++)
    {
      alpha = L*(L+1) - l1*(l1+1) + l2*(l2+1);
      beta = sqrt((2*L+1.)*(2*l1+1.)*(2*l2+1.)/(16*M_PI));

      coefs[i] *= alpha*beta;
    }
}

static
void compute_f_TT(int l1, long& Lmin, long& Lmax, int l2, arr<double>& coefs, arr<double>& coefs2, const arr<double>& TT_spectrum)
{
  compute_F(l1, Lmin, Lmax, l2, 0, coefs2);
  
  assert(Lmax < coefs.size());
  assert(Lmin >= 0);
  
  for (long i = Lmin; i <= Lmax; i++)
    coefs[i] = TT_spectrum[l2] * coefs2[i-Lmin];
  
  compute_F(l2, Lmin, Lmax, l1, 0, coefs2);
  
  assert(Lmax < coefs.size());
  assert(Lmin >= 0);
  
  for (long i = Lmin; i <= Lmax; i++)
    coefs[i] += TT_spectrum[l1] * coefs2[i-Lmin];  
}


void QuadraticEstimator::recompute_A_TT()
{
  long lmax = Cls.size()-1;
  long lmaxCMB = nL_CMB;
  long lmaxMAX = max(lmaxCMB, lmax);
  arr<arr<double> > A_TT_threads;
  int numUsedThreads;

  cerr << "** Recomputing normalization of the quadratic estimator" << endl;

  A_TT_threads.alloc(omp_get_max_threads());

#pragma omp parallel
  { 
    long loc_Lmin, loc_Lmax;
    arr<double> coefs(2*lmaxMAX+2), coefs2(2*lmaxMAX+2);
    int tid = omp_get_thread_num();
    arr<double>& loc_A_TT = A_TT_threads[tid];
    long max_loc_A_TT = A_TT.size()-1;
    
    if (tid == 0)
      numUsedThreads = omp_get_num_threads();
    loc_A_TT.alloc(A_TT.size());
    loc_A_TT.fill(0);
    
//    int lstart = (lmax+1)*tid/omp_get_num_threads();
//    int lend = (lmax+1)*(tid+1)/omp_get_num_threads();
    
//    usleep(100*tid);
//    (cerr << lstart << " ==> " << lend << endl).flush();

#pragma omp for schedule(dynamic)
    for (long l1 = 1; l1 <= lmaxMAX; l1++)
      {
	(cerr << l1 << " ").flush();
	for (long l2 = 1; l2 <= lmaxMAX; l2++)
	  {
	    compute_f_TT(l1, loc_Lmin, loc_Lmax, l2, coefs, coefs2, Cls);
	    for (long L = loc_Lmin; L <= min(loc_Lmax,max_loc_A_TT); L++)
	      loc_A_TT[L] += coefs[L]*coefs[L]/(2*NoisyCls[l1]*NoisyCls[l2]);
	  }
      }

    {
      ostringstream ss;
      ss << "loc_A_TT_" << tid << ".txt";
      string s = ss.str();
      ofstream f(s.c_str());

      for (long L = 0; L <= max_loc_A_TT; L++)
	f << L << " " << loc_A_TT[L] << endl;
    }
  }
  
  cerr << endl;
  A_TT.fill(0);
  for (int i = 0; i < numUsedThreads; i++)
    {
      arr<double>& loc_A_TT = A_TT_threads[i];
      for (long L = 0; L <= nL_compute; L++)
	A_TT[L] += loc_A_TT[L];
    }

  A_TT[0] = A_TT[1] = 0;
  for (long L = 2; L <= nL_compute; L++)
    {
      A_TT[L] = (2*L+1)/A_TT[L];
    }

  {
    ofstream p("A_TT.txt");
    for (long L = 0; L <= nL_compute; L++)
      p << L << " " << A_TT[L] << endl;
  }
 }

