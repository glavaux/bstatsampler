/*+
This is ABYSS (./libsampler/tools/inv.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <iostream>

template<typename ArrayType, typename T>
T computeCofactor(const ArrayType & a, unsigned int col, unsigned int row)
{
  static const unsigned int sel0[] = { 1,0,0,0 };
  static const unsigned int sel1[] = { 2,2,1,1 };
  static const unsigned int sel2[] = { 3,3,3,2 };
  
  // Let's first define the 3x3 matrix:
  const unsigned	int col0 = sel0[col];
  const unsigned	int col1 = sel1[col];
  const unsigned	int col2 = sel2[col];
  const unsigned	int row0 = sel0[row];
  const unsigned	int row1 = sel1[row];
  const unsigned	int row2 = sel2[row];

  // Compute the float sign-mask:
  const unsigned	int signpos  = (col + row) & 1;

  const T	pos_part1 = 
    a[row0*4 + col0] * a[row1*4 + col1] * a[row2*4 + col2];
  const T	pos_part2 = 
    a[row1*4 + col0] * a[row2*4 + col1] * a[row0*4 + col2];
  const T	pos_part3 = 
    a[row2*4 + col0] * a[row0*4 + col1] * a[row1*4 + col2];
  
  const T	neg_part1 = 
    a[row0*4 + col0] * a[row2*4 + col1] * a[row1*4 + col2];
  const T	neg_part2 = 
    a[row1*4 + col0] * a[row0*4 + col1] * a[row2*4 + col2];
  const T	neg_part3 = 
    a[row2*4 + col0] * a[row1*4 + col1] * a[row0*4 + col2];

  const T pos_part  = pos_part1 + pos_part2 + pos_part3;
  const T neg_part  = neg_part1 + neg_part2 + neg_part3;
  const T det3x3 = pos_part - neg_part;

  return (signpos ? -1 : 1) * det3x3; 
}

template<typename ArrayType, typename T>
void inv4x4(const ArrayType& a, ArrayType&  b)
{
  b[0*4 + 0] = computeCofactor<ArrayType,T>(a, 0, 0);
  b[0*4 + 1] = computeCofactor<ArrayType,T>(a, 0, 1);
  b[0*4 + 2] = computeCofactor<ArrayType,T>(a, 0, 2);
  b[0*4 + 3] = computeCofactor<ArrayType,T>(a, 0, 3);

  b[1*4 + 0] = computeCofactor<ArrayType,T>(a, 1, 0);
  b[1*4 + 1] = computeCofactor<ArrayType,T>(a, 1, 1);
  b[1*4 + 2] = computeCofactor<ArrayType,T>(a, 1, 2);
  b[1*4 + 3] = computeCofactor<ArrayType,T>(a, 1, 3);

  b[2*4 + 0] = computeCofactor<ArrayType,T>(a, 2, 0);
  b[2*4 + 1] = computeCofactor<ArrayType,T>(a, 2, 1);
  b[2*4 + 2] = computeCofactor<ArrayType,T>(a, 2, 2);
  b[2*4 + 3] = computeCofactor<ArrayType,T>(a, 2, 3);

  b[3*4 + 0] = computeCofactor<ArrayType,T>(a, 3, 0);
  b[3*4 + 1] = computeCofactor<ArrayType,T>(a, 3, 1);
  b[3*4 + 2] = computeCofactor<ArrayType,T>(a, 3, 2);
  b[3*4 + 3] = computeCofactor<ArrayType,T>(a, 3, 3);

  T determinant =
    a[4*0 + 0] * b[4*0 + 0] +
    a[4*1 + 0] * b[4*0 + 1] +
    a[4*2 + 0] * b[4*0 + 2] +
    a[4*3 + 0] * b[4*0 + 3];

  T inv_det = 1/determinant;

  for (int k = 0; k < 16; k++)
    b[k] *= inv_det;
}

template<typename ArrayType, typename VecType>
void prod_vec(const ArrayType& a, const VecType& b, VecType& c)
{
  for (int k = 0; k < 4; k++)
    {
      c[k] = 
	a[4*k + 0] * b[0] + a[4*k + 1] * b[1] +
	a[4*k + 2] * b[2] + a[4*k + 3] * b[3];
    }
}

template<typename ArrayType, typename T>
void prod(const ArrayType& a, const ArrayType& b, ArrayType& c)
{
  for (int k = 0; k < 16; k++)
    {
      int k1 = k&3;
      int k2 = k>>2;
      T v1, v2, v3, v4;

      v1 = a[k2*4 + 0] * b[0*4 + k1];
      v2 = a[k2*4 + 1] * b[1*4 + k1];
      v3 = a[k2*4 + 2] * b[2*4 + k1];
      v4 = a[k2*4 + 3] * b[3*4 + k1];

      c[k] = v1+v2+v3+v4;      
    }
}
