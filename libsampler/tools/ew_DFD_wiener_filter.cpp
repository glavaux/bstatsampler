/*+
This is ABYSS (./libsampler/tools/ew_DFD_wiener_filter.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/filesystem.hpp>
#include <cmath>
#include <boost/format.hpp>
#include <iostream>
#include <cmath>
#include "ew_DFD_wiener_filter.hpp"
#include <healpix_map.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "noise_weighing.hpp"
#include <CosmoTool/algo.hpp>
#include "cmb_noise_np.hpp"

using namespace std;
using namespace CMB;
using boost::format;
using CosmoTool::square;

EW_DFD_WienerFilter::EW_DFD_WienerFilter(MPI_Communication *Comm, CMB_Data& data)
{
  comm = Comm;
  precision = data.rinfo.epsilonCG;
  default_boost = 1;//00;

  t_nu.alloc(data.numChannels);
  u_nu.alloc(data.numChannels);
  inv_Dtilde.alloc(data.numChannels);
  inv_Ftilde.alloc(data.numChannels);
  regularized_F.alloc(data.numChannels);
  u_nu.alloc(data.numChannels);
  tauD_nu.alloc(data.numChannels);
  tauF_nu.alloc(data.numChannels);
  Ffilter.alloc(data.numChannels);

  tau_init = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_NP_NoiseMap *inv_noise = data.noise[ch]->as<CMB_NP_NoiseMap>();
      CMB_Map& D = inv_noise->modulated_map;

      tauD_nu[ch] = *(max_element(&D[0], &D[D.Npix()]));
      assert(tauD_nu[ch] > 0);
//      tauD_init = max(tauD_init,tauD_nu[ch]);
      tauD_nu[ch] = 1/tauD_nu[ch];

      inv_Dtilde[ch].SetNside(data.Nside, RING);
      inv_Dtilde[ch].alpha=1.0;

      do
        {
          inv_Dtilde[ch].masked = 0;
          for (long p = 0; p < D.Npix(); p++)
            {
              if (D[p] > 0)
                {
                  double DD = 1/(D[p]*D[p]);
                  inv_Dtilde[ch][p] = max(double(0),DD-tauD_nu[ch]);
                  if (inv_Dtilde[ch][p] > 0)
                    inv_Dtilde[ch][p] = 1/inv_Dtilde[ch][p];
                  else {
                    inv_Dtilde[ch][p] = 0;
                    inv_Dtilde[ch].masked++;
                  }
                }
              else
                {
                  inv_Dtilde[ch][p] = 0;
                  inv_Dtilde[ch].masked ++;
                }
              assert(!isnan(inv_Dtilde[ch][p]));
            }
          if (inv_Dtilde[ch].masked == inv_Dtilde[ch].Npix())
            tauD_nu[ch] /= 2;
        }
      while(inv_Dtilde[ch].masked == inv_Dtilde[ch].Npix());


      const PowerSpec& F = inv_noise->Pnoise;
      const PowerSpec& invF = inv_noise->inv_Pnoise;
      tauF_nu[ch] = *(max_element(&invF[0], &invF[invF.size()]));
      tauF_nu[ch] = 1/tauF_nu[ch];

      inv_Ftilde[ch].alloc(F.size());
      regularized_F[ch].alloc(F.size());
      Ffilter[ch].alloc(F.size());
      
      for (int l = 0; l < F.size(); l++)
        {
          if (F[l] > 0)
            {
              double delta = std::max(0.0,F[l]-tauF_nu[ch]);
              inv_Ftilde[ch][l] = (delta > 0) ? 1/delta : 0;
              regularized_F[ch][l] = 1/(1 + delta/(tauF_nu[ch]));
            }
          else
            inv_Ftilde[ch][l] = 0;
            
          Ffilter[ch][l] = 1/(inv_Ftilde[ch][l] + 1/tauF_nu[ch]);
        }      
 
      u_nu[ch].SetNside(data.Nside, RING);
      t_nu[ch].SetNside(data.Nside, RING);
    }

//  tauD_init = 1/tauD_init;

  alpha_N = 12*data.Nside*data.Nside/(4*M_PI);

  active_channels.alloc(data.numChannels);
  active_channels.fill(false);
}


void EW_DFD_WienerFilter::run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s)
{
  CMB_Map s_map(data.Nside, RING, SET_NSIDE);
  
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ALM_Map beam_s = s;
      beam_s.ScaleL(data.beam[ch]);
      alm2map(beam_s, s_map);
      assert(!isnan(s_map[0]));
      
      computeMessengers_F(data, in_data, ch, s_map);
      computeMessengers_D(data, in_data, ch, s_map);
    }
    
  computeWiener(data, s);
  old_chi2 = new_chi2;
  new_chi2 = compute_chi2(data, in_data, s, true);

  long num_active = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    if (active_channels[ch])
      num_active+= in_data[ch].Npix();

  long full_N = 2*s.Alms().size();//(data.Lmax+1)*(data.Lmax+1);

  sigma_chi2 =  sqrt(double(full_N) + double(num_active));
//  cout << str(format("sigma_chi2 = %lg") % sigma_chi2)  <<endl;
}

bool EW_DFD_WienerFilter::check_convergence(const ALM_Map& s)
{
  if (old_chi2 < 0)
    return false;

//  return (old_chi2 > new_chi2) && ((old_chi2-new_chi2)/new_chi2) < (precision/precision_boost);
  return fabs((old_chi2-new_chi2)/new_chi2) < (precision/precision_boost);
}

void EW_DFD_WienerFilter::start_convergence()
{
  l_iter = 16;
  lmax_transform = l_iter;//+100;
  lambda = 1;//alpha_N*cls[l_iter+1] / tau_init;
  assert(!isnan(lambda));
}

void EW_DFD_WienerFilter::computeMessengers_D(CMB_Data& data, CMB_ChannelData& in_data, int ch, const CMB_Map& beamed_s)
{
  double inv_tauD = 1/(tauD_nu[ch]);
  CMB_Map& u = u_nu[ch];
  CMB_Map& t = t_nu[ch];
  CMB_Map& iD = inv_Dtilde[ch];
  
  for (long p = 0; p < t_nu[ch].Npix(); p++)
    {
      u[p] = (t[p]*iD[p] + inv_tauD*beamed_s[p])/(inv_tauD + iD[p]);
      assert(!isnan(u_nu[ch][p]));
    }
}

void EW_DFD_WienerFilter::computeMessengers_F(CMB_Data& data, CMB_ChannelData& in_data, int ch, const CMB_Map& beamed_s)
{
  CMB_NP_NoiseMap *inv_noise = data.noise[ch]->as<CMB_NP_NoiseMap>();
  CMB_Map& D = inv_noise->modulated_map;
  ALM_Map alms_tmp(lmax_transform, lmax_transform), alms_d(lmax_transform, lmax_transform);

  CMB_Map tmp_t;
  CMB_Map& t = t_nu[ch];

  tmp_t.SetNside(data.Nside, RING);

#pragma omp parallel for schedule(static)
  for (long p = 0; p < t.Npix(); p++)
    {
      double q = (D[p] > 0) ? (1/D[p]) : 0;
      t[p] = in_data[ch][p] * q;
      tmp_t[p] = beamed_s[p] * q;
      assert(!isnan(t_nu[ch][p]));
    }
 
  HEALPIX_method(t_nu[ch], alms_d, data.weight);
  alms_d.ScaleL(regularized_F[ch]);

  HEALPIX_method(tmp_t, alms_tmp, data.weight);
  alms_tmp.ScaleL(Ffilter[ch]);

  alms_tmp += alms_d;
  alm2map(alms_tmp, t_nu[ch]);

  t_nu[ch] *= D;
} 

void EW_DFD_WienerFilter::computeWiener(CMB_Data& data, ALM_Map& s)
{
  PowerSpec superB(1+lmax_transform);
  ALM_Map tmp_alms(lmax_transform, lmax_transform);

  superB.fill(1);
  s.Set(lmax_transform, lmax_transform);
  s.SetToZero();
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      double inv_tau = 1/(tauD_nu[ch]*lambda);

      HEALPIX_method(u_nu[ch], tmp_alms, data.weight);
      tmp_alms.Scale(inv_tau);
      tmp_alms.ScaleL(data.beam[ch]);

      s.Add(tmp_alms);
      for (long l = 0; l <= lmax_transform; l++)
        {
          superB[l] += alpha_N*cls[l]*square(data.beam[ch][l])*inv_tau;
        }
    }

  for (long l = 0; l <= lmax_transform; l++)
    {
      superB[l] = alpha_N*cls[l]/superB[l];
      assert(!isnan(superB[l]));
    }

  s.ScaleL(superB);
}

double EW_DFD_WienerFilter::compute_chi2(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s, bool full)
{
  if (full)
    {
            double chi2 = 0, chi2_s = 0;
      ALM_Map tmp_alms;
      CMB_Map tmp_map(data.Nside, RING, SET_NSIDE);
      PowerSpec inv_cls(lmax_transform+1);

      inv_cls.fill(0);
      for (int ch = 0; ch < data.numChannels; ch++)
        {
          if (!active_channels[ch])
            continue;
          tmp_alms = s;
          tmp_alms.ScaleL(data.beam[ch]);
          alm2map(tmp_alms, tmp_map);
          tmp_map -= in_data[ch];

          chi2 += data.noise[ch]->chi2(tmp_map);
        }

      for (long l = 0; l < min(inv_cls.size(),cls.size()); l++)
        inv_cls[l] = (cls[l] > 0) ? (1/cls[l]) : 0;

      tmp_alms = s;

      tmp_alms.ScaleL(inv_cls);
      chi2_s = dot_product(tmp_alms, s).real();
      return chi2 + chi2_s;

    }
  else
    {
      ALM_Map A_reduced(lmax_transform, lmax_transform);
      for (long m = 0; m <= lmax_transform; m++)
        for (long l = m; l <= lmax_transform; l++)
          A_reduced(l,m) = A_map(l,m);

      return chi2_0_ref - dot_product(A_reduced, s).real();
    }
}

bool EW_DFD_WienerFilter::try_restart(ALM_Map& s)
{
  try
    {
      string fname_iter = str(format("%s/s_iter_%d.fits") % restart_dir % l_iter);
      read_Alm_from_fits(fname_iter, s, l_iter, l_iter, planckType<double>());
      return true;
    }
  catch (const PlanckError& err)
    {
       return false;
    }
}

void EW_DFD_WienerFilter::do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted)
{
  bool restart_complete;
  int iter;
  start_convergence();

  // Precompute the helper for fast chi2 computation
  chi2_0_ref = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      if (active_channels[ch])
        chi2_0_ref += data.noise[ch]->chi2(in_data[ch]);
    }

  {
    ALM_Map tmp_alms(data.Lmax, data.Lmax);
    A_map.Set(data.Lmax, data.Lmax);
    A_map.SetToZero();
    for (int ch = 0 ; ch < data.numChannels; ch++)
      {
        if (!active_channels[ch])
          continue;
        CMB_Map m = in_data[ch];
        data.noise[ch]->apply_inv(m);
        HEALPIX_method(m, tmp_alms, data.weight);
        tmp_alms.ScaleL(data.beam[ch]);
        A_map += tmp_alms;
      }
    A_map.Scale(12*data.Nside*data.Nside/(4*M_PI));

    fitshandle h;
    h.create("!debug/A_map.fits");
    write_Alm_to_fits(h, A_map, data.Lmax, data.Lmax, planckType<double>());
  }

  s.Set(lmax_transform,lmax_transform);
  s.SetToZero();
  precision_boost = default_boost;
  restart_complete = !attempt_restart;
  do
    {
      if (restart_complete || !try_restart(s))
        {
          restart_complete = true;
          new_chi2 = -1;
          iter = 0;
          cout << endl << format("[EW_DFD WIENER] -> Lambda = %lg  L_Iter = %d  L_Transform = %d      ") % lambda % l_iter % lmax_transform << endl;
          do
           {
             run_iter(data, in_data, s);
             (cout << format("[EW_DFD WIENER] Delta = %10lg  Chi2 = %10lg  iter=%4d     \r") % ((old_chi2-new_chi2)/new_chi2) % new_chi2 % iter).flush();
             iter++;
           } while (!check_convergence(s) || iter < 2);
 
         if (!save_restart_dir.empty())
           { 
             string fname_iter = str(format("!%s/s_iter_%d.fits") % save_restart_dir % l_iter);
             fitshandle h;
             h.create(fname_iter);
             write_Alm_to_fits(h, s, l_iter, l_iter, planckType<double>());
           }

        if (l_iter == data.Lmax && lambda == 1)
          break;
      }

      l_iter = min(min(l_iter+500L, 2L*l_iter), data.Lmax);
      lmax_transform = l_iter;// min(l_iter + 100L, data.Lmax);
      precision_boost = max(double(1), precision_boost/10);
      lambda = 1; // max(double(1), alpha_N*cls[l_iter+1] / tau_init);
      if (lambda == 1)
        precision_boost = 1;
    }
  while (true);

  start_convergence();
  try
    {
      while (l_iter < data.Lmax)
        {
          boost::filesystem::path primary_iter = str(format("%s/s_iter_%d.fits") % save_restart_dir % l_iter);
          boost::filesystem::remove(primary_iter);
          l_iter = min(min(l_iter+500L, 2L*l_iter), data.Lmax);
        }
    }
  catch(const boost::filesystem::filesystem_error & )
    {
    }
}

void EW_DFD_WienerFilter::log_bench(std::ostream& s)
{
  double delta_chi2_0 = (old_chi2-new_chi2)/new_chi2;
  double delta_chi2_1 = (old_chi2-new_chi2)/sigma_chi2;
  s << format("[EW_FINAL] chi2_solution = %g  delta_chi2(CG)=%lg delta_chi2(EW)=%lg sigma_chi2=%g") % new_chi2 % delta_chi2_0 % delta_chi2_1 % sigma_chi2 << endl;

}
