#ifndef _CMB_MAIN_LOGGER_HPP
#define _CMB_MAIN_LOGGER_HPP

#include <iostream>
#include <list>
#include <string>

namespace CMB
{

  class Logger
  {
  public:
    Logger();

    std::ostream& line();
    
    void enter(const std::string& subsys);
    void leave();
  protected:
    std::list<std::string> subsys_list;
  };

  extern Logger default_logger;

};


#endif
