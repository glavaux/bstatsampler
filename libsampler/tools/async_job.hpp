/*+
This is ABYSS (./libsampler/tools/async_job.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __ASYNC_JOB_HPP
#define __ASYNC_JOB_HPP

#include <vector>
#include <set>
#include "mpi_communication.hpp"

static const int JOB_TODO = 0;
static const int JOB_TENTATIVE_LOCKED = 1;
static const int JOB_LOCKED = 1;
static const int JOB_DONE = 2;

class AsyncJobHelper
{
  private:
    CMB::MPICC_Window win;
    CMB::MPICC_Mutex *mutex;
    CMB::MPI_Communication *Comm;
    int *jstate;
    int numjobs;
  public:
    AsyncJobHelper(CMB::MPI_Communication *c, int num)
      : Comm(c), numjobs(num)
    {
      int s = sizeof(int);

      mutex = c->new_mutex(10024);

      win = Comm->win_create(s*numjobs, s);

      if (Comm->rank() == 0)
        {
          for (int i = 0; i < numjobs; i++)
            {
              win.put<int>(i, JOB_TODO);
            }
        }
    }

    ~AsyncJobHelper()
    {
      delete mutex;
      win.destroy();
    }

    void set_job_state(int job, int state)
    {
      mutex->acquire();
      win.put<int>(job, state);
      mutex->release();
    }

    int acquire_job()
    {
      int good_job = -1;

      mutex->acquire();
      for (int i = 0; i < numjobs; i++)
      {
        if (win.get<int>(i) == JOB_TODO)
          {
            good_job = i;
            win.put<int>(i, JOB_LOCKED);
            break;
          }
      }
      mutex->release();

      return good_job;
    }

    void job_complete(int j)
    {
      int state = win.get<int>(j);
      Comm->comm_assert(state == JOB_LOCKED,"Invalid job state");
      mutex->acquire();
      win.put<int>(j, JOB_DONE);
      mutex->release();
    }

};

#endif
