/*+
This is ABYSS (./libsampler/tools/command_wiener_filter.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __COMMAND_WIENER_FILTER_HPP
#define __COMMAND_WIENER_FILTER_HPP

#include <stdint.h>
#include <arr.h>
#include <string>
#include "mpi_communication.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "wiener.hpp"

namespace boost
{

  namespace interprocess
  {
    class shared_memory_object;
  };
};

namespace CMB
{

  class Commander_WienerFilter: public WienerFilter
  {
  protected:
    PowerSpec cls;
    std::string restart_dir, save_restart_dir, wiener_command;
    bool slaveStarted;
    pid_t slavePid;
    int slavePipeReadMaster[2], slavePipeWriteMaster[2];
    MPI_Communication *Comm;
    
  public:
    Commander_WienerFilter(MPI_Communication *comm, 
                           CMB_Data& data);
    virtual ~Commander_WienerFilter();
    
    virtual void setCls(const PowerSpec& cls)
    {
      this->cls = cls;
    }
    
    virtual void do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool  attempt_restart, bool preweighted = false);

    virtual void set_restart_directory(const std::string& dirpath) {this->restart_dir = dirpath; }
    virtual void set_save_restart_directory(const std::string& dirpath) {this->save_restart_dir = dirpath; }

    virtual void setWienerOptions(const KeywordMap& );

  protected:
    
    void startSlave(CMB_Data& data);
    void killSlave();
    
    template<typename T>
    void sendArray(boost::interprocess::shared_memory_object& shm, const arr_ref<T>& A);
    template<typename T>
    void recvArray(boost::interprocess::shared_memory_object& shm, arr_ref<T>& A);
    template<typename T>
    void recvArrayAlm(boost::interprocess::shared_memory_object& shm, Alm<T>& A);

    void sendCommand(int cmd);
    void sendInt(int i);
    void sendString(const std::string& s);
    
    void setupSlave(CMB_Data& data);
    char waitAck();
    void waitReady(const std::string& statename);


  };
  
};

#endif
