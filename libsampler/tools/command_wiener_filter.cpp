/*+
This is ABYSS (./libsampler/tools/command_wiener_filter.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <cmath>

#include <CosmoTool/algo.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include "command_wiener_filter.hpp"
#include <healpix_map.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "noise_weighing.hpp"
#include "mpi_communication.hpp"


using namespace std;
using namespace CMB;
using boost::format;
using CosmoTool::square;
using namespace boost::interprocess;

static const int OK_ACK = 1;
static const int ERR_ACK = 0;

static const int QUIT_COMMAND = 0;
static const int SETUP_CHANNELS_COMMAND = 1;
static const int SETUP_CLS_COMMAND = 2;
static const int SETUP_DATA_COMMAND = 3;
static const int DO_WIENER_COMMAND = 4;
static const int MASK_READY = 5;
static const int BEAM_READY = 6;
static const int NOISE_READY = 7;
static const int CHANNEL_FINISHED = 8;
static const int DATA_READY = 9;
static const int CLS_READY = 10;

struct shm_remove
{
  shm_remove(int id) : name(str(format("abyss-%d") % id)) { shared_memory_object::remove(name.c_str()); }
  ~shm_remove() { shared_memory_object::remove(name.c_str()); }
   
  std::string name;
};


Commander_WienerFilter::Commander_WienerFilter(MPI_Communication * comm, CMB_Data& data)
  : Comm(comm), slaveStarted(false)
{
  startSlave(data);
}


Commander_WienerFilter::~Commander_WienerFilter()
{
  if (slaveStarted)
    killSlave();
}

void Commander_WienerFilter::startSlave(CMB_Data& data)
{
  if (pipe(slavePipeReadMaster) != 0)
    Comm->abort();
  if (pipe(slavePipeWriteMaster) != 0)
    Comm->abort();
    
  if ((slavePid = fork()) == 0)
    {
      std::string p0 = str(format("%d") % slavePipeReadMaster[1]);
      std::string p1 = str(format("%d") % slavePipeWriteMaster[0]);
      char *const args[] = { (char*)p0.c_str(), (char*)p1.c_str() };
      
      ::close(slavePipeReadMaster[0]);
      ::close(slavePipeWriteMaster[1]);
      ::execve(wiener_command.c_str(), args, environ);
      Comm->abort();
    }
    
  if (waitAck() != OK_ACK) {
    Comm->log("The slave Wiener filter has not loaded successfully. Abort");
    Comm->abort();
  }
  
  // Now we can transfer the specifications
  setupSlave(data);
}

void Commander_WienerFilter::waitReady(const std::string& statename)
{
  if (waitAck() != OK_ACK)
    {
      Comm->log(str(format("Failure while setting channels (%s).") % statename));
      Comm->abort();
    }
}

template<typename T>
void Commander_WienerFilter::sendArray(shared_memory_object& shm, const arr_ref<T>& A)
{
  uint16_t p = sizeof(T);
  uint64_t p2 = A.size();
  uint64_t shmS = A.size() * sizeof(T);  
  
  shm.truncate(shmS);
  
  mapped_region region(shm, read_write);
  
  if (::write(slavePipeWriteMaster[1], &p, sizeof(uint16_t)) != sizeof(uint16_t))
    Comm->abort();
  if (::write(slavePipeWriteMaster[1], &p2, sizeof(uint64_t)) != sizeof(uint64_t))
    Comm->abort();
  
  memcpy(region.get_address(), &A[0], shmS);
}

void Commander_WienerFilter::setupSlave(CMB_Data& data)
{
  shm_remove remover(slavePid);
  
  shared_memory_object shm(create_only, remover.name.c_str(), read_write);
  sendCommand(SETUP_CHANNELS_COMMAND);
  sendCommand(data.numChannels);
  sendString(remover.name);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      sendArray(shm, data.masks[ch].Map());
      sendCommand(MASK_READY);
      waitReady(str(format("MASK ch=%d") % ch));
      
      sendArray(shm, data.beam[ch]);
      sendCommand(BEAM_READY);
      waitReady(str(format("BEAM ch=%d") % ch));
      
      sendArray(shm, (data.noise[ch]->as<CMB_NN_NoiseMap>())->Map());      
      sendCommand(NOISE_READY);
      waitReady(str(format("NOISE ch=%d") % ch));
            
      sendCommand(CHANNEL_FINISHED);
      waitReady(str(format("COMPLETION ch=%d") % ch));
    }
}

char Commander_WienerFilter::waitAck()
{
  char ok = 0;
  
  if (::read(slavePipeReadMaster[0], &ok, 1) != 1)
    Comm->abort();
  return ok;
}

void Commander_WienerFilter::sendCommand(int cmd)
{
  char cmd_ch = cmd;
  
  if (::write(slavePipeWriteMaster[1], &cmd_ch, 1) != 1)
    Comm->abort();
}

void Commander_WienerFilter::sendInt(int cmd)
{
  int32_t icmd = cmd;
  
  if (::write(slavePipeWriteMaster[1], &icmd, 4) != 1)
    Comm->abort();
}

void Commander_WienerFilter::sendString(const std::string& n)
{
  uint16_t l = n.size();
  
  if (::write(slavePipeWriteMaster[1], &l, sizeof(uint16_t)) != 1)
    Comm->abort();
  if (::write(slavePipeWriteMaster[1], n.c_str(), l) != 1)
    Comm->abort();
}

void Commander_WienerFilter::killSlave()
{
  // Send stop command
  sendCommand(QUIT_COMMAND);
  Comm->log("Quit command sent, waiting for ack and termination");
  if (waitAck() != OK_ACK)
    {
      Comm->log("not ok. Tear down slave and abort.");
      ::kill(slavePid, SIGKILL);
      Comm->abort();
    }
}

void Commander_WienerFilter::do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, 
                                       bool  attempt_restart, bool preweighted)
{
  shm_remove remover(slavePid);
  shared_memory_object shm(create_only, remover.name.c_str(), read_write);

  Comm->comm_assert(!preweighted, "Commander Wiener Filter only supports non-preweighted maps");
  
  //  Put in_data in shared memory
  sendCommand(SETUP_DATA_COMMAND);
  sendCommand(in_data.size());
  sendString(remover.name);
  
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      sendArray(shm, in_data[ch].Map());
      sendCommand(DATA_READY);
      waitReady(str(format("DATA ch=%d") % ch));
    }
    
  sendCommand(SETUP_CLS_COMMAND);
  sendString(remover.name);
  sendArray(shm, cls);
  sendCommand(CLS_READY);
  waitReady("CLS");
  
  sendCommand(DO_WIENER_COMMAND);
  sendInt(s.Lmax());
  sendString(remover.name);
  waitReady("WIENER");
  
  recvArrayAlm(shm, s);
}

template<typename T>
void Commander_WienerFilter::recvArrayAlm(shared_memory_object& shm, Alm<T>& alms)
{
  mapped_region region(shm, read_write);
  //p = ((uint16_t*)region.get_address());
  //Comm->comm_assert(p[0] == sizeof(T), "Invalid size of elements in shared memory array");

  Comm->comm_assert(alms.Alms().size()*sizeof(T) == (region.get_size()),
                    "Inconsistent sizes in shared memory");
  
  memcpy(alms.mstart(0), region.get_address(), region.get_size()-sizeof(uint16_t));

}

void Commander_WienerFilter::setWienerOptions(const KeywordMap& kwmap)
{
  wiener_command = CMB::extract_keyword<string>(kwmap, "WIENER_COMMAND", "");
  if (wiener_command.empty())
    {
      Comm->log("Cannot have empty Wiener command (WIENER_COMMAND). Stopping.");
      Comm->abort();
   }
}

