/*+
This is ABYSS (./libsampler/tools/extra_alm_tools.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __EXTRA_ALM_TOOLS_HPP
#define __EXTRA_ALM_TOOLS_HPP

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include <xcomplex.h>
#include <alm.h>
#include <cmath>
#include "cmb_defs.hpp"

namespace CMB {

  static
  CMB_Map operator-(const CMB_Map& m1, const CMB_Map& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    CMB_Map m(m1.Nside(), RING, SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] - m2[i];

    return m;
  }
  
  static
  const ALM_Map& clear(ALM_Map& m)
  {
    m.SetToZero();
    return m;
  }
  
  static 
  ALM_Map operator+(const ALM_Map& alm1, const ALM_Map& alm2)
  {
    ALM_Map m1 = alm1;
    
    m1.Add(alm2);
    return m1;
  }

  static
  ALM_Map operator*(const xcomplex<DataType>& alpha, const ALM_Map& alm1)
  {
    ALM_Map m1 = alm1;
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) *= alpha;
    
    return m1;
  }

  static
  const ALM_Map& operator*=(ALM_Map& alm, const DataType& alpha)
  {
    long mmax = alm.Mmax(), lmax = alm.Lmax();
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	alm(l,m) *= alpha;
    
    return alm;
  }

  static
  const ALM_Map& operator*=(ALM_Map& alm, const xcomplex<DataType>& alpha)
  {
    xcomplex<DataType> inv_alpha = alpha.conj()/alpha.norm();
    long mmax = alm.Mmax(), lmax = alm.Lmax();

    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	alm(l,m) *= inv_alpha;
    
    return alm;
  }

  static 
  ALM_Map operator-(const ALM_Map& alm1, const ALM_Map& alm2)
  {
    planck_assert(alm1.conformable(alm2), "Alm1 and Alm2 are not conformable");
    
    ALM_Map m1 = alm1;
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) = m1(l,m)-alm2(l,m);
    
    return m1;
  }
  
  static
  const ALM_Map &operator+=(ALM_Map& m, const ALM_Map& m2)
  {
    m.Add(m2);
    return m;
  }

  static
  const ALM_Map &operator-=(ALM_Map& m1, const ALM_Map& m2)
  {
    planck_assert(m1.conformable(m2), "Alm1 and Alm2 are not conformable");
    int lmax = m1.Lmax(), mmax = m1.Mmax();
    
    for (int m=0; m<=mmax; ++m)
      for (int l=m; l<=lmax; ++l)
	m1(l,m) = m1(l,m)-m2(l,m);
    
    return m1;
  }

  template<typename T>
  xcomplex<T> dot_product(const Alm<xcomplex<T> >& alm1, const Alm<xcomplex<T> >& alm2)
  {
    T scalar = 0;
    arr<T> tmp_scalar(alm1.Alms().size());
    
    planck_assert(alm1.conformable(alm2), "alm1 and alm2 not conformables");
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();

    // Do m=0 first
    for (long q=0; q <= lmax; ++q)
      {
        const xcomplex<T>& a1 = alm1.Alms()[q];
        const xcomplex<T>& a2 = alm2.Alms()[q];

	tmp_scalar[q] = a1.re * a2.re;
      }

    // Do m>=1 then. It must be doubled because of the hermiticity.
    for (long q=lmax+1; q < tmp_scalar.size(); ++q)
      {
        const xcomplex<T>& a1 = alm1.Alms()[q];
        const xcomplex<T>& a2 = alm2.Alms()[q];

	tmp_scalar[q] = 2*(a1.re * a2.re  + a1.im * a2.im);
      }

    clean_sum(tmp_scalar, tmp_scalar.size());
    return tmp_scalar[0];
  }
  
  template<typename T>
  xcomplex<T> dot_product_simple(const Alm<xcomplex<T> >& alm1, const Alm<xcomplex<T> >& alm2)
  {
    T scalar = 0;
    arr<T> tmp_scalar(alm1.Alms().size());
    
    planck_assert(alm1.conformable(alm2), "alm1 and alm2 not conformables");
    int lmax = alm1.Lmax(), mmax = alm1.Mmax();

    // Do m=0 first
    for (long q=0; q <= lmax; ++q)
      {
        const xcomplex<T>& a1 = alm1.Alms()[q];
        const xcomplex<T>& a2 = alm2.Alms()[q];

	tmp_scalar[q] = a1.re * a2.re;
      }

    // Do m>=1 then. It must be doubled because of the hermiticity.
    for (long q=lmax+1; q < tmp_scalar.size(); ++q)
      {
        const xcomplex<T>& a1 = alm1.Alms()[q];
        const xcomplex<T>& a2 = alm2.Alms()[q];

	tmp_scalar[q] = (a1.re * a2.re  + a1.im * a2.im);
      }

    clean_sum(tmp_scalar, tmp_scalar.size());
    return tmp_scalar[0];
  }
  
  static
  DataType norm_L2(const ALM_Map& alms)
  {
    return dot_product(alms,alms).real();
  }

  /** A reimplementation of Healpix create_alm using GSL's random 
   * generators.
   */

  template<typename T> 
  void create_rand_alm(gsl_rng *rng,
		       const arr<T> &powspec, Alm<xcomplex<T> > &alm)
  {
    int lmax = alm.Lmax();
    int mmax = alm.Mmax();
    const double hsqrt2 = 1/sqrt(2.);
    
    assert(powspec.size() > lmax);
    for (int l=0; l<=lmax; ++l)
      {
	double rms_tt = sqrt(powspec[l]);
	alm(l,0).Set(gsl_ran_gaussian_ziggurat(rng, rms_tt), 0);

	rms_tt *= hsqrt2;
	for (int m=1; m<=std::min(l,mmax); ++m)
	  {
	    double zr = gsl_ran_gaussian_ziggurat(rng, rms_tt);
	    double zi = gsl_ran_gaussian_ziggurat(rng, rms_tt);

	    alm(l,m).Set(zr, zi);
	  }
      }
  }

};

#endif
