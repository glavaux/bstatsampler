/*+
This is ABYSS (./libsampler/tools/conjugateGradient.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __SAMPLER_CONJUGATE_GRADIENT_HPP
#define __SAMPLER_CONJUGATE_GRADIENT_HPP

#include <xcomplex.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "cmb_defs.hpp"
#include "logger.hpp"

//#define INTEGRITY_CHECK
#define PRODUCE_STAT

namespace CMB
{
  static const bool VERBOSE_CG = true;
  static const bool ULTRA_VERBOSE_CG = false;

  template<typename Map, typename MatrixObject1>
  void conjugateGradient(Map& input,
			 Map& solution,
			 MatrixObject1& M,
			 double epsilon = 1e-4,
			 int numIterBeforeClearing = 50, int maxIter = -1, bool superquiet = false,
       Map *delta_solution = 0, Logger *logger = 0)
  {
    DataType input_norm,  rel_residual, solution_norm;
    xcomplex<DataType> betak_1, betak_2, betak, alpha;
    Map tmp_map = input;
    Map residue = input;
    Map gradient = residue;
    int numIter = 0;

    if (logger == 0)
      logger = &default_logger;

    input_norm = std::sqrt(norm_L2(input));
    clear(solution);

    do
      {
        Map& A_gradient = tmp_map;
        Map& new_residue = tmp_map;

        A_gradient = M(gradient);
        alpha =
          dot_product(residue, residue) /
          dot_product(gradient, A_gradient);

        solution += alpha*gradient;
        if (delta_solution != 0)
          *delta_solution = alpha*gradient;

        new_residue = residue - alpha*A_gradient;

        betak_1 = dot_product(new_residue, new_residue);
        betak_2 = dot_product(residue, residue);
        betak = betak_1 / betak_2;
        solution_norm = norm_L2(solution);

        gradient = new_residue + betak*gradient;
        residue = new_residue;

        rel_residual = std::pow(betak_1.norm(),0.25) / input_norm;

        if (VERBOSE_CG) {
          logger->line() << "---- Solution norm is " << solution_norm << std::endl;
          logger->line() << "---- Input norm is " << input_norm << std::endl;
          logger->line() << "---- Betak_1 is " << betak_1 << std::endl;
          logger->line() << "---- Residual is " << rel_residual << std::endl;
        } else {
          if (!superquiet) (logger->line() << rel_residual/epsilon << "        \r").flush();
        }

        assert(rel_residual < 10000);

        numIter++;
        //	if ((numIter % numIterBeforeClearing) == 0 || rel_residual > 1)
        if ((numIter % numIterBeforeClearing) == 0)
          {
            // Recompute completely the residual because of numerical
            // rounding errors in the computation of the gradient.
            // We reinitialize the algorithm with the following new ansatz

            gradient = residue = input - M(solution);
          }

        if (maxIter == 0)
          break;
        if (maxIter > 0)
          maxIter--;
      }
    while(rel_residual > epsilon);
#ifdef PRODUCE_STAT
    std::ofstream gradstat("gradient_stat.txt", std::ios_base::app);
    gradstat << numIter << " " << rel_residual << " 0" << std::endl;
#endif

  }

  template<typename Map, typename MatrixObject1, typename MatrixObject2>
  void conjugateGradient_pre(Map& input,
			     Map& solution,
			     MatrixObject1& M,
			     MatrixObject2& preM,
			     double epsilon = 1e-4,
			     int numIterBeforeClearing = 50,
			     int numMaxIter = -1, bool superquiet = false,
			     Map *delta_solution = 0, Logger *logger = 0,
			     boost::function2<void,int,Map&> save_progression = 0)
  {
    DataType input_norm,  rel_residual, solution_norm;

    xcomplex<DataType> betak_1, betak_2, betak, alpha;
    Map tmp_map = input;
    Map residue = input-M(solution);
    Map pre_residue = preM(residue);
    Map gradient = pre_residue;
    Map new_pre_residue;
    int numIter = 0;
    int sustained = 0;

    if (logger == 0)
      logger = &default_logger;

    input_norm = std::sqrt(norm_L2(input));


    if (ULTRA_VERBOSE_CG) {
      solution_norm = norm_L2(solution);
      logger->line() << "S--- Solution norm is " << solution_norm << std::endl;
      logger->line() << "S--- Input norm is " << input_norm << std::endl;
      logger->line() << "S--- Gradient norm is " << sqrt(norm_L2(gradient)) << std::endl;
      logger->line() << "S--- pre_residue norm is " << sqrt(norm_L2(pre_residue)) << std::endl;
      logger->line() << "S--- residue norm is " << sqrt(norm_L2(residue)) << std::endl;
      logger->line() << "S--- plus_residue norm is " << sqrt(norm_L2(input-M(pre_residue))) << std::endl;
    }

    do
      {
        Map& A_gradient = tmp_map;
        Map& new_residue = tmp_map;

        A_gradient = M(gradient);
        alpha =
          dot_product(residue, pre_residue) /
          dot_product(gradient, A_gradient);

        solution += alpha*gradient;
        if (delta_solution != 0)
          *delta_solution = alpha*gradient;

        new_residue = residue - alpha*A_gradient;

        solution_norm = norm_L2(solution);
        rel_residual = sqrt(norm_L2(new_residue))/input_norm;

        if (VERBOSE_CG) {
          if (ULTRA_VERBOSE_CG) {
            logger->line() << "---- Solution norm is " << solution_norm << std::endl;
            logger->line() << "---- Input norm is " << input_norm << std::endl;
          }
          logger->line() << "---- Residual is " << rel_residual << std::endl;
          assert(!isnan(solution_norm));
        } else {
          if (!superquiet) (logger->line() << rel_residual/epsilon << "       \r").flush();
        }


        numIter++;
        if ((numIter % numIterBeforeClearing) == 0)
          {
            // Recompute completely the residual because of numerical
            // rounding errors in the computation of the gradient.
            // We reinitialize the algorithm with the following new ansatz

            residue = input - M(solution);
            gradient = pre_residue = preM(residue);
          }

        if (rel_residual < epsilon)
          sustained++;
        else
          sustained = 0;

        if (sustained == 1)
          break;

        if (numMaxIter == 0)
          break;
        if (numMaxIter > 0)
          numMaxIter--;
        if (!save_progression.empty())
          save_progression(numIter, solution);

        new_pre_residue = preM(new_residue);

        betak_1 = dot_product(new_residue, new_pre_residue);
        betak_2 = dot_product(residue, pre_residue);
        betak = betak_1 / betak_2;

        gradient = new_pre_residue + betak*gradient;

        pre_residue = new_pre_residue;
        residue = new_residue;

        if (VERBOSE_CG) {
          if (ULTRA_VERBOSE_CG) {
            logger->line() << "---- Betak_1 is " << betak_1 << std::endl;
            logger->line() << "---- Betak_2 is " << betak_2 << std::endl;
            logger->line() << "---- Gradient norm is " << sqrt(norm_L2(gradient)) << std::endl;
            logger->line() << "---- A_Gradient norm is " << sqrt(norm_L2(A_gradient)) << std::endl;
            logger->line() << "---- pre_residue norm is " << sqrt(norm_L2(pre_residue)) << std::endl;
            logger->line() << "---- Alpha is " << alpha << std::endl;        
            logger->line() << "---- New Residue is " << sqrt(norm_L2(new_residue)) << std::endl;
          }
        }
      }
    while(true);

#ifdef INTEGRITY_CHECK
    tmp_map = M(solution) - input;
    logger->line() << "INTEGRITY RESIDUAL=" << sqrt(norm_L2(tmp_map)/norm_L2(input)) << std::endl;
#endif

#ifdef PRODUCE_STAT
    std::ofstream gradstat("gradient_stat.txt", std::ios_base::app);
    gradstat << numIter << " " << rel_residual << " 1" << std::endl;
#endif
    if (superquiet) logger->line() << std::endl;
  }

};

#endif
