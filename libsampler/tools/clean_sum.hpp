/*+
This is ABYSS (./libsampler/tools/clean_sum.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_CLEAN_SUM_HPP
#define __CMB_CLEAN_SUM_HPP
#include <cassert>

namespace CMB
{

  template<typename ArrT>
  void clean_sum(ArrT& a, long cur_len)
  {
    long original_len = cur_len;
    while (cur_len > 1)
      {       
        if ((cur_len % 2) == 0)
          cur_len /= 2;
        else
          {
              // Add the odd numbered one and reduce.
            a[cur_len-2] += a[cur_len-1];
            cur_len = (cur_len-1)/2;
          }
        for (long p = 0; p < cur_len; p++)
          {
            assert((p+cur_len) < original_len);
            a[p] += a[p+cur_len];    
          }
      }
  }

  template<typename ArrT, typename Op>
  void clean_sum_op(ArrT& a, const ArrT& b, long cur_len, Op op)
  {
    long original_size = cur_len;
    assert(cur_len > 0);


    if ((cur_len % 2) == 0)
     {
       cur_len /= 2;
       for (long p = 0; p < cur_len; p++)
          a[p] = op(a[p],b[p]) + op(a[p+cur_len],b[p+cur_len]);
     }
    else
     {
       if (cur_len == 1)
        {
          a[0] = op(a[0],b[0]);
          return;
        }
       cur_len /= 2;
       for (long p = 0; p < cur_len; p++)
          a[p] = op(a[p],b[p]) + op(a[p+cur_len],b[p+cur_len]);
       a[cur_len-1] += op(a[cur_len*2],b[cur_len*2]);
     }

    while (cur_len > 1)
      {
        if ((cur_len % 2) == 0)
          cur_len /= 2;
        else
          {
              // Add the odd numbered one and reduce.
            a[cur_len-2] += a[cur_len-1];
            cur_len = (cur_len-1)/2;
          }
        for (long p = 0; p < cur_len; p++)
          {
            assert((p+cur_len) < original_size);
            a[p] += a[p+cur_len];
          }
      }
  }


};

#endif
