/*+
This is ABYSS (./libsampler/tools/dual_messenger_wiener.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/filesystem.hpp>
#include <fstream>
#include <cmath>
#include <boost/format.hpp>
#include <iostream>
#include <cmath>
#include "dual_messenger_wiener.hpp"
#include <healpix_map.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "noise_weighing.hpp"
#include "mpi_communication.hpp"
#include <CosmoTool/algo.hpp>
#include <boost/serialization/list.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

using namespace std;
using namespace CMB;
using boost::format;
using CosmoTool::square;

static bool lazyCreateDirectory(const std::string& dirpath)
{
  boost::filesystem::path p(dirpath);

  cout << "Check " << dirpath << endl;
  if (boost::filesystem::is_directory(p))
    return true;

  cout << "Create " << dirpath << endl;
  return boost::filesystem::create_directories(p);
}



DualMessenger_WienerFilter::DualMessenger_WienerFilter(MPI_Communication * comm, CMB_Data& data)
  : Comm(comm)
{
  precision = data.rinfo.epsilonCG;
  Comm->log(str(format("Initialize dual_messenger(precision=%lg,numChannels=%d)") % precision % data.numChannels));
  default_boost = 1;//00;
  precision_boost = 1;
  do_full_scaling = true;

  all_t.alloc(data.numChannels);
  inv_Ntilde.alloc(data.numChannels);
  tau.alloc(data.numChannels);
  messenger_mask.alloc(data.numChannels);

  buildNtilde(data);

  alpha_N = 12*data.Nside*data.Nside/(4*M_PI);

  active_channels.alloc(data.numChannels);
  active_channels.fill(false);
}

void DualMessenger_WienerFilter::buildNtilde(CMB_Data& data)
{
  tau_init = INFINITY;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_NN_NoiseMap *inv_noise = data.noise[ch]->as<CMB_NN_NoiseMap>();
      Comm->log(str(format(" EW: Preparing channel %d") % ch)); 
      
      tau[ch] = 0;
      for (long p = 0; p < inv_noise->Npix(); p++)
        {
          tau[ch] = max(tau[ch], (*inv_noise)[p]);
        }
      assert(tau[ch] > 0);
      tau[ch] = 1/tau[ch];
      Comm->log(str(format(" EW: tau[%d] = %lg") % ch % tau[ch])); 
      tau_init = min(tau_init, tau[ch]);

      inv_Ntilde[ch].SetNside(data.Nside, RING);
      messenger_mask[ch].SetNside(data.Nside, RING);
      inv_Ntilde[ch].alpha=1.0;

      inv_Ntilde[ch].masked = 0;
      for (long p = 0; p < inv_noise->Npix(); p++)
        {
          messenger_mask[ch][p] = false;
          if ((*inv_noise)[p] > 0)
            {
              double NN = 1/(*inv_noise)[p];
  
              inv_Ntilde[ch][p] = max(double(0),NN-tau[ch]);
              if (inv_Ntilde[ch][p] > 0)
                inv_Ntilde[ch][p] = 1/inv_Ntilde[ch][p];
              else {
                inv_Ntilde[ch][p] = 0;
                messenger_mask[ch][p] = true;
                inv_Ntilde[ch].masked++;
              }
            }
          else
            {
              inv_Ntilde[ch][p] = 0;
              inv_Ntilde[ch].masked ++;
            }
         assert(!isnan(inv_Ntilde[ch][p]));
       }
      Comm->log(str(format(" EW: masked = %d") % inv_Ntilde[ch].masked));
      all_t[ch].SetNside(data.Nside, RING);
    }
}


void DualMessenger_WienerFilter::run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool preweighted)
{
  ALM_Map prev_s;
  
  Comm->log("[DUAL MESS] compute messengers");
  computeAllMessengers(data, in_data, s);
  computeDualMessenger(data, s);
  prev_s = s;
  computeSignal(data, s);
  new_chi2 = compute_chi2(prev_s, s);
}

bool DualMessenger_WienerFilter::check_convergence(const ALM_Map& s)
{
  Comm->log(str(format("[DUAL MESS] Check convergence delta_chi2=%lg, precision=%lg, boost=%lg") % new_chi2 % precision % precision_boost));
  return (new_chi2) < (precision/precision_boost);
}

void DualMessenger_WienerFilter::start_convergence()
{
  lmax_transform = l_iter;
  lambda_value = 1;//(do_full_scaling) ?  alpha_N*cls[l_iter+1] / tau_init : 1;
  Comm->log(str(format("[DUAL MESS] tau_init=%lg lambda_value=%lg") % tau_init % lambda_value));
  assert(!isnan(lambda_value));
}

void DualMessenger_WienerFilter::buildClTilde()
{
  cltilde.alloc(l_iter+1);
  cltilde_mask.alloc(l_iter+1);
  cltilde.fill(0);

  mu = cls[l_iter];
  for (long l = 0; l < cltilde.size(); l++)
    {
      cltilde[l] = cls[l] - mu;
      if (!(cltilde_mask[l] = cltilde[l]>0))
        cltilde[l] = 0;

      assert(!isnan(cltilde[l]));
    }
  Comm->log(str(format("[DUAL MESS] Built ClTilde for l=%d with mu=%lg") % l_iter % mu));
}


//double EW_WienerFilter::compute_testnorm(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s)
//{
//}

double DualMessenger_WienerFilter::compute_chi2(const ALM_Map& prev_s, const ALM_Map& s)
{
  double final_delta = 0, final_N = 0;
  double ratio = 0;

  if (prev_s.Alms().size() != s.Alms().size())  
    return INFINITY;
    
  for (long l = 0; l <= s.Lmax(); l++)
    {
      double delta = 0, N = 0;
      for (long m = 0; m <= l; m++)
        {
          delta += (prev_s(l,m)-s(l,m)).norm();
          N += (s(l,m)).norm();
          assert(!isnan(delta));
          assert(!isnan(N));
        }
      ratio = max(ratio,delta/N);
      final_delta += delta;
      final_N += N;
    }
 
  assert(final_N>0);
//  assert(final_delta>0);   
  return ratio;//final_delta/final_N;
}

void DualMessenger_WienerFilter::computeDualMessenger(CMB_Data& data, const ALM_Map& s)
{
  arr<double> dual_filter(1+l_iter);

  for (int l = 0; l <= l_iter; l++)
    dual_filter[l] = (!cltilde_mask[l]) ? 0 : (cltilde[l]/(cltilde[l]+mu));

  q.Set(l_iter, l_iter);
  q.SetToZero();
  assert(s.Lmax() <= l_iter);
  for (long l = 0; l <= s.Lmax(); l++)
    for (long m = 0; m <= l; m++)
      q(l,m) = s(l,m)*dual_filter[l];
}

void DualMessenger_WienerFilter::computeOneMessenger(CMB_Data& data, CMB_ChannelData& in_data, int ch, const ALM_Map& s)
{
  CMB_Map& t = all_t[ch];
  ALM_Map tmp_s = s;
  CMB_Map BMap(data.Nside, RING, SET_NSIDE);
  const CMB_Map& inv_NTILDE = inv_Ntilde[ch];
  const CMB_Map& D = in_data[ch];
  double inv_tau = 1/tau[ch];

  tmp_s.ScaleL(data.beam[ch]);
  alm2map(tmp_s, BMap);

  for (long p = 0; p < t.Npix(); p++)
    {
      if (!messenger_mask[ch][p])
        {
          double w = 1/(inv_NTILDE[p] + inv_tau);
          t[p] = w*(inv_tau*BMap[p] + inv_NTILDE[p]*D[p]);
        }
      else
        t[p] = D[p];
    }
}

void DualMessenger_WienerFilter::computeAllMessengers(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s)
{
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      computeOneMessenger(data, in_data, ch, s);
    }
}

void DualMessenger_WienerFilter::computeSignal(CMB_Data& data, ALM_Map& s)
{
  int numChannels = all_t.size();
  arr<double> postFilter(l_iter+1);
  arr<double> weights(2*data.Nside);
  ALM_Map tmp_alm(l_iter, l_iter);

  s.Set(l_iter,l_iter);
  s.SetToZero();

  weights.fill(1);
  postFilter.fill(1);
  
  Comm->log(str(format("Compute signal with lambda=%lg") % lambda_value));

  for (int ch = 0; ch < numChannels; ch++)
    {
      double tau_ch = tau[ch]*lambda_value;
      double inv_tau = alpha_N/tau_ch;
      double Ratio = mu*inv_tau;

      map2alm(all_t[ch], tmp_alm, weights);
      tmp_alm.Scale(inv_tau);
      tmp_alm.ScaleL(data.beam[ch]);

      for (int l = 0; l <= l_iter; l++)
        postFilter[l] += square(data.beam[ch][l]) * Ratio; 

      s.Add(tmp_alm);
    }

  for (int l = 0; l <= l_iter; l++)
    {
      if (cltilde_mask[l])
        postFilter[l] = mu/postFilter[l];
      else
        postFilter[l] = 0;
    }


  assert(l_iter >= q.Lmax());
  double inv_mu = 1/mu;
  assert(s.Lmax() == q.Lmax());
  assert(s.Mmax() == q.Mmax());
  for (long l = 0; l <= q.Lmax(); l++)
    {
      for (long m = 0; m <= l; m++)
        s(l,m) += q(l,m)*inv_mu;
    }
  s.ScaleL(postFilter);      
}

long DualMessenger_WienerFilter::getNextLIter(long l, long Lmax)
{
  return min(l+100,Lmax);////min(min(l+300,l*2),Lmax);
}

bool DualMessenger_WienerFilter::try_restart(ALM_Map& s, DualAction& a)
{
  if (restart_dir.empty())
    return false;

  try
    {
      string fname_iter = str(format("%s/s_iter_%d.fits") % restart_dir % l_iter);
      read_Alm_from_fits(fname_iter, s, l_iter, l_iter);

      string fname_act = str(format("%s/s_iter_%d_actions.txt") % save_restart_dir % l_iter);
      ifstream f(fname_act.c_str());
      {
        boost::archive::text_iarchive ia(f);
        actions.clear();
        ia >> actions;
      }
      return true;
    }
  catch (const PlanckError& err)
    {
       return false;
    }
}

void DualMessenger_WienerFilter::save_restart(const ALM_Map& s)
{
  if (save_restart_dir.empty())
    return;
            
  lazyCreateDirectory(save_restart_dir);

  string fname_iter = str(format("!%s/s_iter_%d.fits") % save_restart_dir % l_iter);
  fitshandle h;
  h.create(fname_iter);
  write_Alm_to_fits(h, s, l_iter, l_iter, planckType<double>());

  string fname_act = str(format("%s/s_iter_%d_actions.txt") % save_restart_dir % l_iter);
  ofstream f(fname_act.c_str());
  {
    boost::archive::text_oarchive oa(f);
    oa << actions;
  }    
  f.close();
}

void DualMessenger_WienerFilter::do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted)
{
  bool restart_complete;
  int iter;

  l_iter = 4;//do_lmax_scaling ? 4 : data.Lmax;

  start_convergence();

  q.Set(l_iter,l_iter);
  q.SetToZero();
  s.Set(l_iter,l_iter);
  s.SetToZero();
  restart_complete = !attempt_restart;
  actions.clear();
  do
    {
      DualAction act;

      if (true || restart_complete || !try_restart(s, act))
        {
          buildClTilde();
          restart_complete = true;
          new_chi2 = -1;
          iter = 0;

          do
           {
             run_iter(data, in_data, s, preweighted);
             iter++;
             Comm->log(str(format("[DUAL MESSENGER] LMax=%d Iter=%d Delta=%lg") % l_iter % iter % new_chi2));
           } while (!check_convergence(s) || iter < 2);
         save_restart(s);
        }
      else
        {
          l_iter = act.l_iter;
          iter = act.iter;
        }

      if (l_iter == data.Lmax)
        break;

      actions.push_back(DualAction(l_iter,iter));

      l_iter = getNextLIter(l_iter, data.Lmax);
    }
  while (true);

//  cleanup_restart();
  log_bench(cout);
}

void DualMessenger_WienerFilter::cleanup_restart()
{
  try
    {
      for (ActionList::iterator i = actions.begin();
           i != actions.end(); ++i)
        {
          boost::filesystem::path primary_iter = str(format("%s/s_iter_%d.fits") % save_restart_dir % i->l_iter);
          boost::filesystem::remove(primary_iter);
        }
    }
  catch(const boost::filesystem::filesystem_error & )
    {
    }
}

void DualMessenger_WienerFilter::log_bench(std::ostream& s)
{
}

void DualMessenger_WienerFilter::setWienerOptions(const KeywordMap& kwmap)
{
}
