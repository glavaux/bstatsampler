/*+
This is ABYSS (./libsampler/tools/cg_wiener_filter.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <iostream>
#include <boost/bind.hpp>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "cg_wiener_filter.hpp"
#include "cg_relax.hpp"
#include "conjugateGradient.hpp"
#include <alm_fitsio.h>
#include <boost/format.hpp>

using boost::str;
using boost::format;
using namespace std;
using namespace CMB;

static const bool FULL_CHI2_CHECK = false;

CG_WienerFilter::CG_WienerFilter(CMB_Data& data)
  : matrix(data.noise, data.weight, data.beam, data.Nside, data.NsideHR,
           data.numChannels, data.transform), w_alm(data.Lmax, data.Lmax)
{
  (cout << "CG starting" << endl).flush();
  rinfo = data.rinfo;
  this->preconditioner = data.preconditioner;
  active_channels.alloc(data.numChannels);
  active_channels.fill(false);
  (cout << "Done construction" << endl).flush();
}

CG_WienerFilter::~CG_WienerFilter()
{
}

void CG_WienerFilter::prepare()
{
}

void CG_WienerFilter::start_convergence()
{
}

void CG_WienerFilter::setCls(const PowerSpec& cls)
{
  sqcls.alloc(cls.size());
  for (long l = 0; l < sqcls.size(); l++)
    sqcls[l] = sqrt(cls[l]);
}

const ALM_Map& CG_WienerFilter::generate_input(CMB_Data *data, CMB_ChannelData *in_data, double boost_factor, long Lmax, bool preweighted)
{
  ALM_Map tmp_alms(Lmax, Lmax);

  w_alm.SetToZero();
  for (int ch = 0; ch < data->numChannels; ch++)
    {
      if (active_channels[ch])
        {
          CMB_Map w_map = (*in_data)[ch];

          if (!preweighted)
            data->noise[ch]->apply_inv(w_map);
          w_map *= w_map.Npix()/(4*M_PI*boost_factor*boost_factor);
          HEALPIX_method(w_map, tmp_alms, data->weight);
          tmp_alms.ScaleL(data->beam[ch]);
          w_alm += tmp_alms;
        }
    }

  w_alm.ScaleL(sqcls);

  return w_alm;
}

static void cg_logger_wiener(const std::string& s)
{
  cout << ("[WIENER] CG ") << s << endl;
}

double CG_WienerFilter::compute_chi2(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s)
{
  double chi2 = 0, chi2_s = 0;
  ALM_Map tmp_alms;
  CMB_Map tmp_map(data.Nside, RING, SET_NSIDE);
  PowerSpec inv_cls(data.Lmax+1);

  inv_cls.fill(0);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      if (!active_channels[ch])
        continue;
      tmp_alms = s;
      tmp_alms.ScaleL(data.beam[ch]);
      alm2map(tmp_alms, tmp_map);
      tmp_map -= in_data[ch];

      chi2 += data.noise[ch]->chi2(tmp_map);
    }

  for (long l = 0; l < min(inv_cls.size(),sqcls.size()); l++)
    inv_cls[l] = (sqcls[l] > 0) ? (1/(sqcls[l]*sqcls[l])) : 0;

  tmp_alms = s;

  tmp_alms.ScaleL(inv_cls);
  chi2_s = dot_product(tmp_alms, s).real();

    cout << "chi2 = " << chi2 <<  "   chi2_s = " << chi2_s << endl;

  return chi2 + chi2_s;
}

static void save_cg_progression(int iter,
               ALM_Map& sol)
{
  fitshandle h;
  h.create(str(format("!debug/cg_jacobi_%d.fits") % iter));
  write_Alm_to_fits(h, sol, sol.Lmax(), sol.Mmax(), planckType<double>());
}

void CG_WienerFilter::run_cg(CMB_Data& data, ALM_Map& cur_input, ALM_Map& s)
{
  clear(s);
  conjugateGradient_pre(cur_input, s, matrix,
    *data.preconditioner, masterEpsilon, 50,
    masterIter, false, (ALM_Map*)0, &default_logger);
}

void CG_WienerFilter::run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool preweighted)
{
  ALM_Map delta_solution;

  matrix.SetSqCls(sqcls);

  s.Set(data.Lmax, data.Lmax);

  default_logger.enter("CG_WIENER");
  if (use_pure_cg) {
// Use pure cg iteration
    conjugateGradient_relax_solve
      (rinfo,
       boost::bind(&CG_WienerFilter::generate_input, this, &data, &in_data, _1, _2, preweighted),
       s, data, &matrix, &default_logger, FULL_CHI2_CHECK ? &delta_solution : 0);
  } else {
// Use combined jacobi/conjugate gradient iteration
    ALM_Map y = generate_input(&data, &in_data, 1, data.Lmax, false);
    double norm_in = norm_L2(y), N;
    int iter = 0;
    
    run_cg(data, y, s);
    do
      {
        ALM_Map delta_in = y - matrix(s);
        ALM_Map delta_s(data.Lmax, data.Lmax);
        
        run_cg(data, delta_in, delta_s);
        
        save_cg_progression(iter++, s);
        s += delta_s;
        N = sqrt(norm_L2(delta_in)/norm_in);
        
        default_logger.line() << format("Norm residual = %lg") % N << endl;
        default_logger.line() << format("Norm delta = %lg") % sqrt(norm_L2(delta_s)/norm_L2(s)) << endl;      
      }
    while(N > rinfo.epsilonCG);

    save_cg_progression(iter++, s);
  }
 
  s.ScaleL(sqcls);
  if (FULL_CHI2_CHECK)
    {
      ALM_Map s2;

      delta_solution.ScaleL(sqcls);
      delta_solution = s - delta_solution;
      double chi2_0 = compute_chi2(data, in_data, s);
      double chi2_1 = compute_chi2(data, in_data, delta_solution);

      default_logger.line() << format("chi2_solution = %g  delta_chi2=%g") % chi2_0 % (chi2_1-chi2_0);
      this->last_chi2 = chi2_0;
      this->last_delta_chi2 = chi2_1-chi2_0;
    }
  default_logger.leave();
}

bool CG_WienerFilter::check_convergence(const ALM_Map& s)
{
  return true;
}

void CG_WienerFilter::do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted)
{
  run_iter(data, in_data, s, preweighted);
}


void CG_WienerFilter::log_bench(std::ostream& s)
{

  s << format("[CG_FINAL] chi2_solution = %g  delta_chi2=%g") % last_chi2 % (last_delta_chi2/last_chi2) << endl;

}



void CG_WienerFilter::setWienerOptions(const KeywordMap& kwmap)
{
  masterIter = extract_keyword<int>(kwmap, "MASTER_ITER", 4);
  masterEpsilon = extract_keyword<double>(kwmap, "MASTER_EPSILON", 0.1);
  use_pure_cg = extract_keyword<int>(kwmap,"HYBRID_JACOBI", 1) == 0;
}
