/*+
This is ABYSS (./libsampler/tools/cg_wiener_filter.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CG_WIENER_FILTER_HPP
#define __CG_WIENER_FILTER_HPP

#include <boost/bind.hpp>
#include "noise_weighing.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "wiener.hpp"

namespace CMB
{

  class CG_WienerFilter: public WienerFilter
  {
  protected:
    MapSignalNoiseSignal_weighing matrix;
    RelaxInfo rinfo;
    PowerSpec sqcls;
    ALM_Map w_alm;
    CMB_Preconditioner *preconditioner;
    double last_chi2, last_delta_chi2;
    int masterIter;
    double masterEpsilon, use_pure_cg;
    
    void run_cg(CMB_Data& data, ALM_Map& cur_input, ALM_Map& s);

  public:
    CG_WienerFilter(CMB_Data& data);

    ~CG_WienerFilter();

    void prepare();

    virtual void setCls(const PowerSpec& cls);
    virtual void do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted = false);

    void start_convergence();

    void run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool preweighted);

    const ALM_Map& generate_input(CMB_Data *data, CMB_ChannelData *in_data,
                                  double boost_factor, long Lmax, bool preweighted);

    bool check_convergence(const ALM_Map& s);

    double compute_chi2(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s);

    virtual void log_bench(std::ostream& s);

    virtual void setWienerOptions(const KeywordMap& kwmap);

  };

};

#endif
