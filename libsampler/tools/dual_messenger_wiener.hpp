/*+
This is ABYSS (./libsampler/tools/dual_messenger_wiener.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __DUALMESSENGER_WIENER_FILTER_HPP
#define __DUALMESSENGER_WIENER_FILTER_HPP

#include "mpi_communication.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "wiener.hpp"

namespace CMB
{

  class DualAction {
  public:
    DualAction() {}
    DualAction(int l_iter, int iter_id) {
      this->l_iter = l_iter;
      this->iter = iter_id;
    }
    
    int l_iter, iter;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* version */)
    {
      ar & l_iter;
      ar & iter;
    }
  };
  
  typedef std::list<DualAction> ActionList;

  class DualMessenger_WienerFilter: public WienerFilter
  {
  protected:
    PowerSpec cls, cltilde;
    PowerSpecMask cltilde_mask;
    CMB_ChannelData all_t;
    arr<CMB_NN_NoiseMap> inv_Ntilde;
    arr<double> tau;
    arr<MapMask > messenger_mask;
    ALM_Map q;


    double lambda_value;
    int l_iter, lmax_transform;
    double alpha_N;
    double tau_init;
    double mu;
    double new_chi2;
    double precision;
    double chi2_0_ref;
    double precision_boost, default_boost;
    std::string restart_dir, save_restart_dir;
    MPI_Communication *Comm;
    bool do_full_scaling, do_lmax_scaling;
    ActionList actions;
  public:
    DualMessenger_WienerFilter(MPI_Communication *comm, CMB_Data& data);

    virtual void setCls(const PowerSpec& cls)
    {
      this->cls = cls;
    }

    void prepare();


    virtual void do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted = false);

    virtual void set_restart_directory(const std::string& dirpath) {this->restart_dir = dirpath; }
    virtual void set_save_restart_directory(const std::string& dirpath) {this->save_restart_dir = dirpath; }

    void set_precision_boost(double boost) { this->default_boost = boost; }

    virtual void setWienerOptions(const KeywordMap& );

  protected:
    void buildNtilde(CMB_Data& data);
    void buildClTilde();

    void start_convergence();
    void run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool preweighted);
    bool check_convergence(const ALM_Map& s);

    void computeDualMessenger(CMB_Data& data, const ALM_Map& s);
    void computeOneMessenger(CMB_Data& data, CMB_ChannelData& in_data, int ch, const ALM_Map& s);
    void computeAllMessengers(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s);
    void computeSignal(CMB_Data& data, ALM_Map& s);
    void runOneIteration(CMB_Data& data, CMB_ChannelData& in_data);
    long getNextLIter(long l, long Lmax);

    bool try_restart(ALM_Map& s, DualAction& a);
    void save_restart(const ALM_Map&s );
    void cleanup_restart();


    double compute_chi2(const ALM_Map& prev_s, const ALM_Map& s);

    virtual void log_bench(std::ostream& s);
 };

};

#endif
