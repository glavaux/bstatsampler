/*+
This is ABYSS (./libsampler/tools/ew_wiener_filter.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/filesystem.hpp>
#include <cmath>
#include <boost/format.hpp>
#include <iostream>
#include <cmath>
#include "ew_wiener_filter.hpp"
#include <healpix_map.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "noise_weighing.hpp"
#include "mpi_communication.hpp"
#include <CosmoTool/algo.hpp>

using namespace std;
using namespace CMB;
using boost::format;
using CosmoTool::square;

static bool lazyCreateDirectory(const std::string& dirpath)
{
  boost::filesystem::path p(dirpath);

  cout << "Check " << dirpath << endl;
  if (boost::filesystem::is_directory(p))
    return true;

  cout << "Create " << dirpath << endl;
  return boost::filesystem::create_directories(p);
}

EW_WienerFilter::EW_WienerFilter(MPI_Communication * comm, CMB_Data& data)
  : Comm(comm)
{
  precision = data.rinfo.epsilonCG;
  default_boost = 1;//00;
  do_lambda_scaling = true;
  ew_multiplier = 2;
  ew_throttler = 500;

  t_nu.alloc(data.numChannels);
  inv_Ntilde.alloc(data.numChannels);
  tau_nu.alloc(data.numChannels);
  messenger_mask.alloc(data.numChannels);

  tau_init = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_NN_NoiseMap *inv_noise = data.noise[ch]->as<CMB_NN_NoiseMap>();
      Comm->log(str(format(" EW: Preparing channel %d") % ch)); 
      
      tau_nu[ch] = 0;
      for (long p = 0; p < inv_noise->Npix(); p++)
        {
          tau_nu[ch] = max(tau_nu[ch], (*inv_noise)[p]);
        }
      Comm->log(str(format(" EW: tau_nu[%d] = %lg") % ch % tau_nu[ch])); 
      assert(tau_nu[ch] > 0);
      tau_init = max(tau_init,tau_nu[ch]);
      tau_nu[ch] = 1/tau_nu[ch];

      inv_Ntilde[ch].SetNside(data.Nside, RING);
      messenger_mask[ch].SetNside(data.Nside, RING);
      inv_Ntilde[ch].alpha=1.0;

//      do
        {
          inv_Ntilde[ch].masked = 0;
          for (long p = 0; p < inv_noise->Npix(); p++)
            {
              messenger_mask[ch][p] = false;
              if ((*inv_noise)[p] > 0)
                {
                  double NN = 1/(*inv_noise)[p];
                  inv_Ntilde[ch][p] = max(double(0),NN-tau_nu[ch]);
                  if (inv_Ntilde[ch][p] > 0)
                    inv_Ntilde[ch][p] = 1/inv_Ntilde[ch][p];
                  else {
                    inv_Ntilde[ch][p] = 0;
                    messenger_mask[ch][p] = true;
                    inv_Ntilde[ch].masked++;
                  }
                }
              else
                {
                  inv_Ntilde[ch][p] = 0;
                  inv_Ntilde[ch].masked ++;
                }
              assert(!isnan(inv_Ntilde[ch][p]));
            }
         Comm->log(str(format(" EW: masked = %d") % inv_Ntilde[ch].masked)); 
        }

      t_nu[ch].SetNside(data.Nside, RING);
    }

  tau_init = 1/tau_init;

  alpha_N = 12*data.Nside*data.Nside/(4*M_PI);

  active_channels.alloc(data.numChannels);
  active_channels.fill(false);
}


void EW_WienerFilter::run_iter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool preweighted)
{
  computeMessengers(data, in_data, s, preweighted);
  computeWiener(data, s);
  old_chi2 = new_chi2;
  new_chi2 = compute_chi2(data, in_data, s, true);

  long num_active = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    if (active_channels[ch])
      num_active+= in_data[ch].Npix();

  long full_N = 2*s.Alms().size();//(data.Lmax+1)*(data.Lmax+1);

  sigma_chi2 =  sqrt(double(full_N) + double(num_active));
//  cout << str(format("sigma_chi2 = %lg") % sigma_chi2)  <<endl;
}

bool EW_WienerFilter::check_convergence(const ALM_Map& s)
{
  if (old_chi2 < 0)
    return false;

  return (fabs(old_chi2-new_chi2)/new_chi2) < (precision/precision_boost);
}

void EW_WienerFilter::start_convergence()
{
  assert(!isnan(lambda));
}

static void messenger_loop_prew(CMB_Map& t,CMB_Map& in_data, CMB_Map& tmp_map, double inv_tau, CMB_Map& w, CMB_Map& inv_noise, Healpix_Map<bool>& mm)
{
//#pragma omp parallel for schedule(static)
  for (long p = 0; p < t.Npix(); p++)
    {
 // mm[p] == true and w[p] == 0 are exclusive
      assert(inv_noise[p]!=0 || (!mm[p]));
      if (mm[p])
       {
        assert(inv_noise[p]>0);
        t[p] = in_data[p]/inv_noise[p];
       }
      else if (inv_noise[p] == 0)
       t[p] = (in_data[p] + tmp_map[p])/(inv_tau+w[p]);
      else
       {
         assert(inv_noise[p]>0);
         t[p] = (in_data[p]*w[p]/inv_noise[p] + tmp_map[p])/ (inv_tau + w[p]);
       }
      assert(!isnan(t[p]));
    }
}

static void messenger_loop(CMB_Map& t, CMB_Map& in_data, CMB_Map& tmp_map, double inv_tau, CMB_Map& w, Healpix_Map<bool>& mm, CMB_NN_NoiseMap *n)
{
#pragma omp parallel for schedule(static)
  for (long p = 0; p < t.Npix(); p++)
    {
      if (mm[p])
        t[p] = ((*n)[p] == 0) ? 0 : in_data[p];
      else
        t[p] = (in_data[p]*w[p] + inv_tau*tmp_map[p])/ (inv_tau + w[p]);
      assert(!isnan(t[p]));
    }
}

static void messenger_loop_nosrc(CMB_Map& t, CMB_Map& tmp_map, double inv_tau, CMB_Map& w, Healpix_Map<bool>& mm)
{
#pragma omp parallel for schedule(static)
  for (long p = 0; p < t.Npix(); p++)
    {
      if (mm[p])
        t[p] = 0;
      else
        t[p] = tmp_map[p]*inv_tau/(inv_tau + w[p]);
      assert(!isnan(t[p]));
    }
}

void EW_WienerFilter::computeMessengers(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s, bool preweighted)
{
  CMB_Map tmp_map(data.Nside, RING, SET_NSIDE);

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ALM_Map beamed_s = s;
      double inv_tau = 1/(tau_nu[ch]*lambda);
      CMB_Map& t = t_nu[ch];
      CMB_Map& w = inv_Ntilde[ch];

      beamed_s.ScaleL(data.beam[ch]);
      alm2map(beamed_s, tmp_map);

      if (active_channels[ch])
        {
          if (preweighted)
            messenger_loop_prew(t, in_data[ch], tmp_map, inv_tau, w, *data.noise[ch]->as<CMB_NN_NoiseMap>(),messenger_mask[ch]);
          else
            messenger_loop(t, in_data[ch], tmp_map, inv_tau, w, messenger_mask[ch], data.noise[ch]->as<CMB_NN_NoiseMap>());
        }
      else
        {
          messenger_loop_nosrc(t, tmp_map, inv_tau, w, messenger_mask[ch]);
        }
    }
}

void EW_WienerFilter::computeWiener(CMB_Data& data, ALM_Map& s)
{
  PowerSpec superB(1+lmax_transform);
  ALM_Map tmp_alms(lmax_transform, lmax_transform);

  superB.fill(1);
  s.Set(lmax_transform, lmax_transform);
  s.SetToZero();
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      double inv_tau = 1/(tau_nu[ch]*lambda);

      HEALPIX_method(t_nu[ch], tmp_alms, data.weight);
      tmp_alms.Scale(inv_tau);
      tmp_alms.ScaleL(data.beam[ch]);

      s.Add(tmp_alms);
      for (long l = 0; l <= lmax_transform; l++)
        {
          superB[l] += alpha_N*cls[l]*square(data.beam[ch][l])*inv_tau;
        }
    }

  for (long l = 0; l <= lmax_transform; l++)
    {
      superB[l] = alpha_N*cls[l]/superB[l];
      assert(!isnan(superB[l]));
    }

  s.ScaleL(superB);
}

double EW_WienerFilter::compute_chi2(CMB_Data& data, CMB_ChannelData& in_data, const ALM_Map& s, bool full)
{
  if (full)
    {
      double chi2 = 0, chi2_s = 0;
      ALM_Map tmp_alms;
      CMB_Map tmp_map(data.Nside, RING, SET_NSIDE);
      PowerSpec inv_cls(lmax_transform+1);

      inv_cls.fill(0);
      for (int ch = 0; ch < data.numChannels; ch++)
        {
          if (!active_channels[ch])
            continue;
          tmp_alms = s;
          tmp_alms.ScaleL(data.beam[ch]);
          alm2map(tmp_alms, tmp_map);
          tmp_map -= in_data[ch];

          chi2 += data.noise[ch]->chi2(tmp_map);
        }

      for (long l = 0; l < min(inv_cls.size(),cls.size()); l++)
        inv_cls[l] = (cls[l] > 0) ? (1/cls[l]) : 0;

      tmp_alms = s;

      tmp_alms.ScaleL(inv_cls);
      chi2_s = dot_product(tmp_alms, s).real();
      return chi2 + chi2_s;
    }
  else
    {
      ALM_Map A_reduced(lmax_transform, lmax_transform);
      for (long m = 0; m <= lmax_transform; m++)
        for (long l = m; l <= lmax_transform; l++)
          A_reduced(l,m) = A_map(l,m);

      return chi2_0_ref - dot_product(A_reduced, s).real();
    }
}

bool EW_WienerFilter::try_restart(ALM_Map& s)
{
  if (restart_dir.empty())
    return false;

  try
    {
      string fname_iter = str(format("%s/s_iter_%d.fits") % restart_dir % l_iter);
      read_Alm_from_fits(fname_iter, s, l_iter, l_iter);
      return true;
    }
  catch (const PlanckError& err)
    {
       return false;
    }
}

void EW_WienerFilter::do_filter(CMB_Data& data, CMB_ChannelData& in_data, ALM_Map& s, bool attempt_restart, bool preweighted)
{
  bool restart_complete;
  int iter;

  l_iter = do_lambda_scaling ? 16 : data.Lmax;
  lmax_transform = do_lmax_scaling ? (std::min(l_iter+long(ew_buffer), data.Lmax)) : data.Lmax;
  lambda = (do_lambda_scaling) ? (alpha_N*cls[l_iter]*square(data.beam[0][l_iter]) / tau_init) : 1;
  lambda = max(lambda, double(1));

  start_convergence();

  // Precompute the helper for fast chi2 computation
  chi2_0_ref = 0;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      if (active_channels[ch])
        {
          if (!preweighted)
            chi2_0_ref += data.noise[ch]->chi2(in_data[ch]);
          else
            {
              CMB_Map m = in_data[ch];
              data.noise[ch]->apply_fwd(m);
              chi2_0_ref += data.noise[ch]->chi2(m);
            }
        }
    }

  {
    ALM_Map tmp_alms(data.Lmax, data.Lmax);
    A_map.Set(data.Lmax, data.Lmax);
    A_map.SetToZero();
    for (int ch = 0 ; ch < data.numChannels; ch++)
      {
        if (!active_channels[ch])
          continue;
        CMB_Map m = in_data[ch];
        if (!preweighted)
          data.noise[ch]->apply_inv(m);
        else
          data.noise[ch]->apply_mask(m);
        HEALPIX_method(m, tmp_alms, data.weight);
        if (!preweighted)
          tmp_alms.ScaleL(data.beam[ch]);
        A_map += tmp_alms;
      }
    A_map.Scale(12*data.Nside*data.Nside/(4*M_PI));

//    fitshandle h;
//    h.create("!A_map.fits");
//    write_Alm_to_fits(h, A_map, data.Lmax, data.Lmax, planckType<double>());
  }

  s.Set(lmax_transform,lmax_transform);
  s.SetToZero();
  precision_boost = default_boost;
  restart_complete = !attempt_restart;
  do
    {
      if (restart_complete || !try_restart(s))
        {
          restart_complete = true;
          new_chi2 = -1;
          iter = 0;
          cout << endl << format("[EW WIENER] -> Boost = %lg Lambda = %lg  L_Iter = %d  L_Transform = %d      ") % precision_boost % lambda % l_iter % lmax_transform << endl;
          do
           {
             run_iter(data, in_data, s, preweighted);
             (cout << format("[EW WIENER] Delta = %10lg  Chi2 = %10lg  iter=%4d     \r") % ((old_chi2-new_chi2)/new_chi2) % new_chi2 % iter).flush();
             iter++;
           } while (!check_convergence(s) || iter < 2);
 

        if (!save_restart_dir.empty())
          { 
            lazyCreateDirectory(save_restart_dir);
            string fname_iter = str(format("!%s/s_iter_%d.fits") % save_restart_dir % l_iter);
            fitshandle h;
            h.create(fname_iter);
            write_Alm_to_fits(h, s, l_iter, l_iter, planckType<double>());
          }

        if (!save_convergence.empty())
          {
            lazyCreateDirectory(save_convergence);
            string fname_iter = str(format("!%s/s_iter_%d.fits") % save_convergence % l_iter);
            fitshandle h;
            h.create(fname_iter);
            write_Alm_to_fits(h, s, l_iter, l_iter, planckType<double>());
          }

      }
      if (l_iter == ew_lmax)
        break;
      if (l_iter == data.Lmax && lambda == 1)
        break;

      l_iter = min(min(l_iter+long(ew_throttler), long(ew_multiplier)*l_iter), data.Lmax);
      if (do_lmax_scaling)
        lmax_transform = min(l_iter+long(ew_buffer), data.Lmax);
      precision_boost = max(double(1), precision_boost/2);
      lambda = max(double(1), alpha_N*square(data.beam[0][l_iter])*cls[l_iter] / tau_init);
      if (l_iter == data.Lmax)
        lambda = 1;
      if (lambda == 1)
        precision_boost = 1;
    }
  while (true);

  l_iter = do_lmax_scaling ? 16 : data.Lmax;
  start_convergence();
  if (!ew_keep_out) {
    try
      {
        while (l_iter < data.Lmax)
          {
            boost::filesystem::path primary_iter = str(format("%s/s_iter_%d.fits") % save_restart_dir % l_iter);
            boost::filesystem::remove(primary_iter);
            l_iter = min(min(l_iter+long(ew_throttler), long(ew_multiplier)*l_iter), data.Lmax);
          }
      }
    catch(const boost::filesystem::filesystem_error & )
      {
      }
  }
  cout << endl;
  log_bench(cout);
}

void EW_WienerFilter::log_bench(std::ostream& s)
{
  double delta_chi2_0 = (old_chi2-new_chi2)/new_chi2;
  double delta_chi2_1 = (old_chi2-new_chi2)/sigma_chi2;
  s << format("[EW_FINAL] chi2_solution = %g  delta_chi2(CG)=%lg delta_chi2(EW)=%lg sigma_chi2=%g") % new_chi2 % delta_chi2_0 % delta_chi2_1 % sigma_chi2 << endl;

}

void EW_WienerFilter::setWienerOptions(const KeywordMap& kwmap)
{
  default_boost = CMB::extract_keyword<double>(kwmap, "EW_BOOST", 1);
  do_lambda_scaling = CMB::extract_keyword<int>(kwmap, "EW_SCALING", 1) == 1;
  do_lmax_scaling = CMB::extract_keyword<int>(kwmap, "EW_LMAX_SCALING", 1) == 1;
  ew_multiplier = CMB::extract_keyword<double>(kwmap, "EW_MULTIPLIER", 2);
  ew_throttler = CMB::extract_keyword<int>(kwmap, "EW_THROTTLER", 500);
  ew_keep_out = CMB::extract_keyword<int>(kwmap, "EW_KEEP_OUT", 0) == 1;
  save_convergence = CMB::extract_keyword<string>(kwmap, "EW_DEBUG_CONVERGENCE", "debug/ew");
  ew_buffer = CMB::extract_keyword<int>(kwmap, "EW_BUFFER", 100);
  ew_lmax = CMB::extract_keyword<int>(kwmap,"EW_LMAX", -1);
}
