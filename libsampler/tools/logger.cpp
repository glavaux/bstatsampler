#include <iostream>
#include "logger.hpp"

using namespace CMB;
using namespace std;

Logger CMB::default_logger;


Logger::Logger()
{
}

ostream& Logger::line()
{
  list<string>::iterator i = subsys_list.begin();
  
  if (i == subsys_list.end())
    return cout;
    
  while (i != subsys_list.end())
    {
      cout << *i << ":";
      ++i;
    }
  return cout;
}
    
void Logger::enter(const std::string& subsys)
{
  subsys_list.push_back(subsys);
}

void Logger::leave()
{
  subsys_list.pop_back();
  cout.flush();
}

