/*+
This is ABYSS (./libsampler/tools/cubic.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CUBIC_HPP

#include <arr.h>
#include "inv.hpp"

namespace CMB
{
  template<typename T>
  struct SplineParam
  {
    T a, b, c, d;
  };

  template<typename T>
  class CubicSpline
  {
  private:
    arr<T> points;
    arr<T> pos;
    arr<SplineParam<T> > spline;
    
  public:
    CubicSpline()
    {
    }
    
    CubicSpline(const arr<T>& xs, const arr<T>& data) throw(Message_error)
      : pos(xs), points(data)
    {
      spline.alloc(data.size());
      planck_assert(data.size() == xs.size(), "The array of positions and samples must be conformal.");
      computeSpline();
    }

    ~CubicSpline()
    {
    }

    void computeSplineParameter(long i);
    void computeSpline();

    template<typename T2>
    static T2 cube(T2 a) { return a*a*a; }
    
    template<typename T2>
    static T2 square(T2 a) { return a*a; }

    int lookupX(T x) const
      throw(Message_error)
    {
      int N = pos.size();
      int pmin = 1;
      int pmax = N;
      int pmid = N/2;

      planck_assert(x <= pos[N-1], "Invalid x for lookup");
      while (pmax-pmin > 1)
	{
	  if (x <= pos[pmid])
	    pmax = pmid;
	  else
	    pmin = pmid;
	  pmid = (pmin+pmax)/2;
	}
      return pmid;
    }

    int lookupX_regular(T x) const
      throw(Message_error)
    {
      planck_assert(x <= pos[pos.size()-1], "Invalid x for lookup");
      return int(floor((x-pos[0])/(pos[1]-pos[0])));
    }

    T compute(T x) const
      throw(Message_error)
    {
      int id = lookupX(x);
      
      if (id == pos.size()-1)
	id = pos.size()-2;

      T dx = x-pos[id];
      T x3 = cube(dx), x2 = square(dx);
      const SplineParam<T>& par = spline[id];

      return 
	x3 * par.a + x2 * par.b +
	dx * par.c + par.d;

    }

    T computeRegular(T x) const
      throw(Message_error)
    {
      int id = lookupX_regular(x);
      
      if (id == pos.size()-1)
	id = pos.size()-2;

      T dx = x-pos[id];
      T x3 = cube(dx), x2 = square(dx);
      const SplineParam<T>& par = spline[id];

      return 
	x3 * par.a + x2 * par.b +
	dx * par.c + par.d;
    }
  };


  template<typename T>
  void CubicSpline<T>::computeSplineParameter(long i)
  {
    fix_arr<T,4*4> matrix_data;
    fix_arr<T,4*4> inv_matrix;
    T values[4],params[4];
    double pos1 = pos[i-1]-pos[i];
    double pos2 = pos[i+1]-pos[i];
    double pos3 = 0.5*pos1;
    double pos4 = 0.5*pos2;

    matrix_data[0*4 + 0] = cube(pos1);
    matrix_data[0*4 + 1] = square(pos1);
    matrix_data[0*4 + 2] = pos1;
    matrix_data[0*4 + 3] = 1;
    values[0] = points[i-1];

    matrix_data[1*4 + 0] = 3*square(pos3);
    matrix_data[1*4 + 1] = 2*pos3;
    matrix_data[1*4 + 2] = 1;
    matrix_data[1*4 + 3] = 0;
    values[1] = (points[i]-points[i-1])/(pos[i]-pos[i-1]);

    matrix_data[2*4 + 0] = cube(pos2);
    matrix_data[2*4 + 1] = square(pos2);
    matrix_data[2*4 + 2] = pos2;
    matrix_data[2*4 + 3] = 1;
    values[2] = points[i+1];

    matrix_data[3*4 + 0] = 3*square(pos4);
    matrix_data[3*4 + 1] = 2*pos4;
    matrix_data[3*4 + 2] = 1;
    matrix_data[3*4 + 3] = 0;
    values[3] = (points[i+1]-points[i])/(pos[i+1]-pos[i]);

    // We must now solve for X in  matrix * X = values
    inv4x4<fix_arr<T,4*4>, T>(matrix_data,inv_matrix);
    prod_vec(inv_matrix, values, params);

    spline[i].a = params[0];
    spline[i].b = params[1];
    spline[i].c = params[2];
    spline[i].d = params[3];
  }

  template<typename T>
  void CubicSpline<T>::computeSpline()
  {
    long Npts = points.size();

    for (long i = 1; i < Npts-1; i++)
      {
	computeSplineParameter(i);
      }
  }

};


#endif
