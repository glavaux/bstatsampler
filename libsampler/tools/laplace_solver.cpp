/*+
This is ABYSS (./libsampler/tools/laplace_solver.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <fitshandle.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <alm_healpix_tools.h>
#include "laplace_solver.hpp"
#include <boost/format.hpp>
#include "extra_map_tools.hpp"

using namespace std;
using namespace CMB;
using boost::format;
using boost::str;

LaplaceSolver::LaplaceSolver(int Lmax, 
			     const Healpix_Map<int>& mask, double maxRadius, int numKernels)
{
  this->mask = mask;
  this->Lmax = Lmax;
  prepareKernels(maxRadius, numKernels);
  prepareSelection();
}


LaplaceSolver::~LaplaceSolver()
{
}

void LaplaceSolver::prepareKernels(double maxRadius, int numKernels)
{
  double minRadius = M_PI/(mask.Nside());
  double delta = log(maxRadius/minRadius)/(numKernels-1);
  arr<double> w;

  cout << "Preparing kernels..." << endl;
  minRadii.alloc(numKernels);
  maxRadii.alloc(numKernels);
  kernels.alloc(numKernels);

  minRadii[0] = 0;
  maxRadii[0] = minRadius;
  for (int i = 1; i < numKernels; i++)
    {
      minRadii[i] = minRadius*exp((i-1)*delta);
      maxRadii[i] = minRadius*exp(i*delta);
      cout << format("   Kernel %d: %g < Theta < %g") % i % (180/M_PI*minRadii[i]) % (180/M_PI*maxRadii[i]) << endl;
    }

  w.alloc(2*mask.Nside());
  w.fill(1);

  Healpix_Map<double> kernel_map;
  kernel_map.SetNside(mask.Nside(), RING);

  for (int k = 0; k < numKernels; k++)
    {
      cout << format("  %d / %d...") % k % numKernels << endl;
      ALM_Map alm;
      double z0 = cos(minRadii[k]);
      double z1 = cos(maxRadii[k]);
          
      for (int p = 0; p < kernel_map.Npix(); p++)
	{
	  double z, phi;

	  kernel_map.pix2zphi(p, z, phi);
	  if (z <= z0 && z > z1)
	    kernel_map[p] = 1;
	  else
	    kernel_map[p] = 0;
	}
      
      alm.Set(Lmax, Lmax);
      map2alm(kernel_map, alm, w);
      kernels[k].alloc(Lmax+1);

      ofstream f(str(format("kl_%d.txt") % k).c_str());
      for (int l = 0; l <= Lmax; l++)
	{
	  kernels[k][l] = alm(l,0).re/alm(0,0).re/sqrt(2*l+1.0);      
	  f << format("%d %g") % l % kernels[k][l] << endl;
	}
    }
}


void LaplaceSolver::prepareSelection(double  threshold)
{
  Healpix_Map<double> filtered, kernel, mask_f;
  arr<double> w(mask.Nside()*2);
  arr<double> K(Lmax+1);
  ALM_Map alm(Lmax, Lmax);  

  cout << "Prepare mask/kernel selection..." << endl;

  w.fill(1.0);
  filtered.SetNside(mask.Nside(), RING);
  kernel.SetNside(mask.Nside(), RING);

  kernel_selection.SetNside(mask.Nside(), RING);
  kernel_selection.fill(-1);

  mask_f.SetNside(mask.Nside(), RING);
  for (int p = 0; p < mask.Npix(); p++)
    mask_f[p] = mask[p];

  for (int k = 0; k < kernels.size(); k++)
    {
      cout << format("   Kernel %d/%d") % k % kernels.size() << endl;
      double z1 = cos(minRadii[k]);
          
      for (int p = 0; p < kernel.Npix(); p++)
	{
	  double z, phi;

	  kernel.pix2zphi(p, z, phi);
	  if (z >= z1)
	    kernel[p] = 1;
	  else
	    kernel[p] = 0;
	}

      map2alm(kernel, alm, w);
      for (int l = 0; l <= Lmax; l++)
	K[l] = alm(l,0).re/alm(0,0).re/sqrt(2.0*l+1);

      map2alm(mask_f, alm, w);
      alm.ScaleL(K);
      alm2map(alm, filtered);

      for (int p =  0; p < filtered.Npix(); p++)
	{
	  //	  filtered[p] = 1-filtered[p];
	  if ((filtered[p] > threshold) && (kernel_selection[p] < 0))
	    kernel_selection[p] = k;
	}
    }

  {
    fitshandle f;
    f.create("!kernel_selection.fits");
    write_Healpix_map_to_fits(f, kernel_selection, planckType<int>());
  }

  for (int p = 0; p < kernel_selection.Npix(); p++)
    if (kernel_selection[p] < 0)
      {
	cout << "Filters are not covering all the mask." << endl;
	abort();
      }
}

void LaplaceSolver::solve(const Healpix_Map<double>& input, Healpix_Map<double>& output, double max_err)
{
  arr<double> w(2*mask.Nside());
  ALM_Map alm(Lmax, Lmax);
  Healpix_Map<double> map_fix, prev_map;
  double delta, err, amplitude;
  int N;
  int fig_cnt = 0;

  w.fill(1.0);

  output = input;
  output *= mask;
  map_fix.SetNside(mask.Nside(), RING);

  for (int k = kernels.size()-1; k >= 0; k--)
    {
      cout << format("Smoothing with kernel %d.") % k << endl;
      do
	{
	  prev_map = output;
	  map2alm(output, alm, w);
	  alm.ScaleL(kernels[k]);
	  alm2map(alm, map_fix);
	  
	  err  = 0;
	  N = 0;
	  for (int p = 0; p < map_fix.Npix(); p++)
	    {
	      if (!mask[p] && kernel_selection[p] >= k)
		{
		  delta = output[p]-map_fix[p];
		  output[p] = map_fix[p];
		  err += delta*delta;
		  amplitude += output[p]*output[p];
		}
	      if (mask[p])
		output[p] = input[p];
	    }
	  err = sqrt(err/amplitude);
	  cout << format("Relative error %g.") % err << endl;

	  if (1)
	    {
	      fitshandle f;
	      
	      f.create(str(format("!fig_%d.fits") % fig_cnt));
	      write_Healpix_map_to_fits(f, output, planckType<double>());
	      fig_cnt++;
	    }
	}
      while (err > max_err);
    }
}
