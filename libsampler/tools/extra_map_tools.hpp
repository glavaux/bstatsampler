/*+
This is ABYSS (./libsampler/tools/extra_map_tools.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __EXTRA_MAP_TOOLS_HPP
#define __EXTRA_MAP_TOOLS_HPP

#include <xcomplex.h>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include "clean_sum.hpp"

namespace CMB
{
  
  template<typename OutMap>
  void makeRandomMap(gsl_rng *r, OutMap& map)
  {
    long N = map.Npix();

    for (long i = 0; i < N; i++)
      map[i] = gsl_ran_gaussian_ziggurat(r, 1.0);
  }

  template<typename OutMap, typename T>
  void fillMap(OutMap& map, const T& val)
  {
    long N = map.Npix();
    for (long i = 0; i < N; i++)
      map[i] = val;
  }

  template<typename T>
  const Healpix_Map<T>& operator+=(Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    long N = m1.Npix();
    planck_assert(m1.Npix() == m2.Npix(), "The two maps must be conformable");
    for (long i = 0; i < N; i++)
      m1[i] += m2[i];
    return m1;
  }
 
  template<typename T>
  const Healpix_Map<T>& operator/=(Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    long N = m1.Npix();
    planck_assert(m1.Npix() == m2.Npix(), "The two maps must be conformable");
    for (long i = 0; i < N; i++)
      m1[i] /= m2[i];
    return m1;
  }

  template<typename T>
  const Healpix_Map<T>& operator-=(Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    long N = m1.Npix();

    planck_assert(m1.Npix() == m2.Npix(), "The two maps must be conformable");
    for (long i = 0; i < N; i++)
      m1[i] -= m2[i];
    return m1;
  }

  template<typename T,typename T2>
  const Healpix_Map<T>& operator*=(Healpix_Map<T>& m1, const T2& val)
  {
    long N = m1.Npix();

    for (long i = 0; i < N; i++)
      m1[i] *= val;
    return m1;
  }
 
  template<typename T>
  xcomplex<T> dot_product(const Healpix_Map<T>& map1, const Healpix_Map<T>& map2)
  {
    T scalar = 0;
    static const int splitting = 4;
    arr<T> tmp_map(map1.Npix());
    
    planck_assert(map1.Nside() == map2.Nside(), "map1 and map2 does not have the same Nside");
    planck_assert(map1.Scheme() == map2.Scheme(), "map1 and map2 are not in the same ordering");

#pragma omp parallel for schedule(static)    
    for (long p = 0; p < map1.Npix(); p++)
      tmp_map[p] = map1[p]*map2[p];

    clean_sum(tmp_map, tmp_map.size());
    return xcomplex<T>(tmp_map[0],0);
  }   

  static double realpart(double r) { return r; }

  template<typename T>  T realpart(const xcomplex<T>& c) { return c.real(); }

  template<typename T,typename T2>
  Healpix_Map<T2> operator*(T val, const Healpix_Map<T2>& m)
  {
    Healpix_Map<T2> outmap(m.Nside(), m.Scheme(), SET_NSIDE);
    long N = m.Npix();

    for (long i = 0; i < N; i++)
      outmap[i] = m[i]*realpart(val);
    return outmap;
   
  }

  template<typename T>
  Healpix_Map<T> operator*(const Healpix_Map<T>& m, const Healpix_Map<T>& m2)
  {
    Healpix_Map<T> outmap(m.Nside(), m.Scheme(), SET_NSIDE);
    long N = m.Npix();

    for (long i = 0; i < N; i++)
      outmap[i] = m[i]*m2[i];
    return outmap;
   
  }
  
  template<typename T>
  Healpix_Map<T> operator-(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    planck_assert(m1.Scheme() == m2.Scheme(),
		  "Map1 and map2 should have the same scheme");
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] - m2[i];

    return m;
  }

  template<typename T>
  Healpix_Map<T> operator+(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    planck_assert(m1.Scheme() == m2.Scheme(),
		  "Map1 and map2 should have the same scheme");
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i] + m2[i];

    return m;
  }

  template<typename T, typename T2>
  Healpix_Map<T>& operator*=(Healpix_Map<T>& m1, const Healpix_Map<T2>& m2)
  {
    planck_assert(m1.Nside() == m2.Nside(), 
		  "Map1 and map2 should have the same number of pixels");
    planck_assert(m1.Scheme() == m2.Scheme(),
		  "Map1 and map2 should have the same scheme");
    
    for (long i = 0; i < m1.Npix(); i++)
      m1[i] *= m2[i];

    return m1;
  }

  template<typename T>
  Healpix_Map<T> abs(const Healpix_Map<T>& m1)
  {
    Healpix_Map<T> m(m1.Order(), m1.Scheme());
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = std::abs(m1[i]);

    return m;
  }
   
  template<typename T>
  Healpix_Map<T> operator/(const Healpix_Map<T>& m1, const Healpix_Map<T>& m2)
  {
    Healpix_Map<T> m(m1.Nside(), m1.Scheme(), SET_NSIDE);
    
    for (long i = 0; i < m.Npix(); i++)
      m[i] = m1[i]/m2[i];
    return m;
  }

  template<typename T>
  T norm_L2(const Healpix_Map<T>& map)
  {
    return dot_product(map,map).real();
  }

  template<typename T>
  const Healpix_Map<T>& clear(Healpix_Map<T>& m)
  {
    for (long i = 0; i < m.Npix(); i++)
      m[i] = 0;
    return m;
  }
  
};


#endif
