/*+
This is ABYSS (./libsampler/operators/recursive_preconditioner.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __RECURSIVE_PRECONDITIONER_HPP
#define __RECURSIVE_PRECONDITIONER_HPP

#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include <arr.h>
#include <iostream>
#include "noise_weighing.hpp"
#include "conjugateGradient.hpp"
#include "complex_tools.hpp"

namespace CMB
{
  class MPI_Communication;

  struct RecursivePreconditionerInfo
  {
    arr<int> maxIterPerLevel;
    arr<double> epsilonPerLevel;
    arr<int> lmaxLevel;
    arr<int> nsideLevel;
    int maxLevels;
    int LmaxBlock, MmaxBlock;
  };

class MapSignalNoiseSignal_weighing_recursive_preconditioner;

  class MapSignalNoiseSignal_weighing_recursive_preconditioner: public GenericAlmPreconditioner
{
private:
  MapSignalNoiseSignal_weighing_recursive_preconditioner *nextLevel;
  MapSignalNoiseSignal_weighing_preconditioner<-1,1> *Pmatrix;
  MapSignalNoiseSignal_weighing *Amatrix;
  double epsilon_CG;
  long lmax, Nside;
  int maxIter, rec_level;
  RecursivePreconditionerInfo rec_info;
  CMB_NoiseChannelData noiseCoarse;
  MPI_Communication *comm;
public:
  MapSignalNoiseSignal_weighing_recursive_preconditioner(
               MPI_Communication *c,
               const CMB_NoiseChannelData& n,
							 const arr<double>& w,
							 const arr<BeamPower>& b,
							 
							 const arr<double>& SqCls,
							 int numCh,
							 CMB_Transform<DataType> *t,
							 const RecursivePreconditionerInfo& info,
							 int level);

  void buildNoiseChannelData(const CMB_NoiseChannelData& n);

  ~MapSignalNoiseSignal_weighing_recursive_preconditioner();
  
  // If channel is -1, then we apply all noise covariance matrices in the
  // preconditioner. 
  // in the other case, only the covariance matrix of the specified
  // channel is applied.
  virtual void chooseChannel(int channel)
  {
    Pmatrix->chooseChannel(channel);    
    Amatrix->chooseChannel(channel); 
    if (nextLevel)
      nextLevel->chooseChannel(channel);
  }

  virtual long getLmaxMinimal() 
  {
    return lmax;
  }

  void setEpsilon(double epsilon, int max_iter)
  {
    epsilon_CG = epsilon;
    if (nextLevel)
      nextLevel->setEpsilon(epsilon, max_iter);
  }
  
  MapSignalNoiseSignal_weighing_recursive_preconditioner *getNext()
  {
    return nextLevel;
  }

  virtual void SetSqCls(const arr<DataType>& sqcls, bool recompute = false)
  {
    Pmatrix->SetSqCls(sqcls, recompute);
    Amatrix->SetSqCls(sqcls); 
    if (nextLevel)
      nextLevel->SetSqCls(sqcls, recompute);
  }

  virtual ALM_Map apply(const ALM_Map& alms);

};

};

#endif
