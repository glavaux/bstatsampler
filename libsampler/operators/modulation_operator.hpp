/*+
This is ABYSS (./libsampler/operators/modulation_operator.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __MODULATION_OPERATOR_HPP
#define __MODULATION_OPERATOR_HPP

#include "cmb_defs.hpp"
#include <healpix_map.h>
#include <alm.h>
#include <xcomplex.h>
#include "cmb_transform.hpp"
#include <alm_healpix_tools.h>

namespace CMB {

  template<typename T,typename T2>
  class ModulationOperator: public CMB_Transform<T>
  {
  protected:
    static const bool VERBOSE = true;
    int Nside;
    double mul1, mul2;
    CMB_Map modmap;
    long rangeMod_min, rangeMod_max;
  public:
    ModulationOperator(long nSide)
      : Nside(nSide), modmap(nSide, RING, SET_NSIDE)
    {
    }

    ~ModulationOperator()
    {
    }

    void setModulation(const Alm<xcomplex<DataType> >& mulcoef)
    {
      Alm<xcomplex<DataType> > modalm;

      modalm = mulcoef;
      alm2map(modalm, modmap);

#pragma omp parallel for schedule(static)
      for (long i = 0; i < modmap.Npix(); i++)
	modmap[i] += 1;
    }

    void setModRange(long lmin, long lmax)
    {
      rangeMod_min = lmin;
      rangeMod_max = lmax;
    }

    void applyModulation(const Healpix_Map<T>& in,
			 Healpix_Map<T>& out)
    {
      planck_assert(in.Nside() == out.Nside(), "incompatible input/output maps");
      planck_assert(in.Scheme() == RING, "input must be in RING ordering");
      planck_assert(out.Scheme() == RING, "output must be in RING ordering");
      planck_assert(in.Nside() == Nside, "input Nside must match the Bayesian interpolation Nside");

#pragma omp parallel for schedule(static)
      for (long i = 0; i < in.Npix(); i++)
	{
	  out[i] = modmap[i]*in[i];
	}
    }

    virtual void apply(const Healpix_Map<T>& in,
		       Healpix_Map<T>& out)
    {
      applyModulation(in, out);
    }

    virtual void applyTranspose(const Healpix_Map<T>& in,
				Healpix_Map<T>& out)
    {
      applyModulation(in, out);
    }

    void operator()(const Healpix_Map<T>& in,
		    Healpix_Map<T>& out)
    {
      applyModulation(in, out);    
    }
  };

};


#endif
