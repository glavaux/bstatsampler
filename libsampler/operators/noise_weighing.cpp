/*+
This is ABYSS (./libsampler/operators/noise_weighing.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#undef NDEBUG
#include <fstream>
#include <iostream>
#include <cmath>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sf_coupling.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "cmb_defs.hpp"
#include "noise_weighing.hpp"
#include "extra_alm_tools.hpp"
#include "extra_map_tools.hpp"
#include "wigner3j.hpp"
#include <CosmoTool/yorick.hpp>
#include <CosmoTool/algo.hpp>
#include <boost/format.hpp>
#include "mpi_communication.hpp"

#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <Eigen/Eigenvalues>


using namespace CMB;
using namespace std;
using CosmoTool::saveArray;
using boost::format;
using CosmoTool::square;

static const bool DUMP_MATRIX_PRE_INVERT = true;
static const bool DUMP_MATRIX_POST_INVERT = true;
static const bool USE_CHOLESKY_FOR_INVERT = false;
static const double snThreshold = 2000.;

struct MatrixAccessor
{
  xcomplex<DataType> *M;
  int N;
  xcomplex<DataType>& operator()(int i, int j) {
    return M[i*N+j];
  }
};

template<int pN, int pS>
MapSignalNoiseSignal_weighing_preconditioner<pN,pS>::MapSignalNoiseSignal_weighing_preconditioner(
	   MPI_Communication *c,
	   const CMB_NoiseChannelData& n,
	   const arr<double>& w,
	   const arr<BeamPower>& b,
	   long nSide, long Lmax, 
	   long LmaxPre, long MmaxPre,
	   const arr<double>& preSqCls,
	   int numCh, bool attemptResume)

 : comm(c),
   noise(n.size()), weight(w), beam(b), map(nSide, RING, SET_NSIDE),
   lmax(Lmax), lmaxPre(LmaxPre), mmaxPre(MmaxPre), preconditioner(n.size())
{
  sqrt_cls = preSqCls;
  numChannels = numCh;      
  for (int i = 0; i < numChannels; i++)
    {
      noise[i] = n[i]->as<CMB_NN_NoiseMap>();
      preconditioner[i].Set(lmax, lmax);
    }
  setupPreconditioner(attemptResume);
  currentChannel = -1;
}

template<typename T>
std::complex<T> c_adaptor(const xcomplex<T>& c)
{
  return std::complex<T>(c.real(), c.imag());
}

template<int pN, int pS>
ALM_Map MapSignalNoiseSignal_weighing_preconditioner<pN,pS>::apply(const ALM_Map& alms)
{
  typedef Eigen::Matrix<std::complex<DataType>,Eigen::Dynamic, 1 > CVector;
  ALM_Map alms_tmp(alms.Lmax(), alms.Mmax());
  long mmax0, lmax0;
  const arr<DataType>& sqcls = sqrt_cls;
  xcomplex<DataType> one(1,0);
  ALM_Map coef(alms.Lmax(), alms.Mmax());

  assert(alms.Lmax() >= lmaxPre);
  assert(alms.Lmax() <= lmax);

  lmax0 = alms.Lmax();
  mmax0 = alms.Mmax();
  
  arr<double> cls(1+lmax0);
  for (long l = 0; l <= lmax0; l++)
    cls[l] = square(sqcls[l]);

  double full_norm = map.Npix()/(4*M_PI);
  long lmaxPre0 = std::max(lmaxPre, 0L);
  long mmaxPre0 = std::max(mmaxPre, 0L);
  Alm_Base alm_accessor(lmaxPre0, mmaxPre0);
  MatrixType *M = 0;
  NumPreElems = Alm_Base::Num_Alms(lmaxPre0, mmaxPre0);
  if (currentChannel < 0)
    {
      clear(coef);

      // Process all channels,  coef is the sum of
      // all "pre"preconditioner.
      for (int channel = 0; channel < numChannels; channel++)
        {
          double alpha = full_norm*square(1/noise[channel]->alpha);
          coef += alpha*preconditioner[channel];
        }

      M = &pre_inverted;
    }
  else
    {
      ALM_Map& precon = preconditioner[currentChannel];
      double alpha2 = full_norm/CosmoTool::square(noise[currentChannel]->alpha);

      coef = preconditioner[currentChannel];
      coef.Scale(alpha2);
      M = &pre_inverted_per_channel[currentChannel];
    }

  coef.ScaleL(cls);

  CVector alms_vec(NumPreElems), alms_vec2(NumPreElems);
  
  // Apply the exact inverse for the lower block
#pragma omp parallel for schedule(dynamic,10)
  for (long m = 0; m <= mmaxPre0; m++)
    for (long l = m; l <= lmaxPre0; l++) {
        long idx = alm_accessor.index(l,m);
        assert(idx < NumPreElems);
        alms_vec[idx] = std::complex<DataType>(alms(l,m));
    }
    
  alms_vec2.noalias() = (*M) * alms_vec;

#pragma omp parallel for schedule(dynamic,10)
  for (long m = 0; m <= mmaxPre0; m++)
    for (long l = m; l <= lmaxPre0; l++)
        alms_tmp(l,m) = alms_vec2[alm_accessor.index(l,m)];

#pragma omp parallel for schedule(dynamic,10)
  for (long l = 0; l <= lmax0; l++)
    {
      for (long m = 0; m <= l; m++)
        {
          if (l <= lmaxPre0 && m <= mmaxPre0)
            continue;
        
          xcomplex<DataType> c = (one + coef(l,m));
          alms_tmp(l,m) = alms(l,m)/c;
        }
    }

    
  for (long l = 0; l <= lmax0; l++)
    for (long m = 0; m <= l; m++) {
      if(isnan(alms_tmp(l,m).real())) abort();
      if(isnan(alms_tmp(l,m).imag())) abort();
    }
  return alms_tmp;
}


template<typename Derived>
static void dumpCovMatrix(const Eigen::MatrixBase<Derived>& M, long lmaxPre, long mmaxPre, const std::string& prefix)
{
  arr<double> pre_real, pre_im;

  long NumPreElems = Alm_Base::Num_Alms(lmaxPre, mmaxPre);
  Alm_Base alm_accessor(lmaxPre, mmaxPre);
  
  pre_real.alloc(NumPreElems*NumPreElems);
  pre_im.alloc(NumPreElems*NumPreElems);
  pre_real.fill(0.);
  pre_im.fill(0.);
  long q, k;
  q = 0;
  for (long l1 = 0; l1 <= lmaxPre; l1++)
    {
      for (long m1 = 0; m1 <= std::min(l1, mmaxPre); m1++)
        {
          long index1 = alm_accessor.index(l1, m1);

          for (long l2 = 0; l2 <= lmaxPre; l2++)
            {
              for (long m2 = 0; m2 <= std::min(l2, mmaxPre); m2++)
                {
                  long index2 = alm_accessor.index(l2, m2);
                  xcomplex<DataType> c = M(index1, index2);

                  pre_real[index2 + NumPreElems * index1] = c.re;
                  pre_im[index2 + NumPreElems * index1] = c.im;

                }
            }
        }
    }

  uint32_t dimList[2] = {NumPreElems, NumPreElems };
  saveArray(str(format("debug/%s_real.nc") % prefix), &pre_real[0], dimList, 2);
  saveArray(str(format("debug/%s_im.nc") % prefix), &pre_im[0], dimList, 2);    
}


template<typename T, int Dyn, int Dyn2>
static void loadCovMatrix(Eigen::Matrix<T, Dyn, Dyn2>& M, long lmaxPre, long mmaxPre, const std::string& prefix)
{
  double *pre_real, *pre_im;

  long NumPreElems = Alm_Base::Num_Alms(lmaxPre, mmaxPre);
  Alm_Base alm_accessor(lmaxPre, mmaxPre);
  uint32_t *dimList;
  uint32_t dimRank;
  
  CosmoTool::loadArray(str(format("debug/%s_real.nc") % prefix), pre_real, dimList, dimRank);
  delete[] dimList;
  CosmoTool::loadArray(str(format("debug/%s_im.nc") % prefix), pre_im, dimList, dimRank);    
  delete[] dimList;


  M.resize(NumPreElems, NumPreElems);
#pragma omp parallel for schedule(dynamic)
  for (long l1 = 0; l1 <= lmaxPre; l1++)
    {
      for (long m1 = 0; m1 <= std::min(l1, mmaxPre); m1++)
        {
          long index1 = alm_accessor.index(l1, m1);

          for (long l2 = 0; l2 <= lmaxPre; l2++)
            {
              for (long m2 = 0; m2 <= std::min(l2, mmaxPre); m2++)
                {
                  long index2 = alm_accessor.index(l2, m2);
                  
                  M(index1, index2) = 
                    T(pre_real[index2 + NumPreElems * index1],
                                           pre_im[index2 + NumPreElems * index1]);
                }
            }
        }
    }

  delete[] pre_real;
  delete[] pre_im;
}


#include "fast_cholesky_invert.hpp"

template<int pN, int pS>
void MapSignalNoiseSignal_weighing_preconditioner<pN,pS>::setupPreconditioner(bool attemptResume)
{
  arr< ALM_Map > noise0;

  cout << "=== SETUP PRECONDITIONER ===" << endl;
  
  if (attemptResume) {
      bool failedResume = false;
      
      // Check if there is a preconditioner file in the debug directory.
      try {
        for (int channel = 0; channel < numChannels; channel++) {
          fitshandle h;
          string s = str(format("debug/diag_preconditioner_Nside_%d_ch%d_lmax_%d.fits") % map.Nside() % channel % lmax);
          h.open(s);
          preconditioner[channel].Set(lmax,lmax);
          read_Alm_from_fits(h, preconditioner[channel], lmax, lmax);
          
          loadCovMatrix(pre_inverted_per_channel[channel], lmaxPre, mmaxPre, str(format("preconditioner_ch_%d_post_%d_Lmax_%d_Mmax_%d") % channel % map.Nside() % lmaxPre % mmaxPre));
        }
        
        loadCovMatrix(pre_inverted, lmaxPre, mmaxPre, str(format("preconditioner_ch_-1_post_%d_Lmax_%d_Mmax_%d") % map.Nside() % lmaxPre % mmaxPre));
      } catch(...) {
        // If anything goes wrong. Fail completely.
        failedResume = true;
      }
      
      if (!failedResume)
        return;
  }
  
  cout << "++ Transforming ++" << endl;

  noise0.alloc(numChannels);
  for (int ch = 0; ch < numChannels; ch++)
    {
      CMB_Map mynoise(noise[ch]->Nside(), RING, SET_NSIDE);
      ALM_Map alm_noise(lmax,lmax);
      
      fillMap(mynoise, 1.0);
      if (pN==-1)
        noise[ch]->apply_inv(mynoise);
      else if (pN==1)
        noise[ch]->apply_fwd(mynoise);
      else
        abort();
      
      HEALPIX_method(mynoise, alm_noise, weight);      
      noise0[ch] = alm_noise;
    }

  /* Double loop to iterate over all elements
   * in the array.
   */
  
  arr<double> prefac;
  prefac.alloc(2*lmax+3);
  for (long l = 0; l <= 2*lmax+2; l++)
    prefac[l] = sqrt((2*l+1)/(4*M_PI));

  cout << "++ Building ++" << endl << "  ";
  
  long lmaxPre0 = std::max(lmaxPre, 0L);
  long mmaxPre0 = std::max(mmaxPre, 0L);
  Alm_Base alm_accessor(lmaxPre0, mmaxPre0);
  NumPreElems = Alm_Base::Num_Alms(lmaxPre0, mmaxPre0);
  double full_norm = noise[0]->Npix()/(4*M_PI);

  pre_inverted.resize(NumPreElems, NumPreElems);
  pre_inverted.setConstant(std::complex<DataType>(0,0));

  pre_inverted_per_channel.alloc(numChannels);

  for (int channel = 0; channel < numChannels; channel++)
    {  
      ALM_Map& precon = preconditioner[channel];

      pre_inverted_per_channel[channel].resize(NumPreElems, NumPreElems);
      pre_inverted_per_channel[channel].setConstant(0.);

      clear(precon);

      cout << "** CHANNEL " << channel << " **" << endl;
      cout << "  -> diagonal elements" << endl;

#pragma omp parallel for schedule(dynamic)
      for (long m = 0; m <= lmax; m++)
        {
          int mysign = ((m & 1) != 0) ? -1 : 1;
          arr<double> coef1, coef2;

          coef1.alloc(3*lmax+3);
          coef2.alloc(3*lmax+3);

          (cout << format("%d ") % m).flush();

          for (long l = m; l <= lmax; l++)
            {
              DataType preval = 0;
              long Lmin, Lmax;

          coef1.fill(std::numeric_limits<double>::infinity());
          coef2.fill(std::numeric_limits<double>::infinity());


              compute3j(l, l, m, -m, Lmin, Lmax, coef1);
              compute3j(l, l, 0, 0, Lmin, Lmax, coef2);

              assert(Lmin ==0);
              assert(Lmax == 2*l);

              for (long l1 = 0; l1 <= min(2*l,lmax); l1++)
                {
                  double mulfac = prefac[l1] * coef1[l1] * coef2[l1];
                  preval += mulfac*(noise0[channel])(l1, 0).real();  
                }
              preval *= mysign*(2*l+1);

              precon(l,m) += preval * square(beam[channel][l]);
            }	 
        }
      cout << endl;
      if (comm->rank() == 0)
        {
          fitshandle h;
          string s = str(format("!debug/diag_preconditioner_Nside_%d_ch%d_lmax_%d.fits") % map.Nside() % channel % lmax);
          h.create(s.c_str());
          write_Alm_to_fits(h, precon, lmax, lmax, planckType<double>());
        }
      
      const ALM_Map& n_alm = noise0[channel];
      cout << "  -> Base square matrix" << endl;
#pragma omp parallel for schedule(dynamic)
      for (long m2 = 0; m2 <= mmaxPre; m2++)
        {
          int mysign = ((m2 % 2) != 0) ? -1 : 1;

          arr<double> coef1, coef2;

          coef1.alloc(3*lmax+3);
          coef2.alloc(3*lmax+3);

          (cout << format("%d ") % m2).flush();
          for (long l2 = m2; l2 <= lmaxPre; l2++)
            {
              long index2 = alm_accessor.index(l2, m2);

              for (long m1 = 0; m1 <= mmaxPre; m1++)
                {
                  for (long l1 = m1; l1 <= lmaxPre; l1++)
                    {
                      long index1 = alm_accessor.index(l1, m1);
                      long L3min_1, L3max_1, L3min_2, L3max_2, L3min, L3max;
                      long m3 = m2-m1;
                      xcomplex<DataType> S = 0;

                      compute3j(l1, l2, 0, 0, L3min_1, L3max_1, coef1);
                      compute3j(l1, l2, m1, -m2, L3min_2, L3max_2, coef2);

                      L3min = max(L3min_1,L3min_2);
                      L3max = min(L3max_1,L3max_2);

                      assert(L3max <= 2*lmax); 
                      if (m3 < 0)
                        {
                          int mysign3;
                          m3 = -m3;

                          mysign3 = ((m3 % 2) == 0) ? 1 : -1;

                          for (long l3 = L3min; l3 <= L3max; l3++)
                            {
                              xcomplex<DataType> c;

                              assert(m3 <= l3);
                              if (l3 > lmax)
                                continue;
			        
                              c = n_alm(l3, m3);
                              c = c.conj();			      
                              S += c * coef1[l3-L3min_1] * coef2[l3-L3min_2] * prefac[l3];
                            }
                          S *= mysign3;
                        }
                      else
                        {
                          for (long l3 = L3min; l3 <= L3max; l3++)
                            {
                              xcomplex<DataType> c;

                              assert(m3 <= l3);
                              if (l3 > lmax)
                                continue;
			        
                              assert(lmax == n_alm.Lmax());
                              assert(m3 <= n_alm.Mmax());
                              c = n_alm(l3, m3);
                              S += c * coef1[l3-L3min_1] * coef2[l3-L3min_2] * prefac[l3];
                            }			  
                        }
		        
                      S *= mysign;
                      S *= full_norm * sqrt_cls[l1] * beam[channel][l1] * beam[channel][l2] *
                      sqrt_cls[l2] * prefac[l1] * prefac[l2] * 4 * M_PI;		
                      assert(index1 < NumPreElems);
                      assert(index2 < NumPreElems);
                      pre_inverted(index2, index1) += c_adaptor(S);
                      pre_inverted_per_channel[channel](index2, index1) = S;
                      if (index1 == index2)
                        pre_inverted_per_channel[channel](index2, index1) += std::complex<DataType>(1,0);

                    }
                }
            }
        }
      cout << endl;
    }
  cout  << "== FINISHED == " << endl;

  for (long i = 0; i < NumPreElems; i++)
    {
      pre_inverted(i, i) += 1;
    }


  if (USE_CHOLESKY_FOR_INVERT) {
    choleskyInversion();
  } else {
    evInversion(snThreshold);
  } 
  
  cout << "Done precomputing the preconditioner." << endl;
}


template<int pN, int pS>
void MapSignalNoiseSignal_weighing_preconditioner<pN,pS>::choleskyInversion()
{
  if (lmaxPre >= 0) {
    pre_cholesky.resize(NumPreElems, NumPreElems);
    
#pragma omp parallel for schedule(static) num_threads(numChannels+1)
    for (int channel = -1; channel < numChannels; channel++)
    {
      MatrixType& M = ((channel < 0) ? pre_inverted : pre_inverted_per_channel[channel]);
      Eigen::LLT<MatrixType> Mllt;

      if (DUMP_MATRIX_PRE_INVERT && lmaxPre>0)
        {
          dumpCovMatrix(M, lmaxPre, mmaxPre, str(format("preconditioner_ch_%d_pre_%d_Lmax_%d_Mmax_%d") % channel % map.Nside() % lmaxPre % mmaxPre));
        }

#pragma omp critical
      {
        (cout << format("Cholesky decomposition of the exact preconditioner (channel %d, eigen_threads %d)...") % channel % Eigen::nbThreads() << endl).flush();
      }
      Mllt = M.llt();
      if (Mllt.info() != Eigen::Success)
      {
#pragma omp critical
        (cout << "Failure to compute the decomposition").flush();
        comm->abort();
      }
  
      typedef Eigen::Matrix<std::complex<DataType>, Eigen::Dynamic, 1> VectorType;
      
      (cout << format("Inversion of channel %d...") % channel << endl).flush();
      double start_t = omp_get_wtime();
/*#pragma omp parallel for schedule(static)    
      for (int i = 0; i < NumPreElems; i++)
        M.col(i) = Mllt.solve(VectorType::Unit(NumPreElems, i));*/
      M = invert_cholesky(Mllt);
      double end_t = omp_get_wtime() - start_t;
      
      (cout << format(" took %lg seconds") % end_t << endl).flush();

      if (DUMP_MATRIX_POST_INVERT && lmaxPre>0)
        {
          dumpCovMatrix(M, lmaxPre, mmaxPre, str(format("preconditioner_ch_%d_post_%d_Lmax_%d_Mmax_%d") % channel % map.Nside() % lmaxPre % mmaxPre));
      }

    }
  }
}

static double get_SN(int channel, const PowerSpec& sqcls, const arr<BeamPower>& beams,
                     const arr<const CMB_NN_NoiseMap *>& noise, long L_threshold)
{
  double SN = 0;
  
  if (channel >= 0) {
    cout << "mean_noise = " <<  noise[channel]->getMeanNoisePerPix() << endl;
    return square(beams[channel][L_threshold] * sqcls[L_threshold]) / (square(noise[channel]->getMeanNoisePerPix()));
  }
  
  for (int ch = 0; ch < noise.size(); ch++)
    SN += get_SN(ch, sqcls, beams, noise, L_threshold);
    
  return SN;
}

template<int pN, int pS>
void MapSignalNoiseSignal_weighing_preconditioner<pN,pS>::evInversion(double SN_threshold)
{
  if (lmaxPre >= 0) {
    typedef Eigen::SelfAdjointEigenSolver<MatrixType> SolverType;

//#pragma omp parallel for schedule(static) num_threads(numChannels+1)
//    for (int channel = -1; channel < numChannels; channel++)
    int channel = -1;
    {
      MatrixType& M = ((channel < 0) ? pre_inverted : pre_inverted_per_channel[channel]);
      double start_t, end_t;
      SolverType solver(NumPreElems);
      double SN = get_SN(channel, sqrt_cls, beam, noise, lmaxPre);
      
      (cout << format("Eigenvectors for channel %d...") % channel << endl).flush();            
      start_t = omp_get_wtime();
      solver.compute(M);
      end_t = omp_get_wtime() - start_t;
      (cout << format(" took %lg seconds...") % end_t << endl).flush();            

      SolverType::RealVectorType evals = solver.eigenvalues();
      { 
        string ef = str(format("debug/eigenvalues_preinv_ch_%d.txt") % channel);
        ofstream f(ef.c_str());
        f << evals << endl;
      }

      assert(evals.size() == NumPreElems);
      for (int j = 0; j < NumPreElems; j++)
        {
          if (fabs(evals[j]) > SN)
            evals[j] = 1/evals[j];
          else
            evals[j] = 1/SN;
        }
        
      const MatrixType& evecs = solver.eigenvectors();

      (cout << format("Inversion of channel %d (S/N cut = %lg)...") % channel % SN << endl).flush();            
      start_t = omp_get_wtime();
      M = evecs*evals.cast<std::complex<DataType> >().asDiagonal()*evecs.adjoint();
      end_t = omp_get_wtime() - start_t;
      
      (cout << format(" took %lg seconds") % end_t << endl).flush();

      if (DUMP_MATRIX_POST_INVERT && lmaxPre>0)
        {
          string ef = str(format("debug/eigenvalues_ch_%d.txt") % channel);
          ofstream f(ef.c_str());
          f << evals << endl;
          dumpCovMatrix(M, lmaxPre, mmaxPre, str(format("preconditioner_ch_%d_post_%d_Lmax_%d_Mmax_%d") % channel % map.Nside() % lmaxPre % mmaxPre));
      }
    }
  }
}

template class CMB::MapSignalNoiseSignal_weighing_preconditioner<-1,1>;
template class CMB::MapSignalNoiseSignal_weighing_preconditioner<1,-1>;

