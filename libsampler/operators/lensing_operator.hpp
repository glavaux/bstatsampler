/*+
This is ABYSS (./libsampler/operators/lensing_operator.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __LENSING_OPERATOR_HPP
#define __LENSING_OPERATOR_HPP

#include <iostream>
#include "cmb_defs.hpp"
#include <healpix_map.h>
#include <alm.h>
#include <xcomplex.h>
#include "cmb_transform.hpp"
#include <alm_healpix_tools.h>
#include <flints/lensing_operator_flints.hpp>

namespace CMB {

  template<typename T,typename T2>
  class Lensing_Operator: public CMB_Transform<T>
  {
protected:
    bool transposed;
    LensingOperatorFlints_Scalar<T,T2> op;
  public:
    Lensing_Operator(long nSideHR, long nSide)
      : op(nSideHR, nSide)
    {
      transposed = false;
    }
    
    ~Lensing_Operator()
    {
    }
    
    void setTranspose(bool i) { transposed = i; }
    
    void update_CMB_spectrum(const arr<T2>& Cls)
    {
      op.update_CMB_spectrum(Cls);
    }
    
    void saveField()
    {
      op.saveField();
    }
    
    void restoreField()
    {
      op.restoreField();
    }
    
    void setLensingField(const Alm<xcomplex<T> >& lenspot)
    {
      op.setLensingField(lenspot);
    }
    
    template<bool transposedLensing>
    void applyLensing(const Healpix_Map<T>& in,
		      Healpix_Map<T>& out)
    {
       op.applyLensing<transposedLensing>(in, out);  
    }
    
    virtual void apply(const Healpix_Map<T>& in,
		       Healpix_Map<T>& out)
    {
      applyLensing<false>(in, out);
    }

    virtual void applyTranspose(const Healpix_Map<T>& in,
				Healpix_Map<T>& out)
    {
      applyLensing<true>(in, out);
    }

    void operator()(const Healpix_Map<T>& in,
		    Healpix_Map<T>& out)
    {
      if (transposed)      
	applyLensing<true>(in, out);
      else
	applyLensing<false>(in, out);
    }
  };
};


#endif
