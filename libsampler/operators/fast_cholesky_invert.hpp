

template<typename T, int Alignment>
void invert_triangular(T *Lout, const T *L, int p, int r, int N)
{
  typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> MatrixType;
  
  Eigen::Map<MatrixType,Alignment> M_Lout(Lout, N, N);
  Eigen::Map<MatrixType,Alignment> M_Lin(const_cast<T*>(L), N, N);
  
  if (r==0)
    return;
    
  if (r == 1)
    M_Lout(p, p) = T(1)/M_Lin(p,p);
  else
    {
      int r_new = r/2;
      int r2_new = r-r_new;

      if (r_new > 128) {
#pragma omp task
      {
        invert_triangular<T,Alignment>(Lout, L, p, r_new, N);
      }
#pragma omp task
      {
        invert_triangular<T,Alignment>(Lout, L, p+r_new, r2_new, N);
      }
#pragma omp taskwait
      } else {
        invert_triangular<T,Alignment>(Lout, L, p, r_new, N);
        invert_triangular<T,Alignment>(Lout, L, p+r_new, r2_new, N);
      }
        
      int b1 = p;
      int b2 = p+r_new;
      
      M_Lout.block(b2,b1,r2_new,r_new).noalias() = 
        -M_Lout.block(b2,b2,r2_new,r2_new) * M_Lin.block(b2,b1,r2_new,r_new) * M_Lout.block(b1,b1,r_new,r_new);
    }
}

template<typename MatrixType>
static MatrixType invert_cholesky(const Eigen::LLT<MatrixType>& Lc)
{
  typedef typename Eigen::LLT<MatrixType> ChoType;
  const MatrixType& L = Lc.matrixLLT();
  typedef typename MatrixType::Scalar Scalar;
  
  MatrixType lowL;
  int N = L.rows();
  
  lowL = MatrixType::Zero(N,N);

#pragma omp parallel 
  {
#pragma omp single
    invert_triangular<Scalar,Eigen::Aligned>(lowL.data(), L.data(), 0, N, N);
  }

  (cout << "Reconstructing the inverse..." << endl).flush();
  // Now we have the inverse of L in lowL
  return lowL.adjoint() * lowL.template triangularView<Eigen::Lower>();
}


#define FORCE_INVERT(T) \
template static Eigen::Matrix<T, Eigen::Dynamic,Eigen::Dynamic> invert_cholesky(const Eigen::LLT<Eigen::Matrix<T, Eigen::Dynamic,Eigen::Dynamic> >& Lc)

FORCE_INVERT(double);
FORCE_INVERT(std::complex<double>);

