/*+
This is ABYSS (./libsampler/operators/recursive_preconditioner.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/format.hpp>
#include <iostream>
#include "operators/recursive_preconditioner.hpp"
#include "data/cmb_noise_nn.hpp"
#include "mpi_communication.hpp"

using namespace std;
using boost::format;
using boost::str;
using namespace CMB;

static void log_enter(int l)
{
  default_logger.enter(str(format("R%d") % l));
}

static void log_leave()
{
  default_logger.leave();
}

MapSignalNoiseSignal_weighing_recursive_preconditioner::MapSignalNoiseSignal_weighing_recursive_preconditioner(
     MPI_Communication *c,
     const CMB_NoiseChannelData& n,
		 const arr<double>& w,
		 const arr<BeamPower>& b,
		 const arr<double>& SqCls,
		 int numCh,
		 CMB_Transform<DataType> *t,
		 const RecursivePreconditionerInfo& info,
		 int level)
    : epsilon_CG(1e-4), comm(c), rec_info(info), rec_level(level)
{
  default_logger.line() << format("++++ Initializing Recursive preconditioner level=%d ++++") % level << endl;
  maxIter = info.maxIterPerLevel[level];
  epsilon_CG = info.epsilonPerLevel[level];
  lmax = info.lmaxLevel[level];
  Nside = info.nsideLevel[level];
  buildNoiseChannelData(n);
  
  Amatrix = new MapSignalNoiseSignal_weighing(noiseCoarse, w, b, Nside, Nside, numCh, t);
  if (level+1 < info.maxLevels)
    {
      // Initialize next level	

      log_enter(rec_level);
      nextLevel = new MapSignalNoiseSignal_weighing_recursive_preconditioner(comm, noiseCoarse, w, b, SqCls, numCh, t, info, level+1);
      log_leave();
      
      Pmatrix = new MapSignalNoiseSignal_weighing_preconditioner<-1,1>(comm, noiseCoarse, w, b, Nside, lmax, -1, -1, SqCls, numCh);
    }
  else
    {
      nextLevel = 0;   
      Pmatrix = new MapSignalNoiseSignal_weighing_preconditioner<-1,1>(comm, noiseCoarse, w, b, Nside, lmax, info.LmaxBlock, info.MmaxBlock, SqCls, numCh);
    }
  default_logger.line() << format("++++ Done Init Recursive preconditioner level=%d ++++") % level << endl;
  SetSqCls(SqCls, false);
}

MapSignalNoiseSignal_weighing_recursive_preconditioner::~MapSignalNoiseSignal_weighing_recursive_preconditioner()
{
  for (int i = 0; i < noiseCoarse.size(); i++)
    delete noiseCoarse[i];
  if (nextLevel)
    delete nextLevel;
  if (Pmatrix)
    delete Pmatrix;
  delete Amatrix;
}

template<typename T>
static void map_compression(const Healpix_Map<T>& original, Healpix_Map<T>& dest, Healpix_Map<int> *hits = 0)
{
  int npix = dest.Npix();
  int fact = original.Nside() / dest.Nside();
  
  if (hits)
    hits->SetNside(original.Nside(), RING);
#pragma omp parallel for schedule(static) 
  for (int m = 0; m < npix; m++)
    {
      int x, y, f;
      int jmin, jmax, imin, imax;
      int h = 0;
      T sum = 0;
      
      dest.pix2xyf(m, x, y, f);
      
      jmin = fact*y; jmax = fact*(y+1);
      imin = fact*x; imax = fact*(x+1);
            
      for (int j = jmin; j < jmax; j++)
        for (int i = imin; i < imax; i++)
          {
            int opix = original.xyf2pix(i, j, f);
            if (!approx<double>(original[opix],Healpix_undef))
              {
                ++h;
                sum += original[opix];
              }
          }
      dest[m] = T( (h==0) ? Healpix_undef : sum);
      if (hits)
        (*hits)[m] = h;
    }
}

void MapSignalNoiseSignal_weighing_recursive_preconditioner::buildNoiseChannelData(const CMB_NoiseChannelData& n)
{
  int nCh = n.size();
  
  noiseCoarse.alloc(nCh);  
  
  for (int c = 0; c < nCh; c++)
    {
      CMB_NN_NoiseMap *nn = n[c]->as<CMB_NN_NoiseMap>();
      CMB_NN_NoiseMap *coarse;
      
      noiseCoarse[c] = (coarse = new CMB_NN_NoiseMap());
      coarse->mean_noise = nn->mean_noise;
      coarse->SetNside(Nside, RING);
      default_logger.line() << format("Build coarser resolution for channel %d (map Nside=%d, here Nside=%d)") % c % nn->Nside() % Nside << endl;
      if (Nside == nn->Nside())
        {
          *coarse = *nn;
          continue;
        }
        
      map_compression(*nn, *coarse);
    }
  default_logger.line() << "Done handling coarsening of noise maps" << endl;
}
 
static void truncate_alms(ALM_Map& alm_reduced, const ALM_Map& alms)
{
#pragma omp parallel for schedule(dynamic,100)
  for (int m = 0; m <= alm_reduced.Mmax(); m++)
    for (int l = m; l <= alm_reduced.Lmax(); l++)
      alm_reduced(l,m) = alms(l,m);    
}

static void extend_alms(ALM_Map& out_inv_alms, const ALM_Map& inv_alms)
{
#pragma omp parallel for schedule(dynamic,100)
  for (int m = 0; m <= inv_alms.Mmax(); m++)
    for (int l = m; l <= inv_alms.Lmax(); l++) {
       out_inv_alms(l,m) = inv_alms(l,m);
    }
}

ALM_Map MapSignalNoiseSignal_weighing_recursive_preconditioner::apply(const ALM_Map& alms)
{
  ALM_Map alms_pre_base = (*Pmatrix)(alms);	
  
  if (nextLevel == 0)
    return alms_pre_base;

  int next_lmax = rec_info.lmaxLevel[rec_level+1];
  
  ALM_Map alm_reduced(next_lmax, next_lmax);
  ALM_Map inv_alms(next_lmax, next_lmax);
  ALM_Map out_inv_alms(lmax, lmax);

  log_enter(rec_level);

  truncate_alms(alm_reduced, alms);
  clear(inv_alms);
  conjugateGradient_pre(alm_reduced, inv_alms, *Amatrix, *nextLevel, epsilon_CG, 50, maxIter, true);
  extend_alms(out_inv_alms, inv_alms);

  log_leave();

  // Copy the solution found by the diagonal approximation for the padding
#pragma omp parallel for schedule(dynamic,100)
  for (int l = next_lmax+1; l <= lmax; l++)
    for (int m = 0; m <= l; m++)
      out_inv_alms(l,m) = alms_pre_base(l,m);    
    
/*  for (int m = next_lmax+1; m <= lmax; m++)
    for (int l = m; l <= lmax; l++) {
      out_inv_alms(l,m) = alms_pre_base(l,m);    
  }*/
  

  return out_inv_alms;
}

