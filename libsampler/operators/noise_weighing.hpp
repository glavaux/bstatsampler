/*+
This is ABYSS (./libsampler/operators/noise_weighing.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __SAMPLER_NOISE_WEIGHING_HPP
#define __SAMPLER_NOISE_WEIGHING_HPP

#include <CosmoTool/algo.hpp>
#include <cstdlib>
#include <cmath>
#include <string>
#include <fitshandle.h>
#include "cmb_defs.hpp"
#include <arr.h>
#include <alm_healpix_tools.h>
#include "extra_alm_tools.hpp"
#include "extra_map_tools.hpp"
#include "ipow.hpp"
#include "cmb_transform.hpp"
#include "mpi_communication.hpp"
#include <boost/format.hpp>
#include "healpix_map_fitsio.h"
#include <Eigen/Core>
#include <complex>

namespace CMB
{
  class MPI_Communication;

  static const bool VERBOSE_WEIGHING = false;

  class MapSignalNoiseSignal_weighing
  {
  public:
    arr<DataType> sqrt_cls;
    const arr<BeamPower>& beam;
    const CMB_NoiseChannelData& noise;
    arr<double> weight,weight2;
    CMB_Map map, map2;
    int currentChannel;
    int numChannels;
    long Lmin;
    CMB_Transform<DataType> *transform;

    static void log(const std::string& s) {
      if (VERBOSE_WEIGHING)
       (std::cerr << s << std::endl).flush();

    }

    MapSignalNoiseSignal_weighing(const CMB_NoiseChannelData& n,
				                          const arr<double>& w,
				                          const arr<BeamPower>& b,
				                          long nSide, long nSideTransform,
				                          int numCh, CMB_Transform<DataType> *t)
      : weight(w), beam(b), transform(t), noise(n)
    {
      using boost::format;
      using boost::str;
      log("starting SNS");
      weight = w;
      log("done weight = w");
      log(str(format("nside_Trans = %d") % nSideTransform));
      assert(nSideTransform > 0);
      weight2.alloc(2*nSideTransform);
      log("done weight2.alloc");
      map2.SetNside(nSideTransform, RING);
      log("done map2.SeTnSIDE");
      map.SetNside(nSide, RING);
      log("done map.SeTnSIDE");
      weight2.fill(1);
      log("done weight2.fill");

      numChannels = numCh;
      currentChannel = -1;
      Lmin = 0;
      log("SNS construction done");
    }

    void SetSqCls(const arr<DataType>& sqcls)
    {
      sqrt_cls = sqcls;
    }
    
    // If channel is -1, then we apply all noise covariance matrices 
    // in the other case, only the covariance matrix of the specified
    // channel is applied. It is possible to serialize the operation
    // outside this function, however it is less efficient.
    void chooseChannel(int channel)
    {
      currentChannel = channel;
    }

    void setLmin(int lmin)
    {
      this->Lmin = lmin;
    }

    // is there a way to avoid doing that many transforms ?
    // the problem comes from the beam operation...
    ALM_Map& applyNoise(ALM_Map& alms, int channel)
    {
      alms.ScaleL(beam[channel]);
      alm2map(alms, map);

      noise[channel]->apply_inv(map);

      HEALPIX_method(map, alms, weight);
      alms.ScaleL(beam[channel]);

      return alms;
    }

    ALM_Map operator()(const ALM_Map& alms)
    {
      ALM_Map alms_tmp, output_alm(alms.Lmax(),alms.Mmax());
      ALM_Map before_noise = alms;
      CMB_Map map_n;
      double mul = map.Npix()/(4*M_PI);
      
      clear(output_alm);
      before_noise.ScaleL(sqrt_cls);
      assert(!isnan(norm_L2(before_noise)));
 
      if (transform)
        {
          log(" == Transform (alm2map)");
          alm2map(before_noise, map2);
          map_n.SetNside(map.Nside(), RING);
          log(" == Transform (apply)");
          transform->apply(map2, map_n);
          log(" == Transform (map2alm)");
          HEALPIX_method(map_n, before_noise, weight);
        }
      
      log(" == Noise weighing...");
      if (currentChannel < 0)
        {
#pragma omp parallel for schedule(static) num_threads(numChannels)
          for (int channel = 0; channel < numChannels; channel++)
            {
              alms_tmp = before_noise;
              alms_tmp = applyNoise(alms_tmp, channel);
#pragma omp critical
              output_alm += alms_tmp;
              assert(!isnan(norm_L2(output_alm)));
            }
        }
      else
        output_alm = applyNoise(before_noise, currentChannel);
      
      if (transform) {
        log(" == Transform (alm2map)");
        alm2map(output_alm, map_n);
        log(" == Transform (apply transposed)");
        transform->applyTranspose(map_n, map2);
        log(" == Transform (map2alm)");
        HEALPIX_method(map2, output_alm, weight2);
        mul=map2.Npix()/(4*M_PI);
      } 
      
      output_alm.ScaleL(sqrt_cls);
      output_alm.Scale(mul);
      
      return output_alm+alms;
    }
  };

  class GenericAlmPreconditioner
  {
  public:
    GenericAlmPreconditioner() {}
    ~GenericAlmPreconditioner() {}

    virtual void chooseChannel(int c) = 0;
    virtual void SetSqCls(const arr<DataType>& sqcls, bool recompute_all = false) = 0;
    virtual ALM_Map apply(const ALM_Map& alms) = 0;
    virtual long getLmaxMinimal() = 0;

    ALM_Map operator()(const ALM_Map& alms) 
    {
      return apply(alms);
    }
  };

  
  template<int pN=-1, int pS=1>
  class MapSignalNoiseSignal_weighing_preconditioner: public GenericAlmPreconditioner
  {
  public:
    typedef Eigen::Matrix<std::complex<DataType>, Eigen::Dynamic, Eigen::Dynamic> MatrixType;

  
    arr<DataType> sqrt_cls;
    const arr<BeamPower>& beam;
    arr<const CMB_NN_NoiseMap *> noise;
    const arr<double>& weight;
    CMB_Map map;
    arr<ALM_Map> preconditioner;
    long lmax, lmaxPre, mmaxPre;
    int currentChannel;
    int numChannels;
    MatrixType pre_inverted, pre_cholesky;
    arr<MatrixType> pre_inverted_per_channel;
    arr<double> preCls;
    long NumPreElems;
    MPI_Communication *comm;
    
    MapSignalNoiseSignal_weighing_preconditioner(
							   MPI_Communication *c,
							   const CMB_NoiseChannelData& n,
							   const arr<double>& w,
							   const arr<BeamPower>& b,
							   long nSide, long Lmax, 
							   long LmaxPre, long MmaxPre,
							   const arr<double>& preSqCls,
							   int numCh, bool attemptResume = false);

    // If channel is -1, then we apply all noise covariance matrices in the
    // preconditioner. 
    // in the other case, only the covariance matrix of the specified
    // channel is applied.
    virtual void chooseChannel(int channel)
    {
      currentChannel = channel;
    }

    void setupPreconditioner(bool attemptResume);
    void choleskyInversion();
    void evInversion(double SN);

    virtual void SetSqCls(const arr<DataType>& sqcls, bool recompute_all = false)
    {
      sqrt_cls = sqcls;
      if (recompute_all)
        setupPreconditioner(false);
    }

    virtual long getLmaxMinimal()
    {
      return lmaxPre;
    }
    
    virtual ALM_Map apply(const ALM_Map& alms);

  };

};

#endif
