/*+
This is ABYSS (./libsampler/real_mpi/mpi_communication.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_REAL_MPI_COMMUNICATION_HPP
#define __CMB_REAL_MPI_COMMUNICATION_HPP

#include <boost/format.hpp>
#include <mpi.h>
#include <cstdlib>

namespace CMB
{

  class MPI_Exception: public std::exception
  {
  public:
    MPI_Exception(int err)
    {
      char s[MPI_MAX_ERROR_STRING];
      int l;

      MPI_Error_string(err, s, &l);
      err_string = s;
    }

    virtual const char *what() const throw() { return err_string.c_str(); }
    int code() const { return errcode; }

    virtual ~MPI_Exception() throw() {}

  private:
    std::string err_string;
    int errcode;
  };

  class MPI_Communication;

  class MPICC_Request
  {
  public:
    MPI_Request request;
    int tofrom_rank;

    MPICC_Request() {}

    bool test(MPI_Status *status = MPI_STATUS_IGNORE )
     throw(MPI_Exception)
    {
      int flag;
      int err;

      if ((err = MPI_Test(&request, &flag, status)) != MPI_SUCCESS)
	throw MPI_Exception(err);
      return flag != 0;
    }

    void free()
    {
      int err;

      if ((err = MPI_Request_free(&request)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void wait(MPI_Status *status = MPI_STATUS_IGNORE)
    {
      int err;

      if ((err = MPI_Wait(&request, status)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }
  };

  class MPICC_Window
  {
    public:
      MPI_Communication *Comm;
      MPI_Win win;
      void *wp;
      int size;
      int rank;

      void lock(bool shared = false)
        throw (MPI_Exception)
       {
          int err;
          if ((err = MPI_Win_lock(shared ? MPI_LOCK_SHARED : MPI_LOCK_EXCLUSIVE, rank, 0, win)) != MPI_SUCCESS)
            throw MPI_Exception(err);
      }

      void unlock()
        throw(MPI_Exception)
      {
        int err;

        if ((err = MPI_Win_unlock(rank, win)) != MPI_SUCCESS)
          throw MPI_Exception(err);
      }

      void fence()
      {
        MPI_Win_fence(rank, win);
      }

      void destroy() {
        MPI_Win_free(&win);
        if (wp != 0)
          MPI_Free_mem(wp);
      }

      template<typename T>
      void put(int r, T v);

      template<typename T>
      T get(int r);

      template<typename T>
      T *get_ptr() {
        return (T *)wp;
      }

      template<typename T>
      const T *get_ptr() const {
        return (const T *)wp;
      }
  };

  class MPICC_Mutex
  {
  public:
    MPICC_Mutex(MPI_Comm comm, int tag);
    ~MPICC_Mutex();

    void acquire();
    void release();
  protected:
    MPI_Comm comm;
    MPI_Win win;
    int *lockArray;
    int host_rank;
    int mutex_tag;
  };

  class MPI_Communication
  {
  private:
    MPI_Comm comm;
    int cur_rank, cur_size;
  public:
    typedef MPICC_Request Request;


    MPI_Communication(MPI_Comm mcomm)
      : comm(mcomm) {
//      MPI_Comm_set_errhandler(comm, MPI_ERRORS_RETURN);
      MPI_Comm_rank(comm, &cur_rank);
      MPI_Comm_size(comm, &cur_size);
    }

    ~MPI_Communication() {}

    MPICC_Mutex *new_mutex(int tag)
    {
      return new MPICC_Mutex(comm, tag);
    }

    int rank() const { return cur_rank; }

    int size() const { return cur_size; }

    void abort()
    {
      MPI_Abort(comm, 99);
    }

    void comm_assert(bool c, const std::string& err)
    {
      if  (!c)
        {
          log(err);
          abort();
        }
    }

    MPICC_Window win_create(int size, int disp_unit)
      throw (MPI_Exception)
    {
      MPICC_Window w;
      int err;

      w.rank = 0;
      w.Comm = this;

      if (rank() == w.rank)
        {
          if ((err = MPI_Alloc_mem(size, MPI_INFO_NULL, &w.wp)) != MPI_SUCCESS)
            throw MPI_Exception(err);
        }
      else
        {
          size = 0;
	  disp_unit = 1;
          w.wp = 0;
        }
      if ((err = MPI_Win_create(w.wp, size, disp_unit, MPI_INFO_NULL, comm, &w.win)) != MPI_SUCCESS)
        {
          if (w.wp != 0)
            MPI_Free_mem(w.wp);
          throw MPI_Exception(err);
        }
      MPI_Win_fence(0, w.win);
      return w;
    }

    void send_recv(const void *sendbuf, int sendcount, MPI_Datatype sdatatype,
		   int dest, int sendtag,
		   void *recvbuf, int recvcount, MPI_Datatype rdatatype,
		   int source, int recvtag,
		   MPI_Status *s = MPI_STATUS_IGNORE)
      throw (MPI_Exception)
    {
      int err;
      if ((err = MPI_Sendrecv((void*)sendbuf, sendcount, sdatatype, dest, sendtag,
			      recvbuf, recvcount, rdatatype, source, recvtag,
			      comm, s)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void send(const void *buf, int count, MPI_Datatype datatype,
	      int dest, int tag)
      throw (MPI_Exception)
    {
      int err;
      using boost::str;
      using boost::format;

      debug(str(format("Sending %p, count %d, type %d, dest %d, tag %d") % buf % count % (long)datatype % dest % tag));
      if ((err = MPI_Send((void*)buf, count, datatype, dest, tag, comm)) != MPI_SUCCESS) {
        debug("Error, throwing exception");
        throw MPI_Exception(err);
      }
    }

    Request Irecv(void *buf, int count, MPI_Datatype datatype,
	       int from, int tag)
      throw (MPI_Exception)
    {
      int err;
      Request req;

      req.tofrom_rank = from;
      if ((err = MPI_Irecv(buf, count, datatype, from, tag, comm, &req.request)) != MPI_SUCCESS)
        throw MPI_Exception(err);
      return req;
    }

    Request Isend(void *buf, int count, MPI_Datatype datatype, int to, int tag)
      throw (MPI_Exception)
    {
      int err;
      Request req;

      req.tofrom_rank = to;
      if ((err = MPI_Isend(buf, count, datatype, to, tag, comm, &req.request)) != MPI_SUCCESS)
        throw MPI_Exception(err);

      return req;
    }

    void recv(void *buf, int count, MPI_Datatype datatype,
	      int from, int tag,
	      MPI_Status *status = MPI_STATUS_IGNORE)
      throw (MPI_Exception)
    {
      int err;
      MPI_Status my_status;
      using boost::str;
      using boost::format;

      debug(str(format("Recv %p, count %d, type %d, from %d, tag %d") % buf % count % (long)datatype % from % tag));
      if ((err = MPI_Recv(buf, count, datatype, from, tag, comm, &my_status)) != MPI_SUCCESS)
        throw MPI_Exception(err);
      debug(str(format("=> STATUS: SOURCE=%d, TAG=%d, ERROR=%d") % my_status.MPI_SOURCE % my_status.MPI_TAG % my_status.MPI_ERROR));
    }

    void reduce(const void *sendbuf, void *recvbuf, int count,
		MPI_Datatype datatype, MPI_Op op, int root)
    {
      int err;

      if ((err = MPI_Reduce((void*)sendbuf, recvbuf, count, datatype, op, root, comm)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void broadcast(void *sendrecbuf, int sendrec_count, MPI_Datatype sr_type,
		   int root)
    {
      int err;

      if ((err = MPI_Bcast(sendrecbuf, sendrec_count, sr_type, root, comm)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		 void *recvbuf, int recvcount, MPI_Datatype recvtype,
		 int root)
    {
      int err;

      if ((err = MPI_Scatter((void*)sendbuf, sendcount, sendtype,
			     recvbuf, recvcount, recvtype, root,
			     comm)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void all_reduce(const void *sendbuf, void *recvbuf, int count,
		    MPI_Datatype datatype, MPI_Op op)
    {
      int err;

      if ((err = MPI_Allreduce((void*)sendbuf, recvbuf, count, datatype, op, comm)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    void barrier()
    {
      int err;
      if ((err = MPI_Barrier(comm)) != MPI_SUCCESS)
        throw MPI_Exception(err);
    }

    template<typename T>
    void accum(T *target_array, const T *source_array, int count, int root)
    {
      MPI_Datatype t = translateMPIType<T>();

      if (rank() == root)
        {
          T *tmp_arr = new T[count];
          for (int other = 0; other < size(); other++)
            {
              if (other == root)
                continue;
              recv(tmp_arr, count, t, other, 0);
              for (int j = 0; j < count; j++)
                target_array[j] += tmp_arr[j];
            }
          delete[] tmp_arr;
        }
      else
        {
          send(source_array, count, t, root, 0);
        }
    }

    template<typename T>
    void all_accum(T *ts_array, int count)
    {
      MPI_Datatype t = translateMPIType<T>();

      accum(ts_array, ts_array, count, 0);
      if (rank() == 0)
        {
          for (int other = 1; other < size(); other++ )
            send(ts_array, count, t, other, 0);
        }
      else
        recv(ts_array, count, t, 0, 0);
    }

    void log_root(const std::string& s)
    {
      if (rank() == 0)
        (std::cout << s << std::endl).flush();
    }

    void log(const std::string& s)
    {
      (std::cout << "[" << rank() << "] " << s << std::endl).flush();
    }

    void progress(const std::string& s)
    {
      (std::cout << "[" << rank() << "] " << s << "\r").flush();
    }

    void debug(const std::string& s)
    {
      if (true)
        log(s);
    }
  };

  template<typename T>
  void MPICC_Window::put(int r, T v)
  {
    int err;

    MPI_Datatype t = translateMPIType<T>();
    lock();
    err = MPI_Put(&v, 1, t,
                       rank,
                       r, 1, t, win);
    unlock();
    if (err != MPI_SUCCESS)
      throw MPI_Exception(err);
  }


  template<typename T>
  T MPICC_Window::get(int r)
  {
    int err;
    T v;

    v = 0;

    MPI_Datatype t = translateMPIType<T>();
    lock();
    err = MPI_Get(&v, 1, t,
                        rank,
                       r, 1, t, win);
    unlock();
    if (err != MPI_SUCCESS)
      {
        throw MPI_Exception(err);
      }

    return v;
  }
  
  static inline MPI_Communication *setupMPI(int& argc, char **& argv)
  {
    ::MPI_Init(&argc, &argv);
    return new MPI_Communication(MPI_COMM_WORLD);
  }
  
  static inline void doneMPI()
  {
    ::MPI_Finalize();
  }


};

#endif
