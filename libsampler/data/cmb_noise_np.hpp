/*+
This is ABYSS (./libsampler/data/cmb_noise_np.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __CMB_NOISE_NP_HPP
#define __CMB_NOISE_NP_HPP

#include <cassert>
#include <alm_healpix_tools.h>
#include <arr.h>
#include "cmb_defs.hpp"
#include "extra_alm_tools.hpp"

namespace CMB
{

  class CMB_NP_NoiseMap: public Noise_Map
  {
  public:
    double alpha;
    long masked;
    double min_noise;
    CMB_Map modulated_map;
    PowerSpec Pnoise, inv_Pnoise, inv_sqPnoise, sqPnoise;
    CMB_Data& data;
    double mean_noise;

    CMB_NP_NoiseMap(CMB_Data& cmb_data)
      : alpha(1.0), masked(0), data(cmb_data) {}

    void setModulationMap(const CMB_Map& modmap)
    {
      DataType *m = &modulated_map[0];

      modulated_map.SetNside(modmap.Nside(), RING);
      
#pragma omp parallel for schedule(static)
      for (long i = 0; i < modmap.Npix(); i++)
        {
          modulated_map[i] = std::sqrt(modmap[i]);
        }
    }

    static double regulated_inv(double a)
    {
      return (a > 0) ? (1/a) : 0;
    }

    void setColoring(const PowerSpec& P)
    {
      inv_Pnoise.alloc(P.size());
      Pnoise = P;
      inv_sqPnoise.alloc(P.size());      
      sqPnoise.alloc(P.size());      
      for (int i = 0; i < P.size(); i++)
        {
          inv_Pnoise[i] = regulated_inv(Pnoise[i]);
          inv_sqPnoise[i] = (inv_Pnoise[i]*inv_Pnoise[i]);
          sqPnoise[i] = Pnoise[i]*Pnoise[i];
        }
    }
    
    virtual void apply_mask(CMB_Map& map) const
    {
      planck_assert(map.Nside() == modulated_map.Nside(), "Noise and map are not compatible");
      long n = map.Npix();

      for (long i = 0; i < n; i++)
        if (modulated_map[i]==0)
          map[i] = 0;
    }

    virtual void apply_inv(CMB_Map& map) const
    {
      planck_assert(map.Nside() == modulated_map.Nside(), "Noise and map are not compatible");

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha*alpha);
      ALM_Map alms(data.Lmax, data.Lmax);

      map *= modulated_map;
      HEALPIX_method(map, alms, data.weight);
      alms.ScaleL(inv_sqPnoise);
      alms.Scale(beta);
      alm2map(alms, map);
      map *= modulated_map;
    }

    virtual void apply_inv_half(CMB_Map& map) const
    {
      planck_assert(map.Nside() == modulated_map.Nside(), "Noise and map are not compatible");

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha);
      ALM_Map alms(data.Lmax, data.Lmax);

      map *= modulated_map;
      HEALPIX_method(map, alms, data.weight);
      // !!!!! THIS IS INEFFICIENT as we are probably going to take a map2alm just after
      // The solution is unfortunately non-trivial. Leave it like that at the moment as
      // the messenger is going to go deeply in the description anyway.
      alms.ScaleL(inv_Pnoise);
      alms.Scale(beta * n / (4*M_PI));
      alm2map(alms, map); 
    }


    virtual void apply_fwd(CMB_Map& map) const
    {
      planck_assert(map.Nside() == modulated_map.Nside(), "Noise and map are not compatible");

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha*alpha);
      ALM_Map alms(data.Lmax, data.Lmax);

#pragma omp parallel for schedule(static)
      for (long i = 0; i < map.Npix(); i++)
        map[i] *= regulated_inv(modulated_map[i]);
      HEALPIX_method(map, alms, data.weight);
      alms.ScaleL(sqPnoise);
      alms.Scale(beta);
      alm2map(alms, map);
#pragma omp parallel for schedule(static)
      for (long i = 0; i < map.Npix(); i++)
        map[i] *= regulated_inv(modulated_map[i]);
    }
    
    virtual void apply_fwd_half(CMB_Map& map) const
    {
      planck_assert(map.Nside() == modulated_map.Nside(), "Noise and map are not compatible");

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha*alpha);
      ALM_Map alms(data.Lmax, data.Lmax);

#pragma omp parallel for schedule(static)
      for (long i = 0; i < map.Npix(); i++)
        map[i] *= regulated_inv(modulated_map[i]);
      HEALPIX_method(map, alms, data.weight);

      // !!!!! THIS IS INEFFICIENT as we are probably going to take a map2alm just after
      // The solution is unfortunately non-trivial. Leave it like that at the moment as
      // the messenger is going to go deeply in the description anyway.
      alms.ScaleL(Pnoise);
      alms.Scale(beta * n/(4*M_PI));
      alm2map(alms, map);
    }


    virtual void SetBaseScaling(double scaling)
    {
      for (long i = 0; i < modulated_map.Npix(); i++)
        modulated_map[i] *= scaling;
    }

    virtual void SetFlexScaling(double scaling)
    {
      alpha = scaling;
    }
    
    virtual double dof() const
    {
      return data.Lmax;
    }
    
    virtual double chi2(const CMB_Map& m) const
    {
      CMB_Map tmp_m;
      long cur_len = m.Npix();
      ALM_Map tmp_alms(data.Lmax, data.Lmax);

      tmp_m.SetNside(data.Nside, RING);
      assert(m.Nside()== data.Nside);
      // This is a large sum. Try to preserve as much precision as possible (need memory for that).
#pragma omp parallel for schedule(static)
      for (long p = 0; p < modulated_map.Npix(); p++)
        {
          tmp_m[p] = m[p]*regulated_inv(modulated_map[p]);
        }
      HEALPIX_method(tmp_m, tmp_alms, data.weight);
      tmp_alms.ScaleL(inv_Pnoise);

      return (4*M_PI)/tmp_m.Npix()*dot_product_simple(tmp_alms, tmp_alms).real();
    }

    virtual double getMeanNoisePerPix() const
    {
      return mean_noise;
    }

  };


};
#endif
