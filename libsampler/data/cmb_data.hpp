/*+
This is ABYSS (./libsampler/data/cmb_data.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __SAMPLER_CMB_DATA_HPP
#define __SAMPLER_CMB_DATA_HPP

#include "cmb_defs.hpp"

namespace CMB
{
  class GenericAlmPreconditioner;
  class WienerFilter;

  template<typename T> class CMB_Transform;
  
  typedef GenericAlmPreconditioner CMB_Preconditioner;

  class CMB_Data
  {
  public:
    int numChannels;
    long Lmax;
    long Nside, NsideHR;

    // The temperature data
    CMB_ChannelData channels;
    // The noise term
    CMB_NoiseChannelData noise;
    // Mask information. Separated from the noise.
    CMB_ChannelData masks;
    // Maybe the polarisation later
    CMB_ChannelData residuals;
    // The beam model
    arr<BeamPower> beam;

    arr<double> weight, pixwin;
    arr<double> guess_sqrt_cls, guess_cls;

    CMB_Preconditioner *preconditioner;
    WienerFilter *wiener_operator;
    CMB_Transform<DataType> *transform;

    RelaxInfo rinfo;
    
    CMB_Data() {} 
    
    ~CMB_Data()
    {
      for (int ch = 0; ch < numChannels; ch++)
        delete noise[ch];
    }
  }; 

  // This provides static indexes in the sampler arrays
  // for fast access. With a hash table we would not need that
  // but we would have to do an extra lookup.
  static const int SAMPLER_NOISE = 0;
  static const int SAMPLER_CLS = 1;
  static const int SAMPLER_CMB = 2;
  static const int SAMPLER_DIPOLE = 3;
  static const int SAMPLER_DMCMB = 4;
  static const int SAMPLER_ABERRATION = 5;
  static const int SAMPLER_HMC_CMB = 6;
  static const int NUM_SAMPLERS = 7;


  static const int SAMPLER_SIGNAL_DEFAULT = 0;
  static const int SAMPLER_SIGNAL_NOISE = 1;
  static const int SAMPLER_SIGNAL_WIENER_CMB = 1;
  static const int SAMPLER_SIGNAL_PROPOSED_CMB = 2;
  static const int SAMPLER_SIGNAL_DIPOLE = 3;
  static const int SAMPLER_SIGNAL_LENSING = 4;
  static const int SAMPLER_SIGNAL_MODULATION = 5;
  static const int SAMPLER_SIGNAL_LAST_SIGNAL = 6;
};

#endif
