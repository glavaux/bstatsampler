/*+
This is ABYSS (./libsampler/data/keyword_tools.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __CMB_KEYWORD_TOOLS_HPP
#define __CMB_KEYWORD_TOOLS_HPP

#include <map>
#include <string>
#include <iostream>
#include <typeinfo>
#include <string>
#include <arr.h>
#include <list>
#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

namespace CMB
{

  typedef std::map<std::string,boost::any> KeywordMap;


  template<typename T>
  T extract_keyword(const KeywordMap& kwmap, const std::string& kwname, T default_value)
  {
    KeywordMap::const_iterator iter = kwmap.find(kwname);

    if (iter == kwmap.end())
      return default_value;

    try {
      return boost::lexical_cast<T>(boost::any_cast<std::string>(iter->second));
    } catch (const boost::bad_any_cast& c) {
      std::cerr << boost::format("Error while trying to convert the value of the keyword %s to %s (requested = %s, stored = %s)") % kwname % typeid(T).name() % c.what()  % iter->second.type().name() << std::endl;
    }
  }
  
  template<typename T>
  std::list<T> extract_keyword_list(const KeywordMap& kwmap, const std::string& kwname)
  {
    KeywordMap::const_iterator iter = kwmap.find(kwname);

    if (iter == kwmap.end())
      return std::list<T>();
    
    try {
        std::list<std::string> l = boost::any_cast<std::list<std::string> >(iter->second);
        std::list<T> l_out;

        std::transform(l.begin(), l.end(), std::back_inserter(l_out), boost::lexical_cast<T,std::string>);
        return l_out;
    } catch (const boost::bad_any_cast& c) {
      std::cerr << boost::format("Error while trying to convert the value of the keyword %s to %s (requested = %s, stored = %s)") % kwname % typeid(T).name() % c.what()  % iter->second.type().name() << std::endl;
    }
    
    return std::list<T>();  
  }
  
  template<typename T>
  arr<T> convert_list_to_arr(const std::list<T>& l)
  {
    arr<T> a(l.size());
    
    std::copy(l.begin(), l.end(), &a[0]);
    return a;
  }
};


#endif
