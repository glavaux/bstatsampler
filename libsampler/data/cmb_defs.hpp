/*+
This is ABYSS (./libsampler/data/cmb_defs.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_DEFS_HPP
#define __CMB_DEFS_HPP

#include <boost/function.hpp>
#include <cassert>
#include <vector>
#include <healpix_map.h>
#include <xcomplex.h>
#include <arr.h>
#include <alm.h>
#include "clean_sum.hpp"

#ifdef MPI_ENABLED
#define MPI_NAME(a) MPI_ ## a
#else
#define MPI_NAME(a) a
#endif

namespace CMB
{
  struct CMB_Data;
  typedef double DataType;
  typedef Healpix_Map<DataType> CMB_Map;
  typedef Alm< xcomplex<DataType> > ALM_Map;
  typedef arr<double> BeamPower;
  typedef arr<double> PowerSpec;
  typedef arr<bool> PowerSpecMask;
  typedef Healpix_Map<bool> MapMask;
  typedef arr<CMB_Map> CMB_ChannelData;
  // This is the prototype for the wiener operator.
  // It takes:
  //   * the data descriptor (CMB_Data)
  //   * the Cls (PowerSpec)
  //   * the input Alms to filter
  //   * the resulting Alms after filtration

  struct RelaxInfo
  {
    double epsilonCG;
    double relaxation_start;
    double relaxation_reduction;
  };

  class Noise_Map
  {
  public:
    Noise_Map() {} 

    virtual void SetBaseScaling(double scaling) = 0;
    virtual void SetFlexScaling(double scaling) = 0;
    virtual void apply_mask(CMB_Map& map) const = 0;
    virtual void apply_inv(CMB_Map& map) const = 0;
    virtual void apply_inv_half(CMB_Map& map) const = 0;
    virtual void apply_fwd(CMB_Map& map) const = 0;
    virtual void apply_fwd_half(CMB_Map& map) const = 0;
    virtual double chi2(const CMB_Map& m) const = 0;
    virtual double dof() const = 0;
    virtual double getMeanNoisePerPix() const = 0; 

    template<typename T>
    const T *as() const
    {
      const T *nm = dynamic_cast<const T *>(this);
      if (nm == 0)
        {
          std::cerr << "Invalid type conversion " << std::endl;
          abort();
        }
      return nm;
    }

    template<typename T>
    T *as()
    {
      T *nm = dynamic_cast<T *>(this);
      assert(nm != 0);
      return nm;
    }

    Noise_Map *reduce(long targetNside) const
    {
      abort();
    }
  };

  typedef arr<Noise_Map *> CMB_NoiseChannelData;
};

#define HEALPIX_method(map,alm,weight) map2alm_iter(map, alm, 0, weight)


#include "cmb_noise_nn.hpp"

#endif
