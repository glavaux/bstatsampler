/*+
This is ABYSS (./libsampler/data/cmb_noise_nn.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_NOISE_NN_HPP
#define __CMB_NOISE_NN_HPP

#include <cmath>

namespace CMB
{

  class CMB_NN_NoiseMap: public CMB_Map, public Noise_Map
  {
  public:
    double alpha;
    long masked;
    double min_noise;
    double mean_noise;

    CMB_NN_NoiseMap()
      : Healpix_Map<DataType> (), alpha(1.0), masked(0), mean_noise(0) {}

    CMB_NN_NoiseMap (int nside, Healpix_Ordering_Scheme scheme, const nside_dummy)
      : Healpix_Map<DataType> (nside, scheme, SET_NSIDE),alpha(1.0), masked(0), mean_noise(0)
    {
    }
    
    virtual void apply_mask(CMB_Map& map) const
    {
      planck_assert(map.Nside() == Nside(), "Noise and map are not compatible");
      const arr<DataType> &amap = Map();
      long n = map.Npix();

      for (long i = 0; i < n; i++)
        {	 
          if (amap[i] == 0)
            map[i] = 0;
        }
    }

    virtual void apply_inv_half(CMB_Map& map) const
    {
      planck_assert(map.Nside() == Nside(), "Noise and map are not compatible");
      const arr<DataType> &amap = Map();

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha);
      for (long i = 0; i < n; i++)
        {	 
           if (amap[i] > 0)
             map[i] *= beta*std::sqrt(amap[i]);
           else
             map[i] = 0;
        }
    }

    virtual void apply_inv(CMB_Map& map) const
    {
      planck_assert(map.Nside() == Nside(), "Noise and map are not compatible");
      const arr<DataType> &amap = Map();

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = 1/(alpha*alpha);
      for (long i = 0; i < n; i++)
        {	 
           if (amap[i] > 0)
             map[i] *= beta*amap[i];
           else
             map[i] = 0;
        }
    }
    
    virtual void apply_fwd(CMB_Map& map) const
    {
      planck_assert(map.Nside() == Nside(), "Noise and map are not compatible");
      const arr<DataType> &amap = Map();

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = (alpha*alpha);
      for (long i = 0; i < n; i++)
        {
          if (amap[i] > 0)
            map[i] /= beta*amap[i];
          else
            map[i] = 0;
        }
    }

    virtual void apply_fwd_half(CMB_Map& map) const
    {
      planck_assert(map.Nside() == Nside(), "Noise and map are not compatible");
      const arr<DataType> &amap = Map();

      // amap stores the inverse variance. Thus the -power. Be sure that power is always negative.
      long n = map.Npix();
      double beta = (alpha);
      for (long i = 0; i < n; i++)
        {
          if (amap[i] > 0)
            map[i] /= beta*std::sqrt(amap[i]);
          else
            map[i] = 0;
        }
    }


    virtual void SetBaseScaling(double scaling)
    {
      for (long i = 0; i < Npix(); i++)
        operator[](i) *= scaling;
    }

    virtual void SetFlexScaling(double scaling)
    {
      alpha = scaling;
    }
    
    virtual double dof() const
    {
      return Npix() - masked;
    }
    
    virtual double chi2(const CMB_Map& m) const
    {
      arr<DataType> tmp_m(m.Npix());
      long cur_len = m.Npix();

      // This is a large sum. Try to preserve as much precision as possible (need memory for that).
#pragma omp parallel for schedule(static)
      for (long p = 0; p < Npix(); p++)
        {
          tmp_m[p] = m[p]*m[p]*Map()[p];
        }

      clean_sum(tmp_m, cur_len);
      return tmp_m[0];
    }
    
    virtual double getMeanNoisePerPix() const
    {
      return mean_noise;
    }

  };


};
#endif
