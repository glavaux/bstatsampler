/*+
This is ABYSS (./libsampler/samplers/cmb_sampler.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_GIBBS_SAMPLER_HPP
#define __CMB_GIBBS_SAMPLER_HPP

#include "sampler.hpp"
#include "cmb_defs.hpp"
#include "noise_weighing.hpp"
#include "cmb_data.hpp"

namespace CMB 
{
  // This is the sampler of the CMB signal
  // It takes Cls, generates some random a_lm and
  // produce a corresponding
  // CMB sky map.
  class CMB_Sampler: public SingleSampler, public MapperSampler
  {
  public:
    CMB_Sampler(gsl_rng *r, CMB_Data& data, 
		long nL, long nSide);
    virtual ~CMB_Sampler();

    void set_CG_precision(double prec) { 
      epsilon_CG = prec;
    }
 
    virtual void saveState(NcFile& f);
    virtual void restoreState(NcFile& f) throw(StateInvalid);

    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers);
    
    virtual const CMB_Map *getEstimatedMap(int ch, int signal) const
    {
      if (signal == SAMPLER_SIGNAL_PROPOSED_CMB || signal == SAMPLER_SIGNAL_DEFAULT)
	return &proposed_map;
      if (signal != SAMPLER_SIGNAL_WIENER_CMB)
	return &wiener_map;

      return 0;
    }

    virtual const ALM_Map *getMapperCurrentAlms() const
    {
      return &alms_noise_cmb;
    }

    virtual const std::vector<int>& getModeledSignals() const;
    

    virtual const CMB_Map *getWienerFilter() const
    {
      return &map;
    }

    const ALM_Map& getAlms(ALM_Map& alm) const
    {
      return alms_noise_cmb;
    }

    void setInitialProposedMap(const CMB_Map& map)
    {
      proposed_map = map;
    }

    void prepareWeighedMap(CMB_Data& data, 
			   ALM_Map& alm_out,
			   const arr<DataType>& sqcls);

    const ALM_Map& prepareInputMap(CMB_Data *data, const arr<double>& sqcls, bool wiener, double alpha_mul);

  protected:
    ALM_Map tmp_alms, alms_noise_cmb, alms_w;
    double alpha_N;
    arr<CMB_Map> noise1;
    CMB_Map map, proposed_map, wiener_map, noise2;
    double epsilon_CG;
  };

};

#endif
