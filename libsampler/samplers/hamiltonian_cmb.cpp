/*+
This is ABYSS (./libsampler/samplers/hamiltonian_cmb.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <assert.h>
#include <string>
#include <cmath>
#include <vector>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <boost/format.hpp>
#include <arr.h>
#include <iostream>
#include <fstream>
#include <alm.h>
#include <alm_fitsio.h>
#include "cl_sampler.hpp"
#include "extra_alm_tools.hpp"
#include "extra_map_tools.hpp"
#include "hamiltonian_cmb.hpp"

using namespace std;
using namespace CMB;
using boost::format;
using boost::str;

HMC_CMB_Sampler::HMC_CMB_Sampler(gsl_rng *r, const std::string& name, CMB_Data& data,
                                 long nL, long nSide, int signal)
  : SingleSampler(r, name), lmax(nL), proposed_map(nSide, RING, SET_NSIDE), current_alms(nL, nL), tune_in(0.7), mass_proxy(nL, nL), output_reconstructed_signal(signal)
{
  this->epsilonmax = epsilonmax; //0.001;
  this->nmax = nmax;
  this->frozen_tune = false;

  preconditioner = dynamic_cast<MapSignalNoiseSignal_weighing_preconditioner<-1,1> *>(data.preconditioner);
  if (preconditioner == 0) // cast failure
    {
      cout << "Only diagonal preconditioner is supported by HMC_CMB sampler" << endl;
      abort();
    }

  clear(current_alms);

  // Process all channels,  coef is the sum of
  // all "pre"preconditioner.
  mass_proxy.SetToZero();
  for (int channel = 0; channel < data.numChannels; channel++)
    {
      long Nalm = preconditioner->preconditioner[channel].Alms().size();
      xcomplex<double> *pAlms = preconditioner->preconditioner[channel].mstart(0);
      double *malms = mass_proxy.mstart(0);

      for (long q = 0;q < Nalm; q++)
        malms[q] += pAlms[q].real();
    }
}

HMC_CMB_Sampler::~HMC_CMB_Sampler()
{
}

template<typename T>
void packAlms(T *outbuf, const Alm<xcomplex<T> >& alms)
{
  int k = 0;
  for (long m = 0; m <= alms.Mmax(); ++m)
    for (long l = m; l <= alms.Lmax(); ++l) {
       outbuf[k] = alms(l,m).re;
       outbuf[k+1] = alms(l,m).im;
       k+=2;
    }
  assert(k ==  2*Alm<xcomplex<T> >::Num_Alms(alms.Lmax(), alms.Mmax()));
}

template<typename T>
void unpackAlms(const T *inbuf, Alm<xcomplex<T> >& alms)
{
  int k = 0;
  for (long m = 0; m <= alms.Mmax(); ++m)
    for (long l = m; l <= alms.Lmax(); ++l) {
       alms(l,m) = xcomplex<T>(inbuf[k], inbuf[k+1]);
       k+=2;
    }
  assert(k ==  2*Alm<xcomplex<T> >::Num_Alms(alms.Lmax(), alms.Mmax()));
}


void HMC_CMB_Sampler::saveState(NcFile& f)
{
  NcDim cmb_dim = f.addDim("hmc_cmb_sampler_cmb_dim", 2*current_alms.Alms().size());
  NcVar cmb_clean = f.addVar("hmc_cmb_sampler_cmb_clean", ncDouble, cmb_dim);

  arr<double> out_alms(2*current_alms.Alms().size());
  packAlms(&out_alms[0], current_alms);
  cmb_clean.putVar(&out_alms[0]);
}

void HMC_CMB_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  NcVar cmb_clean = f.getVar("hmc_cmb_sampler_cmb_clean");
  std::vector<NcDim> dims = cmb_clean.getDims();

  if (dims[0].getSize() != 2*current_alms.Alms().size())
    throw StateInvalid("CLS");

  arr<double> in_alms(2*current_alms.Alms().size());
  cmb_clean.getVar(&in_alms[0]);
  unpackAlms(&in_alms[0], current_alms);
}

template<typename T>
xcomplex<T> invc_adapt(const std::complex<T>& c)
{
  return xcomplex<T>(c);
}

void HMC_CMB_Sampler::generateMomenta(ALM_Map& momentum, const arr<double>& sqcls, const Alm<double>& mass)
{
  double sq2 = sqrt(0.5);
  ALM_Map ran_numbers(momentum.Lmax(), momentum.Mmax());
  
  for (long m = 0; m <= lmax; m++)
    {
      for (long l = m; l <= lmax; l++)
        {
          if (m == 0) {
            ran_numbers(l,0) = xcomplex<DataType>(gsl_ran_gaussian(rGen, 1.), 0);
          } else {
            ran_numbers(l,m) = xcomplex<DataType>(gsl_ran_gaussian(rGen, sq2),gsl_ran_gaussian(rGen, sq2));
          }
        }
    }

  cout << "Random numbers = " << (dot_product_simple(ran_numbers,ran_numbers).real()/ran_numbers.Alms().size()) << endl;

  long lmaxPre = preconditioner->lmaxPre;
  Alm<xcomplex<double> > debug_mass(lmax, lmax);
  if (lmaxPre > 0)
    {
      long NumPreElems = preconditioner->NumPreElems;
      Alm_Base alm_accessor(lmaxPre, lmaxPre);    
      for (long m1 = 0; m1 <= lmaxPre; m1++)
        {
          for (long l1 = m1; l1 <= lmaxPre; l1++)
            {
              long index1 = alm_accessor.index(l1, m1);

              momentum(l1, m1) = 0;
              if (sqcls[l1] <= 0)
                continue;
	  
              for (long m2 = 0; m2 <= lmaxPre; m2++)
                {
                  for (long l2 = m2; l2 <= lmaxPre; l2++)
                    {
                      long index2 = alm_accessor.index(l2, m2);
                      if (index2 > index1 || sqcls[l2] <= 0) // Use only the Lower part
	                      continue;

                      momentum(l1, m1) += invc_adapt(preconditioner->pre_cholesky(index2, index1))/(sqcls[l1]) * ran_numbers(l2, m2);
                    }
               }
              debug_mass(l1,m1) = preconditioner->pre_cholesky(index1, index1).real();
              debug_mass(l1,m1).real() *= debug_mass(l1,m1).real();
          }
       }
    }

  lmaxPre = max(lmaxPre, -1L);

  
  for (long m = 0; m <= lmax; m++)
    {
      for (long l = m; l < std::max(lmaxPre+1,m); l++)
        {
          if (sqcls[l]>0) {
            debug_mass(l,m) = debug_mass(l,m)/(sqcls[l]*sqcls[l]);
          }
        }

      for (long l = std::max(lmaxPre+1,m); l <= lmax; l++)
        {
          momentum(l,m) = sqrt(mass(l,m)) * ran_numbers(l,m);
          debug_mass(l,m) = (mass(l,m));
        }
    }
    
    
  ALM_Map tmp_momentum = momentum;
  tmp_momentum.ScaleL(sqcls);
  tmp_momentum = preconditioner->apply(tmp_momentum);
  tmp_momentum.ScaleL(sqcls);
  
  cout << "Momentum chi2 = " << (dot_product_simple(momentum,tmp_momentum).real()/momentum.Alms().size()) << endl;
  
  write_Alm_to_fits("!masses.fits", debug_mass, debug_mass.Lmax(), debug_mass.Mmax(), planckType<double>());
}

void HMC_CMB_Sampler::computeNewSample(CMB_Data& data,
                                       const AllSamplers& samplers)
{
  arr<DataType> sqcls = data.guess_sqrt_cls;
//    dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getSqrtCls();
  arr<DataType> cls = data.guess_cls;
//    dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getCls();
  ALM_Map momentum(lmax, lmax), coef(lmax, lmax);
  int n = 20+int(ceil((nmax-20)*gsl_rng_uniform(rGen)));
  double epsilon = gsl_rng_uniform(rGen)*epsilonmax;
  double full_norm = data.channels[0].Npix()/(4*M_PI);
  ALM_Map backup_alms = current_alms, backup_momentum;
  Alm<double> mass(lmax,lmax);

  preconditioner->SetSqCls(sqcls);
  preconditioner->chooseChannel(-1);

  mass.SetToZero();
  for (long l = 0; l <= lmax; l++)
     for (long m = 0; m <= l; m++) {
        if (cls[l]>0)
        {
          mass(l,m) = (1/cls[l] + mass_proxy(l,m)*full_norm);
          assert(mass(l,m)>0);
        }
     }

  assert(!isnan(epsilon));
  momentum.Set(lmax, lmax);

  // Random momentum generated.
  generateMomenta(momentum, sqcls, mass);

  backup_momentum = momentum;

  cout << format("[HMC] Integrating equation of motions (N=%d, epsilon=%g)") % n % epsilon << endl;
  cout << "[HMC] Shifting momentum" << endl;

  // Advance by epsilon/2
  momentum += 0.5*epsilon*computeForce(data, cls);

  for (int i = 0; i < n; i++)
    {
      (cout << format("[HMC] step % 3d / % 3d\r") % (i+1) % n).flush();

      long Nalms = current_alms.Alms().size();
      ALM_Map delta_momentum = momentum;
      delta_momentum.ScaleL(sqcls);
      delta_momentum = epsilon*preconditioner->apply(delta_momentum);
      delta_momentum.ScaleL(sqcls);
      
      current_alms += delta_momentum;

      double a = (i==(n-1)) ? 0.5 : 1;

      momentum += (a*epsilon)*computeForce(data, cls);

    }

  double delta_chi2 = compute_delta_chi2(data, sqcls, backup_alms, current_alms, backup_momentum, momentum, mass);

  double proba = min(double(1), exp(-0.5*delta_chi2));
  bool accepted;

  if (gsl_rng_uniform(rGen) > proba)
    {
      // REJECT
      accepted = false;
      current_alms = backup_alms;
      if (!frozen_tune && tune_in > 0 && proba < 0.7)
        epsilonmax *= tune_in;
    }
  else
    { // ACCEPT
      accepted = true;
      if (!frozen_tune && delta_chi2 < 0)
        epsilonmax /= tune_in;
    }
  epsilonmax = min(epsilonmax, double(2));

  {
    ofstream f("debug/hmc_cmb.txt",ios::app);
    f << format("%15lg %lg %lg %d %lg %d") % delta_chi2 % proba % epsilon % n % epsilonmax % accepted << endl;
  }

  {
     static int Nout = 0;
     fitshandle h;
     string fname = str(format("!debug/hmc_cmb_alm_%d.fits") % Nout);
     h.create(fname);
     write_Alm_to_fits(h, current_alms, lmax, lmax, planckType<double>());
     Nout++;
  }
  
  reconstructed_map.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ALM_Map alms = current_alms;
      
      reconstructed_map[ch].SetNside(data.Nside, RING);
      alms.ScaleL(data.beam[ch]);
      alm2map(alms, reconstructed_map[ch]);
    }
}

static double computeChi2(CMB_Data& data, const arr<DataType>& cls, const Alm<double>& mass, const ALM_Map& a1, const ALM_Map& m1)
{
  double chi2 = 0;
  CMB_Map amap(data.Nside, RING, SET_NSIDE);

  for (int ch = 0; ch < data.numChannels; ch++)
    {
        ALM_Map ab = a1;

        ab.ScaleL(data.beam[ch]);
        alm2map(ab, amap);
        amap -= data.residuals[ch];

        CMB_Map tmp_map = amap;
        data.noise[ch]->apply_inv(tmp_map);
        chi2 += dot_product(tmp_map,amap).real();
     }

  ALM_Map ab = a1;

  for (long m = 0; m <= ab.Mmax(); m++)
     for (long l = std::max(2L,m); l <= ab.Lmax(); l++)
       ab(l,m) /= cls[l];

  ab(0,0) = ab(1,0) = ab(1,1) = 0;

  chi2 += dot_product(ab, a1).real();

  ab = m1;
  for (long m = 0; m <= ab.Mmax(); m++)
     for (long l = m; l <= ab.Lmax(); l++)
      if (mass(l,m)>0)
        ab(l,m) /= mass(l,m);
  chi2 += dot_product(ab, m1).real();

  return chi2;
}


double HMC_CMB_Sampler::compute_delta_chi2(CMB_Data& data, const arr<DataType>& sqcls, const ALM_Map& a1, const ALM_Map& a2, const ALM_Map& m1, const ALM_Map& m2, const Alm<double>& mass)
{
   double chi2_a, chi2_b, chi2 = 0;
   CMB_Map delta_map(data.Nside, RING, SET_NSIDE), sum_map(data.Nside, RING, SET_NSIDE);


   for (int ch = 0; ch < data.numChannels; ch++)
     {
        double chi2_epsilon;

        ALM_Map delta = a2 - a1;
        ALM_Map sum = a1 + a2;

#pragma omp task
        {
          delta.ScaleL(data.beam[ch]);
        }
#pragma omp task
        {
          sum.ScaleL(data.beam[ch]);
        }

#pragma omp taskwait
        alm2map(delta, delta_map);
        data.noise[ch]->apply_inv(delta_map);
        chi2_epsilon = -2*dot_product(delta_map, data.residuals[ch]).real();
        assert(!isnan(chi2_epsilon));
        assert(!isinf(chi2_epsilon));

        alm2map(sum, sum_map);
        chi2_epsilon += dot_product(delta_map, sum_map).real();
        assert(!isinf(chi2_epsilon));
        assert(!isnan(chi2_epsilon));

        chi2 += chi2_epsilon;
        assert(!isinf(chi2));
        assert(!isnan(chi2));
     }

   ALM_Map delta;

   assert(!isnan(chi2));
   delta = m2-m1;
   delta.ScaleL(sqcls);
   delta = preconditioner->apply(delta);
   delta.ScaleL(sqcls);

   chi2 += dot_product_simple(delta, m1+m2).real();
   assert(!isnan(chi2));
   assert(!isinf(chi2));

   delta = a2-a1;
   for (long m = 0; m <= delta.Mmax(); m++)
     for (long l = std::max(2L,m); l <= delta.Lmax(); l++)
      delta(l,m) /= sqcls[l]*sqcls[l];
   delta(0,0) = delta(1,0) = delta(1,1) = 0;

   chi2 += dot_product_simple(delta, a1+a2).real();
   assert(!isnan(chi2));
   assert(!isinf(chi2));
   return chi2;
}

ALM_Map HMC_CMB_Sampler::computeForce(CMB_Data& data, const arr<DataType>& cls)
{
  ALM_Map force(lmax, lmax);

//#pragma omp parallel for schedule(static)
  for (int m = 0; m <= force.Lmax(); m++)
    {
      for (int l = m; l <= force.Lmax(); l++)
        {
          if (cls[l] != 0)
            force(l,m) = -current_alms(l,m)/(cls[l]);
          else
            force(l,m) = 0;
          assert(!isinf(force(l,m).real()));
          assert(!isinf(force(l,m).imag()));
          assert(!isnan(force(l,m).real()));
          assert(!isnan(force(l,m).imag()));
        }
    }

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ALM_Map beamed_alms = current_alms;
      CMB_Map m(data.Nside, RING, SET_NSIDE);

      beamed_alms.ScaleL(data.beam[ch]);
      alm2map(beamed_alms, m);

      CMB_Map residual = data.residuals[ch] - m;
      data.noise[ch]->apply_inv(residual);
      HEALPIX_method(residual, beamed_alms, data.weight);
      beamed_alms *= residual.Npix()/(4*M_PI);
      beamed_alms.ScaleL(data.beam[ch]);
      force += beamed_alms;


  for (int m = 0; m <= force.Lmax(); m++)
    {
      for (int l = m; l <= force.Lmax(); l++)
        {
          assert(!isinf(force(l,m).real()));
          assert(!isinf(force(l,m).imag()));
          assert(!isnan(force(l,m).real()));
          assert(!isnan(force(l,m).imag()));
        }
        }
    }
  force(0,0) = force(1,0) = force(1,1) = 0;

  return force;
}


static std::vector<int> cmb_signals;

const std::vector<int>& HMC_CMB_Sampler::getModeledSignals() const
{
  if (cmb_signals.size()==0)
    {
      cmb_signals.push_back(SAMPLER_SIGNAL_PROPOSED_CMB);
    }

  return cmb_signals;
}
