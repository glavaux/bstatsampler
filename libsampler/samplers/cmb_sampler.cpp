/*+
This is ABYSS (./libsampler/samplers/cmb_sampler.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <fstream>
#include <iostream>
#include <cmath>
#include <cassert>
#include <boost/bind.hpp>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "cmb_sampler.hpp"
#include "cl_sampler.hpp"
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include "extra_alm_tools.hpp"
#include "conjugateGradient.hpp"
#include <powspec.h>
#include <alm_powspec_tools.h>
#include "noise_weighing.hpp"
#include "cmb_data.hpp"
#include "extra_map_tools.hpp"
#include "netcdf_adapt.hpp"
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include "cg_relax.hpp"
#include "wiener.hpp"

//#define INTEGRITY_CHECK
//#define WIENER_TEST

static const bool SHOW_WIENER = true;

using namespace std;
using namespace CMB;

static vector<int> cmb_signals;

  // This is the sampler of the CMB signal
  // It takes Cls, generates some random a_lm and
  // produce a corresponding
  // CMB sky map.
CMB_Sampler::CMB_Sampler(gsl_rng *r, CMB_Data& data,
			 long nL, long nSide) 
  : SingleSampler(r), 
    map(nSide, RING, SET_NSIDE),
    noise1(data.numChannels),
    noise2(nSide, RING, SET_NSIDE),
    proposed_map(nSide, RING, SET_NSIDE),
    tmp_alms(nL, nL), alms_w(nL, nL), alms_noise_cmb(nL, nL)
{
  alpha_N = sqrt(map.Npix()/(4*M_PI));
  for (int ch = 0; ch < data.numChannels; ch++)
    noise1[ch].SetNside(nSide, RING);
}

CMB_Sampler::~CMB_Sampler()
{
}


void CMB_Sampler::saveState(NcFile& f)
{
  NcDim cmb_dim = f.addDim("cmb_sampler_cmb_dim", proposed_map.Npix());
  NcVar cmb_clean = f.addVar("cmb_sampler_cmb_clean", ncDouble, cmb_dim);

  cmb_clean.putVar(&proposed_map[0]);  
}

void CMB_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  NcVar cmb_clean = f.getVar("cmb_sampler_cmb_clean");
  std::vector<NcDim> dims = cmb_clean.getDims();

  if (dims[0].getSize() != proposed_map.Npix())
    throw StateInvalid("CLS");

  cmb_clean.getVar(&proposed_map[0]);
}

void CMB_Sampler::computeNewSample(CMB_Data& data,
				   const AllSamplers& samplers)
{
  // We do not use the observational data here.
  arr<DataType> sqcls = data.guess_sqrt_cls;
    //dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getSqrtCls();
  arr<DataType> cls = data.guess_cls;
    //dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getCls();
  arr<DataType> inv_sqcls(sqcls.size());

//  sqcls[0] = sqcls[1] = 0;
//  cls[0] = cls[1] = 0;

  for (int l = 0; l < sqcls.size(); l++)
    inv_sqcls[l] = (sqcls[l] > 0) ? (1/sqcls[l]) : 0;  

  cout << "In CMB_Sampler" << endl;

  int lmax = tmp_alms.Lmax();
  int mmax = tmp_alms.Mmax();

  assert(sqcls.size() >= lmax);

  cout << "Computing mean map..." << endl;

  // We first need to compute the mean map.

  arr<bool> all_active(data.numChannels);
  all_active.fill(true);
  
  data.wiener_operator->setCls(cls);
  data.wiener_operator->set_save_restart_directory("debug/cmbwiener");
  data.wiener_operator->setActiveChannels(all_active);

  if (SHOW_WIENER) {
    data.wiener_operator->do_filter(data, data.residuals, alms_w, false, false);

    {
      fitshandle f;
      f.create("!wiener_alms.fits");

      write_Alm_to_fits(f, alms_w, data.Lmax, data.Lmax, planckType<double>());

    }
    alm2map(alms_w, proposed_map);
    return;
  }

  
  // Now we want to generate some additional randomness to this
  // map.
  // Generate two maps, pure random.
  
  cout << "Computing proposed perturbations for the CMB sky..." << endl;

  double hsqrt2 = 1/sqrt(2.0);
  for (int l = 0; l <= alms_noise_cmb.Lmax(); l++)
    {
      double zetar, zetai;
      double Pl = sqcls[l];

      zetar = gsl_ran_gaussian(rGen, Pl);
      alms_noise_cmb(l,0) = zetar;
      for (int m = 1; m <= min(alms_noise_cmb.Mmax(),l); m++)
        {
          zetar = gsl_ran_gaussian(rGen, Pl*hsqrt2);
          zetai = gsl_ran_gaussian(rGen, Pl*hsqrt2);
          alms_noise_cmb(l,m) = xcomplex<DataType>(zetar, zetai);
        }
     }

   {
     fitshandle f;
     f.create("!rand_gen.fits");
     alm2map(alms_noise_cmb, proposed_map);
    write_Healpix_map_to_fits(f, proposed_map, planckType<double>());
  }

  // Now we compute fluctuations.
  for (int channel = 0; channel < data.numChannels; channel++)
    {
      long dof = data.noise[channel]->dof();
      makeRandomMap(rGen, noise1[channel]);
      data.noise[channel]->apply_fwd_half(noise1[channel]);
      cout << "Chi2 noise = " << (data.noise[channel]->chi2(noise1[channel])/dof) << " dof = " << (double(dof)/noise1[channel].Npix()) << endl;
      tmp_alms = alms_noise_cmb;
      tmp_alms.ScaleL(data.beam[channel]);
      alm2map(tmp_alms, proposed_map);
      noise1[channel] += proposed_map;
      noise1[channel] = data.residuals[channel] - noise1[channel];
      data.noise[channel]->apply_mask(noise1[channel]);
    }

  // Add some more fluctuations.

  cout << "Building new plausible map ..." << endl;
  data.wiener_operator->do_filter(data, noise1, alms_w, false, false);
  // Convert this to a real map.

  alm2map(alms_w, proposed_map);
  {
    fitshandle f;
    f.create("!residual.fits");

    write_Healpix_map_to_fits(f, proposed_map, planckType<double>());
  }

  alms_noise_cmb += alms_w;
  alm2map(alms_noise_cmb, proposed_map);

  {
    fitshandle f;
    f.create("!proposed.fits");

    write_Healpix_map_to_fits(f, proposed_map, planckType<double>());
  }
}

const std::vector<int>& CMB_Sampler::getModeledSignals() const
{
  if (cmb_signals.size()==0)
    {
      cmb_signals.push_back(SAMPLER_SIGNAL_PROPOSED_CMB);
      cmb_signals.push_back(SAMPLER_SIGNAL_WIENER_CMB);
    }

  return cmb_signals;
}

const std::vector<int>& CLS_Sampler::getModeledSignals() const
{
  static vector<int> signals;

  return signals;
}
