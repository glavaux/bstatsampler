/*+
This is ABYSS (./libsampler/samplers/lensing.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <omp.h>
#include <cstdlib>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "interpolate/blens.hpp"
#include "interpolate/qbinterpol.hpp"
#include "cl_sampler.hpp"
#include "extra_map_tools.hpp"
#include "lensing.hpp"
#include "conjugateGradient.hpp"
#include "cmb_rng.hpp"
#include "wigner3j.hpp"

using namespace std;
using namespace CMB;

namespace CMB {

template<typename T,typename T2>
class LensingOperator
{
protected:
  Healpix_Map<T> deflection_phi, deflection_theta, phi_map,
    backup_deflection_phi, backup_deflection_theta;
  QuickInterpolateMap<T,T2> *interpolate;
  bool transposed;
  int Nside;
public:
  LensingOperator(const arr<T2>& Cls, long nSide)
    : deflection_phi(nSide, RING, SET_NSIDE),
      deflection_theta(nSide, RING, SET_NSIDE),
      phi_map(nSide, RING, SET_NSIDE),
      Nside(nSide)
  {
    interpolate = 0;
    transposed = false;
    deflection_theta.fill(0);
    deflection_phi.fill(0);
  }

  ~LensingOperator()
  {
    delete interpolate;
  }

  void setTranspose(bool i) { transposed = i; }

  void update_CMB_spectrum(const arr<T2>& Cls)
  {
    cerr << "Somebody has requested to update the Cls assumed to achieve interpolation" << endl;
    cerr << " => recomputing pixelization..." << endl;
    if (interpolate)
      delete interpolate;
    interpolate = new QuickInterpolateMap<T,T2>(Cls, 1, Nside, 0);
  }

  void saveField()
  {
    backup_deflection_theta = deflection_theta;
    backup_deflection_phi = deflection_phi;
  }

  void restoreField()
  {
    deflection_phi = backup_deflection_phi;
    deflection_theta = backup_deflection_theta;
  }

  void setLensingField(const Alm<xcomplex<T> >& lenspot)
  {
    cerr << " => changing deflection field..." << endl;
    alm2map_der1(lenspot, phi_map, deflection_theta, deflection_phi);
    cerr << " <= done" << endl;
  }

  template<bool transposedLensing>
  void applyLensing(const Healpix_Map<T>& in,
		    Healpix_Map<T>& out)
  {
    
    planck_assert(in.Nside() == out.Nside(), "incompatible input/output maps");
    planck_assert(in.Scheme() == RING, "input must be in RING ordering");
    planck_assert(out.Scheme() == RING, "output must be in RING ordering");
    planck_assert(in.Nside() == Nside, "input Nside must match the Bayesian interpolation Nside");
    planck_assert(interpolate != 0, "CMB spectrum must have been initialized");

    if (transposedLensing)
      out.fill(0);
    
#pragma omp parallel for schedule(dynamic,100000)
    for (long i = 0; i < in.Npix(); i++)
      {
        pointing ptg;
 
        computeMovedDirection(deflection_theta[i],
 			      deflection_phi[i],
			      in.pix2ang(i), ptg);
  	assert(!isnan(ptg.theta));
  	assert(!isnan(ptg.phi));
	
        if (!transposedLensing)
	  out[i] = interpolate->interpolate(in, ptg, 0, 0);
        else
          interpolate->interpolateTransposed(in[i], out, ptg);
      }
  }

  void operator()(const Healpix_Map<T>& in,
		  Healpix_Map<T>& out)
  {
    if (transposed)      
      applyLensing<true>(in, out);
    else
      applyLensing<false>(in, out);
  }
};


};


Lensing_Sampler::Lensing_Sampler(gsl_rng *r, CMB_Data& data,
			       long nL_cmb, long nL_Phi, long nSide)
  : SingleSampler(r), Cls_CMB(nL_cmb), Cls(nL_cmb), 
    lensing_potential(nL_Phi, nL_Phi)
    , Cls_lensing(nL_Phi+1)
    //    preconditioner(data.noise, data.weight, data.beam,
    //		   nSide, nL, data.numChannels),
    //    matrix(data.noise, data.weight, data.beam, nSide, data.numChannels)
    
{
  Cls_CMB.fill(0);
  Cls.fill(0);

  lensing_potential.SetToZero();

  lo = new LensingOperator<DataType,double>(Cls_CMB, nSide);

  L_cmb_given_lensing = -INFINITY;

  alpha_lensing = 0.5;
  
  PowSpec phispec(1, Cls_lensing.size()-1);
  read_powspec_from_fits("spectrum_phi.fits", phispec, 1, Cls_lensing.size()-1);
  Cls_lensing = phispec.tt();
  for (int i = 0; i < Cls_lensing.size(); i++)
    Cls_lensing[i] *= 0.5;
}

Lensing_Sampler::~Lensing_Sampler()
{
  delete lo;
}


namespace CMB {

/* Noise matrix */
class MapLensedSignalNoiseSignal_weighing
{
  public:
  arr<DataType> cls;
  const arr<BeamPower>& beam;
  const CMB_NoiseChannelData& noise;
  const arr<double>& weight;
  CMB_Map map;
  int currentChannel;
  int numChannels;
  LensingOperator<DataType,double>& lensing;
  
  MapLensedSignalNoiseSignal_weighing(const CMB_NoiseChannelData& n,
				      const arr<double>& w,
				      const arr<BeamPower>& b,
				      int nSide, int numCh,
				      LensingOperator<DataType,double>& l)
    : noise(n), weight(w), beam(b), map(nSide, RING, SET_NSIDE), lensing(l)
  {
    numChannels = numCh;
    currentChannel = -1;
  }
  
  void SetCls(const arr<DataType>& cls)
  {
    this->cls = cls;
  }
    
  CMB_Map operator()(const CMB_Map& map)
  {
    int lmax = cls.size()-1;
    CMB_Map outMap0 = map, outMap1(map.Nside(), RING, SET_NSIDE);
    ALM_Map alms(lmax, lmax);

    cout << "Noise weighing (1)" << endl;
    noise[0](outMap0, -0.5);
    cerr << "Unlensing..." << endl;
    lensing.applyLensing<true>(outMap0, outMap1);
//	outMap1 = outMap0;
    cerr << "Spectrum weighing..." << endl;
    HEALPIX_method(outMap1, alms, weight);
    alms.ScaleL(cls);
    alm2map(alms, outMap1);
    cerr << "Relensing..." << endl;
    lensing.applyLensing<false>(outMap1, outMap0);
    //	outMap0 = outMap1;
    cout << "Noise weighing (2)" << endl;
    noise[0](outMap0, -0.5);

#if 0
	{
		fitshandle out;
		out.create("!input_map.fits");
        	write_Healpix_map_to_fits(out,map,
                                  FITSUTIL<DataType>::DTYPE);
	}
      {
        fitshandle out;
        out.create ("!second_term.fits");
        write_Healpix_map_to_fits(out,outMap0,
                                  FITSUTIL<DataType>::DTYPE);
      }
#endif

    return map+outMap0;
  }
  
};

	       

/* Noise matrix */
class MapLensedSignalNoiseSignal_weighing_preconditioner
{
  public:
  const arr<DataType> *cls;
  const arr<BeamPower>& beam;
  const CMB_NoiseChannelData& noise;
  const arr<double>& weight;
  ALM_Map preconditioner;
  int currentChannel;
  int numChannels;
  int myLmax;
  
  MapLensedSignalNoiseSignal_weighing_preconditioner(const CMB_NoiseChannelData& n,
						     const arr<double>& w,
						     const arr<BeamPower>& b,
						     const arr<DataType>& Cls,
						     int nSide, int numCh)
    : noise(n), weight(w), beam(b), myLmax(Cls.size()-1), preconditioner(Cls.size()-1,Cls.size()-1)
  {
    numChannels = numCh;
    currentChannel = -1;
    SetCls(Cls);
  }

  static int mysign(int a) { return (a&1) ? -1 : 1; }

  void setupPreconditioner()
  {
    int lmax = myLmax;
    ALM_Map alms(lmax, lmax);
    CMB_Map tmp_map(noise[0].Nside(), RING, SET_NSIDE);
    arr< arr<double> > coeflist1;
    arr<double> coef3;
    PowSpec powspec_N(1, lmax);

    double tstart = omp_get_wtime();
    cerr << " ===== initializing Lensing preconditioner ===== " << endl;
    fillMap(tmp_map, 1.0);
    noise[0](tmp_map, -0.5);
    HEALPIX_method(tmp_map, alms, weight);

    extract_powspec(alms, powspec_N);

    clear(preconditioner);
    
#if 0
    //    coef1.alloc(2*lmax+1);
    for (int l0 = 0; l0 <= lmax; l0++)
      {
	double accum = 0;
	for (int lp = 0; lp <= lmax; lp++)
	  {
	    for (int l1 = std::abs(lp-l0); l1 <= std::min(lmax,lp+l0); l1++)
	      {
		accum += (2*lp+1)*(2*l1+1)*powspec_N.tt()[l1]*(*cls)[lp];
	      }
	  }
	for (int m0=0;m0<=l0;m0++)
	  preconditioner(l0,m0) = 1+accum/(4*M_PI);
      }
#else
    coeflist1.alloc((lmax+1)*(lmax+2)/2);
    coef3.alloc(lmax+1);

    (cerr << "Precomputing 3j-symbols..." << endl).flush();
    for (int l = 0; l <= lmax; l++)
      {
	for (int lprime = l; lprime <= lmax; lprime++)
	  {
	    int idx = l*(lmax+1)-l*(l-1)/2  + lprime-l;
	    
	    assert(idx < coeflist1.size());
	    assert(coeflist1[idx].size() == 0);
	    coeflist1[idx].alloc(l+lprime+1);
	    coeflist1[idx].fill(0);
	    compute3j(l, lprime, 0, 0, 0, l+lprime, coeflist1[idx]);
	  }
      }
    for (int l = 0; l <= lmax; l++)
      coef3[l] = sqrt(2.*l + 1);
    
    (cerr << "Computing diagonal elements...").flush();
    for (int m = 0; m <= lmax; m++)
      {
	(cerr << m << "...").flush();

#pragma omp parallel for schedule(dynamic)
	for (int l = m; l <= lmax; l++)
	  {
	    arr<double> coef2(2*lmax+1);

	    DataType accum_lprime = 0;	    

	    for (int lprime = 0; lprime <= min(cls->size()-1,(long)lmax); lprime++)
	      {
		DataType local_sum = 0;
		for (int mprime = -lprime; mprime <= lprime; mprime++)
		  {
		    xcomplex<DataType> temp_accum = 0;
		    // If no permutation or a permutation with signature equal to 1, then the sign is 1, else -1.
		    int sign_l0_l1 = ((l <= lprime) || ((l+lprime)&1)) ? 1 : -1;
		    int l0 = min(l,lprime), l1 = max(l,lprime);
		    int idx1 = l0*(lmax+1) - l0*(l0-1)/2 + l1-l0;
		    arr<double>& coef1 = coeflist1[idx1];
		    int lstart = std::abs(l-lprime);
		    int lend = l+lprime;

		    coef2.fill(0);
		    assert(m <= l);
		    assert(std::abs(mprime) <= lprime);
		    compute3j(l, lprime, m, -mprime, lstart, lend, coef2);
		    
		    int msec = std::abs(mprime-m);

		    for (int lsec = lstart; lsec <= min(lend,lmax); lsec++)
		      {
			double alpha = coef3[lsec];
			xcomplex<DataType> aval = alms(lsec,msec);

			temp_accum += aval * alpha * coef1[lsec] * coef2[lsec-lstart];
		      }
		    local_sum += temp_accum.norm();
		    assert(!isnan(accum_lprime));
		  }
		local_sum  *= (2*lprime+1)*(*cls)[lprime];
		accum_lprime += local_sum;
	      }
	    preconditioner(l,m) = xcomplex<DataType>(1+accum_lprime*(2*l+1)/(4*M_PI),0);
	  }
      }
#endif
    
    double tend = omp_get_wtime();
    cerr << endl << " === finished initialization (time=" << tend-tstart << " ===" << endl;
  }
  
  void SetCls(const arr<DataType>& cls)
  {
    this->cls = &cls;
    setupPreconditioner();
  }
    
  CMB_Map operator()(const CMB_Map& map)
  {
    int lmax = myLmax;
    ALM_Map alms(lmax, lmax);
    CMB_Map outMap(map.Nside(), RING, SET_NSIDE);
    CMB_Map residualMap(map.Nside(), RING, SET_NSIDE);

    HEALPIX_method(map, alms, weight);
    alm2map(alms, residualMap);
    residualMap -= map;
 
    for (int m=0;m<=preconditioner.Mmax();m++)
      {
	for (int l=m;l<=preconditioner.Lmax();l++)
	  {
	    alms(l,m) /= preconditioner(l,m);
	  }
      }
    alm2map(alms, outMap);

    return outMap-0.001*residualMap;
  }
  
};

};

double Lensing_Sampler::computeLikelihood(CMB_Data& data,
					 const CMB_Map& datamap,
					  MapLensedSignalNoiseSignal_weighing& matrix,
					  MapLensedSignalNoiseSignal_weighing_preconditioner& precon
					  )
{
  // No multi-channel support yet !!!
  CMB_Map map1 = datamap;

  CMB_Map weighed_map(map1.Nside(), RING, SET_NSIDE);

  data.noise[0](map1, -0.5);
  //conjugateGradient(map1, weighed_map, matrix);  
  conjugateGradient(map1, weighed_map, matrix, precon);  
#if 0
      {
        fitshandle out, out2;
        out.create ("!fake_lens.fits");
        write_Healpix_map_to_fits(out,weighed_map,
                                  FITSUTIL<DataType>::DTYPE);
      }
#endif
  // weighed_map holds now matrix^(-1) * map1
//  return -0.5*dot_product(weighed_map, map1).real()*4*M_PI/map1.Npix();  
  return -0.5*dot_product(weighed_map, map1).real();
}

ALM_Map Lensing_Sampler::computeNewProposedPotential()
{
  ALM_Map proposition = lensing_potential;
  int l0 = min(proposition.Lmax()+1,(int)Cls_lensing.size());

  for (long l = 0; l < l0; l++)
    {
	cout << l << " / " << l0 << "  proposing..." << endl;
      double sqCls = sqrt(Cls_lensing[l]);//*(1-alpha_lensing);

//      proposition(l,0) *= alpha_lensing;
      proposition(l,0) += gsl_ran_gaussian_ziggurat(rGen, sqCls);
      
      sqCls /= M_SQRT2;
      for (long m = 1; m <= min((long)proposition.Mmax(),l); m++)
	{
	  xcomplex<DataType> prop_a_lm;

//	  proposition(l,m) *= alpha_lensing;

	  prop_a_lm = 
	    xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen,sqCls),
			       gsl_ran_gaussian_ziggurat(rGen,sqCls));

	  proposition(l,m) += prop_a_lm;
	}
    }  
   return proposition;
}

double Lensing_Sampler::computeJumpProba(const ALM_Map& original,
					 const ALM_Map& proposed)
{
  double Lold = 0, Lnew = 0;
  return 0;

  for (long l = 0; l <= proposed.Lmax(); l++)
    {
      double Cl = Cls_lensing[l];
      if (Cl == 0) continue;
      double accum_old = 0, accum_new = 0 ;

      Cl *= 2*l+1;

      for (long m = 1; m <= min(l,(long)proposed.Mmax()); m++)
	
	{
	  const xcomplex<DataType>& co = original(l,m),
	    &cp = proposed(l,m);

	  accum_old += (co.conj() * co).real();
	  accum_new += (cp.conj() * cp).real();
	}
      accum_old = 2*accum_old + (original(l,0).conj() * original(l,0)).real();
      accum_new = 2*accum_new + (proposed(l,0).conj() * proposed(l,0)).real();
      
      Lold += accum_old/Cl;
      Lnew += accum_new/Cl;
    }

  return -0.5*(1+alpha_lensing)/(1-alpha_lensing)*(Lold-Lnew);
}

void Lensing_Sampler::computeNewSample(CMB_Data& data,
				      const AllSamplers& samplers)
{
  // Generate new parameters
  const arr<DataType>& cmb_Cls = ((CLS_Sampler*)samplers[SAMPLER_CLS])->getCls();
  ALM_Map proposed_potential;

  cerr << "Update information on CMB spectrum for the interpolator..." << endl;
  lo->update_CMB_spectrum(cmb_Cls);

  proposed_potential = computeNewProposedPotential();
  double jumpProba = computeJumpProba(lensing_potential, proposed_potential);

  cerr << "Jump likelihood = " << jumpProba << endl;
  
  int Nside = data.channels[0].Nside();

  MapLensedSignalNoiseSignal_weighing matrix(data.noise, data.weight,
					     data.beam, data.channels[0].Nside(),
					     data.numChannels,
					     *lo);

  MapLensedSignalNoiseSignal_weighing_preconditioner 
    precon(data.noise, data.weight, data.beam, cmb_Cls, 
	   data.channels[0].Nside(), data.numChannels);

  matrix.SetCls(cmb_Cls);

  lo->saveField();
  lo->setLensingField(proposed_potential);

  // Generate the fake data for the exchange algorithm
  CMB_Map fakedata(Nside, RING, SET_NSIDE);

  {
    makeRandomMap(rGen, fakedata);
    data.noise[0](fakedata, 0.5);
  }

  {
    CMB_Map cmb1(Nside, RING, SET_NSIDE),
      cmb2(Nside, RING, SET_NSIDE);

    generateCMBMap<CMB_Map>(rGen, cmb1, cmb_Cls);
    lo->applyLensing<false>(cmb1, cmb2);

    fakedata += cmb2;
  }

#if 0
      {
        fitshandle out;
        out.create ("!fake_lens.fits");
        write_Healpix_map_to_fits(out,fakedata,
                                  FITSUTIL<DataType>::DTYPE);
      }
#endif

  lo->restoreField();

  // fakedata holds the fake cmb data that we will use for the
  // exchange algorithm


  // Now we need the relative likelihood of the two datasets given 
  // the two sets of "parameters" determining the deflection field.

  cerr << "Likelihood of CMB given old lensing potential..." << endl;
  if (L_cmb_given_lensing == -INFINITY)
   {
     lo->setLensingField(lensing_potential);
     L_cmb_given_lensing = computeLikelihood(data, data.channels[0], matrix, precon);
   }

  double L_cmb_old = L_cmb_given_lensing;
  cerr << "Likelihood of fake data given old lensing potential..." << endl;
  double L_fake_old = computeLikelihood(data, fakedata, matrix, precon);
  
  lo->saveField();
  lo->setLensingField(proposed_potential);

  cerr << "Likelihood of CMB given new lensing potential..." << endl;
  double L_cmb_new = computeLikelihood(data, data.channels[0], matrix, precon);
  cerr << "Likelihood of fake data given new lensing potential..." << endl;
  double L_fake_new = computeLikelihood(data, fakedata, matrix, precon);

  double relative = (L_fake_old-L_fake_new)+(L_cmb_new-L_cmb_old);

  relative += jumpProba;
  
  // accept_probe holds the acceptance probability of the new proposed lensing
  // field
  double accept_proba = (relative > 0) ? 1 : exp(relative);

  ofstream faccept("acceptance_lensing.txt", ios::app);
  faccept << accept_proba << " " << relative 
	  << " " << setprecision(12) << L_fake_new
	  << " " << setprecision(12) << L_fake_old 
	  << " " << setprecision(12) << L_cmb_new 
	  << " " << setprecision(12) << L_cmb_old 
          << " " << setprecision(12) << jumpProba << endl;

  if (gsl_rng_uniform(rGen) < accept_proba)
    {
      lensing_potential = proposed_potential;
      L_cmb_given_lensing = L_cmb_new;
	{
		static int skyCounter = 0;
		char s[255];

		sprintf(s,"!out/alm_lensing_%d.fits",skyCounter);

		fitshandle f;
		f.create(s);
                write_Alm_to_fits(f, lensing_potential, lensing_potential.Lmax(), lensing_potential.Mmax(), FITSUTIL<double>::DTYPE);
		skyCounter++;
	}
    }
  else
    {
      lo->restoreField();
    }
	
}
