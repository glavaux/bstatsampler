/*+
This is ABYSS (./libsampler/samplers/hamiltonian_mess_cmb.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <assert.h>
#include <string>
#include <cmath>
#include <vector>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <boost/format.hpp>
#include <CosmoTool/algo.hpp>
#include <arr.h>
#include <iostream>
#include <fstream>
#include <alm.h>
#include <alm_fitsio.h>
#include "cl_sampler.hpp"
#include "extra_alm_tools.hpp"
#include "extra_map_tools.hpp"
#include "hamiltonian_mess_cmb.hpp"

using namespace std;
using namespace CMB;
using boost::format;
using boost::str;
using CosmoTool::square;

HMC_Messenger_CMB_Sampler::HMC_Messenger_CMB_Sampler(MPI_Communication *comm,
                                 gsl_rng *r, const std::string& name, CMB_Data& data,
                                 long nL, long nSide, int signal)
  : Comm(comm), SingleSampler(r, name), lmax(nL), proposed_map(nSide, RING, SET_NSIDE), tune_in(0.7), output_reconstructed_signal(signal)
{
  this->epsilonmax = epsilonmax; //0.001;
  this->nmax = nmax;
  this->frozen_tune = false;

  current_momenta.position_s.Set(lmax, lmax);
  current_momenta.momenta_s.Set(lmax, lmax);
  current_momenta.position_t.alloc(data.numChannels);
  current_momenta.momenta_t.alloc(data.numChannels);
  masses.inv_ntilde.alloc(data.numChannels);
  masses.tau.alloc(data.numChannels);
  masses.t_mask.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      current_momenta.position_t[ch].SetNside(data.Nside, RING);
      current_momenta.momenta_t[ch].SetNside(data.Nside, RING);
      masses.t_mask[ch].SetNside(data.Nside, RING);
      init_tau_ntilde(data, ch);
      clear(current_momenta.position_t[ch]);
    }
    
  clear(current_momenta.position_s);
}

HMC_Messenger_CMB_Sampler::~HMC_Messenger_CMB_Sampler()
{
}

void HMC_Messenger_CMB_Sampler::init_tau_ntilde(CMB_Data& data, int ch)
{
  CMB_NN_NoiseMap *nn = data.noise[ch]->as<CMB_NN_NoiseMap>();
  long Npix = nn->Npix();
  double tau = std::numeric_limits<double>::max();
  
  for (long p = 0; p < Npix; p++)
    {
      double invN = (*nn)[p];
      if (invN > 0)
        tau = min(tau,1/invN);
    }
    
  masses.inv_ntilde[ch].SetNside(data.Nside, RING);
  masses.t_mask[ch].SetNside(data.Nside, RING);

  for (long p = 0; p < Npix; p++)
    {
      double invN = (*nn)[p];
      masses.t_mask[ch][p] = true;
      if (invN > 0)
        {
          double ntilde = 1/invN-tau;
          if (ntilde <= 0)
            {
              masses.inv_ntilde[ch][p] = 0;
              masses.t_mask[ch][p] = false;
            }
          else
            masses.inv_ntilde[ch][p] = 1/ntilde;
        }
      else
        {
          masses.inv_ntilde[ch][p] = 0;
        }
    }

  masses.tau[ch] = tau;
}


template<typename T>
void packAlms(T *outbuf, const Alm<xcomplex<T> >& alms)
{
  int k = 0;
  for (long m = 0; m <= alms.Mmax(); ++m)
    for (long l = m; l <= alms.Lmax(); ++l) {
       outbuf[k] = alms(l,m).re;
       outbuf[k+1] = alms(l,m).im;
       k+=2;
    }
  assert(k ==  2*Alm<xcomplex<T> >::Num_Alms(alms.Lmax(), alms.Mmax()));
}

template<typename T>
void unpackAlms(const T *inbuf, Alm<xcomplex<T> >& alms)
{
  int k = 0;
  for (long m = 0; m <= alms.Mmax(); ++m)
    for (long l = m; l <= alms.Lmax(); ++l) {
       alms(l,m) = xcomplex<T>(inbuf[k], inbuf[k+1]);
       k+=2;
    }
  assert(k ==  2*Alm<xcomplex<T> >::Num_Alms(alms.Lmax(), alms.Mmax()));
}


void HMC_Messenger_CMB_Sampler::saveState(NcFile& f)
{
  ALM_Map& current_alms = current_momenta.position_s;
  
  NcDim cmb_dim = f.addDim("HMC_Messenger_CMB_sampler_cmb_dim", 2*current_alms.Alms().size());
  NcVar cmb_clean = f.addVar("HMC_Messenger_CMB_sampler_cmb_clean", ncDouble, cmb_dim);

  arr<double> out_alms(2*current_alms.Alms().size());
  packAlms(&out_alms[0], current_alms);
  cmb_clean.putVar(&out_alms[0]);
}

void HMC_Messenger_CMB_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  ALM_Map& current_alms = current_momenta.position_s;
  NcVar cmb_clean = f.getVar("HMC_Messenger_CMB_sampler_cmb_clean");
  std::vector<NcDim> dims = cmb_clean.getDims();

  if (dims[0].getSize() != 2*current_alms.Alms().size())
    throw StateInvalid("CLS");

  arr<double> in_alms(2*current_alms.Alms().size());
  cmb_clean.getVar(&in_alms[0]);
  unpackAlms(&in_alms[0], current_alms);
}

void HMC_Messenger_CMB_Sampler::generateMomenta(CMB_Data& data, HMC_Messenger_Momenta& momenta)
{
  double sq2 = sqrt(0.5);
  ALM_Map ran_numbers(lmax, lmax);
  
  for (long m = 0; m <= lmax; m++)
    {
      for (long l = m; l <= lmax; l++)
        {
          double Ml = sqrt(masses.mass_s[l]);
          
          if (m == 0) {
            momenta.momenta_s(l,0) = xcomplex<DataType>(gsl_ran_gaussian(rGen, Ml), 0);
          } else {
            momenta.momenta_s(l,m) = xcomplex<DataType>(gsl_ran_gaussian(rGen, Ml*sq2),gsl_ran_gaussian(rGen, Ml*sq2));
          }
        }
    }

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      long Npix = momenta.position_t[ch].Npix();
      
      for (long p = 0; p < Npix; p++)
        {
          double mass_t = masses.t_mask[ch][p] ? (masses.inv_ntilde[ch][p] + 1/masses.tau[ch]) : 0;
          momenta.momenta_t[ch][p] = gsl_ran_gaussian(rGen, sqrt(mass_t));
        }
    }
}

void HMC_Messenger_CMB_Sampler::computeNewSample(CMB_Data& data,
                                       const AllSamplers& samplers)
{
  arr<DataType> sqcls = data.guess_sqrt_cls;
//    dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getSqrtCls();
  arr<DataType> cls = data.guess_cls;
//    dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getCls();
  int n = 20+int(ceil(max(0,(nmax-20))*gsl_rng_uniform(rGen)));
  double epsilon = gsl_rng_uniform(rGen)*epsilonmax;
  double full_norm = data.channels[0].Npix()/(4*M_PI);

  masses.mass_s.alloc(data.Lmax+1);
  masses.inv_mass_s.alloc(data.Lmax+1);
  for (long l = 0; l <= lmax; l++)
    {
      masses.mass_s[l] = ((cls[l]==0) ? 0 : (1/cls[l]));
      for (int ch = 0; ch < data.numChannels; ch++)
        masses.mass_s[l] += square(data.beam[ch][l])*full_norm/masses.tau[ch];
      masses.inv_mass_s[l] = 1/masses.mass_s[l];
      masses.mass_s[0] = masses.mass_s[1] = 0;
    }

  assert(!isnan(epsilon));

  // Random momentum generated.
  HMC_Messenger_Momenta backup_momenta;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_Map& t = current_momenta.position_t[ch];
      Healpix_Map<bool>& mask = masses.t_mask[ch];
      for (long p = 0; p < t.Npix(); p++)
        if (!mask[p])
          t[p] = data.residuals[ch][p];
    }

  generateMomenta(data, current_momenta);

  backup_momenta = current_momenta;

  Comm->log(str(format("[HMC] Integrating equation of motions (N=%d, epsilon=%g)") % n % epsilon));
  Comm->log("[HMC] Shifting momenta");

  // Advance by epsilon/2
  shiftMomenta(0.5*epsilon, data, current_momenta);

  for (int i = 0; i < n; i++)
    {
      double a = (i==(n-1)) ? 0.5 : 1;

      Comm->progress(str(format("[HMC] step % 3d / % 3d (positions)") % (i+1) % n));
      shiftPositions(epsilon, data, current_momenta);
      Comm->progress(str(format("[HMC] step % 3d / % 3d (momenta  )") % (i+1) % n));
      shiftMomenta(a*epsilon, data, current_momenta);
    }
  Comm->log("");
  Comm->log("[HMC] Done integrating");

  double delta_chi2 = compute_delta_chi2(data, cls, backup_momenta, current_momenta);

  double proba = min(double(1), exp(-0.5*delta_chi2));
  bool accepted;

  if (gsl_rng_uniform(rGen) > proba)
    {
      // REJECT
      accepted = false;
      current_momenta = backup_momenta;
      if (!frozen_tune && tune_in > 0 && proba < 0.7)
        epsilonmax *= tune_in;
    }
  else
    { // ACCEPT
      accepted = true;
      if (!frozen_tune && delta_chi2 < 0)
        epsilonmax /= tune_in;
    }
  epsilonmax = min(epsilonmax, double(2));

  {
    ofstream f("debug/HMC_Messenger_CMB.txt",ios::app);
    f << format("%15lg %lg %lg %d %lg %d") % delta_chi2 % proba % epsilon % n % epsilonmax % accepted << endl;
  }

  {
     static int Nout = 0;
     fitshandle h;
     string fname = str(format("!debug/HMC_Messenger_CMB_alm_%d.fits") % Nout);
     h.create(fname);
     write_Alm_to_fits(h, current_momenta.position_s, lmax, lmax, planckType<double>());
     Nout++;
  }
  
  reconstructed_map.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ALM_Map alms = current_momenta.position_s;
      
      reconstructed_map[ch].SetNside(data.Nside, RING);
      alms.ScaleL(data.beam[ch]);
      alm2map(alms, reconstructed_map[ch]);
    }
}

static double computeChi2(CMB_Data& data, const arr<DataType>& cls, HMC_Messenger_Mass& masses, HMC_Messenger_Momenta& m)
{
  double chi2, chi2_t = 0;
  PowerSpec inv_cls = cls;
  
  for (int l = 0; l < inv_cls.size(); l++)
    inv_cls[l] = (inv_cls[l]==0) ? 0 : (1/inv_cls[l]);
    
  CMB_Map smap;
  ALM_Map s;
  
  s = m.position_s;
  s.ScaleL(inv_cls);
  chi2 = dot_product_simple(s, m.position_s).real();
  
  s = m.momenta_s;
  s.ScaleL(masses.inv_mass_s);
  chi2 += dot_product_simple(s, m.momenta_s).real();

  smap.SetNside(data.Nside, RING);
  
  for (int ch = 0 ; ch < data.numChannels; ch++)
    {
      s = m.position_s;
      s.ScaleL(data.beam[ch]);
      alm2map(s, smap);
      
      for (long p = 0; p < smap.Npix(); p++)
        smap[p] = square(smap[p]-m.position_t[ch][p]);

      clean_sum(smap, smap.Npix());
      chi2_t += smap[0]/masses.tau[ch];

      for (long p = 0; p < smap.Npix(); p++)
        smap[p] = square(data.residuals[ch][p] - m.position_t[ch][p])*masses.inv_ntilde[ch][p];

      clean_sum(smap, smap.Npix());
      chi2_t += smap[0];
       
      for (long p = 0; p < smap.Npix(); p++)
       {
	 double mass_t = masses.inv_ntilde[ch][p] + 1/masses.tau[ch];
         smap[p] = masses.t_mask[ch][p] ? (square(m.momenta_t[ch][p])/mass_t) : 0;
       }

      clean_sum(smap, smap.Npix());
      chi2_t += smap[0];
    } 
  return chi2 + chi2_t;
}


double HMC_Messenger_CMB_Sampler::compute_delta_chi2(CMB_Data& data, const arr<double>& cls,
                                                     HMC_Messenger_Momenta& m1, HMC_Messenger_Momenta& m2)
{
  return computeChi2(data, cls, masses, m2) - computeChi2(data, cls, masses, m1);
}

void HMC_Messenger_CMB_Sampler::masked_force_t(double scale, CMB_Data& data, HMC_Messenger_Momenta& momenta, int ch)
{
  CMB_Map s_ch;
  ALM_Map tmp_alms = momenta.position_s;
  CMB_Map& mom_t = momenta.momenta_t[ch];
  CMB_Map& pos_t = momenta.position_t[ch];

  s_ch.SetNside(data.Nside, RING);
  tmp_alms.ScaleL(data.beam[ch]);
  alm2map(tmp_alms,s_ch);

  for (int p = 0; p < s_ch.Npix(); p++)
    {
      if (masses.t_mask[ch][p])
        {
	  double mass_t = masses.inv_ntilde[ch][p] + 1/masses.tau[ch];
          mom_t[p] += scale*(-mass_t*pos_t[p] + data.residuals[ch][p]*masses.inv_ntilde[ch][p] + s_ch[p]/masses.tau[ch]);
        }
    }
}

void HMC_Messenger_CMB_Sampler::shiftPositions(double scale, CMB_Data& data,
                                               HMC_Messenger_Momenta& momenta)
{
  {
    ALM_Map delta_s = momenta.momenta_s;
  
    delta_s.ScaleL(masses.inv_mass_s);
    momenta.position_s += scale*delta_s;
  }
  
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_Map& t_pos = momenta.position_t[ch];
      CMB_Map& t_momenta = momenta.momenta_t[ch];
      long Npix = t_pos.Npix();
      
      for (long p = 0; p < Npix; p++)
        {
          double mass_t = masses.inv_ntilde[ch][p] + 1/masses.tau[ch];

          if (masses.t_mask[ch][p])
            momenta.position_t[ch][p] += scale*t_momenta[p]/mass_t;
          else
            momenta.position_t[ch][p] = data.residuals[ch][p];
        }
   }
}

void HMC_Messenger_CMB_Sampler::shiftMomenta(double scale, CMB_Data& data,
                                             HMC_Messenger_Momenta& momenta)
{
  {
    ALM_Map force_s;
    
    force_s = momenta.position_s;
    force_s.ScaleL(masses.mass_s);
    force_s *= -1;
if (true)
    for (int ch = 0; ch < data.numChannels; ch++)
      {
        double factor = momenta.position_t[ch].Npix()/(4*M_PI);

        ALM_Map t_alms(data.Lmax, data.Lmax);
      
        HEALPIX_method(momenta.position_t[ch], t_alms, data.weight);
        t_alms.ScaleL(data.beam[ch]);
        t_alms.Scale(factor/masses.tau[ch]);
        
        force_s += t_alms;
      }
    force_s(0,0) = force_s(1,0) = force_s(1,1) = 0;
    momenta.momenta_s += scale*force_s;
  }

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      masked_force_t(scale, data, momenta, ch);
    }
}


static std::vector<int> cmb_signals;

const std::vector<int>& HMC_Messenger_CMB_Sampler::getModeledSignals() const
{
  if (cmb_signals.size()==0)
    {
      cmb_signals.push_back(SAMPLER_SIGNAL_PROPOSED_CMB);
    }

  return cmb_signals;
}
