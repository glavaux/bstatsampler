/*+
This is ABYSS (./libsampler/samplers/hamiltonian_cmb.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __HMC_CMB_HPP
#define __HMC_CMB_HPP

#include "sampler.hpp"
#include "cmb_defs.hpp"
#include "noise_weighing.hpp"
#include "preconditioners.hpp"
#include "cmb_data.hpp"


namespace CMB
{

  class HMC_CMB_Sampler: public SingleSampler, public MapperSampler
  {
  public:
    HMC_CMB_Sampler(gsl_rng *r, const std::string& name, CMB_Data& data,
                    long nL, long nSide, int signal);
    virtual ~HMC_CMB_Sampler();

    void freeze() { frozen_tune = true; }
    void set_Nmax(int nmax) {
      this->nmax = nmax;
    }

    void set_EpsilonMax(double epsilonmax)
    {
      this->epsilonmax = epsilonmax;
    }

    virtual void saveState(NcFile& f);
    virtual void restoreState(NcFile& f) throw(StateInvalid);

    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers);

    virtual const CMB_Map *getEstimatedMap(int ch, int signal) const
    {
      if (signal == SAMPLER_SIGNAL_PROPOSED_CMB)
	return &proposed_map;

      if (signal == output_reconstructed_signal)
        {
          return &reconstructed_map[ch];
        }
        
      return 0;
    }

    virtual const ALM_Map *getMapperCurrentAlms() const
    {
      return &current_alms;
    }

    virtual const std::vector<int>& getModeledSignals() const;

    void generateMomenta(ALM_Map& momentum, const arr<double>& cls, const Alm<double>& mass);

    void setInitialProposedMap(const CMB_Map& map)
    {
      proposed_map = map;
    }

  protected:
    bool frozen_tune;
    double epsilonmax;
    int nmax, lmax;
    int output_reconstructed_signal;
    double tune_in;
    CMB_Map proposed_map;
    arr<CMB_Map> reconstructed_map;
    ALM_Map current_alms;
    Alm<double> mass_proxy;
    MapSignalNoiseSignal_weighing_preconditioner<-1,1> *preconditioner;

    ALM_Map computeForce(CMB_Data& data, const arr<DataType>& sqcls);
    double compute_delta_chi2(CMB_Data& data, const arr<DataType>& cls, const ALM_Map& a1, const ALM_Map& a2, const ALM_Map& m1, const ALM_Map& m2, const Alm<double>& mass);

  };

};

#endif
