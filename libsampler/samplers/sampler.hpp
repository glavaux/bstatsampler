/*+
This is ABYSS (./libsampler/samplers/sampler.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __GIBBS_SAMPLER_HPP
#define __GIBBS_SAMPLER_HPP

#include <string>
#include "cmb_defs.hpp"
#include <healpix_map.h>
#include <vector>
#include <gsl/gsl_rng.h>
#include <exception>
#include "netcdf_adapt.hpp"
#include <boost/function.hpp>

namespace CMB
{
  class SingleSampler;
  class CMB_Data;

  // We use static assignment for the samplers.
  // The indices are indicated in cmb_defs.hpp
  typedef std::vector<SingleSampler *> AllSamplers;

  class StateInvalid: public std::exception
  {
  public:
      StateInvalid(const std::string& sname) : sampler_name(sname) {}
     virtual ~StateInvalid() throw () {}
     const std::string& sname() const throw () { return sampler_name; }

     std::string sampler_name;
  };

  class CacheCapable
  {
  public:
    CacheCapable() {
    }

    virtual void setInitialSpectrum(const arr<double>& Cls) = 0;
    virtual void loadCache(const std::string& prefix) = 0;
    virtual void saveCache(const std::string& prefix) = 0;
    virtual void pinCMBspectrum(CMB_Data& data) = 0;
  };
  
  // This is the base class for a Gibbs sampler.
  // All other samplers must inherit this class.
  class SingleSampler
  {
  public:
    SingleSampler(gsl_rng *r, const std::string& name = "");
    virtual ~SingleSampler();

    // In this method we will compute a new sample
    // of the distribution of the considered parameter.
    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers) = 0;


    virtual const CMB_Map *getEstimatedMap(int ch, int signal) const {
      return 0;
    } 

    virtual const std::vector<int>& getModeledSignals() const  = 0; 

    virtual void saveState(NcFile& f) = 0;
    virtual void restoreState(NcFile& f) throw(StateInvalid) = 0;

    // The other function are defined depending on the type of
    // the sampled distribution.
 
    void cg_logger_fun(const std::string& msg);

    virtual bool isMPI() const { return false; }
  private:
    SingleSampler();
  protected:
    gsl_rng *rGen;
    std::string m_name;
    boost::function1<void,const std::string&> cg_logger;    
  };

  class MapperSampler
  {
  public:
    virtual const ALM_Map *getMapperCurrentAlms() const = 0;
  };

};

#endif
