/*+
This is ABYSS (./libsampler/samplers/template_fitter/template_pmatrix.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/format.hpp>
#include "parallel_multi_template_sampler_wiener.hpp"
#include <fstream>
#include <string>
#include <arr.h>
#include <list>
#include <vector>
#include <map>
#include "netcdf_adapt.hpp"
#include "static_template_symbols.hpp"
#include <unistd.h>
#include <signal.h>
#include <inttypes.h>
#include<sys/mman.h>

using namespace CMB;
using namespace std;

using boost::str;
using boost::format;

void MPI_Multi_Template_Sampler_Wiener::initPmatrix()
{
  int Nv = alpha.size();
  int Ntemp = numChannels*numTemplates;

  Pmatrix.alloc(Nv*Ntemp);
  Pmatrix.fill(0);

  for (int idx = 0; idx < Ntemp; idx++)
    {
      for (list<int>::iterator iter_id = locked_id[idx].begin();
          iter_id != locked_id[idx].end();
          ++iter_id) {
        int meta_id = *iter_id;

        Pmatrix[idx*Nv + meta_id] = 1;
      }
    }
}

void MPI_Multi_Template_Sampler_Wiener::setupPmatrix(const std::string& fname)
{
  int Ntemp = numChannels*numTemplates;
  int id_alloc = 0;
  int numLockedId;
  NcVar vPmatrix;
  vector<NcDim> pdims;

  Comm->comm_assert(!id_initialized, "Locked amplitudes can only be initialized once!");

  NcFile f(fname, NcFile::read);
  NcGroupAtt gatt = f.getAtt(NETCDF_PMATRIX_SIZE);

  if (gatt.isNull())
    {
      Comm->log("Format of the PMATRIX file is incorrect. Stopping");
      Comm->abort();
    }
  gatt.getValues(&numLockedId);

  locked_id_table.clear();
  locked_id.alloc(numLockedId);
  locked_id_name.alloc(numLockedId);

  for (int i = 0; i < numLockedId; i++)
    {
      string meta_name = str(format("meta_id_%d") % i);
      NcGroupAtt meta_att = f.getAtt(meta_name);
      Comm->comm_assert(!meta_att.isNull(), "Invalid PMATRIX format");

      meta_att.getValues(locked_id_name[i]);
    }

  vPmatrix = f.getVar(NETCDF_PMATRIX);
  Comm->comm_assert(!vPmatrix.isNull(), "Invalid PMATRIX format (no Pmatrix)");
  Comm->comm_assert(vPmatrix.getDimCount() == 2, "PMATRIX must have two dimensions");
  pdims = vPmatrix.getDims();

  Comm->comm_assert(pdims[0].getSize() == numLockedId, "Invalid PMATRIX dimension 0");
  Comm->comm_assert(pdims[1].getSize() == Ntemp, "Invalid PMATRIX dimension 1");

  Pmatrix.alloc(numLockedId*Ntemp);

  for (int t = 0; t < Ntemp; t++)
    {
      vector<size_t> start_idx, count_idx;

      start_idx.resize(2);
      count_idx.resize(2);

      start_idx[0] = 0;
      start_idx[1] = t;
      count_idx[0] = numLockedId;
      count_idx[1] = 1;
      vPmatrix.getVar(start_idx, count_idx, &Pmatrix[t*numLockedId]);
    }

  initMetaId(numLockedId);
}

void MPI_Multi_Template_Sampler_Wiener::setLockedAmplitudes(const arr<list<string> >& locked_id)
{
  int id_alloc = 0;

  Comm->comm_assert(!id_initialized, "Locked amplitudes can only be initialized once!");

  // Build an internal id table.
  this->locked_id_table.clear();
  this->locked_id.alloc(locked_id.size());
  vector<string> tmp_id_name;

  for (int i = 0; i < locked_id.size(); i++)
    {
      int q;
      this->locked_id[i].clear();
      for (list<string>::const_iterator itername = locked_id[i].begin();
           itername != locked_id[i].end();
           ++itername) {
        map<string,int>::iterator iter = locked_id_table.find(*itername);
        if (iter == locked_id_table.end())
          {
            Comm->log_root(str(format("Allocated id=%d for meta name '%s'") % id_alloc % (*itername)));
            locked_id_table[*itername] = id_alloc;
            tmp_id_name.resize(id_alloc+1);
            tmp_id_name[id_alloc] = *itername;
            q = id_alloc;
            id_alloc++;
          }
        else
          q = iter->second;
        Comm->log(str(format("Added meta %d to template %d") % q % i));
        this->locked_id[i].push_back(q);
      }
    }

  int max_id = id_alloc;
  locked_id_name.alloc(max_id);
  copy(tmp_id_name.begin(), tmp_id_name.end(), &locked_id_name[0]);

  if (locked_id.size() != numChannels*numTemplates)
    {
      Comm->log(str(format("Template %d: Invalid size of the locked amplitudes vector") % template_name));
      Comm->abort();
    }

  initMetaId(max_id);
  for (int i = 0; i < this->locked_id.size(); i++)
    {
      for(list<int>::iterator citer = this->locked_id[i].begin();
          citer != this->locked_id[i].end();
          ++citer)
        Comm->log(str(format("Locked_id[%d] -> meta %d") % i % (*citer)));
    }

  initPmatrix();
}

void MPI_Multi_Template_Sampler_Wiener::initMetaId(int max_id)
{
  base_rand.resize(max_id);

  if (!dynamic_jobs)
    {
      globalMin = max_id*Comm->rank()/Comm->size();
      globalMax = max_id*(Comm->rank()+1)/Comm->size();
    }
  else
    {
      globalMin = 0;
      globalMax = max_id;
    }
  id_to_rank.alloc(max_id);

  for (int i = 0; i < Comm->size(); i++)
    {
      int m = max_id*i / Comm->size();
      int M = max_id*(i+1) / Comm->size();
      for (int j = m; j < M; j++)
        id_to_rank[j] = i;
    }

  meta_location = Comm->win_create(sizeof(int)*max_id, sizeof(int));
  meta_location_init = true;

  if (Comm->rank() == ROOT_COMM)
   {
      for (int j = 0; j < max_id; j++)
        meta_location.put<int>(j, -1);
   }

  for (int j = 0; j < max_id; j++)
    {
      Comm->log_root(str(format("id_to_rank[%d] = %d") % j % id_to_rank[j]));
    }

  Comm->log(str(format("globalMin = %d, globalMax = %d") % globalMin % globalMax));
  alpha.alloc(max_id);
  inv_variance.resize(max_id,max_id);
  meta_template.alloc(max_id);
  for (int j = globalMin; j < globalMax; j++)
    {
      meta_template[j].active_channels.alloc(numChannels);
      meta_template[j].active_channels.fill(0);
      meta_template[j].template_data.alloc(numChannels);
    }

  id_initialized = true;
}
