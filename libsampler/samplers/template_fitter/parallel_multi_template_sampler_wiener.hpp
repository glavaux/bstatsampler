/*+
This is ABYSS (./libsampler/samplers/template_fitter/parallel_multi_template_sampler_wiener.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __MPI_MULTI_TEMPLATE_SAMPLER_WIENER_HPP
#define __MPI_MULTI_TEMPLATE_SAMPLER_WIENER_HPP

#include <map>
#include <gsl/gsl_rng.h>
#include <string>
#include <list>
#include <vector>
#include <arr.h>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "sampler.hpp"
#include <gsl/gsl_matrix.h>
#include "mpi_communication.hpp"
#include <Eigen/Core>
#include <Eigen/Cholesky>

namespace CMB
{
  struct MPI_MetaTemplate
  {
    CMB_ChannelData template_data;
    arr<int> active_channels;
    ALM_Map wiener_map;
    bool prefiltered;
  };

  // This is the sampler of the unknown monopole and dipole foreground.
  // Its aim is to remove dipole foreground contamination for the CMB
  // sampler.
  class MPI_Multi_Template_Sampler_Wiener: public SingleSampler, public CacheCapable
  {
    enum MatrixTypeEnum {
      EIGEN_MATRIX, CHOLESKY_MATRIX
    };
  public:
    typedef Eigen::MatrixXd MatrixType;
    typedef Eigen::VectorXd VectorType;
    typedef Eigen::LLT<MatrixType> ChoType;
    typedef Eigen::ArrayXd ArrayType;

    MPI_Multi_Template_Sampler_Wiener(
                                      MPI_Communication *comm,
                                      std::string template_name,
                                      gsl_rng *r, CMB_Data& data,
                                      long nSide, long lmaxCMB,
                                      long template_lmax,
                                      long lmin,
                                      int template_signal_type,
                                      int numTemplates, bool dynjobs);
    virtual ~MPI_Multi_Template_Sampler_Wiener();

    void loadTemplates(CMB_Data& data, int set_id,
		       const std::vector<std::string> fnames, bool templates_are_maps);


    void set_CG_precision(double prec) {
      epsilon_CG = prec;
    }

    bool is_here(int id) const;

    virtual const CMB_Map *getEstimatedMap(int ch, int signal) const
    {
      if (signal != template_signal_type && signal != SAMPLER_SIGNAL_DEFAULT)
	return 0;

      return &estimated_map[ch];
    }

    virtual void saveCache(const std::string& prefix) { mpiSaveCache(prefix, false); }
    void mpiSaveCache(const std::string& prefix, bool local_save);
    virtual void loadCache(const std::string& prefix);

    virtual void saveState(NcFile& f);
    virtual void restoreState(NcFile& f) throw(StateInvalid);

    void generate_alpha(arr<double>& correl, double chi2_value, long chi2_dof);

    void setComputeFullChi2(bool on) { full_chi2 = on; }
    void setPositiveSign(bool positive);
    void setInitialSpectrum(const arr<double>& Cl);
    void setAutoRepeat(int a) { autorepeat = a; }
    void dumpResidual(bool a) { dump_residuals = a; }
    void doMarginalCMB(bool on) { do_wiener = on; }
    virtual void pinCMBspectrum(CMB_Data& data);
    // This is a one-shot call. It precomputes the cmb+noise-weighed template, that way the only required operation is a scalar product.
    // The template becomes insensitive to change in the spectrum sampler.

    void setLockedAmplitudes(const arr<std::list<std::string> >& locked_id);

    double getComponentValue(int id = 0) const { return  alpha[id]; }

    void set_TT_Cls(const arr<double>& temperature_spectrum);
    void setupPmatrix(const std::string& pmatrix_fname);

    double computeChi2(CMB_Data& data,
		       int id1, int id2,
		       CMB_ChannelData *in_datamap = 0);

    virtual const std::vector<int>& getModeledSignals() const;

    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers);

  protected:
    MPI_Communication *Comm;
    arr<int> global_to_local;
    int globalMin, globalMax;
    bool id_initialized, do_wiener;

    arr<CMB_Map> template_map;
    arr<CMB_Map> estimated_map;
    bool useBeam;
    long lmaxCMB, Nside, template_Lmax;
    double L_data_given_old;
    int template_signal_type;
    int autorepeat;
    int nside;
    long lmin;
    int numChannels, numTemplates;
    int currentMetaDone;
    double epsilon_CG;
    std::string template_name;
    bool preweighed, inv_variance_loaded, dump_residuals;
    bool do_filter_refresh;
    arr<double> initial_Cls,initial_SqCls;
    bool recoverMatrix;

    std::string cache_name;

    arr<double> eig_variance;
    arr<double> alpha;
    arr<std::list<int> > locked_id;
    arr<int> id_to_rank;
    arr<std::string> locked_id_name;
    std::map<std::string,int> locked_id_table;
    arr<MPI_MetaTemplate> meta_template;
    arr<double> Pmatrix;

    MPICC_Window meta_location;
    bool meta_location_init;

    bool dynamic_jobs;

    bool enforce_positive;

    MatrixType inv_variance;
    ChoType inv_variance_chol;
    VectorType base_rand;
    ArrayType backup_std_dev;

    bool full_chi2;
    MatrixTypeEnum matrix_type;

    bool doLoadCache();
    void computeInvVariance(CMB_Data& data);
    void finalizeVarianceMatrix();

    void uploadMeta(int id1, int dest_rank);
    MPI_MetaTemplate *downloadMeta(int from_rank, int id2);

    void saveRecoverMatrix(const std::string& prefix);
    bool loadAndProcessRecoverMatrix();

    void save_eigen_covar(const std::string& prefix);
    void save_cholesky_covar(const std::string& prefix);
    void read_eigen_covar(NcFile& f);
    void read_cholesky_covar(NcFile& f);

    VectorType generate_random_Cholesky(VectorType v);
    VectorType generate_random_Eigen(VectorType v);
    VectorType apply_inverse_Cholesky(VectorType v);
    VectorType apply_inverse_Eigen(VectorType v);
    VectorType apply_Cholesky(VectorType v);
    VectorType apply_Eigen(VectorType v);

    void initMetaId(int max_id);
    void initPmatrix();
    void setupMatrixType(MatrixTypeEnum mtype);

    VectorType (MPI_Multi_Template_Sampler_Wiener::*gen_random)(VectorType v);
    VectorType (MPI_Multi_Template_Sampler_Wiener::*apply_M_inverse)(VectorType v);
    VectorType (MPI_Multi_Template_Sampler_Wiener::*apply_M)(VectorType v);
    void (MPI_Multi_Template_Sampler_Wiener::*save_covar)(const std::string& n);
  };

};
#endif
