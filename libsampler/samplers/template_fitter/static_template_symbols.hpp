/*+
This is ABYSS (./libsampler/samplers/template_fitter/static_template_symbols.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#ifndef __TEMPLATE_FITTER_SYMBOLS_HPP
#define __TEMPLATE_FITTER_SYMBOLS_HPP

#include <string>

static const int ROOT_COMM = 0;
static const bool VERBOSE_OUTPUT_FILES = true;

static const int COVAR_MATRIX_CHOLESKY = 0;
static const int COVAR_MATRIX_EIGEN = 1;

static const std::string NETCDF_STD_DEV_NAME = "template_StdDev";
static const std::string NETCDF_INV_MATRIX_NAME ="template_InvVarMatrix";
static const std::string NETCDF_MATRIX_TYPE_ATTRIBUTE = "matrix_type";
static const std::string NETCDF_MATRIX_NAME = "template_VarMatrix";
static const std::string NETCDF_PMATRIX_SIZE = "num_MetaId";
static const std::string NETCDF_PMATRIX = "Pmatrix";


#endif
