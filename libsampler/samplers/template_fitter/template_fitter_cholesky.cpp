/*+
This is ABYSS (./libsampler/samplers/template_fitter/template_fitter_cholesky.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "sampler.hpp"
#include "noise_weighing.hpp"
#include <gsl/gsl_rng.h>
#include "parallel_multi_template_sampler_wiener.hpp"
#include "extra_map_tools.hpp"
#include "extra_alm_tools.hpp"
#include "cmb_rng.hpp"
#include "cl_sampler.hpp"
#include "noise_weighing.hpp"
#include <boost/format.hpp>
#include "netcdf_adapt.hpp"
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include "cg_relax.hpp"
#include <boost/format.hpp>
#include "wiener.hpp"
#include <boost/chrono.hpp>
#include "static_template_symbols.hpp"

#undef SELF_TEST

using namespace std;

using namespace CMB;
using boost::format;
using boost::chrono::system_clock;
using boost::chrono::duration;

MPI_Multi_Template_Sampler_Wiener::VectorType MPI_Multi_Template_Sampler_Wiener::generate_random_Cholesky(VectorType v)
{
  return inv_variance_chol.matrixL().solve(v);
}

MPI_Multi_Template_Sampler_Wiener::VectorType MPI_Multi_Template_Sampler_Wiener::apply_inverse_Cholesky(VectorType v)
{
  return inv_variance_chol.solve(v);
}

MPI_Multi_Template_Sampler_Wiener::VectorType MPI_Multi_Template_Sampler_Wiener::apply_Cholesky(VectorType v)
{
  return inv_variance*v;
}

void MPI_Multi_Template_Sampler_Wiener::save_cholesky_covar(const string& prefix)
{
  string fname = str(format("cache/%s_template_%s/covarMatrix.nc") % prefix % template_name);
  NcFile f(fname.c_str(), NcFile::replace);

  vector<NcDim> matrix_dim, dev_dim;

  matrix_dim.push_back(
	 f.addDim(
		  str(format("template_covar_matrix")),
		  inv_variance.size()
		  )
	 );
  dev_dim.push_back(
      f.addDim(
	       str(format("template_covar_dev")),
	       backup_std_dev.size()
		  )
    );
  NcVar inv_var_matrix_var = f.addVar(NETCDF_INV_MATRIX_NAME, ncDouble, matrix_dim);
  NcVar dev_var = f.addVar(NETCDF_STD_DEV_NAME, ncDouble, dev_dim);

  inv_var_matrix_var.putVar(inv_variance.data());
  dev_var.putVar(backup_std_dev.data());
  
  f.putAtt(NETCDF_MATRIX_TYPE_ATTRIBUTE, ncInt, COVAR_MATRIX_CHOLESKY);
}

void MPI_Multi_Template_Sampler_Wiener::read_cholesky_covar(NcFile& f)
{
  NcVar imatrix_var = f.getVar(NETCDF_INV_MATRIX_NAME);
  NcVar dev_var = f.getVar(NETCDF_STD_DEV_NAME);
  int Nv = alpha.size();

  backup_std_dev.resize(Nv);

  imatrix_var.getVar(inv_variance.data());
  dev_var.getVar(backup_std_dev.data());

  apply_M = &MPI_Multi_Template_Sampler_Wiener::apply_Cholesky;
  apply_M_inverse = &MPI_Multi_Template_Sampler_Wiener::apply_inverse_Cholesky;
  gen_random = &MPI_Multi_Template_Sampler_Wiener::generate_random_Cholesky;
  save_covar = &MPI_Multi_Template_Sampler_Wiener::save_cholesky_covar;
}


