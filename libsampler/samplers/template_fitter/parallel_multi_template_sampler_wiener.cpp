/*+
This is ABYSS (./libsampler/samplers/template_fitter/parallel_multi_template_sampler_wiener.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "sampler.hpp"
#include "noise_weighing.hpp"
#include <gsl/gsl_rng.h>
#include "parallel_multi_template_sampler_wiener.hpp"
#include "extra_map_tools.hpp"
#include "extra_alm_tools.hpp"
#include "cmb_rng.hpp"
#include "cl_sampler.hpp"
#include "noise_weighing.hpp"
#include <boost/format.hpp>
#include "netcdf_adapt.hpp"
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include "cg_relax.hpp"
#include <boost/format.hpp>
#include "wiener.hpp"
#include <boost/chrono.hpp>
#include "static_template_symbols.hpp"
#include "async_job.hpp"

#undef SELF_TEST

using namespace std;

using namespace CMB;
using boost::format;
using boost::chrono::system_clock;
using boost::chrono::duration;

MPI_Multi_Template_Sampler_Wiener::MPI_Multi_Template_Sampler_Wiener(MPI_Communication *comm,
                                                                     std::string template_name, gsl_rng *r,
					       CMB_Data& data, long nSide, long lmaxCMB, long template_lmax,
					       long lmin, int template_type,
					       int num_templates, bool dynjobs)
  : SingleSampler(r), numTemplates(num_templates), Comm(comm), do_wiener(true), dynamic_jobs(dynjobs)
{
  Comm->log_root(str(format("= Initializing template-based sampler (name=%s) =") % template_name));
  this->lmaxCMB = lmaxCMB;
  this->Nside = nSide;
  L_data_given_old = 0;
  template_signal_type = template_type;
  useBeam = true;
  save_covar = 0;
  epsilon_CG = 1e-3;
  enforce_positive = false;
  full_chi2 = false;
  recoverMatrix = false;
  dump_residuals = false;
  this->lmin = lmin;
  this->template_Lmax = template_lmax;
  this->template_name = template_name;
  this->cache_name = "";
  currentMetaDone = 0;

  if (template_Lmax < 0)
    template_Lmax = lmaxCMB;

  do_filter_refresh = false;
  preweighed = false;
  inv_variance_loaded = false;
  id_initialized = false;

  locked_id.alloc(data.numChannels*numTemplates);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      for (int c = 0; c < numTemplates; c++)
        locked_id[ch + c*data.numChannels].push_back(c);
    }

  alpha.alloc(numTemplates);
  inv_variance.resize(numTemplates,numTemplates);
  alpha.fill(0);

  numChannels = data.numChannels;

  base_rand.resize(numTemplates);

  estimated_map.alloc(numChannels);
  for (int ch = 0 ; ch < numChannels; ch++) {
    estimated_map[ch].SetNside(data.channels[ch].Nside(), RING);
    estimated_map[ch].fill(0);
  }

  initPmatrix();

  meta_location_init = false;
}


MPI_Multi_Template_Sampler_Wiener::~MPI_Multi_Template_Sampler_Wiener()
{
  if (meta_location_init)
    meta_location.destroy();
}

void MPI_Multi_Template_Sampler_Wiener::loadTemplates(CMB_Data& data, int template_id,
                                                      const std::vector<std::string> fnames, bool templates_are_maps)
{
  int Nv = alpha.size();
  CMB_Map this_map;
  ALM_Map tmp_alms, alms(template_Lmax, template_Lmax);

  if (fnames.size() != data.numChannels)
    {
      cerr << "The number of templates should be equal to the number of channels." << endl;
      Comm->abort();
    }

  Comm->comm_assert(id_initialized, "Locked amplitudes must have been setup before calling loadTemplates.");

  template_map.alloc(data.numChannels*numTemplates);

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      int idx = ch + data.numChannels * template_id;
      bool none_here = true;

      for (int meta_id = globalMin; meta_id < globalMax; meta_id++)
        {
          none_here = none_here && (Pmatrix[idx*Nv + meta_id] == 0);
        }

      if (none_here)
        {
          Comm->log(str(format("Template (%d,%d) not used") % ch % template_id));
          continue;
        }

      Comm->log(str(format("Loading template %s (channel=%d) ...") % fnames[ch] % ch));

      if (templates_are_maps)
        {
          read_Healpix_map_from_fits(fnames[ch], this_map);

          if (this_map.Order() == NEST)
            this_map.swap_scheme();
          HEALPIX_method(this_map, alms, data.weight);
        }
      else
        {
          read_Alm_from_fits(fnames[ch], alms, template_Lmax, template_Lmax);
        }


      tmp_alms = alms;
      tmp_alms.ScaleL(data.beam[ch]);
      template_map[idx].SetNside(data.channels[ch].Nside(), RING);

      alm2map(tmp_alms, template_map[idx]);

      for (int meta_id = globalMin; meta_id < globalMax; meta_id++) {
        MPI_MetaTemplate& meta = meta_template[meta_id];

        if (Pmatrix[idx*Nv + meta_id] == 0)
          continue;

        Comm->log(str(format("Filling meta-template '%s' with template=(%d,%d)") % locked_id_name[meta_id] % template_id % ch));
        Comm->comm_assert(meta.active_channels.size() > 0, str(format("Active channels not allocated for meta_id %d") % meta_id));
        if (!meta.active_channels[ch])
          meta.template_data[ch] = Pmatrix[idx*Nv + meta_id]*template_map[idx];
        else
          meta.template_data[ch] += Pmatrix[idx*Nv + meta_id]*template_map[idx];
        meta.active_channels[ch] = 1;
        meta.prefiltered = false;
      }
      template_map[idx].SetNside(1,RING);

      Comm->log("Done");

    }
}


void MPI_Multi_Template_Sampler_Wiener::uploadMeta(int id, int rank)
{
  int Nv = alpha.size();
  int tag = rank*Nv + id;

  assert(meta_template[id].active_channels.size() != 0);

  try
    {
      Comm->log(str(format("rank %d will require meta %d (use tag=%d)") % rank % id % tag));

      for (int ch = 0; ch < numChannels; ch++)
        {
          int nside = meta_template[id].template_data[ch].Nside();
          Comm->log(str(format("sending channel %d") % ch));
          Comm->send(&nside, 1, MPI_INT, rank, tag);
          Comm->log(str(format("  nside=%d...") % nside));
          if (nside != 0)
            {
              MPI_MetaTemplate& meta = meta_template[id];
              CMB_Map& t_data = meta.template_data[ch];
              double *data_template = &t_data[0];
              long Npix = t_data.Npix();
              Comm->log(str(format("  data_template = %p, Npix = %d") % data_template % Npix));
              Comm->send(data_template, Npix,
                         MPI_DOUBLE,
                         rank, tag);
              Comm->log("done sent");
            }
        }

      long wsize = (!meta_template[id].prefiltered) ? 0 : meta_template[id].wiener_map.Alms().size();
      long this_lmax = (!meta_template[id].prefiltered) ? 0 : meta_template[id].wiener_map.Lmax();
      Comm->log("sending wiener");
      Comm->send(&wsize, 1, MPI_LONG, rank, tag);
      Comm->send(&this_lmax, 1, MPI_LONG, rank, tag);
      if (wsize != 0)
        Comm->send((double *)meta_template[id].wiener_map.mstart(0), wsize*2, MPI_DOUBLE, rank, tag);

      Comm->send(&meta_template[id].active_channels[0], meta_template[id].active_channels.size(), MPI_INT, rank, tag);
    }
  catch (const MPI_Exception& e)
    {
      Comm->log(str(format("MPI Error while doing I/O: %s") % e.what()));
      Comm->abort();
    }
  Comm->log("conclude");
}

MPI_MetaTemplate *MPI_Multi_Template_Sampler_Wiener::downloadMeta(int from_rank, int id2)
{
  MPI_MetaTemplate *meta = new MPI_MetaTemplate;
  int tag = Comm->rank()*alpha.size() + id2;

  meta->template_data.alloc(numChannels);

  Comm->comm_assert(!is_here(id2), str(format("id2(%d) is already residing on this process") % id2));
  Comm->comm_assert(from_rank != Comm->rank(), "Trying to download from myself");
  Comm->log(str(format("Need the meta %d from rank %d (use tag=%d)") % id2 % from_rank % tag));

  for (int ch = 0; ch < numChannels; ch++)
    {
      int nside;

      Comm->log(str(format("Receiving meta, channel %d") % ch));
      Comm->recv(&nside, 1, MPI_INT, from_rank, tag);
      Comm->log(str(format("   nside=%d...") % nside));
      if (nside != 0)
        {
          CMB_Map& t_data = meta->template_data[ch];
          t_data.SetNside(nside, RING);

          double *data_template = &t_data[0];
          long Npix = t_data.Npix();
          Comm->log(str(format("data_template = 0x%p, Npix = %d") % data_template % Npix));
          Comm->recv(data_template, Npix,
                     MPI_DOUBLE, from_rank, tag);
          Comm->log("Got it... continue");
        }
    }

  long wsize;
  long this_lmax;
  Comm->log(str(format("receiving wiener filtered map of meta %d") % id2));
  Comm->recv(&wsize, 1, MPI_LONG, from_rank, tag);
  Comm->recv(&this_lmax, 1, MPI_LONG, from_rank, tag);
  if (wsize != 0)
    {
      meta->wiener_map.Set(this_lmax, this_lmax);
      Comm->comm_assert(meta->wiener_map.Alms().size() == wsize, "Incoherent size with peer");
      Comm->recv((double *)meta->wiener_map.mstart(0), 2*wsize, MPI_DOUBLE, from_rank, tag);
      meta->prefiltered = true;
    }
  else
    meta->prefiltered = false;

  meta->active_channels.alloc(numChannels);
  Comm->recv(&meta->active_channels[0], numChannels, MPI_INT,
             from_rank, tag);

  Comm->log("conclude");

  return meta;
}

bool MPI_Multi_Template_Sampler_Wiener::is_here(int id) const
{
  if (dynamic_jobs)
    return id_to_rank[id] == Comm->rank();
  else
    return (id >= globalMin && id < globalMax);
}

double MPI_Multi_Template_Sampler_Wiener::computeChi2(CMB_Data& data,int id1, int id2,
                                                      CMB_ChannelData *in_datamap)
{
  CMB_Map map1;
  ALM_Map alms1(template_Lmax, template_Lmax), alms2;
  ALM_Map
    weighed_alms1(template_Lmax, template_Lmax), tmp_alms(template_Lmax, template_Lmax);
  CMB_ChannelData *real_data;
  arr<int> active_data;
  MPI_MetaTemplate *tmp_meta;

  /* We need to compute here:
   *
   *  if ch1==ch2:
   *     data1 N^(-1) data2 - data1 N1^(-1) S^{1/2} ( ) ^{-1) S^{1/2} N2^(-1) data2
   *  else:
   *     - data1 N1^(-1) S^{1/2} ( ) ^{-1) S^{1/2} N2^(-1) data2
   *
   * data1 := template[ch1]
   * data2 := (in_datamap != 0) ?  *in_datamap  : template[ch2]
   *
   */

  if (in_datamap)
    {
      if (!is_here(id2))
        return 0;

      real_data = in_datamap;
      active_data.alloc(data.numChannels);
      active_data.fill(1);
      tmp_meta = 0;

    }
  else
    {
      // - id1 is here, id2 is not, we have to upload, and quit
      // - id1 is not here, id2 is not, do nothing
      // - id1 is not here, id2 is, we have to download id1, and process
      // - id1 is here, id2 is here, continue processing

      bool id1_here = is_here(id1), id2_here = is_here(id2);

      if (id1_here)
        {
          if (!id2_here)
            {
              uploadMeta(id1, id_to_rank[id2]);
              return 0;
            }
          tmp_meta = 0;
          real_data = &meta_template[id1].template_data;
          active_data = meta_template[id1].active_channels;
        }
      else
        {
          if (!id2_here)
            return 0;
          tmp_meta = downloadMeta(id_to_rank[id1], id1); // I grab id1 from id_to_rank[id1]

          real_data = &tmp_meta->template_data;
          active_data = tmp_meta->active_channels;
        }

    }

  if (do_wiener && !meta_template[id2].prefiltered)
    {
      Comm->log("   + Wiener filter ");
      arr<bool> cur_active(meta_template[id2].active_channels.size());
      for (int i = 0; i < data.numChannels; i++) cur_active[i] = meta_template[id2].active_channels[i] != 0;
      data.wiener_operator->setActiveChannels(cur_active);
      data.wiener_operator->do_filter(data, meta_template[id2].template_data, meta_template[id2].wiener_map, false);
    }

  Comm->log("   + Dot products");
  double product;
  double chi2 = 0;

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_Map map1;
      CMB_Map tmp_map2;
      ALM_Map w;
      double a, b = 0;

      if (!active_data[ch])
        continue;

      map1 = (*real_data)[ch];
      data.noise[ch]->apply_inv(map1);


      if (meta_template[id2].active_channels[ch])
        a = dot_product(map1, meta_template[id2].template_data[ch]).real();
      else
        a = 0;

      if (do_wiener)
        {
          w = meta_template[id2].wiener_map;
          w.ScaleL(data.beam[ch]);
          tmp_map2.SetNside(data.Nside, RING);
          alm2map(w, tmp_map2);

          b = dot_product(map1, tmp_map2).real();
        }
      chi2 += a-b;
    }

  if (tmp_meta != 0)
    delete tmp_meta;

  return chi2;
}


void MPI_Multi_Template_Sampler_Wiener::computeNewSample(CMB_Data& data,
                                                         const AllSamplers& samplers)
{
  const arr<DataType> *SqCls = &((CLS_Sampler*)(samplers[SAMPLER_CLS]))->getSqrtCls();
  const arr<DataType> *Cls = &((CLS_Sampler*)(samplers[SAMPLER_CLS]))->getCls();

  if (preweighed)
    {
      Cls = &initial_Cls;
      SqCls = &initial_SqCls;
    }

  cout << "++ Template " << template_name << " new sample ++" << endl;
  double chi2_value = 0;

  data.wiener_operator->setCls(*Cls);

  if (full_chi2)
    {
    }

  arr<double> correl;

  int Nv = alpha.size();
  correl.alloc(Nv);
  correl.fill(0);

  for (int id = 0; id < Nv; id++)
    {
      correl[id] = computeChi2(data, -1, id, &data.residuals);
    }


  if (!preweighed)
    {
      computeInvVariance(data);
    }

  Comm->accum(&correl[0], &correl[0], Nv, ROOT_COMM);
  if (Comm->rank() == ROOT_COMM)
    {
      long chi2_dof = (long)(data.numChannels*data.noise[0]->dof());

      generate_alpha(correl, chi2_value, chi2_dof);
    }

  Comm->broadcast(&alpha[0], alpha.size(), MPI_DOUBLE, ROOT_COMM);
  Comm->log("Got alphas");

  for (int ch = 0; ch < data.numChannels; ch++)
    {
      estimated_map[ch].fill(0);

      Comm->log("Building estimated map");
      for (int id = 0; id < Nv; id++)
        {
          if (!is_here(id) || !meta_template[id].active_channels[ch])
            continue;
          Comm->log(str(format(" -> Pushing %d") % id));
          estimated_map[ch] += alpha[id]*meta_template[id].template_data[ch];
        }

      Comm->log("Synchronizing");
      Comm->all_accum(&estimated_map[ch][0], estimated_map[ch].Npix());
      Comm->log(str(format("Got data for channel %d") % ch));

      if (dump_residuals && Comm->rank() == ROOT_COMM) {
        CMB_Map m = data.masks[ch] * (data.residuals[ch] - estimated_map[ch]);
        fitshandle f;
        string fname = str(format("!debug/residual_%s_channel_%d.fits") % template_name % ch);
        f.create(fname);
        write_Healpix_map_to_fits(f, m, planckType<double>());
      }
    }
  Comm->log("Concluding sample");
}

void MPI_Multi_Template_Sampler_Wiener::generate_alpha(arr<double>& correl, double chi2_value, long chi2_dof)
{
  for (int multi_gen = 0; multi_gen < autorepeat; multi_gen++) {
    bool done;
    int Nv = alpha.size();

    arr<double> mean_alpha(Nv);

    Eigen::Map<VectorType> base_rand_v(&alpha[0], Nv);
    Eigen::Map<VectorType> mv(&mean_alpha[0], Nv);

    do {
      for (int i = 0; i < Nv; i++)
        {
          alpha[i] = gsl_ran_gaussian(rGen, 1.0);
        }

      base_rand_v = (this->*gen_random)(base_rand_v);

      // Generation of random numbers

      done = true;

      memcpy(&mean_alpha[0], &correl[0], sizeof(double)*Nv);

      base_rand_v += (this->*apply_M_inverse)(mv);

    } while(!done);


    for (int i = 0; i<Nv; i++)
      {
        double iv = inv_variance(i,i);

        string fname = str(format("text/%s_sampler_i_%s.txt") % template_name % locked_id_name[i]);
        ofstream f(fname.c_str(), ios::app);
        f << (format("%20.10e %20.10e %20.10e %20.10e %20.10e") % alpha[i] % mean_alpha[i] % (iv) % (1/iv) % backup_std_dev[i]) << endl;
      }

    {
      double delta_chi2 = 0, chi2 = 0, result = 0;
      Eigen::Map<VectorType> correl_v(&correl[0], Nv);

      delta_chi2 = -2*mv.dot(correl_v);
      result = mv.dot((this->*apply_M)(mv));

      delta_chi2 += result;
      chi2 = delta_chi2 + chi2_value;
      string fname_chi2 = str(format("text/%s_chi2.txt") % template_name);
      ofstream f_chi2(fname_chi2.c_str(), ios::app);
      f_chi2 << (format("%20.10e %20.10e %20.10e %d %20.10e") % chi2_value % chi2 % delta_chi2 % chi2_dof % (chi2/chi2_dof)) << endl;
    }
  }
}

const std::vector<int>& MPI_Multi_Template_Sampler_Wiener::getModeledSignals() const
{
  static vector<int> signals;

  if (signals.size()==0)
    {
      signals.push_back(template_signal_type);
    }

  return signals;
}


void MPI_Multi_Template_Sampler_Wiener::saveState(NcFile& f)
{
  string dim_name = str(format("%s_sampler_dim") % template_name);
  string var_name = str(format("%s_sampler_alpha") % template_name);
  NcDim alpha_dim = f.addDim(dim_name, alpha.size());
  NcVar alpha_var = f.addVar(var_name, ncDouble, alpha_dim);

  alpha_var.putVar(&alpha[0]);
}

void MPI_Multi_Template_Sampler_Wiener::restoreState(NcFile& f)
  throw(StateInvalid)
{
  string var_name = str(format("%s_sampler_alpha") % (template_name));
  NcVar v = f.getVar(var_name);

  if (v.getDims()[0].getSize() != alpha.size())
    {
      throw StateInvalid("template_name");
    }

  v.getVar(&alpha[0]);

  for (int ch = 0; ch < numChannels; ch++)
    {
      estimated_map[ch].fill(0);

      for (int id = 0; id < alpha.size(); id++)
        {
          if (!meta_template[id].active_channels[ch])
            continue;

          estimated_map[ch] += alpha[id]*meta_template[id].template_data[ch];
        }
    }
}

void MPI_Multi_Template_Sampler_Wiener::setInitialSpectrum(const arr<double>& Cl)
{
  initial_Cls = Cl;
  initial_SqCls.alloc(Cl.size());
  for (int i = 0; i < initial_SqCls.size(); i++)
    initial_SqCls[i] = sqrt(initial_Cls[i]);
}

void MPI_Multi_Template_Sampler_Wiener::loadCache(const string& prefix)
{
  cache_name =  prefix;
}

static bool lazyCreateDirectory(const std::string& dirpath)
{
  boost::filesystem::path p(dirpath);

  if (boost::filesystem::is_directory(p))
    return true;

  return boost::filesystem::create_directories(p);
}

void MPI_Multi_Template_Sampler_Wiener::mpiSaveCache(const string& prefix, bool local_save)
{
  if (Comm->rank() == ROOT_COMM)
    {
      string info_path = str(format("cache/%s_template_%s/info.txt") % prefix % template_name);
      ofstream f(info_path.c_str());
      //f << rinfo.epsilonCG << endl;
    }
  for (int id = globalMin; id < globalMax; id++)
    {
      if (!meta_template[id].prefiltered)
        {
          continue;
        }

      string base_path = str(format("cache/%s_template_%s/meta_%s") % prefix % template_name % locked_id_name[id]);
      Comm->comm_assert(lazyCreateDirectory(base_path), "Cannot create directory for cache");

      for (int ch = 0; ch < numChannels; ch++)
        {
          fitshandle h;
          h.create(str(format("!%s/data_ch_%d.fits") % base_path % ch));
          write_Healpix_map_to_fits(h, meta_template[id].template_data[ch], planckType<double>());
        }

      {
        fitshandle h;
        h.create(str(format("!%s/wiener_alms.fits") % base_path));

        write_Alm_to_fits(h, meta_template[id].wiener_map, meta_template[id].wiener_map.Lmax(), meta_template[id].wiener_map.Lmax(), planckType<double>());
      }
    }

  if (local_save)
    return;

  if (Comm->rank() == ROOT_COMM && save_covar != 0)
    (this->*save_covar)(prefix);
}


bool MPI_Multi_Template_Sampler_Wiener::doLoadCache()
{
  int N = Alm_Base::Num_Alms(template_Lmax, template_Lmax);
  double *alm_arr = new double[N*2];

  try
    {

      Comm->log(str(format(" Attempting to load cache %s") % cache_name));

      {
        string info_path = str(format("cache/%s_template_%s/info.txt") % cache_name % template_name);
        ifstream f(info_path.c_str());
        double Wprec;

        f >> Wprec;
        do_filter_refresh = false;//(Wprec > rinfo.epsilonCG);
      }


      for (int id = globalMin; (id < globalMax); id++)
        {
          string base_path = str(format("cache/%s_template_%s/meta_%s") % cache_name % template_name % locked_id_name[id]);
          fitshandle h;
          ALM_Map wmap;
          bool skip_meta = false;

          meta_template[id].prefiltered = false;
          for (int ch = 0; ch < numChannels; ch++)
            {
              int colnum = 1, hdunum = 2;
              string fname = str(format("%s/data_ch_%d.fits") % base_path % ch);
              try
                {
                  int Nside;

                  h.open(fname);
                  h.goto_hdu(hdunum);
                  Nside = h.get_key<int>("NSIDE");
                  if (Nside == 0)
                    {
                      meta_template[id].template_data[ch] = CMB_Map();
                    }
                  else
                    read_Healpix_map_from_fits(h, meta_template[id].template_data[ch]);
                }
              catch (const PlanckError& e)
                {
                  Comm->log(str(format("Failed to load %s") % fname));
                  skip_meta = true;
                  break;
                }
             }
          if (skip_meta)
            continue;

          string fname = str(format("%s/wiener_alms.fits") % base_path);
          try
            {
              h.open(fname);
              h.goto_hdu(2);
              read_Alm_from_fits(h, wmap, template_Lmax, template_Lmax);
            }
          catch (const PlanckError& e)
            {
              Comm->log(str(format("Failed to load %s") % fname));
              continue;
            }
          Comm->log(str(format("Successfully loaded meta template %d") % id));
          meta_template[id].wiener_map.swap(wmap);
          meta_template[id].prefiltered = true;
        }

      try
        {
          string fname = str(format("cache/%s_template_%s/covarMatrix.nc") % cache_name % template_name);
          NcFile f(fname, NcFile::read);
          NcGroupAtt matrixType = f.getAtt(NETCDF_MATRIX_TYPE_ATTRIBUTE);
          int iType;

          if (matrixType.isNull())
            throw PlanckError("No matrix type defined");

          matrixType.getValues(&iType);

          if (iType == COVAR_MATRIX_EIGEN)
            read_eigen_covar(f);
          else if (iType == COVAR_MATRIX_CHOLESKY)
            read_cholesky_covar(f);
          else
            throw PlanckError("Unknown matrix type");
          inv_variance_loaded = true;
          cout << "Got the covariance matrix" << endl;
        }
      catch (const PlanckError& e)
        {
          inv_variance_loaded = false;
        }
      catch (const exceptions::NcException& e)
        {
          cout << "Exception while loading covariance matrix" << endl;//: " << e.what() << endl;
          // Absorb any exceptions there.
          inv_variance_loaded = false;
        }
    }
  catch (const PlanckError& e)
    {
      return false;
    }

  return true;
}

void MPI_Multi_Template_Sampler_Wiener::pinCMBspectrum(CMB_Data& data)
{
  bool attempt_cache = false;
  int Nv = alpha.size();
  int id;
  AsyncJobHelper *job_helper = 0;

  cout << format("++ Template %s precomputing weighed template ++") % template_name << endl;

  if (initial_Cls.size() == 0)
    {
      cerr << "Something is wrong: initial spectrum has 0-size." << endl;
      abort();
    }
  data.wiener_operator->setCls(initial_Cls);

  cout << "=> Precomputing wiener filtered meta templates for fixed CMB spectrum" << endl;

  if (cache_name != "")
    {
      Comm->log_root("Attempting loading cache file.");
      attempt_cache = doLoadCache();
      if (attempt_cache)
        Comm->log_root("  Cache (at least partially) loaded successfully.");
      else
        Comm->log_root("  Cache failure (weird...).");
    }

  if (dynamic_jobs)
    job_helper = new AsyncJobHelper(Comm, globalMax-globalMin);

  id = globalMin;
  while (true)
    {
      int job;

      if (dynamic_jobs)
        {
          job = job_helper->acquire_job();
          if (job < 0)
            break;
        }
      else
        {
          if (id == globalMax)
            break;
          job = id;
          id = id+1;
        }

      meta_location.put<int>(job, Comm->rank());

      // Check if it is already done
      if (meta_template[job].prefiltered)
        continue;

      cout << format("[%d] - Meta Template %s (%d / %d): wiener filtering ") % Comm->rank() % locked_id_name[job] % job % Nv << endl;

      string tmp_path = str(format("cache/debug/tmp_template_%s/meta_%s/wiener/") % template_name % locked_id_name[job]);
      Comm->comm_assert(lazyCreateDirectory(tmp_path), "Cannot create directory for wiener cache");
      if (do_wiener)
        {
          data.wiener_operator->set_restart_directory(tmp_path);
          data.wiener_operator->set_save_restart_directory(tmp_path);

          arr<bool> active(meta_template[job].active_channels.size());
          for (int i = 0; i < data.numChannels; i++) active[i] = meta_template[job].active_channels[i] != 0;
          data.wiener_operator->setActiveChannels(active);
          system_clock::time_point start = system_clock::now();
          data.wiener_operator->do_filter(data, meta_template[job].template_data, meta_template[job].wiener_map, cache_name != "");
          duration<double> sec = system_clock::now() - start;
          string fname = str(format("debug/multi_template_wiener_bench_%d.txt") % Comm->rank());
          ofstream f(fname.c_str(), ios::app);
          f << format("id=%d %20.10e") % job % sec << endl;
          data.wiener_operator->log_bench(f);
          f.close();
          meta_template[job].prefiltered = true;

        }
      {
        mpiSaveCache("debug/tmp", true);
      }
    }

  Comm->barrier();

  for (int j = 0; j < Nv; j++)
    id_to_rank[j] = meta_location.get<int>(j);

  preweighed = true;
  if (!inv_variance_loaded && !recoverMatrix)
    computeInvVariance(data);
  else if (recoverMatrix)
    if (!loadAndProcessRecoverMatrix())
      computeInvVariance(data);
  saveCache("debug/tmp");

  if (job_helper)
    delete job_helper;
}

void MPI_Multi_Template_Sampler_Wiener::computeInvVariance(CMB_Data& data)
{
  int Nv = alpha.size();
  inv_variance.setConstant(0);

  for (int id1 = 0; id1 < Nv; id1++)
    {
      for (int id2 = 0; id2 < Nv; id2++)
        {
          Comm->log(str(format(" * Correlate (meta %s) / (meta %s) *") % locked_id_name[id1] % locked_id_name[id2]));
          inv_variance(id1,id2) = computeChi2(data, id1, id2, 0);
          Comm->log(str(format("Done correlating %d / %d") % locked_id_name[id1] % locked_id_name[id2]));
        }
    }

  Comm->log("Done correlation, waiting for the others");
  Comm->barrier();
  Comm->log("Accumulating");
  Comm->accum(inv_variance.data(), inv_variance.data(), inv_variance.size(), ROOT_COMM);
  saveRecoverMatrix("debug/tmp");
  if (Comm->rank() == ROOT_COMM)
    finalizeVarianceMatrix();
  int iType = (int)matrix_type;
  Comm->broadcast(&iType, 1, MPI_INT, ROOT_COMM);
  setupMatrixType((MatrixTypeEnum)iType);
}

void MPI_Multi_Template_Sampler_Wiener::setPositiveSign(bool on)
{
  enforce_positive = on;
}


