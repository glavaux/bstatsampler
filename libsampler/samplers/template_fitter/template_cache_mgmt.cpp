/*+
This is ABYSS (./libsampler/samplers/template_fitter/template_cache_mgmt.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "parallel_multi_template_sampler_wiener.hpp"
#include "extra_map_tools.hpp"
#include "extra_alm_tools.hpp"
#include "noise_weighing.hpp"
#include "netcdf_adapt.hpp"
#include <boost/format.hpp>
#include "static_template_symbols.hpp"

#undef SELF_TEST

using namespace std;

using namespace CMB;
using boost::format;

void MPI_Multi_Template_Sampler_Wiener::loadCache(const string& prefix)
{
  cache_name =  prefix;
}

static bool lazyCreateDirectory(const std::string& dirpath)
{
  boost::filesystem::path p(dirpath);

  if (boost::filesystem::is_directory(p))
    return true;

  return boost::filesystem::create_directories(p);
}

void MPI_Multi_Template_Sampler_Wiener::mpiSaveCache(const string& prefix, bool local_save)
{
  if (Comm->rank() == ROOT_COMM)
    {
      string info_path = str(format("cache/%s_template_%s/info.txt") % prefix % template_name);
      ofstream f(info_path.c_str());
      //f << rinfo.epsilonCG << endl; 
    }
  for (int id = globalMin; id < globalMax; id++)
    {
      if (!meta_template[id].prefiltered)
        {
          continue;
        }

      string base_path = str(format("cache/%s_template_%s/meta_%s") % prefix % template_name % locked_id_name[id]);
      Comm->comm_assert(lazyCreateDirectory(base_path), "Cannot create directory for cache");

      for (int ch = 0; ch < numChannels; ch++)
        {
          fitshandle h;
          h.create(str(format("!%s/data_ch_%d.fits") % base_path % ch));
          write_Healpix_map_to_fits(h, meta_template[id].template_data[ch], planckType<double>());
        }

      {
        fitshandle h;
        h.create(str(format("!%s/wiener_alms.fits") % base_path));

        write_Alm_to_fits(h, meta_template[id].wiener_map, meta_template[id].wiener_map.Lmax(), meta_template[id].wiener_map.Lmax(), planckType<double>());
      }
    }

  if (local_save)
    return;

  if (eig_variance.size() == 0)
    save_cholesky_covar(prefix);
  else
    save_eigen_covar(prefix);
}


bool MPI_Multi_Template_Sampler_Wiener::doLoadCache()
{
  int N = Alm_Base::Num_Alms(template_Lmax, template_Lmax);
  double *alm_arr = new double[N*2];

  try
    {

      Comm->log(str(format(" Attempting to load cache %s") % cache_name));
 
      {
        string info_path = str(format("cache/%s_template_%s/info.txt") % cache_name % template_name);
        ifstream f(info_path.c_str());
        double Wprec;

        f >> Wprec;
        do_filter_refresh = false;//(Wprec > rinfo.epsilonCG);
      }


      for (int id = globalMin; (id < globalMax); id++)
        {
          string base_path = str(format("cache/%s_template_%s/meta_%s") % cache_name % template_name % locked_id_name[id]);
          fitshandle h;
          ALM_Map wmap;
          bool skip_meta = false;

          meta_template[id].prefiltered = false;
          for (int ch = 0; ch < numChannels; ch++)
            {
              int colnum = 1, hdunum = 2;
              string fname = str(format("%s/data_ch_%d.fits") % base_path % ch);
              try
                {
                  int Nside;
                  
                  h.open(fname);
                  h.goto_hdu(hdunum);
                  Nside = h.get_key<int>("NSIDE");
                  if (Nside == 0)
                    {
                      meta_template[id].template_data[ch] = CMB_Map();
                    }
                  else
                    read_Healpix_map_from_fits(h, meta_template[id].template_data[ch]);
                }
              catch (const PlanckError& e)
                {
                  Comm->log(str(format("Failed to load %s") % fname));
                  skip_meta = true;
                  break; 
                }
             }
          if (skip_meta) 
            continue;

          string fname = str(format("%s/wiener_alms.fits") % base_path);
          try
            {
              h.open(fname);
              h.goto_hdu(2);
              read_Alm_from_fits(h, wmap, template_Lmax, template_Lmax);
            }
          catch (const PlanckError& e)
            { 
              Comm->log(str(format("Failed to load %s") % fname));
              continue;
            }
          Comm->log(str(format("Successfully loaded meta template %d") % id));
          meta_template[id].wiener_map.swap(wmap);
          meta_template[id].prefiltered = true;
        }

      try
	      {
          string fname = str(format("cache/%s_template_%s/covarMatrix.nc") % cache_name % template_name);
          NcFile f(fname, NcFile::read);
          NcGroupAtt matrixType = f.getAtt(NETCDF_MATRIX_TYPE_ATTRIBUTE);
          int iType;

          if (matrixType.isNull())
            throw PlanckError("No matrix type defined");

          matrixType.getValues(&iType);
          
          if (iType == COVAR_MATRIX_EIGEN)
            read_eigen_covar(f);
          else if (iType == COVAR_MATRIX_CHOLESKY)
            read_cholesky_covar(f);
          else
            throw PlanckError("Unknown matrix type");
          inv_variance_loaded = true;
          cout << "Got the covariance matrix" << endl;
        }
      catch (const PlanckError& e)
        {
          inv_variance_loaded = false;
        }
      catch (const exceptions::NcException& e)
        {
          cout << "Exception while loading covariance matrix: " << e.what() << endl;
          // Absorb any exceptions there.
          inv_variance_loaded = false;
        }
    }
  catch (const PlanckError& e)
    {
      return false;
    }

  return true;
}

