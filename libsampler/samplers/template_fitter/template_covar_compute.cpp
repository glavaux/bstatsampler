/*+
This is ABYSS (./libsampler/samplers/template_fitter/template_covar_compute.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include "extra_map_tools.hpp"
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "sampler.hpp"
#include "noise_weighing.hpp"
#include <gsl/gsl_rng.h>
#include "parallel_multi_template_sampler_wiener.hpp"
#include "extra_map_tools.hpp"
#include "extra_alm_tools.hpp"
#include "cmb_rng.hpp"
#include "cl_sampler.hpp"
#include "noise_weighing.hpp"
#include <boost/format.hpp>
#include "netcdf_adapt.hpp"
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_errno.h>
#include "cg_relax.hpp"
#include <boost/format.hpp>
#include "wiener.hpp"
#include <boost/chrono.hpp>
#include "static_template_symbols.hpp"

#undef SELF_TEST

using namespace std;

using namespace CMB;
using boost::format;
using boost::chrono::system_clock;
using boost::chrono::duration;

void MPI_Multi_Template_Sampler_Wiener::finalizeVarianceMatrix()
{
  MatrixType M;
  int N = alpha.size();

  backup_std_dev.resize(alpha.size());
//  for (int i = 0; i < alpha.size(); i++)
//    backup_std_dev[i] = sqrt(inv_variance(i,i));

  if (Comm->rank() == ROOT_COMM)
    {
      string fname0 = str(format("debug/id_table_%s.txt") % template_name);
      ofstream f0(fname0.c_str());
      
      for (int i = 0; i < alpha.size(); i++)
        {
          f0 << i << " " << locked_id_name[i] << endl;
        }
        
      string fname = str(format("debug/inv_covariance_matrix_%s.txt") % template_name);
      ofstream f(fname.c_str());
      for (int i = 0; i < alpha.size(); i++)
        {
          for (int j = 0; j < alpha.size(); j++)
            {
              f << format("% 25.25lg ") % M(i,j);
            }
          f << endl;
        }
    }

  inv_variance_chol = inv_variance.llt();

  if (inv_variance_chol.info() != Eigen::Success)
    {
      Comm->log_root("Cholesky decomposition failed. Stopping here.");
      Comm->abort();
      
      matrix_type = EIGEN_MATRIX;
    }
  else
    {
      for (int i = 0; i < alpha.size(); i++)
        {
          VectorType v = inv_variance_chol.solve(VectorType::Unit(alpha.size(), i));
          backup_std_dev[i] = v[i];
        }
      matrix_type = CHOLESKY_MATRIX;
    }
}


void MPI_Multi_Template_Sampler_Wiener::setupMatrixType(MatrixTypeEnum mtype)
{
  int Nv = alpha.size();

  switch (mtype) {
  case CHOLESKY_MATRIX:
    apply_M = &MPI_Multi_Template_Sampler_Wiener::apply_Cholesky;
    apply_M_inverse = &MPI_Multi_Template_Sampler_Wiener::apply_inverse_Cholesky;
    gen_random = &MPI_Multi_Template_Sampler_Wiener::generate_random_Cholesky;
    save_covar = &MPI_Multi_Template_Sampler_Wiener::save_cholesky_covar;

    if (Comm->rank() != ROOT_COMM)
      inv_variance.resize(Nv,Nv);
    Comm->broadcast(inv_variance.data(), inv_variance.size(), MPI_DOUBLE, ROOT_COMM);

    if (Comm->rank() != ROOT_COMM)
      backup_std_dev.resize(Nv);
    Comm->broadcast(backup_std_dev.data(), Nv, MPI_DOUBLE, ROOT_COMM);
    break;
  case EIGEN_MATRIX:
    Comm->comm_assert(false, "Unsupported");
    apply_M = &MPI_Multi_Template_Sampler_Wiener::apply_Eigen;
    apply_M_inverse = &MPI_Multi_Template_Sampler_Wiener::apply_inverse_Eigen;
    gen_random = &MPI_Multi_Template_Sampler_Wiener::generate_random_Eigen;
    save_covar = &MPI_Multi_Template_Sampler_Wiener::save_eigen_covar;
    break;
  default:
   Comm->abort();
  }
}
