/*+
This is ABYSS (./libsampler/samplers/mpi/job_helper.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __JOB_HELPER_HPP
#define __JOB_HELPER_HPP

namespace CMB
{
  class TemplateJobHelper;
  
  struct TemplateJobIterator
  {
    int ch;
    int t;
    int job;
    TemplateJobHelper *helper;

    bool operator!=(const TemplateJobIterator& i2)
    {
      return (job!=i2.job);
    }

    bool operator==(const TemplateJobIterator& i2)
    {
      return (job==i2.job);
    }

    TemplateJobIterator& operator++();
    TemplateJobIterator& operator--();

  };


  class TemplateJobHelper
  {
  public:
    typedef TemplateJobIterator iterator;

    TemplateJobHelper(int numChannels, int numTemplates,
		      int rank, int size)
    {
      int endJob;

      this->numChannels = numChannels;
      this->numTemplates = numTemplates;
      commRank = rank;
      commSize = size;
      startJob = numChannels*numTemplates*(rank)/size;
      endJob = numChannels*numTemplates*(rank+1)/size;
      numJobs = endJob-startJob;
      
      startTemplate = startJob / numChannels;
      startChannel = startJob - startTemplate*numChannels;

      endTemplate = endJob / numChannels;
      endChannel = endJob - endTemplate*numChannels;
    }

    ~TemplateJobHelper() {}

    int getHandler(int t, int ch) const
    {
      int j = t * numChannels + ch;

      for (int r = 0; r < commSize; r++)
	{
	  int sj = numChannels*numTemplates*(r)/commSize;
	  int ej = numChannels*numTemplates*(r+1)/commSize;
	  
	  if (j >= sj  && j < ej)
	    return r;
	}
      return -1;
    }

    int size() const { return numJobs; }

    iterator begin()
    {
      iterator i;

      i.ch = startChannel;
      i.t = startTemplate;
      i.job = 0;
      i.helper = this;

      return i;
    }

    iterator end()
    {
      iterator i;

      i.ch = startChannel;
      i.t = startTemplate;
      i.job = numJobs;
      i.helper = this;

      return i;
    }

    int getJob(int t, int ch) const 
    {
      int job =  ch + t*numChannels - startJob;

      return (job < 0 || job >= numJobs) ? -1 : job;
    }

    //  private:
  public:
    int numChannels, numTemplates;
    int commRank, commSize;
    int numJobs, startJob;
    int startChannel, startTemplate;
    int endChannel, endTemplate;

    friend struct TemplateJobIterator;
  };

  inline TemplateJobIterator& TemplateJobIterator::operator++()
    {
      ++ch;
      ++job;
      if (ch == helper->numChannels)
	{
	  ch = 0;
	  t++;
	}
      return *this;
    }

  inline TemplateJobIterator& TemplateJobIterator::operator--()
    {
      --ch;
      --job;
      if (ch == 0)
	{
	  ch = helper->numChannels-1;
	  t--;
	}
      return *this;
    }

};

#endif
