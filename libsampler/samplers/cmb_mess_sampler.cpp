/*+
This is ABYSS (./libsampler/samplers/cmb_mess_sampler.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include "mpi_communication.hpp"
#include <fstream>
#include <iostream>
#include <cmath>
#include <cassert>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "cmb_mess_sampler.hpp"
#include "cl_sampler.hpp"
#include <alm_healpix_tools.h>
#include "extra_alm_tools.hpp"
#include "conjugateGradient.hpp"
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <alm_fitsio.h>
#include "noise_weighing.hpp"
#include "cmb_data.hpp"
#include "extra_map_tools.hpp"
#include "netcdf_adapt.hpp"
#include <fitshandle.h>
#include <healpix_map_fitsio.h>

//#define INTEGRITY_CHECK
//#define WIENER_TEST

using namespace std;
using namespace CMB;
using boost::str;
using boost::format;

static vector<int> cmb_signals;

static const double temperature0 = 1.0;

  // This is the sampler of the CMB signal
  // It takes Cls, generates some random a_lm and
  // produce a corresponding
  // CMB sky map.
CMB_Mess_Sampler::CMB_Mess_Sampler(gsl_rng *r, const string& name, MPI_Communication *comm, CMB_Data& data,
			 long nL, long nSide, int numBath, double deltaBath) 
  : SingleSampler(r, name), 
    Comm(comm),
    proposed_map(nSide, RING, SET_NSIDE),
    tau(data.numChannels),
    noise_tilde(data.numChannels),
    estimated_map(data.numChannels),
    noise_tilde_passthrough(data.numChannels)
{
  state.messengers.alloc(data.numChannels);
  state.alms.Set(data.Lmax, data.Lmax);
  this->numBath = numBath;
  this->deltaBath = deltaBath;
  alpha_N = sqrt(proposed_map.Npix()/(4*M_PI));
  state.alms.SetToZero();
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_NN_NoiseMap *inv_noise = data.noise[ch]->as<CMB_NN_NoiseMap>();
      CMB_Map& mm = state.messengers[ch];
      
      estimated_map[ch].SetNside(nSide, RING);
      fill(&estimated_map[ch][0], &estimated_map[ch][estimated_map[ch].Npix()], 0);
      mm.SetNside(nSide, RING);
      fill(&mm[0], &mm[mm.Npix()], 0);

      tau[ch] = 0;
      for (long p = 0; p < inv_noise->Npix(); p++)
        tau[ch] = max(tau[ch], (*inv_noise)[p]);

      tau[ch] = 1/tau[ch];

      Comm->log(str(format("sqrt(Tau[%d]) = %lg") % ch % sqrt(tau[ch])));

      noise_tilde[ch].SetNside(data.Nside, RING);
      noise_tilde_passthrough[ch].SetNside(data.Nside, RING);
      noise_tilde[ch].masked = 0;
      for (long p = 0; p < inv_noise->Npix(); p++)
        {
          if ((*inv_noise)[p] == 0)
            {
              noise_tilde[ch][p] = 0;
              noise_tilde[ch].masked++;
              noise_tilde_passthrough[ch][p] = false;
            }
          else
            {
              double delta = 1/((*inv_noise)[p]) - tau[ch];

              assert(delta>=0);
              if (delta == 0)
                {
                  noise_tilde_passthrough[ch][p] = true;
                  noise_tilde[ch][p] = 0;
                }
              else
                {
                  noise_tilde_passthrough[ch][p] = false;
                  noise_tilde[ch][p] = 1/delta;
                }
            }
        }
    }
  generate_ran_alms(state.alms, 1.0);
}

CMB_Mess_Sampler::~CMB_Mess_Sampler()
{
}


void CMB_Mess_Sampler::saveState(NcFile& f)
{
  string dimname = str(format("%s_sampler_cmb_dim") % m_name);
  string cleanname = str(format("%s_sampler_cmb_clean") % m_name);
  NcDim cmb_dim = f.addDim(dimname.c_str(), proposed_map.Npix());
  NcVar cmb_clean = f.addVar(cleanname.c_str(), ncDouble, cmb_dim);

  cmb_clean.putVar(&proposed_map[0]);  

  for (int ch = 0; ch < state.messengers.size(); ch++)
  {
    string messname = str(format("%s_sampler_messenger_%d") % m_name % ch);
    NcVar messvar = f.addVar(messname.c_str(), ncDouble, cmb_dim);
    messvar.putVar(&state.messengers[ch][0]);
  }
}

void CMB_Mess_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  string cleanname = str(format("%s_sampler_cmb_clean") % m_name);
  NcVar cmb_clean = f.getVar(cleanname.c_str());
  std::vector<NcDim> dims = cmb_clean.getDims();

  if (dims[0].getSize() != proposed_map.Npix())
    throw StateInvalid("CLS");

  cmb_clean.getVar(&proposed_map[0]);
}

void CMB_Mess_Sampler::generate_ran_alms(ALM_Map& alms, double temperature)
{
  double hsqrt2 = 1/sqrt(2.0);
  double Q = 1/sqrt(temperature);
  for (int l = 0; l <= alms.Lmax(); l++)
    {
      double zetar, zetai;

      zetar = gsl_ran_gaussian(rGen, 1.0);
      alms(l,0) = zetar*Q;
      for (int m = 1; m <= min(alms.Mmax(),l); m++)
        {
          zetar = gsl_ran_gaussian(rGen, hsqrt2*Q);
          zetai = gsl_ran_gaussian(rGen, hsqrt2*Q);
          alms(l,m) = xcomplex<DataType>(zetar, zetai);
        }
    }
}

void CMB_Mess_Sampler::generate_mock_data_cmb(CMB_Data& data, const arr<DataType>& sqcls,
                                              ALM_Map& bare_alms, Messenger_State& this_state,
                                              arr<CMB_Map>& delta_mock, double temperature)
{
  CMB_Map noise;

  delta_mock.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    delta_mock[ch].SetNside(data.Nside, RING);

  noise.SetNside(data.Nside, RING);

  ALM_Map tmp_alms(data.Lmax, data.Lmax);
  for (int ch = 0; ch < data.numChannels; ch++)
   {
     tmp_alms = bare_alms;
     tmp_alms.ScaleL(sqcls);
     tmp_alms.ScaleL(data.beam[ch]);
     alm2map(tmp_alms, delta_mock[ch]);

     makeRandomMap(rGen, noise);
     double TauDev = sqrt(tau[ch]/temperature);
     for (long p = 0; p < delta_mock[ch].Npix(); p++)
       delta_mock[ch][p] = this_state.messengers[ch][p] - delta_mock[ch][p] - TauDev*noise[p];
   }
}

void CMB_Mess_Sampler::compute_messenger_filter(CMB_Data& data,
                                                const arr<DataType>& sqcls,
                                                Messenger_State& this_state,
                                                CMB_Map& refmap, int ch)
{
  ALM_Map tmp_alms = this_state.alms;
  CMB_Map s_delta(data.Nside, RING, SET_NSIDE);

  tmp_alms.ScaleL(sqcls);
  tmp_alms.ScaleL(data.beam[ch]);
  alm2map(tmp_alms, s_delta);

  double inv_tau = 1/tau[ch];

  for (long idx = 0; idx < this_state.messengers[ch].Npix(); idx++)
    {
      if (noise_tilde_passthrough[ch][idx])
        this_state.messengers[ch][idx] = refmap[idx];
      else {
        double a = noise_tilde[ch][idx]*refmap[idx] + s_delta[idx]*inv_tau;
        this_state.messengers[ch][idx] = a / (noise_tilde[ch][idx] + inv_tau);
      }
    }
}

void CMB_Mess_Sampler::generate_mock_data_messengers(CMB_Data& data, arr<CMB_Map>& ran_messenger, arr<CMB_Map>& mock_data, double temperature)
{
  double Q = 1/sqrt(temperature);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      ran_messenger[ch].SetNside(data.Nside, RING);
      mock_data[ch].SetNside(data.Nside, RING);
      makeRandomMap(rGen, ran_messenger[ch]);
      ran_messenger[ch] *= sqrt(tau[ch]);
      makeRandomMap(rGen, mock_data[ch]);
      noise_tilde[ch].apply_fwd_half(mock_data[ch]);
      for (long p = 0; p < mock_data[ch].Npix(); p++)
        mock_data[ch][p] = data.residuals[ch][p] - Q*(mock_data[ch][p] - ran_messenger[ch][p]);
    }
}

void CMB_Mess_Sampler::compute_cmb_filter(CMB_Data& data,
                                          const arr<DataType>& sqcls,
                                          Messenger_State& this_state,
                                          arr<CMB_Map>& mock_data)
{
  ALM_Map tmp_alms(data.Lmax, data.Lmax);
  arr<double> filter(data.Lmax+1);

  for (int l = 0; l <= data.Lmax; l++)
    filter[l] = 1;

  this_state.alms.SetToZero();
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      double alpha = 12.0*data.Nside*data.Nside/(4*M_PI);
      double Q = alpha/tau[ch];

      HEALPIX_method(mock_data[ch], tmp_alms, data.weight);

      tmp_alms.Scale(Q);
      tmp_alms.ScaleL(data.beam[ch]);
      this_state.alms.Add(tmp_alms);

      for (int l = 0; l <= data.Lmax; l++)
        filter[l] += Q*data.beam[ch][l]*data.beam[ch][l]*sqcls[l]*sqcls[l];
    }
  this_state.alms.ScaleL(sqcls);

  for (int l = 0; l <= data.Lmax; l++)
    filter[l] = 1/filter[l];

  this_state.alms.ScaleL(filter);
}

void CMB_Mess_Sampler::do_messenger_sampling(
       CMB_Data& data, const arr<DataType>& sqcls,
       Messenger_State& this_state, double temperature, bool reverse)
{
  arr<CMB_Map> ran_messenger(data.numChannels), mock_data(data.numChannels);
  CMB_Map s_delta;
  ALM_Map bare_alms(data.Lmax, data.Lmax);

  s_delta.SetNside(data.Nside, RING);

  if (!reverse)
  {
    generate_mock_data_messengers(data, ran_messenger, mock_data, temperature);
  
    // Filter
    for (int ch = 0; ch < data.numChannels; ch++)
    {
      compute_messenger_filter(data, sqcls, this_state, mock_data[ch], ch);
      this_state.messengers[ch] += ran_messenger[ch];
    }

    generate_ran_alms(bare_alms, temperature);
    generate_mock_data_cmb(data, sqcls, bare_alms, this_state, mock_data, temperature);
    compute_cmb_filter(data, sqcls, this_state, mock_data);
    this_state.alms.Add(bare_alms);
  }
  else
  {
    generate_ran_alms(bare_alms, temperature);
    generate_mock_data_cmb(data, sqcls, bare_alms, this_state, mock_data, temperature);
    compute_cmb_filter(data, sqcls, this_state, mock_data);
    this_state.alms.Add(bare_alms);

    generate_mock_data_messengers(data, ran_messenger, mock_data, temperature);
    for (int ch = 0; ch < data.numChannels; ch++)
    {
      compute_messenger_filter(data, sqcls, this_state, mock_data[ch], ch);
      this_state.messengers[ch] += ran_messenger[ch];
    }
  }
}

static double tau_w(double tau, double a, double b)
{
  return a*b*tau;
}

double CMB_Mess_Sampler::computeLikelihood(CMB_Data& data, const arr<DataType>& sqcls,
                                         Messenger_State& this_state)
{
  arr<double> chi2_d(data.numChannels), chi2_s(data.numChannels);
  double chi2_a;

  for (int ch = 0; ch < data.numChannels; ch++)
  {
    int masked = 0;
    CMB_Map delta = data.residuals[ch] - this_state.messengers[ch];

    for (int p = 0; p < delta.Npix(); p++)
    {
      if (noise_tilde_passthrough[ch][p])
        delta[p] = 0;
      else
      {
        delta[p] = noise_tilde[ch][p]*delta[p]*delta[p];
        masked += (noise_tilde[ch][p]!=0) ? 1 : 0;
      }
    }

    clean_sum(delta, delta.Npix());
    chi2_d[ch] = delta[0];
    Comm->log(str(format("chi2_d[ch=%d]=%lg") % ch % (chi2_d[ch]/masked)));

    ALM_Map s = this_state.alms;
    s.ScaleL(sqcls);
    s.ScaleL(data.beam[ch]);
    alm2map(s, estimated_map[ch]);

    delta = estimated_map[ch] - this_state.messengers[ch];
    clean_sum_op(delta, delta, delta.Npix(), boost::bind(tau_w, 1/tau[ch], _1, _2));

    chi2_s[ch] = delta[0];
    Comm->log(str(format("chi2_s[ch=%d]=%lg") % ch % (chi2_s[ch]/delta.Npix())));;
  }

  chi2_a = dot_product_simple(this_state.alms, this_state.alms).real();
  Comm->log(str(format("chi2_a=%lg") % (chi2_a/this_state.alms.Alms().size())));

  clean_sum(chi2_d, chi2_d.size());
  clean_sum(chi2_s, chi2_s.size());

  Comm->log(str(format("chi2_d = %lg, chi2_s = %lg, chi2_a = %lg") % chi2_d[0] % chi2_s[0] % chi2_a));

  return chi2_d[0]+chi2_s[0]+chi2_a;
}

void CMB_Mess_Sampler::computeNewSample(CMB_Data& data,
				   const AllSamplers& samplers)
{
  // We do not use the observational data here.
  arr<DataType> sqcls = data.guess_sqrt_cls;
    //dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getSqrtCls();
  arr<DataType> cls = data.guess_cls;
    //dynamic_cast<CLS_Sampler *>(samplers[SAMPLER_CLS])->getCls();

  if (Comm->rank() != 0)
  {
    for (int ch = 0; ch < data.numChannels; ch++)
      Comm->broadcast(&estimated_map[ch], estimated_map[ch].Npix(), MPI_DOUBLE, 0);
    return;
  }

  sqcls[0] = sqcls[1] = 0;
  cls[0] = cls[1] = 0;

  cout << "In CMB_Sampler" << endl;

  assert(sqcls.size() >= data.Lmax);

  Messenger_State tmp_state;
  arr<double> likelihood(numBath);
  double Q = 1+deltaBath;
  double R = 1-1/Q;

  tmp_state = state;

#if 1
  for (int b = 1; b <= numBath; b++)
  {
    double beta = temperature0*pow(Q,-b);

    Comm->log(str(format("[T %d -> %d] Bath at beta=%lg") % (b-1) % b % beta));
    do_messenger_sampling(data, sqcls,
                          tmp_state, beta, false);
    likelihood[b-1] = beta*R*computeLikelihood(data, sqcls, tmp_state);
  }

  for (int b = numBath; b >= 1; b--)
  {
    double beta = temperature0*pow(Q,-b);
    Comm->log(str(format("[T %d -> %d] Bath at beta=%lg") % (b) % (b-1) % beta));
    do_messenger_sampling(data, sqcls,
                          tmp_state, beta, true);
    likelihood[b-1] -= beta*R*computeLikelihood(data, sqcls, tmp_state);
    Comm->log(str(format("Delta likelihood = %lg") % likelihood[b-1]));
  }

  double D = 0;
  for (int b = 0; b < numBath; b++)
    D += likelihood[b];

  Comm->log(str(format("Final Delta chi2: D = %lg") % D));
  
  if (D < 0 || gsl_rng_uniform(rGen) < exp(-0.5*D))
  {
    // Accept the entire move
    Comm->log("Accepted move");
    state = tmp_state;
  }
  else
  {
    Comm->log("Rejected move");
  }
#endif

  // Do an extra sampling at beta=1, to continue moving
  // whatever happens in the tempered transition.

  Comm->log("Final messenger sampling");
  do_messenger_sampling(data, sqcls, state, temperature0, false);
  Comm->log("Generating proposed map");
  computeLikelihood(data, sqcls, state);
  for (int ch = 0; ch < data.numChannels; ch++)
  {
    ALM_Map bare_alms = state.alms;
    bare_alms.ScaleL(sqcls);
    bare_alms.ScaleL(data.beam[ch]);
    alm2map(bare_alms, estimated_map[ch]);

    Comm->broadcast(&estimated_map[ch], estimated_map[ch].Npix(), MPI_DOUBLE, 0);
  }

  {
    static int iter = 0;

    if ((iter%1)==0) {
      ALM_Map bare_alms = state.alms;
      bare_alms.ScaleL(sqcls);
      CMB_Map proposed_map;

      proposed_map.SetNside(data.Nside, RING);
     alm2map(bare_alms, proposed_map);

    int j = iter/1;
    fitshandle f;
    f.create(str(format("!debug/proposed_mess_cmb_i%d.fits") % j));
    
    write_Healpix_map_to_fits(f, proposed_map, planckType<double>());

    f.create(str(format("!debug/proposed_mess_alms_i%d.fits") % j));
    write_Alm_to_fits(f, bare_alms, bare_alms.Lmax(), bare_alms.Mmax(), planckType<double>());
    }
    iter++;
  }
  for (int ch = 0; ch < data.numChannels; ch++)
  {
    fitshandle f;
    f.create(str(format("!debug/messenger_ch_%d.fits") %ch));

    write_Healpix_map_to_fits(f, state.messengers[ch], planckType<double>());
  }
}

const std::vector<int>& CMB_Mess_Sampler::getModeledSignals() const
{
  if (cmb_signals.size()==0)
    {
      cmb_signals.push_back(SAMPLER_SIGNAL_PROPOSED_CMB);
    }

  return cmb_signals;
}
