/*+
This is ABYSS (./libsampler/samplers/aberration_sampler.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cassert>
#include <omp.h>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include <vector>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "cl_sampler.hpp"
#include "extra_map_tools.hpp"
#include "aberration_sampler.hpp"
#include "conjugateGradient.hpp"
#include "cmb_rng.hpp"
#include "wigner3j.hpp"
#include "lensing_operator.hpp"
#include "netcdf_adapt.hpp"

using namespace std;
using namespace CMB;

Aberration_Sampler::~Aberration_Sampler()
{
}

Aberration_Sampler::Aberration_Sampler(gsl_rng *r, CMB_Data& data,
				       long nSide)
  : SingleSampler(r), Cls_CMB(data.Lmax+1), Cls(data.Lmax+1), modalm(1,1)
{
  Cls_CMB.fill(0);
  Cls.fill(0);

  modalm.SetToZero();

  L_cmb_given_mulling = -INFINITY;
  skyCounter = 0;
  epsilon_CG = 5e-6;
  weight_alm.alloc(data.numChannels);
  filtered_alm.alloc(data.numChannels);
  for (int i = 0; i < data.numChannels; i++)
    {
      weight_alm[i].Set(data.Lmax, data.Lmax);
      filtered_alm[i].Set(data.Lmax, data.Lmax);
    }
}

void Aberration_Sampler::preweightData(CMB_Data& data,
                                       CMB_ChannelData& raw_data,
				       ALM_Map& walms,
				       int i,
				 MapSignalNoiseSignal_weighing& matrix)
{  
  CMB_Map map1 = raw_data[i],map2(data.NsideHR, RING, SET_NSIDE);
  arr<double> w2(2*data.NsideHR);
  w2.fill(1);

  cerr << "   + Noise" << endl;
  data.noise[i]->apply_inv(map1);
  cerr << "   + Modulation" << endl;
  assert(data.transform != 0);
  HEALPIX_method(map1, walms, data.weight);
  walms.ScaleL(data.beam[i]);
  alm2map(walms, map1);
  data.transform->applyTranspose(map1, map2);
  cerr << "   + Spherical harmonic transform" << endl;
  HEALPIX_method(map2, walms, w2);  
  cerr << "   + Scaling" << endl;
  walms.ScaleL(matrix.sqrt_cls);
  walms *= (map2.Npix()/(4*M_PI));
}

void Aberration_Sampler::computeFilteredData(CMB_Data& data,
					     ALM_Map& alms,
					     ALM_Map& filtered_alms,
					     MapSignalNoiseSignal_weighing& matrix)
{
  conjugateGradient_pre(alms, filtered_alms, matrix, *data.preconditioner, epsilon_CG);
}
				       
double Aberration_Sampler::computeLikelihood(CMB_Data& data,
					     CMB_ChannelData& raw_data,
					     const arr<ALM_Map> & weighted_alms,
					     const arr<ALM_Map> & filtered_alms
					     )
{
  CMB_Map map1;
  double chi2_1, chi2 = 0;

  cerr << " ==== COMPUTING LIKELIHOOD ====" << endl;
  // No multi-channel support yet !!!
 
  for (int i = 0; i < data.numChannels; i++)
    {
      double chi2_1b, dof;

      map1 = raw_data[i];
      data.noise[i]->apply_inv(map1);
      chi2_1 = dot_product(map1, raw_data[i]).real();  
      chi2_1b = dot_product(weighted_alms[i], filtered_alms[i]).real();      

      dof = map1.Npix();
      cout << "chi2_1/dof = " << chi2_1/dof << " chi2_1b/dof=" << chi2_1b/dof << endl;

      chi2 += chi2_1-chi2_1b;
    }

  for (int i = 0; i < data.numChannels; i++)
    {
      for (int j = 0; j < data.numChannels; j++)
	{
	  if (i==j)
	    continue;

	  chi2 -= dot_product(weighted_alms[i], filtered_alms[j]).real();   
	}
    }
  cerr << " ==== Likelihood DONE ====" <<endl;
  return -0.5*chi2;
 
}

void Aberration_Sampler::computeNewAberration(Alm<xcomplex<DataType> >& newmods) const
{
  double stepsize = 0.00005;
//  double stepsize = 0.001;
  double s0 = stepsize;///(1+2*l);

  newmods.SetToZero();
  newmods(1,0) = modalm(1,0) + xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, s0),0);
  newmods(1,1) = modalm(1,1) + 
    xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, s0),
		       gsl_ran_gaussian_ziggurat(rGen, s0))/sqrt(2.0);
}

static void checkNoiseChi2(CMB_Map& m, Noise_Map& n)
{
  double chi2 = n.chi2(m);
  cout << "Chi2 Noise / Npix = " << chi2/n.dof() << endl;
}

static void checkCMBChi2(CMB_Map& m, const arr<DataType>& cls)
{
  Alm<xcomplex<DataType> > alms(cls.size()-1, cls.size()-1);
  double chi2 = 0;
  int cnt = 0;
  arr<double> w(m.Nside()*2);
  CMB_Map m2(m.Nside(), RING, SET_NSIDE);

  w.fill(1);

  HEALPIX_method(m, alms, w);
  for (int l = 2; l <= alms.Lmax(); l++)
    {
    for (int m = 0; m <= min(l,alms.Mmax()); m++)
      {
        alms(l,m)/=sqrt(cls[l]);
        chi2 += ((m==0) ? 1 : 2) * alms(l,m).norm();
        cnt+=(m==0)?1:2;
      }
    }
   alms(0,0) = alms(1,0) = alms(1,1) = 0;
   alm2map(alms,m2);
   cout << "Chi2 CMB / Nalm = " << chi2/cnt << " DP=" <<  dot_product(m2, m2).real()*4*M_PI/m2.Npix()/cnt << endl;
}

double Aberration_Sampler::computeFullLikelihood(CMB_Data& data, CMB_ChannelData& raw_data, MapSignalNoiseSignal_weighing& matrix)
{
  for (int i = 0; i < data.numChannels; i++)
    {
      preweightData(data, raw_data, weight_alm[i], i, matrix);
      computeFilteredData(data, weight_alm[i], filtered_alm[i], matrix);
    }
  return computeLikelihood(data, raw_data, weight_alm, filtered_alm);
}

void Aberration_Sampler::computeNewSample(CMB_Data& data,
				      const AllSamplers& samplers)
{
  // Generate new parameters
  const arr<DataType>& cmb_Cls = data.guess_cls;//((CLS_Sampler*)samplers[SAMPLER_CLS])->getCls();
  const arr<DataType>& cmb_sqCls = data.guess_sqrt_cls;//((CLS_Sampler*)samplers[SAMPLER_CLS])->getSqrtCls();
  MapSignalNoiseSignal_weighing matrix(data.noise, data.weight,
				       data.beam, data.channels[0].Nside(),
				       data.NsideHR, data.numChannels,
				       data.transform);
 
  Alm<xcomplex<DataType> > newmod(1,1);
  Lensing_Operator<DataType,double> *mo = ((Lensing_Operator<DataType,DataType> *)data.transform);
  assert(mo != 0);

  matrix.log("Suggest new aberrration...");
  computeNewAberration(newmod);

  int Nside = data.channels[0].Nside();

  data.preconditioner->SetSqCls(cmb_sqCls);
  matrix.SetSqCls(cmb_sqCls);
  
  matrix.log("Setup new lensing field");
  //mo->saveField();
  std::cout << "NewMod(0,0) = " << newmod(0,0) << endl;
  std::cout << "NewMod(1,0) = " << newmod(1,0) << endl;
  std::cout << "NewMod(1,1) = " << newmod(1,1) << endl;
  mo->setLensingField(newmod);

  // Generate the fake data for the exchange algorithm
  matrix.log("Generate fake CMB data");
  ALM_Map afake(data.Lmax, data.Lmax);

  {
    CMB_Map cmb1(data.NsideHR, RING, SET_NSIDE), cmb2(Nside, RING, SET_NSIDE);
    ALM_Map a(data.Lmax, data.Lmax);

    matrix.log("  CMB map...");
    generateCMBMap<CMB_Map>(rGen, cmb1, cmb_Cls);
//    checkCMBChi2(cmb1, cmb_Cls);
    matrix.log("  do lensing...");
    mo->apply(cmb1, cmb2);
    matrix.log("  map2alm...");

    HEALPIX_method(cmb2, afake, data.weight);
  }

  matrix.log("Add noise to fake CMB");
  CMB_ChannelData fakedata;
  fakedata.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_Map cmbfake(Nside, RING, SET_NSIDE);

      fakedata[ch].SetNside(Nside, RING);

      makeRandomMap(rGen, fakedata[ch]);
      data.noise[ch]->apply_fwd_half(fakedata[ch]);
      
      ALM_Map a = afake;
      a.ScaleL(data.beam[ch]);
      alm2map(a, cmbfake);
      fakedata[ch] += cmbfake;
    }

  write_Healpix_map_to_fits("!fake.fits", fakedata[0], planckType<double>());

  cerr << "Likelihood of CMB given new potential..." << endl;
  double L_cmb_new = computeFullLikelihood(data, data.residuals, matrix);

  cerr << "Likelihood of fake data given new potential..." << endl;
  double L_fake_new = computeFullLikelihood(data, fakedata, matrix);

  matrix.log("Restore old lensing");
  mo->setLensingField(modalm);

  // fakedata holds the fake cmb data that we will use for the
  // exchange algorithm

  // Now we need the relative likelihood of the two datasets given 
  // the two sets of "parameters" determining the deflection field.
  cerr << "Likelihood of fake data given old potential..." << endl;
  double L_fake_old = computeFullLikelihood(data, fakedata, matrix);

  cerr << "Likelihood of CMB given old potential..." << endl;
  if (L_cmb_given_mulling == -INFINITY)
    L_cmb_given_mulling = computeFullLikelihood(data, data.residuals, matrix);

  double L_cmb_old = L_cmb_given_mulling;
  double relative = (L_fake_old-L_fake_new)+(L_cmb_new-L_cmb_old);

  relative += 0; //jump Proba is "1"
  
  // accept_probe holds the acceptance probability of the new proposed mulling
  // field
  double accept_proba = (relative > 0) ? 1 : exp(relative);
  int dof = data.channels[0].Npix();// - data.noise[0].masked;

  ofstream faccept("acceptance_aberration.txt", ios::app);
  faccept << accept_proba << " " << relative 
	  << " " << setprecision(12) << L_fake_new
	  << " " << setprecision(12) << L_fake_old 
	  << " " << setprecision(12) << L_cmb_new 
	  << " " << setprecision(12) << L_cmb_old 
	  << " " << setprecision(12) << -2*L_fake_new/dof
	  << " " << setprecision(12) << -2*L_fake_old/dof
	  << " " << setprecision(12) << -2*L_cmb_new/dof
	  << " " << setprecision(12) << -2*L_cmb_old/dof
          << " " << setprecision(12) << 0
	  << endl;

  if (gsl_rng_uniform(rGen) < accept_proba)
    {
      modalm = newmod;

      L_cmb_given_mulling = L_cmb_new;
      {
	ostringstream s;
	
	s << "!out/alm_aberration_" << skyCounter << ".fits";
	
	fitshandle f;
	string fname_s = s.str();
	f.create(fname_s.c_str());
	write_Alm_to_fits(f, modalm, modalm.Lmax(), modalm.Mmax(), planckType<double>());
	skyCounter++;
      }
      ofstream f("weight_aberration.txt", ios::app);
      f << numStay << endl;
      numStay = 1;
    }
  else
    {
      numStay++;
    }	
}

void Aberration_Sampler::saveState(NcFile& f)
{
  NcDim mod_dim = f.addDim("aberration_sampler_dim", modalm.Alms().size()*2);
  NcVar mod_var = f.addVar("aberration_sampler_alms", ncDouble, mod_dim);

  double *out_mod = new double[modalm.Alms().size()*2];

  for (int i = 0; i < modalm.Alms().size(); i++)
    {
      out_mod[2*i + 0] = modalm.Alms()[i].re;
      out_mod[2*i + 1] = modalm.Alms()[i].im;
    }

  mod_var.putVar(out_mod);
  delete[] out_mod;
}

void Aberration_Sampler::restoreState(NcFile& f) throw(StateInvalid)
{
  NcVar mod_var = f.getVar("aberration_sampler_alms");
  double *out_mod = new double[modalm.Alms().size()*2];
  xcomplex<DataType> *alms = modalm.mstart(0);

  mod_var.getVar(out_mod);
  for (int i = 0; i < modalm.Alms().size(); i++)
    {
      alms[i].re = out_mod[2*i + 0];
      alms[i].im = out_mod[2*i + 1];
    }
  delete[] out_mod;
}

const std::vector<int>& Aberration_Sampler::getModeledSignals() const
{
  static vector<int> signals;

  if (signals.size()==0)
    {
    }

  return signals;
}
