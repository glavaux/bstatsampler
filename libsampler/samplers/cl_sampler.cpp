/*+
This is ABYSS (./libsampler/samplers/cl_sampler.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <CosmoTool/algo.hpp>
#include <boost/format.hpp>
#include <fstream>
#include <iostream>
#include <cmath>
#include <cassert>
#include <boost/bind.hpp>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "cl_sampler.hpp"
#include <alm_healpix_tools.h>
#include "extra_alm_tools.hpp"
#include <powspec.h>
#include <alm_powspec_tools.h>
#include "cmb_data.hpp"
#include "extra_map_tools.hpp"
#include "netcdf_adapt.hpp"
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <powspec_fitsio.h>
#include <powspec.h>

using namespace std;
using namespace CMB;
using boost::format;
using boost::str;
using CosmoTool::square;

static vector<int> cl_signals;

CLS_Sampler::CLS_Sampler(gsl_rng *r, const std::string& name, long nL, int map_id, const std::string& init_name, int locked)
  : SingleSampler(r, name), cls(nL+1), sqrt_cls(nL+1), map_mapper_id(map_id), m_locked(locked > 0)
{
  PowSpec pspec;

  if (!init_name.empty())
    {
      read_powspec_from_fits (init_name, pspec, 1, nL);
      cls = pspec.tt();
      sqrt_cls.alloc(cls.size());
      
      for (long i = 0; i < cls.size(); i++)
	sqrt_cls[i] = sqrt(cls[i]);
    }
}

CLS_Sampler::~CLS_Sampler()
{
}

void CLS_Sampler::computeNewSample(CMB_Data& data,
				   const AllSamplers& samplers)
{
  MapperSampler *mapper = dynamic_cast<MapperSampler *>(samplers[map_mapper_id]);
  const ALM_Map& alms = *(mapper->getMapperCurrentAlms());

  long Lmax = alms.Lmax(), Mmax = alms.Mmax();
  PowSpec powspec;

  if (m_locked)
    return;

  cout << "In CLS_Sampler." << endl;

  cout << "Estimating CLS.." << endl;
  // clear the old cls
  cls.fill(0);
  // make an estimate of the cls
  for (long l = 0; l <= Lmax; ++l)
    {
      long limit = min(l,Mmax);
      for (long m = 1; m <= limit; ++m)
	{
	  cls[l] += norm(alms(l,m));
	}
      cls[l] += norm(alms(l,0));
    }

  // Note that we do not divide by (2*l + 1) here

  // generate new random cls
  cout << "Generating CLS.." << endl;
  for (long l = 1; l <= Lmax; ++l)
    {
      long iMax = 2*l-1;
      DataType z = 0;
      for (long i = 1; i < iMax; ++i)
	{
	  z += square(gsl_ran_gaussian_ziggurat(rGen, 1.0));
	}
      z = z/2 + square(gsl_ran_gaussian_ziggurat(rGen,1.0));
      // Here it is already proper normalized. cls[l]
      // is a new candidate
      cls[l] = cls[l]/z;
      sqrt_cls[l] = sqrt(cls[l]);
    }
  // l=1. Is different
}

void CLS_Sampler::saveState(NcFile& f)
{
  NcDim cl_dim = f.addDim(str(format("%s_sampler_dim") % m_name), cls.size());
  NcVar cls_var = f.addVar(str(format("%s_sampler_cls") % m_name), ncDouble, cl_dim);
  NcVar sqcls_var = f.addVar(str(format("%s_sampler_sqcls") % m_name), ncDouble, cl_dim);

  cls_var.putVar(&cls[0]);
  sqcls_var.putVar(&sqrt_cls[0]);
}

void CLS_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  try
   {
  NcVar cls_var = f.getVar(str(format("%s_sampler_cls") % m_name));
  NcVar sqcls_var = f.getVar(str(format("%s_sampler_sqcls") % m_name));
  std::vector<NcDim> cls_dims = cls_var.getDims();
  std::vector<NcDim> sqcls_dims = sqcls_var.getDims();

  if (cls_dims[0].getSize() != cls.size())
    throw StateInvalid(m_name);

  if (sqcls_dims[0].getSize() != cls.size())
    throw StateInvalid(m_name);

  cls_var.getVar(&cls[0]);
  sqcls_var.getVar(&sqrt_cls[0]);  
  }
  catch (const netCDF::exceptions::NcBadId)
   {
   }
}
    
