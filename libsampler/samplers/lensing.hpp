/*+
This is ABYSS (./libsampler/samplers/lensing.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __LENSING_HPP
#define __LENSING_HPP

#include <arr.h>
#include "sampler.hpp"
#include "cmb_defs.hpp"
#include "noise_weighing.hpp"

namespace CMB
{
  
  template<typename T,typename T2> class LensingOperator;
  class MapLensedSignalNoiseSignal_weighing;
  class MapLensedSignalNoiseSignal_weighing_preconditioner;

  class Lensing_Sampler: public SingleSampler
  {
  public:
    Lensing_Sampler(gsl_rng *r, CMB_Data& data, 
		   long nL, long nLPhi, long nSide);
    virtual ~Lensing_Sampler();

    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers);

    double computeLikelihood(CMB_Data& data,
			     const CMB_Map& datamap,
			     MapLensedSignalNoiseSignal_weighing& matrix,
			     MapLensedSignalNoiseSignal_weighing_preconditioner& precon);

    const ALM_Map& getLensingPotential() const { return lensing_potential; }

    virtual const CMB_Map *getEstimatedMap() const { return 0; }

    double computeJumpProba(const ALM_Map& original, const ALM_Map& proposed);
    ALM_Map computeNewProposedPotential();

  protected:
    arr<double> Cls_CMB, Cls, Cls_lensing;
    LensingOperator<DataType,double> *lo;
    double L_cmb_given_lensing;
    ALM_Map lensing_potential;
    double alpha_lensing;

    //    MapSignalNoiseSignal_weighing matrix;
    //    MapSignalNoiseSignal_weighing_preconditioner preconditioner;
  };

};

#endif
