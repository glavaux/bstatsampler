/*+
This is ABYSS (./libsampler/samplers/noise.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <fstream>
#include <iostream>
#include <cmath>
#include <cassert>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <alm_healpix_tools.h>
#include "noise.hpp"
#include "extra_alm_tools.hpp"
#include "cmb_data.hpp"
#include "extra_map_tools.hpp"
#include <boost/format.hpp>
#include "netcdf_adapt.hpp"

using namespace std;
using namespace CMB;
using boost::format;

NOISE_Sampler::NOISE_Sampler(gsl_rng *r)
  : SingleSampler(r)
{
  alpha = 1.0;
}

NOISE_Sampler::~NOISE_Sampler()
{
}
  
void NOISE_Sampler::computeNewSample(CMB_Data& data,
				     const AllSamplers& samplers)
{     
  ofstream nFile("noiseFile", ios::app);
  
  cout << "In NOISE_Sampler" << endl;
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      cout << format("+++ CHANNEL %d +++ (dof = %d)") % ch % data.noise[ch]->dof() << endl;

      // First thing, compute the Chi2 of the noise.
      // To this we must have the residual of the map
      // where CMB and foreground has been substracted
      CMB_Map&r = data.residuals[ch];
      CMB_Map delta = r;
      Noise_Map& noise = *data.noise[ch];
      double chi2 = 0;
      long Npix = delta.Npix();
      long N = noise.dof();
      
      cout << "--- Computing chi2" << endl;

      noise.SetFlexScaling(1.0);
      noise.apply_inv(delta);
      chi2 = dot_product(delta, r).real();

      cout << "--- chi2/npix = " << chi2/N << endl;
      
      // Now that we have the chi2, compute the equivalent mean
      // and variance of the inverse-gamma distribution.
      // We are using here the gaussian limit of this distribution
      // for a large number of samples.
      double chi2_over_n = chi2/N;
      double mu = sqrt(chi2_over_n);
      double sigma = sqrt(chi2_over_n/(2*N));
      
      cout << "---> mu = " << mu << "    sigma = " << sigma << endl;

      // Generate a new plausible noise level            
      alpha = mu+gsl_ran_gaussian_ziggurat(rGen, sigma);
      noise.SetFlexScaling(alpha);

      nFile << alpha << " ";
    }

   nFile << endl;

}
  

const std::vector<int>& NOISE_Sampler::getModeledSignals() const
{
  static vector<int> modeled;

  return modeled;
}


void NOISE_Sampler::saveState(NcFile& f)
{
  NcDim dim_noise = f.addDim("noise_sampler_dim", 1);
  NcVar var_noise = f.addVar("noise_sampler_alpha", ncDouble, dim_noise);

  var_noise.putVar(&alpha);
}

void NOISE_Sampler::restoreState(NcFile& f)
  throw(StateInvalid)
{
  NcVar var_noise = f.getVar("noise_sampler_alpha");

  if (var_noise.getDims()[0].getSize() != 1)
    {
      throw StateInvalid("Noise");
    }

  var_noise.getVar(&alpha);

}
