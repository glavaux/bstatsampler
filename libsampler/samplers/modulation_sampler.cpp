/*+
This is ABYSS (./libsampler/samplers/modulation_sampler.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <CosmoTool/algo.hpp>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <cassert>
#include <omp.h>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <gsl/gsl_rng.h>
#include <powspec.h>
#include <alm_powspec_tools.h>
#include <powspec_fitsio.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <alm_fitsio.h>
#include <vector>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "cl_sampler.hpp"
#include "extra_map_tools.hpp"
#include "modulation_sampler.hpp"
#include "cg_relax.hpp"
#include "cmb_rng.hpp"
#include "wigner3j.hpp"
#include "modulation_operator.hpp"
#include <fstream>
#include "netcdf_adapt.hpp"

using namespace std;
using namespace CMB;
using boost::format;
using boost::str;

Modulation_Sampler::~Modulation_Sampler()
{
}

Modulation_Sampler::Modulation_Sampler(gsl_rng *r, CMB_Data& data,
				       long lmax, long nSide, double stepsize,
				       long lmax_base_mod_min, long lmax_base_mod_max,
				       int cl_id)
  : SingleSampler(r,"modulation"), Cls_CMB(data.Lmax+1), Cls(data.Lmax+1), modalm(lmax, lmax), lmaxMod(lmax), 
    lmaxBaseModMin(lmax_base_mod_min), lmaxBaseModMax(lmax_base_mod_max),
    cl_sampler_id(cl_id)
{
  Cls_CMB.fill(0);
  Cls.fill(0);

  modalm.SetToZero();
  this->stepsize = stepsize;

  L_cmb_given_mulling = -INFINITY;
  skyCounter = 0;
  epsilon_CG = 5e-6;
  numStay = 1;
  weight_alm.alloc(data.numChannels);
  filtered_alm.alloc(data.numChannels);
  for (int i = 0; i < data.numChannels; i++)
    {
      weight_alm[i].Set(data.Lmax, data.Lmax);
      filtered_alm[i].Set(data.Lmax, data.Lmax);
    }
}

void Modulation_Sampler::preweightData(CMB_Data& data,
                                       CMB_ChannelData& raw_data,
				       ALM_Map& walms,
				       int i,
				 MapSignalNoiseSignal_weighing& matrix)
{  
  CMB_Map map1 = raw_data[i],map2(data.Nside, RING, SET_NSIDE);

  cerr << "   + Noise" << endl;
  data.noise[i]->apply_inv(map1);
  cerr << "   + Modulation" << endl;
  assert(data.transform != 0);
  HEALPIX_method(map1, walms, data.weight);
  walms.ScaleL(data.beam[i]);
  alm2map(walms, map1);
  data.transform->applyTranspose(map1, map2);
  cerr << "   + Spherical harmonic transform" << endl;
  HEALPIX_method(map2, walms, data.weight);  
  cerr << "   + Scaling" << endl;
  walms.ScaleL(matrix.sqrt_cls);
  walms *= map1.Npix()/(4*M_PI);
}

static const ALM_Map& prepareInputMap(CMB_Data *datap, ALM_Map *raw_alms, ALM_Map *tmp_alms, double alpha_mul)
{
  *tmp_alms = *raw_alms;
  tmp_alms->Scale(1/(alpha_mul*alpha_mul));
  return *tmp_alms;
}

void Modulation_Sampler::computeFilteredData(CMB_Data& data,
					     ALM_Map& alms,
					     ALM_Map& filtered_alms,
					     MapSignalNoiseSignal_weighing& matrix)
{
  ALM_Map tmp_alms(data.Lmax, data.Lmax);

  default_logger.enter("MODULATION");
  conjugateGradient_relax_solve
                   (data.rinfo, boost::bind(&prepareInputMap, &data, &alms, &tmp_alms, _1),
                    filtered_alms,
                    data,
                    &matrix,
                    &default_logger);
  default_logger.leave();
}
				       
double Modulation_Sampler::computeLikelihood(CMB_Data& data,
					     CMB_ChannelData& raw_data,
					     const arr<ALM_Map> & weighted_alms,
					     const arr<ALM_Map> & filtered_alms
					     )
{
  CMB_Map map1;
  double chi2_1, chi2 = 0;

  cerr << " ==== COMPUTING LIKELIHOOD ====" << endl;
  // No multi-channel support yet !!!
 
  for (int i = 0; i < data.numChannels; i++)
    {
      map1 = raw_data[i];
      data.noise[i]->apply_inv(map1);
      chi2_1 = dot_product(map1, raw_data[i]).real();  
      chi2_1 -= dot_product(weighted_alms[i], filtered_alms[i]).real();      

      chi2 += chi2_1;
    }

  for (int i = 0; i < data.numChannels; i++)
    {
      for (int j = 0; j < data.numChannels; j++)
	{
	  if (i==j)
	    continue;

	  chi2 -= dot_product(weighted_alms[i], filtered_alms[j]).real();   
	}
    }
  cerr << " ==== Likelihood DONE ====" <<endl;
  return -0.5*chi2;
 
}

void Modulation_Sampler::computeNewModulations(Alm<xcomplex<DataType> >& newmods) const
{
  double beta_blend = sqrt(1-stepsize*stepsize);

  // No monopole...
  newmods.Set(modalm.Lmax(), modalm.Mmax());
  newmods.SetToZero();
  for (long l = 1; l <= lmaxMod; l++)
    {
      double Cl_l = sqrt(proposal_spec.tt()[l])*stepsize;
      newmods(l,0) = beta_blend*modalm(l,0) + xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, Cl_l),0);
      for (long m = 1; m <= l; m++)
	{
	  newmods(l,m) = beta_blend*modalm(l,m) + 
	    xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, Cl_l),
			       gsl_ran_gaussian_ziggurat(rGen, Cl_l))/sqrt(2.0);
	}
    }
  
#if 0
  int l;

  cout << "LMAX = " << modalm.Lmax() << " newmodmax = " << newmods.Lmax() << endl;
  newmods(0,0) = 0;
  for (l = 1; l <= modalm.Lmax(); l++)
    {
      double s0 = stepsize;///(1+2*l);

      newmods(l,0) = modalm(l,0) + xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, s0),0);
      cout << l << endl;
      cout << "(" << l << ",0) = " << newmods(l,0) << endl;
      for (int m = 1; m <= min(l,modalm.Mmax()); m++)
	{
	  newmods(l,m) = modalm(l,m) + 
	    xcomplex<DataType>(gsl_ran_gaussian_ziggurat(rGen, s0),
			       gsl_ran_gaussian_ziggurat(rGen, s0))/sqrt(2.0);
	}
    }
#endif
}


static void checkNoiseChi2(CMB_Map& m, Noise_Map& n)
{
  double chi2 = n.chi2(m);
  cout << "Chi2 Noise / Npix = " << chi2/n.dof() << endl;
}

static void checkCMBChi2(CMB_Map& m, const arr<DataType>& cls)
{
  Alm<xcomplex<DataType> > alms(cls.size()-1, cls.size()-1);
  double chi2 = 0;
  int cnt = 0;
  arr<double> w(m.Nside()*2);
  CMB_Map m2(m.Nside(), RING, SET_NSIDE);

  w.fill(1);

  HEALPIX_method(m, alms, w);
  for (int l = 2; l <= alms.Lmax(); l++)
    {
    for (int m = 0; m <= min(l,alms.Mmax()); m++)
      {
        alms(l,m)/=sqrt(cls[l]);
        chi2 += ((m==0) ? 1 : 2) * alms(l,m).norm();
        cnt+=(m==0)?1:2;
      }
    }
   alms(0,0) = alms(1,0) = alms(1,1) = 0;
   alm2map(alms,m2);
   cout << "Chi2 CMB / Nalm = " << chi2/cnt << " DP=" <<  dot_product(m2, m2).real()*4*M_PI/m2.Npix()/cnt << endl;
}

double Modulation_Sampler::computeFullLikelihood(CMB_Data& data, CMB_ChannelData& raw_data, MapSignalNoiseSignal_weighing& matrix)
{
  for (int i = 0; i < data.numChannels; i++)
    {
      preweightData(data, raw_data, weight_alm[i], i, matrix);
      computeFilteredData(data, weight_alm[i], filtered_alm[i], matrix);
    }
  return computeLikelihood(data, raw_data, weight_alm, filtered_alm);
}

double Modulation_Sampler::computeJumpProba(ALM_Map& newalm)
{
  double beta_blend = sqrt(1-stepsize*stepsize);
  double alpha_blend = stepsize;
  ALM_Map r_proposed = (newalm-beta_blend*modalm);
  ALM_Map r_rev_proposed = (modalm-beta_blend*newalm);
  double alpha2 = alpha_blend*alpha_blend;
  double chi2 = 0;

  for (long l = 0; l <= lmaxMod; l++)
    {
      double Cl_l = proposal_spec.tt()[l];
      if(Cl_l <= 0)
        continue;

      {
          xcomplex<DataType>& r_alm_0 = r_proposed(l,0);
          xcomplex<DataType>& r_rev_alm_0 = r_rev_proposed(l,0);
          double a = CosmoTool::square(r_alm_0.real());
          double b = CosmoTool::square(r_rev_alm_0.real());
          chi2 += (a-b)/(Cl_l*alpha2);
      }

      for(long m = 1; m <= l; m++)
        {
          xcomplex<DataType>& r_alm = r_proposed(l,m);
          xcomplex<DataType>& r_rev_alm = r_rev_proposed(l,m);

          double a = CosmoTool::square(r_alm.real()) + CosmoTool::square(r_alm.imag());
          double b = CosmoTool::square(r_rev_alm.real()) + CosmoTool::square(r_rev_alm.imag());
          chi2 += (a-b)/(Cl_l*alpha2);
        }
    }
  return 0.5*chi2;
}

double Modulation_Sampler::computeLogPrior(const ALM_Map& alm) const
{
  double chi2 = 0;

  for (long l = 0; l < alm.Lmax(); l++)
   {
     double iCl = (Cls[l]==0) ? 0 : 1/Cls[l];
     double chi2_l = 0;
     for (long m = 0; m <= l && m <= alm.Mmax(); m++)
       {
         chi2_l += alm(l,m).norm()*iCl;
       }
     chi2 += chi2_l;
    }
  return -0.5*chi2;
}


void Modulation_Sampler::computeNewSample(CMB_Data& data,
				      const AllSamplers& samplers)
{
  // Generate new parameters
  const arr<DataType>& cmb_Cls = data.guess_cls;//((CLS_Sampler*)samplers[SAMPLER_CLS])->getCls();
  const arr<DataType>& cmb_sqCls = data.guess_sqrt_cls;//((CLS_Sampler*)samplers[SAMPLER_CLS])->getSqrtCls();

  CLS_Sampler *cls_s = dynamic_cast<CLS_Sampler *>(samplers[cl_sampler_id]);

  if (cls_s == 0)
    {
      cerr << "INTERNAL ERROR. The CLS Sampler does not exists or is not a CLS_Sampler." << endl;
      abort();
    }

  Cls = cls_s->getCls();

  Alm<xcomplex<DataType> > newmod(lmaxMod,lmaxMod);
  ModulationOperator<DataType,double> *mo = ((ModulationOperator<DataType,DataType> *)data.transform);

  computeNewModulations(newmod);

  int Nside = data.channels[0].Nside();

  MapSignalNoiseSignal_weighing matrix(data.noise, data.weight,
				       data.beam, data.channels[0].Nside(),
				       data.NsideHR, data.numChannels,
				       data.transform);
  data.preconditioner->SetSqCls(cmb_sqCls);
  matrix.SetSqCls(cmb_sqCls);
  
  mo->setModulation(newmod);
  mo->setModRange(lmaxBaseModMin, lmaxBaseModMax);

  // Generate the fake data for the exchange algorithm
  ALM_Map afake(data.Lmax, data.Lmax);

  {
    CMB_Map cmb1(Nside, RING, SET_NSIDE), cmb2(Nside, RING, SET_NSIDE);
    ALM_Map a(data.Lmax, data.Lmax);

    generateCMBMap<CMB_Map>(rGen, cmb1, cmb_Cls);
//    checkCMBChi2(cmb1, cmb_Cls);
    mo->apply(cmb1, cmb2);

    HEALPIX_method(cmb2, afake, data.weight);
  }

  CMB_ChannelData fakedata;
  fakedata.alloc(data.numChannels);
  for (int ch = 0; ch < data.numChannels; ch++)
    {
      CMB_Map cmbfake(Nside, RING, SET_NSIDE);

      fakedata[ch].SetNside(Nside, RING);

      makeRandomMap(rGen, fakedata[ch]);
      data.noise[ch]->apply_fwd_half(fakedata[ch]);
      
      ALM_Map a = afake;
      a.ScaleL(data.beam[ch]);
      alm2map(a, cmbfake);
      fakedata[ch] += cmbfake;
    }

  mo->setModulation(modalm);

  // fakedata holds the fake cmb data that we will use for the
  // exchange algorithm


  // Now we need the relative likelihood of the two datasets given 
  // the two sets of "parameters" determining the deflection field.

  cerr << "Likelihood of CMB given old mulling potential..." << endl;
  if (L_cmb_given_mulling == -INFINITY)
    L_cmb_given_mulling = computeFullLikelihood(data, data.residuals, matrix);

  double L_cmb_old = L_cmb_given_mulling;
  cerr << "Likelihood of fake data given old mulling potential..." << endl;
  double L_fake_old = computeFullLikelihood(data, fakedata, matrix);
  
  mo->setModulation(newmod);

  cerr << "Likelihood of CMB given new mulling potential..." << endl;
  double L_cmb_new = computeFullLikelihood(data, data.residuals, matrix);

  cerr << "Likelihood of fake data given new mulling potential..." << endl;
  double L_fake_new = computeFullLikelihood(data, fakedata, matrix);

  double relative = (L_fake_old-L_fake_new)+(L_cmb_new-L_cmb_old);
  double jumpProba = computeJumpProba(newmod);
  double log_prior_new = computeLogPrior(newmod);
  double log_prior_old = computeLogPrior(modalm);

  relative += jumpProba; //jump Proba is "1"
  relative += log_prior_new-log_prior_old;
  
  // accept_probe holds the acceptance probability of the new proposed mulling
  // field
  double accept_proba = (relative > 0) ? 1 : exp(relative);
  int dof = data.noise[0]->dof();

  ofstream faccept("acceptance_modulating.txt", ios::app);
  faccept << accept_proba << " " << relative 
	  << " " << setprecision(12) << L_fake_new
	  << " " << setprecision(12) << L_fake_old 
	  << " " << setprecision(12) << L_cmb_new 
	  << " " << setprecision(12) << L_cmb_old 
	  << " " << setprecision(12) << -2*L_fake_new/dof
	  << " " << setprecision(12) << -2*L_fake_old/dof
	  << " " << setprecision(12) << -2*L_cmb_new/dof
	  << " " << setprecision(12) << -2*L_cmb_old/dof
          << " " << setprecision(12) << jumpProba
          << " " << setprecision(12) << (log_prior_new-log_prior_old)
	  << endl;

  if (gsl_rng_uniform(rGen) < accept_proba)
    {
      modalm = newmod;

      L_cmb_given_mulling = L_cmb_new;
      {
	ostringstream s;
	
	s << "!out/alm_modulating_" << skyCounter << ".fits";
	
	fitshandle f;
	string fname_s = s.str();
	f.create(fname_s.c_str());
	write_Alm_to_fits(f, modalm, modalm.Lmax(), modalm.Mmax(), planckType<double>());
	skyCounter++;
      }
      ofstream f("weight_modulated.txt", ios::app);
      f << numStay << endl;
      numStay = 1;
    }
  else
    {
      numStay++;
    }	
}

void Modulation_Sampler::saveState(NcFile& f)
{
  NcDim mod_dim = f.addDim(str(format("%s_sampler_dim") % m_name), modalm.Alms().size()*2);
  NcVar mod_var = f.addVar(str(format("%s_sampler_alms") % m_name), ncDouble, mod_dim);

  double *out_mod = new double[modalm.Alms().size()*2];

  for (int i = 0; i < modalm.Alms().size(); i++)
    {
      out_mod[2*i + 0] = modalm.Alms()[i].re;
      out_mod[2*i + 1] = modalm.Alms()[i].im;
    }

  mod_var.putVar(out_mod);
  delete[] out_mod;
}

void Modulation_Sampler::restoreState(NcFile& f) throw(StateInvalid)
{
  NcVar mod_var = f.getVar(str(format("%s_sampler_alms") % m_name));
  double *out_mod = new double[modalm.Alms().size()*2];
  xcomplex<double> *alms = modalm.mstart(0);
  mod_var.getVar(out_mod);
  for (int i = 0; i < modalm.Alms().size(); i++)
    {
      alms[i].Set(out_mod[2*i+0], out_mod[2*i+1]);
    }
  delete[] out_mod;
}

const std::vector<int>& Modulation_Sampler::getModeledSignals() const
{
  static vector<int> signals;

  if (signals.size()==0)
    {
    }

  return signals;
}

void Modulation_Sampler::setInitialCondition(const string& name)
{
  read_Alm_from_fits(name, modalm, lmaxMod, lmaxMod);
}

void Modulation_Sampler::setProposalSpectrum(const string& name)
{
  read_powspec_from_fits(name, proposal_spec, 1, lmaxMod);
}
