/*+
This is ABYSS (./libsampler/samplers/cmb_mess_sampler.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_MESS_GIBBS_SAMPLER_HPP
#define __CMB_MESS_GIBBS_SAMPLER_HPP

#include "mpi_communication.hpp"
#include "sampler.hpp"
#include "cmb_defs.hpp"
#include "noise_weighing.hpp"
#include "cmb_data.hpp"

namespace CMB 
{
  // This is the sampler of the CMB signal
  // It takes Cls, generates some random a_lm and
  // produce a corresponding
  // CMB sky map.
  struct Messenger_State
  {
    arr<CMB_Map> messengers;
    ALM_Map alms;
  };

  class CMB_Mess_Sampler: public SingleSampler, public MapperSampler
  {
  public:
    CMB_Mess_Sampler(gsl_rng *r, const std::string& name, MPI_Communication *comm, CMB_Data& data, 
		long nL, long nSide, int numBath, double deltaBath);
    virtual ~CMB_Mess_Sampler();

    virtual void saveState(NcFile& f);
    virtual void restoreState(NcFile& f) throw(StateInvalid);

    virtual void computeNewSample(CMB_Data& data,
				  const AllSamplers& samplers);
    
    virtual const CMB_Map *getEstimatedMap(int ch, int signal) const
    {
      if (signal == SAMPLER_SIGNAL_PROPOSED_CMB || signal == SAMPLER_SIGNAL_DEFAULT)
        return &estimated_map[ch];

      return 0;
    }

    virtual const ALM_Map *getMapperCurrentAlms() const
    {
      return &state.alms;
    }

    virtual const std::vector<int>& getModeledSignals() const;
    
    const ALM_Map& getAlms(ALM_Map& alm) const
    {
      alm = state.alms;
      return alm;
    }

    void setInitialProposedMap(const CMB_Map& map)
    {
      proposed_map = map;
    }

  protected:
    Messenger_State state;
    double alpha_N;
    arr<CMB_Map> estimated_map;
    arr<double> tau;
    arr<CMB_NN_NoiseMap> noise_tilde;
    arr<Healpix_Map<bool> > noise_tilde_passthrough;
    CMB_Map proposed_map;
    MPI_Communication *Comm;
    int numBath;
    double deltaBath;

    void generate_ran_alms(ALM_Map& alms, double temperature);
    void generate_mock_data_cmb(CMB_Data& data, const arr<DataType>& sqcls,
                                ALM_Map& alms, Messenger_State& this_state,
                                arr<CMB_Map>& delta_mock, double temperature);
    void generate_mock_data_messengers(CMB_Data& data, arr<CMB_Map>& ran_messenger, arr<CMB_Map>& mock_data, double temperature);

    void compute_messenger_filter(CMB_Data& data,
                                  const arr<DataType>& sqcls,
                                  Messenger_State& this_state,
                                  CMB_Map& refmap, int ch);
    void compute_cmb_filter(CMB_Data& data,
                            const arr<DataType>& sqcls,
                            Messenger_State& this_state,
                            arr<CMB_Map>& mock_data);

    double computeLikelihood(CMB_Data& data, const arr<DataType>& sqcls,
                             Messenger_State& this_state);

    void do_messenger_sampling(CMB_Data& data, const arr<DataType>&, Messenger_State& this_state, double temperature, bool reverse);
  };
};

#endif
