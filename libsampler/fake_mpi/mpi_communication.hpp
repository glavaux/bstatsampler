/*+
This is ABYSS (./libsampler/fake_mpi/mpi_communication.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_FAKE_MPI_COMMUNICATION_HPP
#define __CMB_FAKE_MPI_COMMUNICATION_HPP

#include <cstdlib>
#include <cstring>

namespace CMB
{
  typedef int MPI_Status;
  typedef int MPI_Op;

  class MPI_Exception: public std::exception
  {
  public:
    MPI_Exception(int err) : errcode(err)
    {
    }

    virtual const char *what() const throw() { return err_string.c_str(); }
    int code() const { return errcode; }

    virtual ~MPI_Exception() throw() {}

  private:
    std::string err_string;
    int errcode;
  };

  class MPICC_Window
  {
  public:
    void *w;

    void lock(bool) {}
    void unlock() {}

    void fence() {}
    void destroy()
    {
      delete[] ((char *)w);
    }

    template<typename T>
    void put(int r, T v) {
      (reinterpret_cast<T *>(w))[r] = v;
    }

    template<typename T>
    T get(int r) {
      return (reinterpret_cast<T *>(w))[r];
    }

    template<typename T>
    T *get_ptr() {
      return (T *)w;
    }

    template<typename T>
    const T *get_ptr() const {
      return (const T *)w;
    }

  };

  class MPICC_Mutex
  {
  public:
    void acquire() {}
    void release() {}
  };

  class MPI_Communication
  {
  private:
  public:
    MPI_Communication() { }

    ~MPI_Communication() {}

    int rank() const { return 0; }

    int size() const { return 1; }

    void abort()
    {
      ::abort();
    }

    void comm_assert(bool c, const std::string& err)
    {
      if  (!c)
        {
          if (rank() == 0)
            {
              std::cout << err << std::endl;
            }
          abort();
        }
    }

    MPICC_Mutex *new_mutex(int tag)
    {
      return new MPICC_Mutex();
    }

    MPICC_Window win_create(int size, int disp_unit)
    {
      MPICC_Window w;

      w.w = new char[size];
      return w;
    }

    void send_recv(const void *sendbuf, int sendcount, MPI_Datatype sdatatype,
		   int dest, int sendtag,
		   void *recvbuf, int recvcount, MPI_Datatype rdatatype,
		   int source, int recvtag,
		   MPI_Status *s = 0)
      throw (MPI_Exception)
    {
      if (source != 0 || dest != 0 ||
          sendcount != recvcount ||
          recvtag != sendtag)
        throw MPI_Exception(0);
      ::memcpy(recvbuf, sendbuf, sendcount*sdatatype);
    }

    void send(const void *buf, int count, MPI_Datatype datatype,
	      int dest, int tag)
      throw (MPI_Exception)
    {
      throw MPI_Exception(0);
    }

    void recv(void *buf, int count, MPI_Datatype datatype,
	      int from, int tag,
	      MPI_Status *status = 0)
      throw (MPI_Exception)
    {
      throw MPI_Exception(0);
    }

    void reduce(const void *sendbuf, void *recvbuf, int count,
		MPI_Datatype datatype, MPI_Op op, int root)
    {
      throw MPI_Exception(0);
    }

    void broadcast(void *sendrecbuf, int sendrec_count, MPI_Datatype sr_type,
		   int root)
    {
    }

    void scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
		 void *recvbuf, int recvcount, MPI_Datatype recvtype,
		 int root)
    {
      throw MPI_Exception(0);
    }

    void all_reduce(const void *sendbuf, void *recvbuf, int count,
		    MPI_Datatype datatype, MPI_Op op)
    {
      throw MPI_Exception(0);
    }

    void barrier()
    {
    }

    template<typename T>
    void accum(T *target_array, const T *source_array, int count, int root)
    {
      if (root != 0)
        throw MPI_Exception(0);

      if (target_array != source_array)
        ::memcpy(target_array, source_array, count*sizeof(T));
    }

    template<typename T>
    void all_accum(T *ts_array, int count)
    {
    }

    void log_root(const std::string& s)
    {
      (std::cout << s << std::endl).flush();
    }

    void log(const std::string& s)
    {
      (std::cout << s << std::endl).flush();
    }

    void progress(const std::string& s)
    {
      (std::cout << s << "\r").flush();
    }
  };


  static inline MPI_Communication *setupMPI(int& argc, char **& argv)
  {
    return new MPI_Communication();
  }

  static inline void doneMPI()
  {
  }
};

#endif
