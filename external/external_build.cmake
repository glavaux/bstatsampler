
OPTION(ENABLE_OPENMP "Set to Yes if Healpix and/or you need openMP" OFF)
OPTION(BUILD_SHARED_LIBS "Build shared libraries" OFF)
OPTION(INTERNAL_HDF5 "Download and build an internal HDF5 lib" OFF)
OPTION(INTERNAL_NETCDF "Download and build an internal NetCDF lib" OFF)
OPTION(INTERNAL_BOOST "Download and build an internal Boost lib" OFF)
OPTION(INTERNAL_GSL "Download and build an internal GSL lib" OFF)
OPTION(INTERNAL_EIGEN "Download and build an internal EIGEN lib" ON)

option(USE_INTEL_MKL "Set to Yes to try to use Intel MKL" OFF)
option(USE_OPENBLAS "Set to Yes to use OpenBLAS instead of MKL" OFF)


SET(EXTRA_LD_FLAGS "" CACHE STRING "Extra flags to be passed to the linker")
SET(EXTRA_CPP_FLAGS "" CACHE STRING "Extra flags to be passed to the preprocessor")

SET(OPENBLAS_GIT "git://github.com/xianyi/OpenBLAS" CACHE STRING "GIT URL for openblas")
SET(HDF5_URL "http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.9/src/hdf5-1.8.9.tar.gz" CACHE STRING "URL to download HDF5 from")
SET(NETCDF_URL "http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-4.2.1.1.tar.gz" CACHE STRING "URL to download NetCDF from")
SET(NETCDFCPP_URL "http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-cxx4-4.2.tar.gz" CACHE STRING "URL to download NetCDF-C++ from")
SET(BOOST_URL "http://sourceforge.net/projects/boost/files/boost/1.49.0/boost_1_49_0.tar.gz/download" CACHE STRING "URL to download Boost from")
SET(EIGEN_URL "https://bitbucket.org/eigen/eigen/get/3.1.4.tar.gz" CACHE URL "URL to download Eigen from")
SET(GSL_URL "ftp://ftp.gnu.org/gnu/gsl/gsl-1.15.tar.gz" CACHE STRING "URL to download GSL from ")

mark_as_advanced(NETCDF_URL NETCDFCPP_URL BOOST_URL EIGEN_URL OPENBLAS_GIT)

IF(NOT INTERNAL_BOOST)
  find_package(Boost 1.49.0 COMPONENTS format spirit phoenix FATAL_ERROR)
ENDIF(NOT INTERNAL_BOOST)

SET(eigen_built)

SET(EXT_INSTALL ${CMAKE_BINARY_DIR}/ext_build/stage)

SET(HEALPIX_BASE_PATH $ENV{HEALPIX})
SET(HEALPIX_TARGET $ENV{HEALPIX_TARGET})
SET(HEALPIX_PATH ${HEALPIX_BASE_PATH}/src/cxx/${HEALPIX_TARGET})
SET(HEALPIX_HINT_INCLUDE ${HEALPIX_PATH}/include)
SET(HEALPIX_HINT_LIB ${HEALPIX_PATH}/lib)

MESSAGE(STATUS "Found healpix: ${HEALPIX_HINT_LIB}")


#
#
#

SET(CONFIGURE_CPP_FLAGS "${EXTRA_CPP_FLAGS}")
SET(CONFIGURE_LD_FLAGS "${EXTRA_LD_FLAGS}")

SET(cosmotool_DEPS)
SET(flints_DEPS)

# ===============
# Build cfitsio
#
ExternalProject_Add(cfitsio
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/cfitsio
  CONFIGURE_COMMAND ${CMAKE_SOURCE_DIR}/external/cfitsio/configure --prefix=${EXT_INSTALL} CPPFLAGS=${CONFIGURE_CPP_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
  BUILD_COMMAND make
  BUILD_IN_SOURCE 1
  INSTALL_COMMAND make install
)
SET(CFITSIO_LIBRARY ${EXT_INSTALL}/lib/libcfitsio.a)

if (INTERNAL_HDF5)
  SET(HDF5_SOURCE_DIR ${CMAKE_BINARY_DIR}/hdf5-prefix/src/hdf5)
  SET(HDF5_BIN_DIR ${EXT_INSTALL})
  ExternalProject_Add(hdf5
    URL ${HDF5_URL}
    CONFIGURE_COMMAND ${HDF5_SOURCE_DIR}/configure --disable-shared --enable-cxx --prefix=${HDF5_BIN_DIR} CPPFLAGS=${CONFIGURE_CPP_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND make install
  )
  SET(cosmotool_DEPS ${cosmotool_DEPS} hdf5)
  SET(hdf5_built hdf5)
  set(HDF5_LIBRARY ${HDF5_BIN_DIR}/lib/libhdf5.a CACHE STRING "HDF5 lib" FORCE)
  set(HDF5HL_LIBRARY ${HDF5_BIN_DIR}/lib/libhdf5_hl.a CACHE STRING "HDF5-HL lib" FORCE)
  SET(HDF5_INCLUDE_PATH ${HDF5_BIN_DIR}/include CACHE STRING "HDF5 include path" FORCE)
  SET(ENV{HDF5_ROOT} ${HDF5_BIN_DIR})
  SET(HDF5_ROOTDIR ${HDF5_BIN_DIR})
  SET(CONFIGURE_LDFLAGS "${CONFIGURE_LDFLAGS} -L${HDF5_BIN_DIR}/lib")
else(INTERNAL_HDF5)
  find_path(HDF5_INCLUDE_PATH hdf5.h)
  find_library(HDF5_LIBRARY hdf5)
  find_library(HDF5HL_LIBRARY hdf5_hl)
endif (INTERNAL_HDF5)
SET(CONFIGURE_CPP_FLAGS "${CONFIGURE_CPP_FLAGS} -I${HDF5_INCLUDE_PATH}")

if (INTERNAL_NETCDF)
  SET(NETCDF_SOURCE_DIR ${CMAKE_BINARY_DIR}/netcdf-prefix/src/netcdf)
  SET(NETCDFCPP_SOURCE_DIR ${CMAKE_BINARY_DIR}/netcdfcpp-prefix/src/netcdfcpp)
  SET(NETCDF_BIN_DIR ${EXT_INSTALL})
  SET(CONFIGURE_CPP_FLAGS "${CONFIGURE_CPP_FLAGS} -I${NETCDF_BIN_DIR}/include")
  SET(CONFIGURE_LDFLAGS "${CONFIGURE_LDFLAGS} -L${NETCDF_BIN_DIR}/lib")
  SET(EXTRA_NC_FLAGS CPPFLAGS=${CONFIGURE_CPP_FLAGS} LDFLAGS=${CONFIGURE_LDFLAGS})
  ExternalProject_Add(netcdf
    DEPENDS ${hdf5_built}
    URL ${NETCDF_URL}
    CONFIGURE_COMMAND ${NETCDF_SOURCE_DIR}/configure --prefix=${NETCDF_BIN_DIR} --enable-netcdf-4 --disable-shared --disable-dap --disable-cdmremote --disable-rpc --disable-examples ${EXTRA_NC_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND make install
  )
  SET(CONFIGURE_CPP_LDFLAGS "${CONFIGURE_LDFLAGS}")
  SET(EXTRA_NC_FLAGS CPPFLAGS=${CONFIGURE_CPP_FLAGS} LDFLAGS=${CONFIGURE_CPP_LDFLAGS})
  ExternalProject_Add(netcdfcpp
    DEPENDS ${hdf5_built} netcdf
    URL ${NETCDFCPP_URL}
    CONFIGURE_COMMAND ${NETCDFCPP_SOURCE_DIR}/configure --prefix=${NETCDF_BIN_DIR} --disable-shared --disable-examples ${EXTRA_NC_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
    PATCH_COMMAND patch -p1 -d ${NETCDFCPP_SOURCE_DIR} -i ${CMAKE_SOURCE_DIR}/patch-netcdfcpp
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND make install
  )

  SET(cosmotool_DEPS ${cosmotool_DEPS} netcdf netcdfcpp)
  SET(NETCDF_LIBRARY ${NETCDF_BIN_DIR}/lib/libnetcdf.a CACHE STRING "NetCDF lib" FORCE)
  SET(NETCDFCPP_LIBRARY ${NETCDF_BIN_DIR}/lib/libnetcdf_c++4.a CACHE STRING "NetCDF-C++ lib" FORCE)
  SET(NETCDF_INCLUDE_PATH ${NETCDF_BIN_DIR}/include CACHE STRING "NetCDF include" FORCE)
  SET(NETCDFCPP_INCLUDE_PATH ${NETCDF_INCLUDE_PATH} CACHE STRING "NetCDF C++ include path" FORCE)

  FILE(WRITE ${CMAKE_BINARY_DIR}/libsampler/gibbs_netcdf_ver.hpp "#define NETCDFCPP4")
  SET(netcdf_built netcdf)
ELSE(INTERNAL_NETCDF)
  find_library(NETCDF_LIBRARY netcdf)
  find_library(NETCDFCPP_LIBRARY netcdf_c++4)
  find_path(NETCDF_INCLUDE_PATH NAMES netcdf.h)
  find_path(NETCDFCPP_INCLUDE_PATH NAMES netcdf)
  SET(CONFIGURE_CPP_FLAGS ${CONFIGURE_CPP_FLAGS} -I${NETCDF_INCLUDE_PATH} -I${NETCDFCPP_INCLUDE_PATH})
  SET(netcdf_built)
endif (INTERNAL_NETCDF)

if (INTERNAL_BOOST)
  SET(BOOST_SOURCE_DIR ${CMAKE_BINARY_DIR}/boost-prefix/src/boost)
  ExternalProject_Add(boost
    URL ${BOOST_URL}
    CONFIGURE_COMMAND ${BOOST_SOURCE_DIR}/bootstrap.sh --prefix=${CMAKE_BINARY_DIR}/ext_build/boost
    BUILD_IN_SOURCE 1
    BUILD_COMMAND ${BOOST_SOURCE_DIR}/b2 --with-filesystem --with-exception --with-serialization --with-chrono
    PATCH_COMMAND patch -p1 -d ${BOOST_SOURCE_DIR} -i ${CMAKE_SOURCE_DIR}/patch-boost
    INSTALL_COMMAND echo "No install"
  )

  SET(BOOST_ROOT ${BOOST_SOURCE_DIR})
  set(Boost_INCLUDE_DIRS ${BOOST_SOURCE_DIR} CACHE STRING "Boost path" FORCE)
  SET(BOOST_LIBRARIES ${BOOST_SOURCE_DIR}/stage/lib/libboost_serialization.a ${BOOST_SOURCE_DIR}/stage/lib/libboost_chrono.a
${BOOST_SOURCE_DIR}/stage/lib/libboost_filesystem.a ${BOOST_SOURCE_DIR}/stage/lib/libboost_system.a ${BOOST_SOURCE_DIR}/stage/lib/libboost_exception.a)
  SET(boost_built boost)
ELSE(INTERNAL_BOOST)
  find_package(Boost REQUIRED COMPONENTS filesystem serialization chrono system)
  SET(BOOST_LIBRARIES ${Boost_LIBRARIES})
  SET(BOOST_ROOT) 
  SET(boost_built)
endif (INTERNAL_BOOST)

IF(USE_OPENBLAS)
  ExternalProject_Add(openblas
    GIT_REPOSITORY ${OPENBLAS_GIT}
    GIT_TAG 4d42368214f2fd102b2d163c086e0f2d8c166dc6
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo "No configure"
    BUILD_IN_SOURCE 1
    BUILD_COMMAND ${CMAKE_MAKE_PROGRAM} USE_OPENMP=1
    INSTALL_COMMAND ${CMAKE_MAKE_PROGRAM} PREFIX=${EXT_INSTALL} install
    )
  SET(OPENBLAS_LIBRARIES ${EXT_INSTALL}/lib/libopenblas.a)
ENDIF(USE_OPENBLAS)


IF(INTERNAL_EIGEN)

  ExternalProject_Add(eigen
    URL ${EIGEN_URL}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXT_INSTALL}
    PATCH_COMMAND ${CMAKE_COMMAND} 
      -DBUILD_PREFIX=${CMAKE_BINARY_DIR}/eigen-prefix 
      -DPATCH_FILE=${CMAKE_SOURCE_DIR}/external/patch_eigen 
      -DSOURCE_PREFIX=${CMAKE_BINARY_DIR}/eigen-prefix/src/eigen 
      -P ${CMAKE_SOURCE_DIR}/external/check_and_apply_patch.cmake
  )
  include_directories(${EXT_INSTALL}/include/eigen3)
  SET(eigen_built eigen)
ENDIF(INTERNAL_EIGEN)

IF(INTERNAL_GSL)
  SET(GSL_SOURCE_DIR ${CMAKE_BINARY_DIR}/gsl-prefix/src/gsl)
  ExternalProject_Add(gsl
    URL ${GSL_URL}
    CONFIGURE_COMMAND ${GSL_SOURCE_DIR}/configure --prefix=${EXT_INSTALL} --with-pic --disable-shared CPPFLAGS=${CONFIGURE_CPP_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
    BUILD_IN_SOURCE 1
    BUILD_COMMAND make
    INSTALL_COMMAND make install
  )
  SET(GSL_INTERNAL_LIBS ${EXT_INSTALL}/lib)
  SET(GSL_LIBRARY ${GSL_INTERNAL_LIBS}/libgsl.a CACHE STRING "GSL internal path" FORCE)
  SET(GSLCBLAS_LIBRARY ${GSL_INTERNAL_LIBS}/libgslcblas.a CACHE STRING "GSL internal path" FORCE)
  set(GSL_INCLUDE_PATH ${EXT_INSTALL}/include CACHE STRING "GSL internal path" FORCE)
  SET(cosmotool_DEPS ${cosmotool_DEPS} gsl)
  SET(gsl_built gsl)
ELSE(INTERNAL_GSL)
  find_library(GSL_LIBRARY gsl)
  find_library(GSLCBLAS_LIBRARY gslcblas)
  find_path(GSL_INCLUDE_PATH NAMES gsl/gsl_blas.h)
  SET(gsl_built)
ENDIF(INTERNAL_GSL)


ExternalProject_Add(cosmotool
  DEPENDS ${cosmotool_DEPS}
  URL ${CMAKE_SOURCE_DIR}/external/cosmotool.tgz
  CMAKE_ARGS -DHDF5_DIR=${HDF5_ROOTDIR} 
             -DHDF5_ROOTDIR=${HDF5_ROOTDIR} 
             -DCMAKE_INSTALL_PREFIX=${EXT_INSTALL}
             -DNETCDF_INCLUDE_PATH=${NETCDF_INCLUDE_PATH} 
             -DNETCDFCPP_INCLUDE_PATH=${NETCDFCPP_INCLUDE_PATH} 
             -DGSL_INCLUDE_PATH=${GSL_INCLUDE_PATH} 
             -DGSL_LIBRARY=${GSL_LIBRARY}
             -DGSLCBLAS_LIBRARY=${GSLCBLAS_LIBRARY} 
             -DNETCDF_LIBRARY=${NETCDF_LIBRARY} 
             -DNETCDFCPP_LIBRARY=${NETCDFCPP_LIBRARY}
             -DENABLE_SHARP=OFF
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}

)
SET(COSMOTOOL_LIBRARY ${EXT_INSTALL}/lib/libCosmoTool.a)
set(COSMOTOOL_INCLUDE_PATH ${CMAKE_BINARY_DIR}/ext_build/cosmotool/include)

ExternalProject_Add(healpix
  DEPENDS cfitsio
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/healpix
  CONFIGURE_COMMAND echo No configure
  BUILD_COMMAND make HEALPIX_TARGET=sampler HEALPIX_CC=${CMAKE_C_COMPILER} HEALPIX_CXX=${CMAKE_CXX_COMPILER} HEALPIX_BASE_PATH=${CMAKE_BINARY_DIR} OMP_SUPPORT=${ENABLE_OPENMP} EXTRA_CPPFLAGS=${CONFIGURE_CPP_FLAGS} OMP_FLAGS=${OpenMP_C_FLAGS}
  BUILD_IN_SOURCE 1
  INSTALL_COMMAND ${CMAKE_COMMAND} -DHEALPIX_DIR:STRING=${CMAKE_SOURCE_DIR}/external/healpix -DDEST_DIR:STRING=${CMAKE_BINARY_DIR}/ext_build/healpix -P ${CMAKE_SOURCE_DIR}/external/install_healpix.cmake
)
set(HPIX_LIBPATH ${CMAKE_BINARY_DIR}/ext_build/healpix/lib)
set(HEALPIX_LIBRARY ${HPIX_LIBPATH}/libhealpix_cxx.a)
set(FFTPACK_LIBRARY ${HPIX_LIBPATH}/libfftpack.a)
set(CXXSUPPORT_LIBRARY ${HPIX_LIBPATH}/libcxxsupport.a)
set(SHARP_LIBRARY ${HPIX_LIBPATH}/libsharp.a)
set(CUTILS_LIBRARY ${HPIX_LIBPATH}/libc_utils.a)

file(WRITE ${CMAKE_BINARY_DIR}/src/flints_healpix_version.hpp "#undef BEFORE_HEALPIX_2_20")
SET(EXTRA_HEALPIX_LIBRARIES ${SHARP_LIBRARY} ${CUTILS_LIBRARY})



SET(HEALPIX_LIBRARIES 
  ${HEALPIX_LIBRARY} ${EXTRA_HEALPIX_LIBRARIES} 
  ${FFTPACK_LIBRARY} ${CXXSUPPORT_LIBRARY} ${CFITSIO_LIBRARY}
) 
set(HEALPIX_INCLUDE_PATH ${CMAKE_BINARY_DIR}/ext_build/healpix/include)

ExternalProject_Add(flints
  DEPENDS cfitsio healpix cosmotool ${gsl_built} ${eigen_built}
  URL ${CMAKE_SOURCE_DIR}/external/flints.tgz
  CMAKE_ARGS 
        -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DENABLE_OPENMP=${ENABLE_OPENMP}
        -DHEALPIX_PATH=${CMAKE_BINARY_DIR}/ext_build/healpix 
        -DCMAKE_INSTALL_PREFIX=${EXT_INSTALL}
        -DCFITSIO_LIBRARY=${CFITSIO_LIBRARY}
        -DGSL_LIBRARY=${GSL_LIBRARY}
        -DGSLCBLAS_LIBRARY=${GSLCBLAS_LIBRARY}
        -DGSL_INCLUDE_PATH=${GSL_INCLUDE_PATH}
        -DGENGETOPT=${GENGETOPT}
        -DBOOST_ROOT=${BOOST_ROOT}
        -DPKG_CONFIG_ENV=${EXT_INSTALL}/lib/pkgconfig
        -DINSTALL_PYTHON_LOCAL=ON
)
set(FLINTS_LIBRARY ${EXT_INSTALL}/lib/libflints.a)
set(FLINTS_INCLUDE_PATH ${CMAKE_BINARY_DIR}/ext_build/flints/include)

set(GSL_LIBRARIES ${GSL_LIBRARY} ${GSLCBLAS_LIBRARY})

include_directories(${CMAKE_BINARY_DIR}/src ${NETCDF_INCLUDE_PATH} ${NETCDFCPP_INCLUDE_PATH} ${GSL_INCLUDE_PATH} ${HEALPIX_INCLUDE_PATH} ${HDF5_INCLUDE_PATH} ${COSMOTOOL_INCLUDE_PATH} ${FLINTS_INCLUDE_PATH} ${Boost_INCLUDE_DIRS})


SET(abyss_DEPS healpix cosmotool flints ${gsl_built} ${netcdf_built} ${boost_built} ${hdf5_built})  

SET(USE_MATH_LIBS)
IF(USE_OPENBLAS)
  set(EIGEN_DEF -DEIGEN_USE_LAPACKE -DEIGEN_USE_OPENBLAS)
  SET(USE_MATH_LIBS "${OPENBLAS_LIBRARIES}")
ENDIF(USE_OPENBLAS)

IF(USE_INTEL_MKL)
  set(EIGEN_DEF -DEIGEN_USE_LAPACKE)
  find_library(MKL_RT mkl_rt)
  find_path(MKL_HEADER mkl.h)
  SET(USE_MATH_LIBS ${MKL_RT})
  include_directories(${MKL_HEADER})
ENDIF(USE_INTEL_MKL)

add_definitions(${EIGEN_DEF})

