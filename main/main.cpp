/*+
This is ABYSS (./main/main.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cstdlib>
#include <cstring>
#include <cassert>
#include <sys/stat.h>
#include <sys/types.h>
#include <string>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "cl_sampler.hpp"
#include "sampler.hpp"
#include "noise.hpp"
#include "cmb_data.hpp"
#include <pointing.h>
#include "extra_map_tools.hpp"
#include <healpix_data_io.h>
#include "main_conf.h"
#include "sampler_program.hpp"
#include "noise_weighing.hpp"
#include "netcdf_adapt.hpp"
#include <boost/format.hpp>
#include <boost/chrono.hpp>
#include <powspec_fitsio.h>
#include <powspec.h>
#include <Eigen/Core>
#include "mpi_communication.hpp"

using boost::format;
using namespace CMB;
using namespace std;

void saveCls(ostream& o, CLS_Sampler *sampler)
{
  const arr<DataType>& cls = sampler->getCls();

  for (long l = 0; l < cls.size(); l++)
    {
      o << l << " " << cls[l]*l*(l+1)/(2*M_PI) << endl;
    }
  o << endl << endl;
}


string safe_str(bool valid, const char *s)
{
  assert(!valid || (s!=0));
  return !valid ? string("") : string(s);
}

int main(int argc, char **argv)
{
  using boost::chrono::system_clock;
  using boost::chrono::duration;

  omp_set_nested(1);
  Eigen::initParallel();

  system_clock::time_point start_run = system_clock::now();
  duration<double> elapsed_time;
  MPI_Communication *comm = setupMPI(argc, argv);

  long nL;

  gsl_rng_env_setup();

  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  CMB_Data data;
  struct mainParams args;
  struct mainConf_params params;

  data.transform = 0;
  data.rinfo.epsilonCG = 1e-6;
  data.rinfo.relaxation_start = 1;
  data.rinfo.relaxation_reduction = 1;

  mainConf_init(&args);
  mainConf_params_init(&params);

  params.check_required = 0;
  if (mainConf_ext(argc, argv, &args, &params))
    return -1;

  if (args.configFile_given)
    {
      params.override = 0;
      params.initialize = 0;
      if (mainConf_config_file(args.configFile_arg, &args, &params))
        {
          mainConf_free(&args);
          return -1;
        }
    }

  if (mainConf_required(&args, argv[0]))
    {
      doneMPI();
      return -1;
    }

  if (args.cg_prec_given)
    data.rinfo.epsilonCG = args.cg_prec_arg;
  if (args.relaxStart_given)
    data.rinfo.relaxation_start = args.relaxStart_arg;
  if (args.relaxReduce_given)
    data.rinfo.relaxation_reduction = args.relaxReduce_arg;

  if (args.seed_given)
    gsl_rng_set(r, args.seed_arg);

  mkdir("debug", 0777);
  mkdir("out", 0777);
  mkdir("text", 0777);


  data.Lmax = args.Lmax_arg;

  nL = args.Lmax_arg;

  SamplerProgram *prog;

  PowSpec pspec;
  read_powspec_from_fits(args.guess_cls_arg, pspec, 1, nL);

  data.guess_cls = pspec.tt();
  data.guess_sqrt_cls.alloc(data.guess_cls.size());
  data.NsideHR = (args.NsideHR_given ? args.NsideHR_arg : -1);

  for (int i = 0; i < data.guess_cls.size(); i++)
    data.guess_sqrt_cls[i] = sqrt(data.guess_cls[i]);

  prog = new SamplerProgram(r, *comm, data, args.sample_arg);
  if (args.solver_option_given)
    prog->setPreconditionerOptions(args.solver_option_arg);
  prog->setWienerAlgorithm(args.wiener_arg);
  if (args.do_weighing_flag)
    prog->setHealpixWeighing(args.healpix_data_arg);

  if (args.load_template_cache_given)
    prog->setCachePrefix(args.load_template_cache_arg);
  if (args.save_template_cache_given)
    prog->saveTemplateCache(args.save_template_cache_arg);

  if (args.pin_dmcmb_flag)
    prog->setPinDMCMB();

  if (args.modulated_cmb_flag)
    prog->setModulated(args.modulated_cmb_flag);
  if (args.lensed_cmb_flag)
    prog->setLensed(args.lensed_cmb_flag);

  prog->init();


  AllSamplers& samplers = prog->getSamplers();
  int state_id = 0;


  if (!args.restart_given)
    {
      ((CLS_Sampler *)samplers[SAMPLER_CLS])->setInitialCondition(data.guess_sqrt_cls);
    }
  else
    {
      string fname = str(format("out/state_%d.nc") % args.restart_arg);
      try
        {
          NcFile f(fname, NcFile::read);

          state_id = args.restart_arg+1;

          try
            {
              list<int> active_list = prog->getActiveList();

              for (list<int>::iterator it = active_list.begin(); it != active_list.end(); ++it)
                if (samplers[*it])
                  samplers[*it]->restoreState(f);
            }
          catch (const StateInvalid& s)
            {
              cerr << "Invalid state in file " << args.restart_arg << endl;
              comm->abort();
            }
        }
        catch (const exceptions::NcException& e)
          {
            cerr << "Invalid restart file" << endl;
            comm->abort();
          }
    }


  ofstream out_cls("all_cls.txt");
  saveCls(out_cls, dynamic_cast<CLS_Sampler*>(samplers[SAMPLER_CLS]));

  do
    {
      if (args.maxIterator_arg == 0)
        break;
      if (args.maxIterator_arg > 0)
        args.maxIterator_arg--;

      for (prog->start(); !prog->finished(); prog->next())
        {
          int sampler_id = prog->getSampler();
          SamplerProgram::OrderList::const_iterator iter_sampler = prog->getAssumed().begin();
          SamplerProgram::OrderList::const_iterator iter_signal = prog->getSignals().begin();

          for (int ch = 0; ch < data.numChannels; ch++)
            data.residuals[ch] = data.channels[ch];

          cout << format("===> Sampler %d: signal is INPUT ") % sampler_id;
          while (iter_sampler != prog->getAssumed().end())
            {
              cout << format(" - [%d, %d] ") % (*iter_sampler) % (*iter_signal) ;
              for (int ch = 0; ch < data.numChannels; ch++)
                {
                  const CMB_Map *map = samplers[*iter_sampler]->getEstimatedMap(ch, *iter_signal);

                  if (map == 0)
                    {
                        {
                          cout << endl;
                          cerr << format("No estimated map for sampler %d, channel %d, signal %d. Stop here.") % *iter_sampler % ch % *iter_signal << endl;
                        }
                      comm->abort();
                    }

                  data.residuals[ch] -= *map;
                }

              ++iter_sampler;
              ++iter_signal;
            }
          cout << endl;

#if 1
            {
              fitshandle f;
              string fname = str(format("!debug/input_sampler_%d.fits") % sampler_id);
              f.create(fname.c_str());

              write_Healpix_map_to_fits(f, data.residuals[0], planckType<double>());
            }
#endif

          if (!samplers[sampler_id]->isMPI())
            {
              if (comm->rank() == 0)
                samplers[sampler_id]->computeNewSample(data, samplers);
              comm->barrier();
            }
          else
            // Sampler support MPI style parallelization
            samplers[sampler_id]->computeNewSample(data, samplers);
        }

      if (comm->rank() == 0)
        {
          ofstream f_state("current_state.txt");
          string fname = str(format("out/state_%d.nc") % state_id);

          try
            {
              NcFile f(fname.c_str(), NcFile::replace);
              list<int> active_list = prog->getActiveList();

              for (list<int>::iterator it = active_list.begin(); it != active_list.end(); ++it)
                {
                  if (samplers[*it])
                    samplers[*it]->saveState(f);
                }

              f_state << state_id << endl;

              state_id++;
            }
          catch(const exceptions::NcException& e)
            {
              cout << "Cannot replace state file..." << endl;
              comm->abort();
            }
        }
      elapsed_time = system_clock::now() - start_run;
    }
  while (!args.time_given || (args.time_given && elapsed_time.count() < args.time_arg));

  if (data.preconditioner != 0)
    delete data.preconditioner;

  gsl_rng_free(r);

  doneMPI();


  return 0;
}
