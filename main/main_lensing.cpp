/*+
This is ABYSS (./main/main_lensing.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <fstream>
#include <iostream>
#include "cl_sampler.hpp"
#include "sampler.hpp"
#include "noise.hpp"
#include "cmb_data.hpp"
#include <pointing.h>
#include "dipole_sampler.hpp"
#include "extra_map_tools.hpp"
#include <healpix_data_io.h>
#include "lensing.hpp"
#include <CosmoTool/miniargs.hpp>

using namespace CMB;
using namespace std;
using namespace CosmoTool;

void saveCls(ostream& o, CLS_Sampler *sampler)
{
  const arr<DataType>& cls = sampler->getCls();

  for (long l = 0; l < cls.size(); l++)
    {
      o << l << " " << cls[l]*l*(l+1)/(2*M_PI) << endl;
    }
  o << endl << endl;
} 

void buildForeground(CMB_Map& noise, double angle)
{
  for (long i = 0; i < noise.Npix(); i++)
    {
      pointing p = noise.pix2ang(i);
      
      if (fabs(M_PI/2-p.theta) < angle*M_PI/180)
	noise[i] *= 1e6;
    }
}

int main(int argc, char **argv)
{
  AllSamplers samplers;
  int nL = 50, nLPhi;
  long nSide = -1;
  double noiseLevel;
  MiniArgDesc args[] = {
    { "LMAX CMB", &nL, MINIARG_INT },
    { "LMAX PHI", &nLPhi, MINIARG_INT },
    { "NOISE LEVEL", &noiseLevel, MINIARG_DOUBLE },
    { 0, 0, MINIARG_NULL }
  };
  if (!parseMiniArgs(argc, argv, args))
    return 1;

  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  CMB_Data data;
  PowSpec cmbspec(1, nL);

  noiseLevel *= 1e-6;

  gsl_rng_set(r, 1000);

  //  fitshandle myfile("mycmb.fits");

  data.channels.alloc(1);
  data.noise.alloc(1);
  data.residuals.alloc(1);
  data.beam.alloc(1);

  data.numChannels = 1;

  read_powspec_from_fits("spectrum.fits", cmbspec, 1, nL);

  read_Healpix_map_from_fits("mycmb.fits", data.channels[0], 1, 2);
  if (data.channels[0].Scheme() == NEST)
    data.channels[0].swap_scheme();
  nSide = data.channels[0].Nside();

  //read_weight_ring ("../Healpix_2.11c/data/", nSide,
  //data.weight);
//  for (int i = 0; i < 2*nSide; i++)
//    data.weight[i] += 1;  
  data.weight.alloc(2*nSide);
  data.weight.fill(1);

  cout << "nside = " << nSide << endl;
  data.noise[0].SetNside(nSide, RING);
  data.residuals[0].SetNside(nSide, RING);
  data.noise[0].fill(noiseLevel*noiseLevel);
  data.noise[0].alpha = 1.0;
  data.beam[0].alloc(nL+1);
  data.beam[0].fill(1.);

  samplers.resize(NUM_SAMPLERS);

  samplers[SAMPLER_CMB] = new CMB_Sampler(r, data, nL, nSide);
  samplers[SAMPLER_CLS] = new CLS_Sampler(r, nL);
  samplers[SAMPLER_NOISE] = new NOISE_Sampler(r);
  samplers[SAMPLER_DIPOLE] = new Dipole_Sampler(r, nSide);
  samplers[SAMPLER_LENSING] = new Lensing_Sampler(r, data, nL, nLPhi, nSide);

  ((CLS_Sampler *)samplers[SAMPLER_CLS])->setInitialCondition(cmbspec.tt());

	ofstream f_alpha("alpha.txt");
//  double alpha = 1e-6;
  while (1)
    {
      for (int ch = 0; ch < data.numChannels; ch++)
	data.residuals[ch] = data.channels[ch];      
      samplers[SAMPLER_LENSING]->computeNewSample(data, samplers);
//  data.noise[0].fill(alpha*alpha);
//	if (alpha > 1e-9) alpha /= 1.001;
//	f_alpha << alpha << endl;
    }
  

  gsl_rng_free(r);

  return 0;
}
