/*+
This is ABYSS (./main/data_loader.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_DATA_LOADER_HPP
#define __CMB_DATA_LOADER_HPP

#include <string>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"

namespace CMB
{

  class GenericDataLoader
  {
  public:
    virtual void load(CMB_Data& data, int ch_id) = 0;
  };

  
  class HealpixTemperatureMapLoader: public GenericDataLoader
  {
  private:
    std::string fname;
  public:
    HealpixTemperatureMapLoader(const std::string& fname);
    
    virtual void load(CMB_Data& data, int ch_id);    
  };

  class SphericalBeamLoader: public GenericDataLoader
  {
  private:
    std::string fname;
  public:
    SphericalBeamLoader(const std::string& fname);
    
    virtual void load(CMB_Data& data, int ch_id);    
  };

  class HealpixNoiseMapLoader: public GenericDataLoader
  {
  private:
    double err_norm;
    double mean_noise;
    std::string fname;
  public:
    HealpixNoiseMapLoader(const std::string& fname, double normalization, double mn);
    
    virtual void load(CMB_Data& data, int ch_id);    
  };

  class HealpixColoredNoiseMapLoader: public GenericDataLoader
  {
  private:
    double err_norm;
    double mean_noise;
    std::string fname;
  public:
    HealpixColoredNoiseMapLoader(const std::string& fname, double normalization, double mn);
    
    virtual void load(CMB_Data& data, int ch_id);    
  };
  
  class HealpixMaskLoader: public GenericDataLoader
  {
  private:
    std::string fname;
  public:
    HealpixMaskLoader(const std::string& fname);
    
    virtual void load(CMB_Data& data, int ch_id);    
  };

};

#endif
