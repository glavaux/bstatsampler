/*+
This is ABYSS (./main/load_data.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cassert>
#include <string>
#include <cmath>
#include <healpix_map.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "noise.hpp"
#include "cmb_data.hpp"
#include "extra_map_tools.hpp"
#include <healpix_data_io.h>
#include <cstdlib>
#include <cstring>
#include "netcdf_adapt.hpp"
#include "noise_weighing.hpp"
#include <powspec_fitsio.h>
#include <powspec.h>
#include "load_data.hpp"

using namespace CMB;
using namespace std;

void CMB::loadChannels(CMB_Data& data, long& nSide, const string& input_list)
{
  string fname, data_list = input_list;
  size_t p;
  
  data.numChannels = 1;
  
  while ((p = data_list.find(",")) != string::npos)
    {
      data_list = data_list.substr(p+1);
      data.numChannels++;
    }

  cout << "-> Detected " << data.numChannels << " channels available" << endl;
  data.channels.alloc(data.numChannels);
  data.noise.alloc(data.numChannels);
  data.residuals.alloc(data.numChannels);
  data.beam.alloc(data.numChannels);
  data.masks.alloc(data.numChannels);
 
  data_list = input_list;
  int q = 0;
  do
    {
      p = data_list.find(",");
      fname = data_list.substr(0, p);
      
      cout << "-> Loading " << fname << "..." << endl;
      read_Healpix_map_from_fits(fname, data.channels[q], 1, 2);
      if (data.channels[q].Scheme() == NEST)
        data.channels[q].swap_scheme();
      data_list = data_list.substr(p+1);
      
      if (nSide != data.channels[q].Nside() && nSide >= 0)
        {
          cerr << "Incompatible Nsides. Stopping now." << endl;
          exit(1);
        }

      nSide = data.channels[q].Nside();
      q++;
    }
  while (p != string::npos);

  data.Nside = nSide;
  cout << "nside = " << nSide << endl;
}

void CMB::loadBeams(CMB_Data& data, const string& input_list)
{
  long Lmax = data.Lmax;
  string beam_list = input_list;
  string fname;
  int q = 0;
  size_t p;

  do
    {
      if (q >= data.numChannels)
      	{
	        cerr << "Too many beams provided !" << endl;
      	  exit(1);
      	}
      p = beam_list.find(",");
      fname = beam_list.substr(0, p);

      cout << "-> Loading beam " << fname << endl;

      ifstream f(fname.c_str());
      if (!f)
        {
          cerr << "Non existing file " << fname << endl;
          exit(1);
        }
 
      beam_list = beam_list.substr(p+1);
 
      string line;
      vector<double> data_beam;

      while (getline(f, line))
      	{	  
          if (line[0] == '#')
             continue;

      	  istringstream sf(line);
       	  double l0;
          int l;

	        sf >> l0;
          l = int(l0);
      	  if (l >= data_beam.size())
	          data_beam.resize(l+1);

      	  sf >> data_beam[l];
          if (l < data.pixwin.size())
            data_beam[l] *= data.pixwin[l];
          else
            data_beam[l] = 0;
      	}
									
      data.beam[q].alloc(Lmax+1);
      data.beam[q].fill(0);
      long s = min((long)data_beam.size(), Lmax+1);
      memcpy(&data.beam[q][0], &data_beam[0], sizeof(double)*s);

      q++;
    }
  while (p != string::npos);

  if (q < data.numChannels)
    {
      cerr << "Too few beams provided !" << endl;
      exit(1);
    }
}

void CMB::load_NN_Masks(CMB_Data& data, const list<string>& mask_list  )
{
  string fname, data_list = mask_list;
  size_t p;
 
  int q = 0;
  do
    {
      p = data_list.find(",");
      fname = data_list.substr(0, p);
      CMB_Map mask;
      CMB_NN_NoiseMap *noise = new CMB_NN_NoiseMap();
      
      cout << "-> Loading mask " << fname << "..." << endl;
      read_Healpix_map_from_fits(fname, mask, 1, 2);
      if (mask.Scheme() == NEST)
        mask.swap_scheme();
      data_list = data_list.substr(p+1);
      
      noise->SetNside(data.channels[0].Nside(), RING);
      noise->Import(mask);
      data.noise[q] = noise;
      
      data.masks[q].SetNside(data.channels[0].Nside(), RING);
      data.masks[q].Import(mask);

      noise->masked = 0;

      for (long i = 0; i < noise->Npix(); i++)
        if ((*noise)[i] < 0.1) // This should be sufficiently ok for zero and one right ?
          noise->masked++;
      cout << "-> " << setprecision(3) << 100*noise->masked/float(noise->Npix()) << "% sky is masked" << endl;

      q++;
    }
  while (p != string::npos);
}

void CMB::load_NN_Errors(CMB_Data& data,  const list<string>& error_list, const list<string>& error_norm_list)
{
  string fname, data_list = error_list, data_list2 = error_norm_list;
  size_t p, p2;
  string errnorm;

  int q = 0;
  do
    {
      if (valid)
      	p = data_list.find(",");
      p2 = data_list2.find(",");

      if (valid)
      	fname = data_list.substr(0, p);
      errnorm = data_list2.substr(0, p2);

      CMB_Map error;
      
      cout << "-> Loading error variance " << fname << "..." << endl;
      if (valid)
        {
          read_Healpix_map_from_fits(fname, error, 1, 2);
          if (error.Scheme() == NEST)
            error.swap_scheme();
          data_list = data_list.substr(p+1);
          data_list2 = data_list2.substr(p2+1);
        }

      double dnorm = strtod(errnorm.c_str(), 0);
      cout << "++ Normalized to " << dnorm*1e3 << " mK " << endl;

      // This is an unnormalized error. We still have to multiply
      // this by a constant to convert this to Kelvins.

      CMB_NN_NoiseMap *noise = data.noise[q]->as<CMB_NN_NoiseMap>();


      if (valid)
        *noise *= error;
        
      // Convert this to Kelvins
      *noise *= 1/(dnorm*dnorm);

      noise->min_noise = 0;
      for (long p = 0; p < noise->Npix(); p++)
        noise->min_noise = max(noise->min_noise, (*noise)[p]);
      noise->min_noise = 1/noise->min_noise;
     
      q++;
    }
  while ((!valid || p != string::npos) && p2 != string::npos);

  if ((valid && p != string::npos) || p2 != string::npos)
    {
      cout << "Missing error data to proceed. Stopping." << endl;
      exit(1);
    }
}
