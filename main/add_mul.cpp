/*+
This is ABYSS (./main/add_mul.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <fstream>
#include <iostream>
#include <CosmoTool/miniargs.hpp>

typedef Healpix_Map<double> CMB_Map;

using namespace CosmoTool;

int main(int argc, char **argv)
{
  CMB_Map data;
  char *inMap, *outMap;
  double mulLevel1, mulLevel2;

  MiniArgDesc args[] = {
    { "INPUT MAP", &inMap, MINIARG_STRING },
    { "OUTPUT MAP", &outMap, MINIARG_STRING },
    { "MUL1", &mulLevel1, MINIARG_DOUBLE },
    { "MUL2", &mulLevel2, MINIARG_DOUBLE },
    { 0, 0, MINIARG_NULL }
  };
  if (!parseMiniArgs(argc, argv, args))
    return 0;

  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);

  read_Healpix_map_from_fits(inMap, data, 1, 2);
  
  std::cout << "Npix=" << data.Npix() << std::endl;

  double nlevel = 0.0;
  for (long i = 0; i < data.Npix(); ++i)
    {
      pointing ptg = data.pix2ang(i);

      if (ptg.theta > M_PI/2)
	data[i] *= mulLevel2;
      else
	data[i] *= mulLevel1;
    }

  fitshandle out;
  out.create(outMap);
  write_Healpix_map_to_fits(out, data, FITSUTIL<double>::DTYPE);

  return 0;
}
