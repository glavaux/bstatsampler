/*+
This is ABYSS (./main/sampler_parser_0.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/config/warning_disable.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/qi_eol.hpp>
#include <boost/spirit/include/qi_as.hpp>
#include <boost/spirit/include/qi_char_class.hpp>
#include <boost/spirit/include/qi_matches.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/container/set.hpp>
#include <boost/fusion/include/set.hpp>
#include <boost/fusion/container/set/set_fwd.hpp>
#include <boost/fusion/include/set_fwd.hpp>
#include <boost/spirit/home/phoenix/bind/bind_function.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include "sampler_parser.hpp"
#include "sampler_program_exception.hpp"

using namespace std;
using boost::algorithm::trim;
using boost::format;
using namespace CMB;

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;
namespace spirit = boost::spirit;

using CMB::SamplerProgramException;

typedef list<string> arg_list;
typedef map<string, arg_list> arg_container;

struct sampler_definition
{
  string sampler_name;
  string sampler_type;
  arg_container args;
  set<string> flags;
  string cl_fname;
};

typedef std::vector<sampler_definition> sampler_definitions;

struct arg_struct
{
  string argname;
  arg_list parameters;
};

BOOST_FUSION_ADAPT_STRUCT(
			  sampler_definition,
			  (std::string, sampler_name)
			  (std::string, sampler_type)
			  (arg_container, args)
			  (std::set<std::string>, flags)
			  (std::string, cl_fname)
			  )

BOOST_FUSION_ADAPT_STRUCT(
			  arg_struct,
			  (std::string, argname)
			  (arg_list, parameters)
)

static void check_and_insert_arg(arg_container& c, arg_struct const& p)
{
    // check for duplicates and throw on error
  string argname = boost::to_upper_copy(p.argname);
  
  if (c.find(argname) != c.end())
    {
      cerr << format("Duplicated keyword %s.") % p.argname << endl;
      throw SamplerProgramException();
    }

//  cerr << format("== Inserting argument %s.") % p.argname << endl;

  c[argname] = p.parameters;
}

void check_and_insert_flag(set<string>& all_flags, string const& s)
{
  string cap_s = boost::to_upper_copy(s);

  if (all_flags.find(cap_s) != all_flags.end())
    {
      cerr << format("Duplicated flag %s.") % cap_s << endl;
      throw SamplerProgramException();
    }

  all_flags.insert(cap_s);
}
 
template<typename Iterator>
struct sampler_definition_grammar : qi::grammar<Iterator, sampler_definitions(), ascii::space_type>
{
  sampler_definition_grammar() : sampler_definition_grammar::base_type(start)
  {
    using qi::_1;
    using ascii::space;
    using phoenix::ref;
    using phoenix::push_back;
    using qi::char_;
    using qi::as;
    using ascii::alpha;
    using ascii::alnum;
    using qi::_val;
    using phoenix::at_c;
    using spirit::lexeme;
    using qi::eoi;
    using phoenix::bind;

    identifier %= as<std::string>()[+(alnum|char_('_')|char_('.'))];
    value %= as<std::string>()[+(alnum|char_('-')|char_('+')|char_('_')|char_('.'))];
    quoted_string %= lexeme['"' >> +(char_ - '"') >> '"'];

    parsed_arg_list = 
      ("{" >> ( (quoted_string[push_back(_val,_1)]) % ",") >> "}") |
      (quoted_string[push_back(_val,_1)]) |
      (value[push_back(_val,_1)]);

    one_arg %= (identifier >> "=" >> parsed_arg_list );

    args = one_arg[bind(check_and_insert_arg, _val, _1)] % ",";
    flags = identifier[bind(check_and_insert_flag, _val, _1)] % ",";

    one_definition = identifier[at_c<0>(_val) = _1] >> ":=" >>
      identifier[at_c<1>(_val) = _1] >>
      "(" >> args[at_c<2>(_val) = _1] >> ")" 
      >> -("[" >> flags[at_c<3>(_val) = _1] >> "]") >>
      ";";

    start %= one_definition;
  }

  qi::rule<Iterator, std::string(), ascii::space_type> quoted_string;
  qi::rule<Iterator, std::string(), ascii::space_type> identifier;
  qi::rule<Iterator, std::string(), ascii::space_type> value;
  qi::rule<Iterator, arg_list(), ascii::space_type> parsed_arg_list;
  qi::rule<Iterator, arg_struct(), ascii::space_type> one_arg;
  qi::rule<Iterator, arg_container(), ascii::space_type> args;
  qi::rule<Iterator, sampler_definition(), ascii::space_type> one_definition;
  qi::rule<Iterator, std::set<std::string>(), ascii::space_type> flags;
  qi::rule<Iterator, sampler_definitions(), ascii::space_type> start;
};

template<typename Iterator>
static Iterator run_parser(Iterator first, Iterator last, sampler_definition& def)
{
  using boost::spirit::ascii::space;
  sampler_definition_grammar<Iterator> g;
  sampler_definitions def_multi;

  bool r = qi::phrase_parse(first, last, g, space, def_multi);

  if (!r)
    {
      cerr << format("Error while parsing. Stopped at '%s'.") % string(first, last) << endl;
      throw SamplerParseException();
    }
  
  def = def_multi[0];
  
  return first;
}


static bool grab_single_keyword(sampler_definition& def, const string& kwname, string& value)
{
  arg_container::iterator optarg = def.args.find(kwname);
  if (optarg == def.args.end())
    return false;
    
  if (optarg->second.size() != 1)
    {    
      cerr << format("An argument to '%s' is required for %s and it must a singleton") % kwname % def.sampler_name << endl;
      throw SamplerParseException();
    }
  value = *optarg->second.begin();
  return true;
}

static string grab_single_keyword(sampler_definition& def, const string& kwname)
{
  arg_container::iterator optarg = def.args.find(kwname);
  if (optarg == def.args.end() || optarg->second.size() != 1)
    {
      cerr << format("An argument to '%s' is required for %s and it must a singleton") % kwname % def.sampler_name << endl;
      throw SamplerParseException();
    }
  return *optarg->second.begin();
}

static list<string> grab_multi_keyword(sampler_definition& def, const string& kwname)
{
  arg_container::iterator optarg = def.args.find(kwname);
  if (optarg == def.args.end())
    {
      cerr << format("An argument to '%s' is required for %s") % kwname % def.sampler_name << endl;
      throw SamplerParseException();
    }
  return optarg->second;
}


template<typename T> static const char * get_name_grab();

template<> const char *get_name_grab<int>()
{
  return "an integer";
}

template<> const char *get_name_grab<double>()
{
  return "a floating point value";
}

template<typename T>
static T grab_single(sampler_definition& def, const std::string& kwname)
{
  try
    {
      return boost::lexical_cast<T>(grab_single_keyword(def, kwname));
    }
  catch (boost::bad_lexical_cast&)
    {
      cerr << format("The argument to '%s' must be %s") % kwname % get_name_grab<T>() << endl;
      throw SamplerParseException();
    }
}

template<typename T>
static T grab_single(sampler_definition& def, const std::string& kwname, T default_value)
{
  string value;
  
  if (!grab_single_keyword(def, kwname, value))
    return default_value;
  
  try
    {
      return boost::lexical_cast<T>(value);
    }
  catch (boost::bad_lexical_cast&)
    {
      cerr << format("The argument to '%s' must be %s") % kwname % get_name_grab<T>() << endl;
      throw SamplerParseException();
    }
}


template<typename Iterator>
Iterator CMB::parse_sampler_definition(Iterator first, Iterator last, 
			      string& sampler_name, string& sampler_type,
			      sampler_def& definition)
  throw(SamplerParseException)
{
  sampler_definition def;

  first = run_parser(first, last, def);

  sampler_name = def.sampler_name;
  sampler_type = def.sampler_type;

  if (def.args.find("IN_CL") == def.args.end() && def.flags.count("PINNED_SPECTRUM")>0)
    {
      cerr << format("PINNED_SPECTRUM flag given but no input Cl") << endl;
      throw SamplerParseException();
    }

  definition.Lmin = grab_single<int>(def, "LMIN", 0);
  definition.Lmax = grab_single<int>(def, "LMAX", -1);
  definition.args = def.args;
  definition.signal_type = grab_single_keyword(def, "SIGNAL");
  definition.flags = def.flags;

  return first;
}

template<typename Iterator>
Iterator CMB::parse_sampler_data_def(Iterator first, Iterator last, sampler_data_def& data_def)
  throw(SamplerParseException)
{
  sampler_definition def;
  
  first = run_parser(first, last, def);

  if (def.sampler_type != "healpix")
    {
      cerr << "Only 'healpix' definitions are supported for DATA (found=" << def.sampler_type << ")" << endl;
      throw SamplerParseException();
    }

  data_def.name = def.sampler_name;
  data_def.data_type = def.sampler_type;
  data_def.data_file = grab_single_keyword(def, "INPUT");
  data_def.noise_type = grab_single_keyword(def, "NOISE_TYPE");
  data_def.noise_file = grab_single_keyword(def, "NOISE");
  data_def.noise_norm = grab_single<double>(def, "NOISE_NORM");
  if (!grab_single_keyword(def, "MASK", data_def.mask_file))
    data_def.mask_file = string();
  data_def.beam_file = grab_single_keyword(def, "BEAM");
  data_def.mean_noise = grab_single<double>(def, "MEAN_NOISE", -1);

  return first;  
}


template string::iterator CMB::parse_sampler_definition<string::iterator>(string::iterator first, string::iterator last,
							 string& sampler_name, string& sampler_type,
							 sampler_def& definition)
   throw(SamplerParseException);
   
template string::iterator CMB::parse_sampler_data_def<string::iterator>(string::iterator first, string::iterator last, sampler_data_def& def)
   throw(SamplerParseException);


