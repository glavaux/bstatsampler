/*+
This is ABYSS (./main/mask_preprocess.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cmath>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <CosmoTool/miniargs.hpp>
#include <string>
#include <fitshandle.h>
#include <boost/format.hpp>
#include <vector>
#include <pointing.h>
#include "extra_map_tools.hpp"

using namespace CMB;
using namespace CosmoTool;
using namespace std;
using boost::format;

void do_smoothing(Healpix_Map<double>& input_mask,
		  Healpix_Map<double>& output_mask,
		  double radius,
		  double threshold)
{ 
  radius *= M_PI/180;

#pragma omp parallel for schedule(static)
  for (int p = 0; p < output_mask.Npix(); p++)
    {
      vector<int> listpix;
      pointing dir = output_mask.pix2ang(p);
      input_mask.query_disc(dir, radius, listpix);

      output_mask[p] = 0;
      for (int q = 0; q < listpix.size(); q++)
	output_mask[p] += input_mask[listpix[q]];

      output_mask[p] /= listpix.size();

      if (threshold > 0)
	{
	  if (output_mask[p] < threshold)
	    output_mask[p] = 0;
	  else
	    output_mask[p] = 1;
	}
    }
}

int main(int argc, char **argv)
{
  Healpix_Map<double> input_mask, out_mask;
  char *inMask, *outMask;  
  double baseFilterRadius, largeRadius;
  int numFilters;
  double threshold;

  MiniArgDesc args[] = {
    { "INPUT MASK", &inMask, MINIARG_STRING },
    { "OUTPUT MASK", &outMask, MINIARG_STRING },
    { "SMALL FILTER", &baseFilterRadius, MINIARG_DOUBLE },
    { "LARGE FILTER", &largeRadius, MINIARG_DOUBLE },
    { "NUM FILTERS", &numFilters, MINIARG_INT },
    { "THRESHOLD", &threshold, MINIARG_DOUBLE },
    { 0, 0, MINIARG_NULL }
  };
  if (!parseMiniArgs(argc, argv, args))
    return 0;
  
  read_Healpix_map_from_fits(inMask, input_mask, 1, 2);

  out_mask.SetNside(input_mask.Nside(), RING);

  double delta = log(largeRadius/baseFilterRadius)/numFilters;

  Healpix_Map<double> out_mask2, previous_mask;
  Healpix_Map<int> filter_id;

  filter_id.SetNside(input_mask.Nside(), RING);
  filter_id.fill(-1);

  for (int fn = 0; fn < numFilters; fn++)
    {
      cout << format("Doing filter %d / %d") % fn % numFilters << endl;
      double filterRadius = baseFilterRadius*exp(delta*fn);
      double filterRadius2 = baseFilterRadius*exp(delta*(fn+1));

      do_smoothing(input_mask, out_mask, filterRadius, threshold);
      //      if (fn != 0)
      //	{
      //	  out_mask2 = out_mask - previous_mask;
      //	}
      //      else
	out_mask2 = out_mask;

      {
	fitshandle f;
	string fname = str(format("!%s_%d.fits") % outMask % fn);
	f.create(fname.c_str());
    
	write_Healpix_map_to_fits(f, out_mask2, planckType<double>());
      }
      //      input_mask = out_mask;

      for (int p = 0; p < input_mask.Npix(); p++)
	{
	  if (filter_id[p] == -1 && out_mask[p] > threshold)
	    filter_id[p] = fn;
	}
    }
  fitshandle f;
  f.create("!filterId.fits");
  write_Healpix_map_to_fits(f, filter_id, planckType<int>());
  return 0;
}
