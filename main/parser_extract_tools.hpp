/*+
This is ABYSS (./main/parser_extract_tools.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __CMB_PARSER_EXTRACT_TOOLS_HPP
#define __CMB_PARSER_EXTRACT_TOOLS_HPP

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <map>
#include <iostream>
#include <string>
#include <list>

namespace CMB
{
  typedef std::list<std::string> ArgList;
  typedef std::map<std::string, ArgList> KeywordArg;

  template<typename T> static const char *_argtypename_extract();
  
  template<> const char *_argtypename_extract<int>()
  {
    return "an integer";
  }

  template<> const char *_argtypename_extract<float>()
  {
    return "a floating point value";
  }

  template<> const char *_argtypename_extract<double>()
  {
    return "a floating point value";
  }

  template<> const char *_argtypename_extract<std::string>()
  {
    return "a string of character";
  }

  static std::string extract_name(const std::string& field_name, const KeywordArg& args)
  {
    double d;
    KeywordArg::const_iterator it = args.find(field_name);
    std::string s;

    if (it == args.end() || it->second.empty())
      {
        std::cerr << boost::format("Argument %s is not specified") % field_name << std::endl;
        throw SamplerProgramException();
      }

    return *(it->second.begin());
  }

  static bool extract_name(const std::string& field_name, const KeywordArg& args, std::string& result)
  {
    double d;
    KeywordArg::const_iterator it = args.find(field_name);
    std::string s;

    if (it == args.end() || it->second.empty())
      {
        return false;
      }

    result = *(it->second.begin());
    return true;
  }

  
  template<typename T>
  static T extract_value(const std::string& field_name, const KeywordArg& args)
  {
    T i;
    KeywordArg::const_iterator it = args.find(field_name);
    std::string s;

    s = extract_name(field_name, args);
    std::cout << "Extract KW " << field_name << " -> " << s << std::endl;

    try
      {
        i = boost::lexical_cast<T>(s);
        std::cout << "Got value " << i << std::endl;
      }
    catch (boost::bad_lexical_cast&)
      {
        std::cerr << boost::format("The argument to %d must be %s") % field_name % (_argtypename_extract<T>()) << std::endl;
        throw SamplerProgramException();
      }
    return i;
  }


  template<typename T>
  static bool extract_value(const std::string& field_name, const KeywordArg& args, T& result)
  {
    double d;
    KeywordArg::const_iterator it = args.find(field_name);
    std::string s;

    if (it == args.end() || it->second.empty())
      return false;

    s = *(it->second.begin());

    try
      {
        result = boost::lexical_cast<T>(s);
      }
    catch (boost::bad_lexical_cast&)
      {
        std::cerr << boost::format("The argument to %d must be %s") % field_name % (_argtypename_extract<T>()) << std::endl;
        throw SamplerProgramException();
      }

    return true;
  }

};

#endif
