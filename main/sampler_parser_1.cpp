/*+
This is ABYSS (./main/sampler_parser_1.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/config/warning_disable.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/qi_eol.hpp>
#include <boost/spirit/include/qi_as.hpp>
#include <boost/spirit/include/qi_char_class.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/container/set.hpp>
#include <boost/fusion/include/set.hpp>
#include <boost/fusion/container/set/set_fwd.hpp>
#include <boost/fusion/include/set_fwd.hpp>
#include <boost/spirit/home/phoenix/bind/bind_function.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include "sampler_parser.hpp"
#include "sampler_program_exception.hpp"

using namespace std;
using boost::algorithm::trim;
using boost::format;
using namespace CMB;

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;
namespace spirit = boost::spirit;

using CMB::SamplerProgramException;

BOOST_FUSION_ADAPT_STRUCT(sampler_requirement,
			  (std::string, sampler)
			  (std::string, signal)
			  )

typedef list<string> arg_list;
typedef map<string, arg_list> arg_container;

struct sampler_instruction
{
  string sampler_id;
  list<sampler_requirement> needed;
  int auto_req;
};

BOOST_FUSION_ADAPT_STRUCT(
			  sampler_instruction,
			  (std::string, sampler_id)
			  (std::list<sampler_requirement>, needed)
			  (int, auto_req)
			  )
 
template<typename Iterator>
struct sampler_instruction_grammar : qi::grammar<Iterator, sampler_instruction(), ascii::space_type>
{
  sampler_instruction_grammar() : sampler_instruction_grammar::base_type(start)
  {
    using qi::_1;
    using qi::eoi;
    using ascii::space;
    using phoenix::ref;
    using phoenix::push_back;
    using qi::char_;
    using qi::as_string;
    using ascii::alpha;
    using ascii::alnum;
    using qi::_val;
    using phoenix::at_c;

    one_requirement %= 
      '[' >> as_string[+(alnum|char_('_'))] >> 
      ',' >> as_string[+(alnum|char_('_'))] >>  ']';

    requirements =
      one_requirement[push_back(_val,_1)] % char_(',');

    auto_requirement = char_('?');

    start =       
      as_string[+(alnum|char_('_'))][at_c<0>(_val) = _1] >> -(':' >> (auto_requirement[at_c<2>(_val) = 1] | requirements[at_c<1>(_val) = _1])) >> ';';
  }

  qi::rule<Iterator, sampler_requirement(), ascii::space_type> one_requirement;
  qi::rule<Iterator, std::list<sampler_requirement>(), ascii::space_type> requirements;
  qi::rule<Iterator, sampler_instruction(), ascii::space_type> start;
  qi::rule<Iterator, ascii::space_type> auto_requirement;
};



template<typename Iterator>
Iterator CMB::parse_sampler_instruction(Iterator first, Iterator last, 
					string& sampler_id, list<sampler_requirement>& requirements,
					bool& auto_req)
  throw(SamplerParseException)
{
  sampler_instruction_grammar<Iterator> g;
  sampler_instruction instruct;

  using boost::spirit::ascii::space;

  instruct.auto_req = 0;

  bool r = qi::phrase_parse(first, last, g, space, instruct);
  if (!r)
    {
      cout << format("Error while parsing. Stopped at '%s'.") % string(first, last) << endl;
      throw SamplerParseException();
    }
  
  requirements = instruct.needed;
  sampler_id = instruct.sampler_id;
  auto_req = (instruct.auto_req == 1);

  return first;
}


template string::iterator 
  CMB::parse_sampler_instruction<string::iterator>(string::iterator first,
						   string::iterator last,
						   string& sampler_id,
						   list<sampler_requirement>& requirements, 
						   bool& auto_req)
  throw (SamplerParseException);
