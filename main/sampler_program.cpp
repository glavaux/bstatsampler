/*+
This is ABYSS (./main/sampler_program.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/lambda/construct.hpp>
#include <boost/config/warning_disable.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <powspec.h>
#include <powspec_fitsio.h>

#include <cstdlib>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <unistd.h>

#include <alm_healpix_tools.h>
#include <healpix_data_io.h>


#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "sampler_program.hpp"
#include "noise_weighing.hpp"
#include "sampler_parser.hpp"

#ifdef MPI_ENABLED
#include "mpi_communication.hpp"
#endif
#include "parallel_multi_template_sampler_wiener.hpp"

#include "noise.hpp"
#include "cl_sampler.hpp"
#include "cmb_sampler.hpp"
#include "cmb_mess_sampler.hpp"
#include "modulation_sampler.hpp"
#include "aberration_sampler.hpp"
#include "lensing_sampler.hpp"
#include "hamiltonian_cmb.hpp"
#include "hamiltonian_mess_cmb.hpp"

#include "modulation_operator.hpp"
#include "lensing_operator.hpp"


#include "parser_extract_tools.hpp"
#include "noise_weighing.hpp"
#include "recursive_preconditioner.hpp"
#include "keyword_parser.hpp"

#include "ew_wiener_filter.hpp"
#include "ew_DFD_wiener_filter.hpp"
#include "cg_wiener_filter.hpp"
#include "cmb_noise_np.hpp"
#include "dual_messenger_wiener.hpp"
#include "command_wiener_filter.hpp"

using namespace CMB;
using namespace std;
using boost::algorithm::trim;
using boost::format;
using boost::split;
using boost::is_any_of;
using boost::token_compress_on;

ProgramPostprocess::ProgramPostprocess()
{
}

ProgramPostprocess::~ProgramPostprocess()
{
}

class DefinitionPostprocess: public ProgramPostprocess
{
public:
  CacheCapable *s;
  bool lensing_enabled;
  string cl_fname;

  DefinitionPostprocess()
  {}

  virtual ~DefinitionPostprocess()
  {
  }

  virtual void exec(CMB_Data& data) {
    PowSpec pspec;

    if (cl_fname != "")
      {
	read_powspec_from_fits(cl_fname, pspec, 1, data.Lmax);
	s->setInitialSpectrum(pspec.tt());
      }


    s->pinCMBspectrum(data);
  }

  virtual void loadCache(const string&prefix) {
    s->loadCache(prefix);
  }

  virtual void saveCache(const string& prefix) {
    cout << "save cache to " << prefix << endl;
    s->saveCache(prefix);
  }
};

class LazyConstruction: public ProgramPostprocess
{
public:
  typedef boost::function0<SingleSampler *> ConstructorType;
private:
  AllSamplers& samplers;
  int mid;
  ConstructorType lazy_constructor;
public:
  LazyConstruction(AllSamplers& s, int id,
		   ConstructorType c)
    : lazy_constructor(c), samplers(s), mid(id)
  {
  }

  virtual void exec(CMB_Data& data)
  {
    samplers[mid] = lazy_constructor();
  }

  virtual void loadCache(const string&)
  {
  }

  virtual void saveCache(const string&)
  {
  }
};


struct SamplerName {
  const char *name;
  int id;
};

static SamplerName s_sampler_names[] = {
  { "cmb", SAMPLER_CMB },
  { "noise", SAMPLER_NOISE },
  { "cls", SAMPLER_CLS },
  { "aberration", SAMPLER_ABERRATION }
};

static SamplerName s_signal_names[] = {
  { "wiener_cmb", SAMPLER_SIGNAL_WIENER_CMB },
  { "proposed_cmb", SAMPLER_SIGNAL_PROPOSED_CMB },
  { "dipole", SAMPLER_SIGNAL_DIPOLE },
  { "0", SAMPLER_SIGNAL_DEFAULT }
};

static int numSamplerNames = sizeof(s_sampler_names)/sizeof(SamplerName);
static int numSignalNames = sizeof(s_signal_names)/sizeof(SamplerName);

void SamplerProgram::handleProgramLine(string& line, int line_no)
{
  string sampler_name;
  list<sampler_requirement> requirements;
  bool auto_req;
  int id = 0;
  string::iterator it;

  while (!line.empty())
    {
     try
        {
          it = parse_sampler_instruction(line.begin(), line.end(), sampler_name, requirements, auto_req);
        }
      catch(const SamplerParseException& exc)
        {
          cerr << format("%d: Error while parsing sampler template definition.") % line_no << endl;
          throw SamplerProgramException();
        }

     if (sampler_keys.find(sampler_name) == sampler_keys.end())
       {
	 cerr << "Unknown sampler " << sampler_name << " at line " << line_no <<endl;
	 throw SamplerProgramException();
       }

      sampler_list.push_back(sampler_keys[sampler_name]);
      sampler_order.resize(id+1);
      sampler_signals.resize(id+1);

      list<sampler_requirement>::iterator iter_r = requirements.begin();

      cout << "Adding " << sampler_name << " in chain" << endl;

      while (iter_r != requirements.end())
	{
	  if (sampler_keys.find(iter_r->sampler)==sampler_keys.end())
	    {
	      cerr << "Unknown sampler " << iter_r->sampler << " at line " << line_no <<  endl;
	      throw SamplerProgramException();
	    }
	  if (signal_keys.find(iter_r->signal)==signal_keys.end())
	    {
	      cerr << "Unknown signal " << iter_r->signal << " at line " << line_no << endl;
	      throw SamplerProgramException();
	    }

	  cout << "   Depends on " << iter_r->sampler << " , signal " << iter_r->signal << endl;

	  sampler_order[id].push_back(sampler_keys[iter_r->sampler]);
	  sampler_signals[id].push_back(signal_keys[iter_r->signal]);
	  ++iter_r;
	}

      if (auto_req)
	{
	  cout << "   ** automatic requirements selected, will postprocess **" << endl;
	  auto_req_post.push_back(id);
	}

      line = string(it, line.end());
      id++;
    }
}

void SamplerProgram::handleDefinitionLine(std::string& line, int line_no)
{
  string sampler_name, sampler_type;
  string::iterator it;
  sampler_def def;

  def.Lmin = 0;
  def.Lmax = -1;

  while (!line.empty())
    {
      try
        {
          it = parse_sampler_definition(line.begin(), line.end(), sampler_name, sampler_type, def);
        }
      catch(const SamplerParseException& exc)
        {
          cerr << format("%d: Error while parsing sampler template definition.") % line_no << endl;
          throw SamplerProgramException();
        }

      if (sampler_type == "multi_template")
        {
          handleDefinitionMultiTemplateWiener(line_no, def, sampler_name);
        }
      else if(sampler_type == "lensing")
        {
          handleDefinitionLensing(line_no, def, sampler_name);
        }
      else if(sampler_type == "modulation")
        {
          handleDefinitionModulation(line_no, def, sampler_name);
        }
      else if (sampler_type == "cls")
        {
          handleDefinitionCLS(line_no, def, sampler_name);
        }
      else if (sampler_type == "hmc_cmb")
        {
          handleDefinitionHMC_CMB(line_no, def, sampler_name);
        }
      else if (sampler_type == "hmc_mess_cmb")
        {
          handleDefinitionHMC_MESS_CMB(line_no, def, sampler_name);
        }
      else if (sampler_type == "mess_cmb")
        {
          handleDefinitionCMB_MESS(line_no, def, sampler_name);
        }
      else
        {
          cerr << format("%d: Unknown sampler type (type='%s').") % line_no % sampler_type << endl;
          throw SamplerProgramException();
        }

      {
        ofstream f("sampler_id.txt", ios::app);
        f << format("%d %s") % last_sampler_id % sampler_name << endl;
      }

      last_sampler_id++;
      last_type_id++;
      line = string(it, line.end());
   }
}


void SamplerProgram::handleDataLine(std::string& line, int line_no)
{
  string sampler_name, sampler_type;
  string::iterator it;
  sampler_data_def def;

  numChannels = 0;
  while (!line.empty())
    {
      try
        {
          it = parse_sampler_data_def(line.begin(), line.end(), def);
        }
      catch(const SamplerParseException& exc)
        {
          cerr << format("%d: Error while parsing sampler template definition.") % line_no << endl;
          throw SamplerProgramException();
        }

      DataDescriptor data_desc;

      assert(def.data_type == "healpix");
      data_desc.channel_loaders.push_back(new HealpixTemperatureMapLoader(def.data_file));
      data_desc.channel_loaders.push_back(new SphericalBeamLoader(def.beam_file));
      if (def.noise_type == "NN")
        data_desc.channel_loaders.push_back(new HealpixNoiseMapLoader(def.noise_file, def.noise_norm, def.mean_noise));
      else if (def.noise_type == "NP")
        data_desc.channel_loaders.push_back(new HealpixColoredNoiseMapLoader(def.noise_file, def.noise_norm, def.mean_noise));
      else
        {
          comm->log_root(str(format("Only two supported noise covariance matrix 'NN' or 'NP' (you specified '%s' for map '%s'). Stopping now.") % def.noise_type % def.name));
          comm->abort();
        }
      if (!def.mask_file.empty())
        data_desc.channel_loaders.push_back(new HealpixMaskLoader(def.mask_file));
      data_desc.name = def.name;

      all_data.push_back(data_desc);
      numChannels++;
      
      line = string(it, line.end());
   }

}

static SingleSampler *create_cl_sampler(gsl_rng *rng, std::string name, int lmax, map<string, int>& sampler_keys,
					string id, string init_cls, int locked)
{
  return new CLS_Sampler(rng, name, lmax, sampler_keys[id], init_cls, locked);
}

void SamplerProgram::handleDefinitionCMB_MESS(int line_no, sampler_def& def, const string& sampler_name)
{
  int numBath = 0;
  double deltaBath = 0;
    
  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  extract_value<int>("NUMBATH", def.args, numBath);
  extract_value<double>("DELTABATH", def.args, deltaBath);

  samplers[last_sampler_id] = new CMB_Mess_Sampler(rng, sampler_name, comm, cmb_data, lmaxCMB, nSide, numBath, deltaBath);
  sampler_keys[sampler_name] = last_sampler_id;
}

void SamplerProgram::handleDefinitionCLS(int line_no, sampler_def& def, const string& sampler_name)
{
  string mapper_id = extract_name("MAPPER", def.args);
  int lmax = extract_value<int>("LMAX", def.args);
  string init_cls = extract_name("INIT", def.args);
  int locked = 0;

  extract_value<int>("LOCKED", def.args, locked);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  map<string,int>::iterator iter = sampler_keys.find(mapper_id);
  if (iter == sampler_keys.end())
    {
      LazyConstruction *lazy = new LazyConstruction(samplers, last_sampler_id,
						    boost::bind(&create_cl_sampler, rng, sampler_name,
								lmax,
								boost::ref(sampler_keys),
								mapper_id, init_cls, locked));
      post_fun.push_back(lazy);
    }
  else
    {
      int mid = iter->second;

      samplers[last_sampler_id] = new CLS_Sampler(rng, sampler_name, lmax, mid, init_cls, locked);
    }
  sampler_keys[sampler_name] = last_sampler_id;
}

void SamplerProgram::handleDefinitionHMC_MESS_CMB(int line_no, sampler_def& def, const string& sampler_name)
{
  int lmax = extract_value<int>("LMAX", def.args);
  int nmax = extract_value<int>("NMAX", def.args);
  double epsilonmax = extract_value<double>("EPSILON", def.args);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  HMC_Messenger_CMB_Sampler *hmc;
  samplers[last_sampler_id] = (hmc = new HMC_Messenger_CMB_Sampler(comm, rng, sampler_name, cmb_data, lmax, nSide, last_type_id));
  sampler_keys[sampler_name] = last_sampler_id;

  hmc->set_Nmax(nmax);
  hmc->set_EpsilonMax(epsilonmax);

  if (def.flags.count("NO_TUNE") > 0)
    hmc->freeze();
  string signal_type = *def.args["SIGNAL"].begin();
  signal_keys[signal_type] = last_type_id;
}

void SamplerProgram::handleDefinitionHMC_CMB(int line_no, sampler_def& def, const string& sampler_name)
{
  int lmax = extract_value<int>("LMAX", def.args);
  int nmax = extract_value<int>("NMAX", def.args);
  double epsilonmax = extract_value<double>("EPSILON", def.args);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  HMC_CMB_Sampler *hmc;

  samplers[last_sampler_id] = (hmc = new HMC_CMB_Sampler(rng, sampler_name, cmb_data, lmax, nSide, last_type_id));
  sampler_keys[sampler_name] = last_sampler_id;

  hmc->set_Nmax(nmax);
  hmc->set_EpsilonMax(epsilonmax);

  if (def.flags.count("NO_TUNE") > 0)
    hmc->freeze();

  string signal_type = *def.args["SIGNAL"].begin();
  signal_keys[signal_type] = last_type_id;
}

void SamplerProgram::handleDefinitionModulation(int line_no, sampler_def& def, const string& sampler_name)
{
  int lmaxModulation = extract_value<int>("LMAX_MOD", def.args);
  double stepsize = extract_value<double>("STEP_SIZE", def.args);
  string clModulation = extract_name("CLMOD", def.args);
  string clProposal = extract_name("CLPROPOSAL", def.args);

  if (!modulation_enabled)
    {
      cerr << "Modulation is not enabled. Cannot create sampler." << endl;
      throw SamplerProgramException();
    }

  map<string,int>::iterator iter = sampler_keys.find(clModulation);
  if (iter == sampler_keys.end())
    {
      cerr << format("No such sampler '%s' for sampling CLS of the modulation field") % clModulation << endl;
      throw SamplerProgramException();
    }

  int range_mod_min = 0, range_mod_max = cmb_data.Lmax;
  extract_value<int>("RANGE_MIN", def.args, range_mod_min);
  extract_value<int>("RANGE_MAX", def.args, range_mod_max);

  Modulation_Sampler *s = new Modulation_Sampler(rng, cmb_data, lmaxModulation, nSide, stepsize,
						 range_mod_min, range_mod_max, sampler_keys[clModulation]);

  string init_modulation;
  if (extract_name("INITMOD", def.args, init_modulation))
   {
    cout << format("Using %s for initial conditions") % init_modulation << endl;
    s->setInitialCondition(init_modulation);
   }
  s->setProposalSpectrum(clProposal);

  s->set_CG_precision(cmb_data.rinfo.epsilonCG);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  samplers[last_sampler_id] = s;
  sampler_keys[sampler_name] = last_sampler_id;
}

void SamplerProgram::handleDefinitionLensing(int line_no, sampler_def& def, const string& sampler_name)
{
  vector<vector<string> > template_fnames;

  if (all_sampler_names.find(sampler_name) != all_sampler_names.end())
    {
      cerr << format("%d: Sampler %s already exists.") % line_no % sampler_name << endl;
      throw SamplerProgramException();
    }

  double stepsize = extract_value<double>("STEP_SIZE", def.args);
  int lmaxLens = extract_value<int>("LmaxLens", def.args);

  Lensing_Sampler *s = new Lensing_Sampler(rng, cmb_data, nSide, lmaxLens, stepsize);

  s->set_CG_precision(cmb_data.rinfo.epsilonCG);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  samplers[last_sampler_id] = s;
  sampler_keys[sampler_name] = last_sampler_id;
}


void SamplerProgram::handleDefinitionMultiTemplateWiener(int line_no, sampler_def& def, const string& sampler_name)
{
  vector<vector<string> > template_fnames;
  int autorepeat;

  if (all_sampler_names.find(sampler_name) != all_sampler_names.end())
    {
      cerr << format("%d: Sampler %s already exists.") % line_no % sampler_name << endl;
      throw SamplerProgramException();
    }

  if (def.args["NUM_TEMPLATES"].size() == 0)
    {
      cerr << format("%d: Sampler %s has no NUM_TEMPLATES argument") % line_no % sampler_name << endl;
      throw SamplerProgramException();
    }

  int numTemplates = extract_value<int>("NUM_TEMPLATES", def.args);

  template_fnames.resize(numTemplates);

  for (int n = 0; n < numTemplates; n++)
    {
      string code = str(format("TEMPLATE_%d") % n);
      list<string>& templates = def.args[code];

      template_fnames[n].insert(template_fnames[n].begin(), templates.begin(), templates.end());
    }

  list<string>& amplitudes = def.args["AMPL_LOCKED"];
  arr<list<string> > amplitude_id;

  if (amplitudes.size() != 0)
    {
      if (amplitudes.size() != numTemplates*template_fnames[0].size())
        {
          cerr << format("%d: The number of locked amplitudes must be the same as the number of channels, if defined.") % line_no << endl;
          throw SamplerProgramException();
        }

      amplitude_id.alloc(amplitudes.size());
      std::list<string>::iterator i = amplitudes.begin();
      for (int j = 0; i != amplitudes.end(); ++i, ++j)
        {
          char *endptr = 0;
          vector<string> all_amplitudes;          

          split(all_amplitudes, (*i), is_any_of(","), token_compress_on);

          amplitude_id[j].clear();
          copy(all_amplitudes.begin(), all_amplitudes.end(), back_inserter(amplitude_id[j]));
        } 
      }

  string signal_type = *def.args["SIGNAL"].begin();

  cout << "Template signal type = " << signal_type << endl;
  cout << "Template Lmin= " << def.Lmin << endl;
  signal_keys[signal_type] = last_type_id;

  if (!extract_value<int>("REPEAT", def.args, autorepeat))
    autorepeat = 1;

  bool dynjobs = (def.flags.count("DYNAMIC_JOB") != 0);

  MPI_Multi_Template_Sampler_Wiener *s = new MPI_Multi_Template_Sampler_Wiener(comm, sampler_name, rng, cmb_data, nSide, lmaxCMB, (def.Lmax > 0) ? def.Lmax : lmaxCMB, def.Lmin, last_type_id, numTemplates, dynjobs);

  s->setAutoRepeat(autorepeat);

  bool templates_are_maps = (def.flags.count("ALM_MAP") == 0);
  string pmatrix;

  if (amplitude_id.size() != 0)
    s->setLockedAmplitudes(amplitude_id);
  else if (extract_value<string>("PMATRIX", def.args, pmatrix))
    {
      s->setupPmatrix(pmatrix);
    }

  all_sampler_names[sampler_name] = s;
  for (int n = 0; n < numTemplates; n++)
    s->loadTemplates(cmb_data, n, template_fnames[n], templates_are_maps);
  s->set_CG_precision(cmb_data.rinfo.epsilonCG);

  if (def.flags.count("PINNED_SPECTRUM") > 0)
    {
      DefinitionPostprocess *dp = new DefinitionPostprocess();

      dp->cl_fname = *def.args["IN_CL"].begin();
      dp->s = s;
      dp->lensing_enabled = false;
      post_fun.push_back(dp);
    }

  if (def.flags.count("ENFORCE_SIGN") > 0)
    {
      s->setPositiveSign(true);
    }

   if (def.flags.count("FULL_CHI2") > 0)
    {
      s->setComputeFullChi2(true);
    }
  if (def.flags.count("DUMP_RESIDUALS") > 0)
    {
      s->dumpResidual(true);
    }

  if (def.flags.count("NO_CMB") > 0)
    s->doMarginalCMB(false);

  if (last_sampler_id >= samplers.size())
    samplers.resize(last_sampler_id+1);

  samplers[last_sampler_id] = s;
  sampler_keys[sampler_name] = last_sampler_id;
}


SamplerProgram::SamplerProgram(gsl_rng *rng,
                               MPI_Communication& comm,
			       CMB_Data& data, 
			       const std::string& fname)
  : rng(rng), cmb_data(data),lmaxCMB(data.Lmax),
    prog_fname(fname),  comm(0)
{
  pin_dmcmb = false;
  enable_cache = false;
  enable_save_cache = false;
  modulation_enabled = false;
  lensing_enabled = false;
  healpix_data_set = false;
  do_weighing = false;
  this->comm = &comm;
}

void SamplerProgram::init()
{
  ifstream f(prog_fname.c_str());
  string line;
  int id;
  int line_no = 1;
  bool definition_mode, program_mode, data_mode;
  string definition_buffer, program_buffer, data_buffer;
  map<string, int> template_samplers;

  {
    unlink("sampler_id.txt");
  }

  if (!f)
    {
      cerr << format("File %s is invalid/inexistent.") % prog_fname  << endl;
      throw SamplerProgramException();
    }
    
  samplers.resize(NUM_SAMPLERS);
  for (int i = 0; i < NUM_SAMPLERS; i++)
    samplers[i] = 0;
    

  last_type_id = SAMPLER_SIGNAL_LAST_SIGNAL;
  last_sampler_id = NUM_SAMPLERS;

  for (int i = 0; i < numSamplerNames; i++) {
    sampler_keys[s_sampler_names[i].name] = s_sampler_names[i].id;
  }

  for (int i = 0; i < numSignalNames; i++)
    signal_keys[s_signal_names[i].name] = s_signal_names[i].id;

  // Entering the parser now, line by line.
  definition_mode = false;
  program_mode = false;
  data_mode = false;

  while (getline(f, line))
    {
      trim(line);
      if (line.size()==0 || line[0] == '#' || line[0]=='\n')
        {
          line_no++;
          continue;
        }

      while (line.length() >= 1 && (line[line.length()-1] == '\n' || line[line.length()-1] == '\r'))
        line = line.substr(0, line.length()-1);

      if (line == "DATA")
        {
          if (data_mode)
            {
              cerr << format("%d: Error. already in data mode.") % line_no << endl;
              throw SamplerProgramException();
            }
          if (program_mode || definition_mode)
            {
              cerr << format("%d: Error. already entered in program or definition mode") % line_no << endl;
              throw SamplerProgramException();
            }
          data_mode = true;
          definition_mode = false;
          program_mode = false;
          line_no++;
          continue;
        }

      if (line == "DEFINITION")
        {
          if (definition_mode)
            {
              cerr << format("%d: Error. already in definition mode.") % line_no << endl;
              throw SamplerProgramException();
            }
          if (program_mode)
            {
              cerr << format("%d: Error. Already entered the program mode.") % line_no << endl;
              throw SamplerProgramException();
            }
          definition_mode = true;
          data_mode = false;
          program_mode = false;
          line_no++;
          continue;
        }

      if (line == "PROGRAM")
        {
          if (program_mode)
            {
              cerr << format("%d: Error. already in program mode.") % line_no << endl;
              throw SamplerProgramException();
            }

          definition_mode = false;
          data_mode = false;
          program_mode = true;
          line_no++;
          continue;
        }

      if (data_mode)
        {
          data_buffer += line;
        }
      else
        if (definition_mode)
        {
          definition_buffer += line;
        }
      else
        if (program_mode)
          {
            program_buffer += line;
          }
        else
          {
            cerr << format("%d: Not entered in any mode.") % line_no << endl;
            throw SamplerProgramException();
          }
    }

  handleDataLine(data_buffer, 0);
  loadDataFiles();
  buildWienerRelatedOperators();

  initBasicSamplers();
  
  handleDefinitionLine(definition_buffer, 0);

  for (PostList::iterator i = post_fun.begin();
       i != post_fun.end();
       ++i)
    {
      if (enable_cache)
        (*i)->loadCache(cache_prefix);
      (*i)->exec(cmb_data);
      if (enable_save_cache)
        (*i)->saveCache(save_cache_prefix);
    }
  post_fun.clear();

  handleProgramLine(program_buffer, 0);

  doAutoRequirements();
}

SamplerProgram::~SamplerProgram()
{
  for (PostList::iterator i = post_fun.begin();
       i != post_fun.end();
       ++i)
    {
      delete (*i);
    }

  post_fun.clear();
}


void SamplerProgram::doAutoRequirements()
{
  vector<bool> sampler_active;
  vector<int> sampler_code;
  int k = 0;

  sampler_active.resize(last_sampler_id);
  fill(sampler_active.begin(), sampler_active.end(), false);
  sampler_code.resize(sampler_list.size());

  for (SamplerArray::iterator i = sampler_list.begin(); i != sampler_list.end(); ++i) {
    sampler_code[k] = *i;
    sampler_active[*i] = true;
    k++;
  }

  for (std::list<int>::iterator i = auto_req_post.begin();
       i != auto_req_post.end();
       ++i)
    {
      // We build the requirements automatically from the list of active samplers.
      int id = *i;
      cout << format("Auto %d => ") % sampler_code[id];
      for (int j = 0; j < last_sampler_id; j++)
         {
           if (!sampler_active[j] || j == sampler_code[id])
             continue;
           sampler_order[id].push_back(j);
           sampler_signals[id].push_back(SAMPLER_SIGNAL_DEFAULT);
           cout << format(" %d,0 ") % j;
         }
      cout << endl;
    }
}


void SamplerProgram::initBasicSamplers()
{
  DefinitionPostprocess *dp;
  
  cmb_data.transform  = 0;
  if (modulation_enabled)
    cmb_data.transform = new ModulationOperator<DataType,double>(nSide);
  else if (lensing_enabled)
   {
    Lensing_Operator<DataType,double> *lop = new Lensing_Operator<DataType,double>(cmb_data.NsideHR, nSide);
    cmb_data.transform = lop;
    lop->update_CMB_spectrum(cmb_data.guess_cls);
    }

  cout << lmaxCMB << " " << nSide << endl;
  samplers[SAMPLER_CMB] = new CMB_Sampler(rng, cmb_data, lmaxCMB, nSide);
  ((CMB_Sampler *)samplers[SAMPLER_CMB])->set_CG_precision(cmb_data.rinfo.epsilonCG);
  samplers[SAMPLER_CLS] = new CLS_Sampler(rng, "cls", lmaxCMB, SAMPLER_CMB, string(), 0);
  samplers[SAMPLER_NOISE] = new NOISE_Sampler(rng);

  if (lensing_enabled) {
    samplers[SAMPLER_ABERRATION] = new Aberration_Sampler(rng, cmb_data, nSide);
  }

}


void SamplerProgram::setCachePrefix(const string& prefix)
{
  enable_cache = true;
  cache_prefix = prefix;
}

void SamplerProgram::saveTemplateCache(const string& prefix)
{
  enable_save_cache = true;
  save_cache_prefix = prefix;

  for (PostList::iterator i = post_fun.begin();
       i != post_fun.end();
       ++i)
    {
      (*i)->saveCache(prefix);
    }
}

list<int> SamplerProgram::getActiveList() const
{
  set<int> slist;
  list<int> keys;

  for (SamplerArray::const_iterator it = sampler_list.begin(); it != sampler_list.end(); ++it)
    slist.insert(*it);

  keys.insert(keys.begin(), slist.begin(), slist.end());
  return keys;
}

void SamplerProgram::loadDataFiles()
{
  typedef  list<DataDescriptor>::iterator DataIter;
  int ch_id = 0;
  
  nSide = cmb_data.Nside = -1;
  cmb_data.numChannels = numChannels;
  cmb_data.channels.alloc(numChannels);
  cmb_data.noise.alloc(numChannels);
  cmb_data.residuals.alloc(numChannels);
  cmb_data.beam.alloc(numChannels);
  cmb_data.masks.alloc(numChannels);

  for ( DataIter iter = all_data.begin();
        iter != all_data.end();
        ++iter, ++ch_id)
    {
      for_each( iter->channel_loaders.begin(),
                iter->channel_loaders.end(),
                boost::bind(&GenericDataLoader::load, _1, boost::ref(cmb_data), ch_id));      
      for_each(iter->channel_loaders.begin(),
               iter->channel_loaders.end(),
               boost::bind<void>(boost::lambda::delete_ptr(), _1));
    }
    
  all_data.clear();
  
  cmb_data.NsideHR = nSide = cmb_data.Nside;
#if 0
  if (cmb_data.NsideHR < 0)
    cmb_data.NsideHR = cmb_data.Nside;
#endif
    
  if (do_weighing)
    {
      if (!healpix_data_set)
        {
          cerr << "Healpix directory is required for weighing." << endl;
          exit(1);
        }

      read_weight_ring (healpix_data, cmb_data.Nside, cmb_data.weight);
      cmb_data.pixwin.alloc(4*cmb_data.Nside);
      cmb_data.pixwin.fill(0);
      read_pixwin(healpix_data, cmb_data.Nside, cmb_data.pixwin);
    }
  else
    {
      cmb_data.weight.alloc(2*cmb_data.Nside);
      cmb_data.weight.fill(0);
      cmb_data.pixwin.alloc(4*cmb_data.Nside);
      cmb_data.pixwin.fill(1);
    }

  for (int i = 0; i < 2*nSide; i++)
    cmb_data.weight[i] += 1; 

  for (int ch = 0; ch < cmb_data.numChannels; ch++)
    cmb_data.noise[ch]->SetFlexScaling(1.0);
}


void SamplerProgram::buildWienerRelatedOperators()
{
  KeywordMap kw;
  if (!preconditioner_options.empty())
    kw = parseKeywordOptions(preconditioner_options);
  string precon_type = extract_keyword<std::string>(kw, "PRECONDITIONER", std::string("NO"));
  PreconditionerType preconditioner_type;

  if (precon_type == string("NO"))
    preconditioner_type = NoPreconditioner;
  else if (precon_type == string("SIMPLE"))
    preconditioner_type = SimplePreconditioner; 
  else if (precon_type == string("RECURSIVE"))
    preconditioner_type = RecursivePreconditioner;
  else
    {
      comm->log("Invalid preconditioner type " + precon_type);
      comm->abort();
    }
  switch (preconditioner_type)
    {
    case NoPreconditioner:
      cmb_data.preconditioner = 0;
      break;
    case SimplePreconditioner: {
      int lmaxPre = extract_keyword<int>(kw, "LMAX_PRE", -1);
      int mmaxPre = extract_keyword<int>(kw, "MMAX_PRE", -1);
      cmb_data.preconditioner =
        new MapSignalNoiseSignal_weighing_preconditioner<-1,1>(
                     comm,
							       cmb_data.noise, cmb_data.weight,
							       cmb_data.beam, cmb_data.Nside, cmb_data.Lmax, lmaxPre, mmaxPre, cmb_data.guess_sqrt_cls,
							       cmb_data.numChannels);
      break;
     }
    case RecursivePreconditioner: {
#ifndef MPI_ENABLED
      RecursivePreconditionerInfo pre_info;
      
      pre_info.LmaxBlock = extract_keyword<int>(kw, "LMAX_PRE", -1);
      pre_info.MmaxBlock = extract_keyword<int>(kw, "MMAX_PRE", -1);
      pre_info.maxIterPerLevel = convert_list_to_arr(extract_keyword_list<int>(kw, "MAX_ITER_PER_LEVEL"));
      pre_info.epsilonPerLevel = convert_list_to_arr(extract_keyword_list<double>(kw, "EPSILON_PER_LEVEL"));
      pre_info.lmaxLevel = convert_list_to_arr(extract_keyword_list<int>(kw, "LMAX_PER_LEVEL"));
      pre_info.nsideLevel = convert_list_to_arr(extract_keyword_list<int>(kw, "NSIDE_LEVEL"));
      if (pre_info.maxIterPerLevel.size() == 0 || pre_info.epsilonPerLevel.size() == 0 || pre_info.lmaxLevel.size() == 0 || pre_info.nsideLevel.size() == 0) {
        cerr << "Recursive preconditioner needs details in keys MAX_ITER_PER_LEVEL and EPSILON_PER_LEVEL and LMAX_PER_LEVEL and NSIDE_LEVEL" << endl;
        comm->abort(); 
      }
      pre_info.maxLevels = pre_info.maxIterPerLevel.size();

      if (pre_info.epsilonPerLevel.size() != pre_info.maxLevels || pre_info.lmaxLevel.size() != pre_info.maxLevels || pre_info.nsideLevel.size() != pre_info.maxLevels) {
        cerr << "Recursive preconditioner needs number of levels to be consistent in MAX_ITER_PER_LEVEL and EPSILON_PER_LEVEL and LMAX_PER_LEVEL and NSIDE_LEVEL" << endl;
        comm->abort();
      }
      
      if (pre_info.nsideLevel[0] != cmb_data.Nside) {
        cerr << "First Nside level must be the same as cmb data" << endl;
        comm->abort();
      } 

      cmb_data.preconditioner =
        new MapSignalNoiseSignal_weighing_recursive_preconditioner(
                    comm,
                    cmb_data.noise, cmb_data.weight, cmb_data.beam, 
                    cmb_data.guess_sqrt_cls, cmb_data.numChannels, cmb_data.transform,
                    pre_info, 0);
#else
      cerr << "Recursive preconditioner is unsupported in MPI mode" << endl;
      comm->abort();
#endif
      break;
    }
    default:
      cerr << "Invalid value for preconditioner" << endl;
      exit(1);
    }

  /* +++++++++++++++++++++++
   * Messenger support 
   */
  if (wiener_type == "messenger")
    {
      bool is_nn = true, is_np = true;
      for (int ch = 0; ch < cmb_data.numChannels; ch++)
        {
          is_nn = is_nn && (dynamic_cast<CMB_NN_NoiseMap *>(cmb_data.noise[ch]) != 0);
          is_np = is_np && (dynamic_cast<CMB_NP_NoiseMap *>(cmb_data.noise[ch]) != 0);
        }
      
      if (is_nn)
        {
          EW_WienerFilter *ew = new EW_WienerFilter(comm,cmb_data);
          cmb_data.wiener_operator = ew;
        }
      else if (is_np)
        {
          EW_DFD_WienerFilter *ew = new EW_DFD_WienerFilter(comm,cmb_data);
          cmb_data.wiener_operator = ew;
        }
      else
        {
          comm->log_root("Messenger algorithm only supports NN and NP noise covariance matrices, and it must not "
                         "be a hybrid configuration. Stopping.");
          comm->abort();
        }
    }
  /* +++++++++++++++++++++++
   * DualMessenger support 
   */
  else if (wiener_type == "dual_messenger")
    {
      bool is_nn = true, is_np = true;
      for (int ch = 0; ch < cmb_data.numChannels; ch++)
        {
          is_nn = is_nn && (dynamic_cast<CMB_NN_NoiseMap *>(cmb_data.noise[ch]) != 0);
          is_np = is_np && (dynamic_cast<CMB_NP_NoiseMap *>(cmb_data.noise[ch]) != 0);
        }
      
      if (is_nn)
        {
          DualMessenger_WienerFilter *ew = new DualMessenger_WienerFilter(comm,cmb_data);
          cmb_data.wiener_operator = ew;
        }
      else
        {
          comm->log_root("DualMessenger algorithm only supports NN noise covariance matrices, and it must not "
                         "be a hybrid configuration. Stopping.");
          comm->abort();
        }
    }
  /* +++++++++++++++++++++++
   * Conjugate gradient support 
   */
  else if (wiener_type == "cg")
    {
      comm->log("Initializing CG wiener filter");
      cmb_data.wiener_operator = new CG_WienerFilter(cmb_data);
      comm->log("Done CG init");
    }
  /* +++++++++++++++++++++++
   * Conjugate gradient support 
   */
  else if (wiener_type == "command")
    {
      comm->log("Initializing Command wiener filter");
      cmb_data.wiener_operator = new Commander_WienerFilter(comm, cmb_data);
    }
  /* +++++++++++++++++++++++
   * Failure
   */
  else
    {
      cerr << "Invalid wiener operator. Stopping here." << endl;
      exit(1);
    }

  cmb_data.wiener_operator->setWienerOptions(kw);

}

