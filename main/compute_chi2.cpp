/*+
This is ABYSS (./main/compute_chi2.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cassert>
#include <string>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <fitshandle.h>
#include <healpix_map_fitsio.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "cl_sampler.hpp"
#include "sampler.hpp"
#include "noise.hpp"
#include "cmb_data.hpp"
#include <pointing.h>
#include "extra_map_tools.hpp"
//#include "template_sampler.hpp"
#include <healpix_data_io.h>
#include "main_conf.h"
#include <cstdlib>
#include <cstring>
#include "sampler_program.hpp"
#include "netcdf_adapt.hpp"
#include <boost/format.hpp>
#include "noise_weighing.hpp"
#include <powspec_fitsio.h>
#include <powspec.h>
#include "recursive_preconditioner.hpp"
#include "load_data.hpp"
#include "mpi_communication.hpp"

using boost::format;
using namespace CMB;
using namespace std;

string safe_str(bool valid, const char *s)
{
  assert(!valid || (s!=0));
  return !valid ? string("") : string(s);
}

int main(int argc, char **argv)
{
  long nL;
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  CMB_Data data;
  struct mainParams args;
  struct mainConf_params params;

  data.transform = 0;
  
  mainConf_init(&args);
  mainConf_params_init(&params);

  params.check_required = 0;
  if (mainConf_ext(argc, argv, &args, &params))
    return -1;

  if (args.configFile_given)
    {
      params.override = 0;
      params.initialize = 0;
      if (mainConf_config_file(args.configFile_arg, &args, &params))
        {
          mainConf_free(&args);
          return -1;
        }
    }

  if (mainConf_required(&args, argv[0]))
    return -1;

  if (args.seed_given)
    gsl_rng_set(r, args.seed_arg);

  // First detect the number of channels and load the maps

  data.Lmax = args.Lmax_arg;
  nL = args.Lmax_arg;

  SamplerProgram *prog;
  MPI_Communication comm;
   
  PowSpec pspec;
  read_powspec_from_fits(args.guess_cls_arg, pspec, 1, nL);

  data.guess_cls = pspec.tt();
  data.guess_sqrt_cls.alloc(data.guess_cls.size());

  for (int i = 0; i < data.guess_cls.size(); i++)
    data.guess_sqrt_cls[i] = sqrt(data.guess_cls[i]);


  data.rinfo.epsilonCG = 1e-6;
  data.rinfo.relaxation_start = 200;
  data.rinfo.relaxation_reduction = 10;

  prog = new SamplerProgram(r, comm, data, args.sample_arg);
  if (args.do_weighing_flag)
    prog->setHealpixWeighing(args.healpix_data_arg);

  if (args.load_template_cache_given)
    prog->setCachePrefix(args.load_template_cache_arg);
  if (args.save_template_cache_given)
    prog->saveTemplateCache(args.save_template_cache_arg);

  if (args.pin_dmcmb_flag)
    prog->setPinDMCMB();
  
  if (args.modulated_cmb_flag)
    prog->setModulated(args.modulated_cmb_flag);

  prog->init();
  
  AllSamplers& samplers = prog->getSamplers();
  string fname = str(format("out/state_%d.nc") % args.restart_arg);
  NcFile f(fname, NcFile::read);
  
  try
    {
      for (int i = 0; i < samplers.size(); i++)
	if (samplers[i])
	  samplers[i]->restoreState(f);
    }
  catch (const StateInvalid& s)
    {
      cerr << "Invalid state in file " << args.restart_arg << endl;
      exit(1);
    }

  return 0;
}
