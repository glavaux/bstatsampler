/*+
This is ABYSS (./main/sampler_program.hpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#ifndef __GIBBS_SAMPLE_PROGRAM_HPP
#define __GIBBS_SAMPLE_PROGRAM_HPP

#include <gsl/gsl_rng.h>
#include <list>
#include <string>
#include <vector>
#include <exception>
#include "cmb_defs.hpp"
#include "preconditioners.hpp"
#include "sampler.hpp"
#include "sampler_program_exception.hpp"
#include "data_loader.hpp"

namespace CMB
{
  struct CMB_Data;
  struct sampler_def;
  class SingleSampler;
  class MPI_Communication;

  class ProgramPostprocess
  {
  public:
    ProgramPostprocess();
    virtual ~ProgramPostprocess();

    virtual void exec(CMB_Data& data) = 0;
    virtual void saveCache(const std::string& s) = 0;
    virtual void loadCache(const std::string& s) = 0;
  };
  
  struct DataDescriptor
  {
    std::string name;
    std::list<GenericDataLoader *> channel_loaders;
  };  

  class SamplerProgram
  {
  public:
    typedef std::vector<int> SamplerArray;
    typedef std::list<int> OrderList;
    typedef std::vector<OrderList> OrderListArray;
    typedef std::list<ProgramPostprocess *> PostList;

    SamplerProgram(gsl_rng *rng,
                   MPI_Communication& comm,
		   CMB_Data& data, 
		   const std::string& fname);
    ~SamplerProgram();

    void start() {
      current_position = 0; 
    }

    int getSampler() {
      return sampler_list[current_position]; 
    }
    
    const OrderList& getAssumed() const { 
      return sampler_order[current_position]; 
    }

    const OrderList& getSignals() const {
      return sampler_signals[current_position];
    }

    bool next() { 
      if (current_position == sampler_list.size()) 
        return false;
      current_position++;
      return true;
    }

    bool finished() { 
      return (current_position == sampler_list.size());
    }

    AllSamplers& getSamplers() {
      return samplers;
    }


    void init();

    void setCachePrefix(const std::string& prefix);
    void saveTemplateCache(const std::string& prefix);

    void setPinDMCMB(bool p = true)
    {
      pin_dmcmb = p;
    }

    void setModulated(bool m = true)
    {
      modulation_enabled = m;
    }

    void setLensed(bool l = true)
    {
      lensing_enabled = l;
    }

    std::list<int> getActiveList() const;
    
    void setWienerAlgorithm(const std::string& algo_name)
    {
      wiener_type = algo_name;
    }
    
    void setHealpixWeighing(const std::string& healpix_data_path)
    {
      do_weighing = true;
      healpix_data_set = true;
      healpix_data = healpix_data_path;
    }

    void setPreconditionerOptions(const std::string& precon_opt) { preconditioner_options = precon_opt; }
  private:
    MPI_Communication *comm;
    SamplerArray sampler_list;
    PostList post_fun;
    int current_position;
    OrderListArray sampler_order;
    OrderListArray sampler_signals;
    std::map<std::string, int> sampler_keys, signal_keys;
    gsl_rng *rng;
    CMB_Data& cmb_data;
    long nSide, lmaxCMB;
    int numChannels;
    std::map<std::string, SingleSampler *> all_sampler_names;
    std::map<std::string, int> all_stype;
    int last_type_id, last_sampler_id;
    AllSamplers samplers;
    double cg_prec;
    std::string prog_fname;
    std::string cache_prefix, save_cache_prefix;
    bool enable_cache, enable_save_cache;
    std::list<int> auto_req_post;
    bool pin_dmcmb;
    bool modulation_enabled, lensing_enabled;
    std::list<DataDescriptor> all_data;
    bool do_weighing, healpix_data_set;
    std::string healpix_data;
    std::string preconditioner_options, wiener_type;
    
    void handleDataLine(std::string& line, int line_no);
    void handleProgramLine(std::string& line, int line_no);
    void handleDefinitionLine(std::string& line, int line_no);
    void handleDefinitionMultiTemplateWiener(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionLensing(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionModulation(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionCLS(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionHMC_CMB(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionHMC_MESS_CMB(int line_no, sampler_def& def, const std::string& sampler_name);
    void handleDefinitionCMB_MESS(int line_no, sampler_def& def, const std::string& sampler_name);

    void initBasicSamplers();
    void doAutoRequirements();
    void loadDataFiles();
    void buildWienerRelatedOperators();
  };

};


#endif
