/*+
This is ABYSS (./main/data_loader.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <healpix_map_fitsio.h>
#include <healpix_map.h>
#include <cstdlib>
#include <powspec.h>
#include <powspec_fitsio.h>
#include "cmb_defs.hpp"
#include "cmb_data.hpp"
#include "data_loader.hpp"
#include "extra_map_tools.hpp"
#include "cmb_noise_np.hpp"
#include "sampler_program.hpp"

using namespace CMB;
using namespace std;

HealpixTemperatureMapLoader::HealpixTemperatureMapLoader(const std::string& fname)
{
  this->fname = fname;
}
    
void HealpixTemperatureMapLoader::load(CMB_Data& data, int ch_id)
{
   cout << "-> Loading " << fname << "..." << endl;
   read_Healpix_map_from_fits(fname, data.channels[ch_id], 1, 2);
   if (data.channels[ch_id].Scheme() == NEST)
      data.channels[ch_id].swap_scheme();

   if (data.Nside != data.channels[ch_id].Nside() && data.Nside >= 0)
     {
       cerr << "Incompatible Nsides. Stopping now." << endl;
       exit(1);
     }
  data.Nside = data.channels[ch_id].Nside();
}

SphericalBeamLoader::SphericalBeamLoader(const std::string& fname)
{
  this->fname = fname;
}
    
void SphericalBeamLoader::load(CMB_Data& data, int ch_id)
{
  cout << "-> Loading beam " << fname << endl;

  ifstream f(fname.c_str());
  if (!f)
    {
      cerr << "Non existing file " << fname << endl;
      exit(1);
    }
 
  string line;
  vector<double> data_beam;

  while (getline(f, line))
    {	  
      if (line[0] == '#')
        continue;

      istringstream sf(line);
      double l0;
      int l;

      sf >> l0;
      l = int(l0);
      if (l >= data_beam.size())
        data_beam.resize(l+1);
        
      sf >> data_beam[l];
    }
									
  data.beam[ch_id].alloc(data.Lmax+1);
  data.beam[ch_id].fill(0);
  long s = min((long)data_beam.size(), data.Lmax+1);
  memcpy(&data.beam[ch_id][0], &data_beam[0], sizeof(double)*s);
}

HealpixNoiseMapLoader::HealpixNoiseMapLoader(const std::string& fname, double normalization, double mean_noise)
{
  this->fname = fname;
  this->err_norm = normalization;
  this->mean_noise = mean_noise;
}
    
void HealpixNoiseMapLoader::load(CMB_Data& data, int ch_id)
{
  CMB_NN_NoiseMap *noise;

   cout << "-> Loading noise map " << fname << " (norm = " << err_norm << ", mean noise=" << mean_noise << ") ..." << endl;
  
  noise = new CMB_NN_NoiseMap();
  noise->SetNside(data.Nside, RING);
  read_Healpix_map_from_fits(fname, *noise, 1, 2);
  if (noise->Scheme() == NEST)
    noise->swap_scheme();

  // Convert this to Kelvins
  *noise *= 1/(err_norm*err_norm);

  noise->min_noise = 1/ ( *max_element(&(*noise)[0], &(*noise)[noise->Npix()]) );
  noise->mean_noise = mean_noise;


  data.noise[ch_id] = noise;
}

HealpixMaskLoader::HealpixMaskLoader(const std::string& fname)
{
  this->fname = fname;
}
    
void HealpixMaskLoader::load(CMB_Data& data, int ch_id)
{
  CMB_Map mask_data;

   cout << "-> Loading mask " << fname << "..." << endl;
  
  read_Healpix_map_from_fits(fname, mask_data, 1, 2);
  if (mask_data.Scheme() == NEST)
    mask_data.swap_scheme();

  // Convert this to Kelvins
  CMB_NN_NoiseMap *noise_map = data.noise[ch_id]->as<CMB_NN_NoiseMap>();

  data.masks[ch_id] = mask_data;
  *noise_map *= mask_data;

  for (long p = 0; p < mask_data.Npix(); p++)
     if (mask_data[p] == 0)
       noise_map->masked++;
}



HealpixColoredNoiseMapLoader::HealpixColoredNoiseMapLoader(const std::string& fname_map, double normalization, double mean_noise)
{
  this->fname = fname_map;
  this->err_norm = normalization;
  this->mean_noise = mean_noise;

  if (fname.empty())
    throw SamplerProgramException();//Empty noise file name");
}
    
void HealpixColoredNoiseMapLoader::load(CMB_Data& data, int ch_id)
{
  CMB_NP_NoiseMap *noise;
  fitshandle h;
  PowSpec powspec;

  cout << "-> Loading colored noise " << fname << " (norm = " << err_norm << ") ..." << endl;
    
  noise = new CMB_NP_NoiseMap(data);
  h.open(fname);

  CMB_Map w_map;
  h.goto_hdu(2);
  read_Healpix_map_from_fits(h, w_map);
  planck_assert (w_map.Nside() == data.Nside, "Incompatible Nside between noise and data");
  h.goto_hdu(3);
  read_powspec_from_fits(h, powspec, 1, data.Lmax);
  
  if (w_map.Scheme() == NEST)
    w_map.swap_scheme();

  // Convert this to Kelvins
  w_map *= 1/(err_norm*err_norm);

  noise->setModulationMap(w_map);
  noise->setColoring(powspec.tt());
  noise->mean_noise = mean_noise;
//  noise->min_noise = 1/ ( *max_element(&(*noise)[0], &(*noise)[noise->Npix()]) );

  data.noise[ch_id] = noise;
}

