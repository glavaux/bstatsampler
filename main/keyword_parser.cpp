/*+
This is ABYSS (./main/keyword_parser.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <boost/config/warning_disable.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/qi_eol.hpp>
#include <boost/spirit/include/qi_as.hpp>
#include <boost/spirit/include/qi_char_class.hpp>
#include <boost/spirit/include/qi_matches.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/container/set.hpp>
#include <boost/fusion/include/set.hpp>
#include <boost/fusion/container/set/set_fwd.hpp>
#include <boost/fusion/include/set_fwd.hpp>
#include <boost/spirit/home/phoenix/bind/bind_function.hpp>
#include <boost/format.hpp>
#include "sampler_program_exception.hpp"
#include "keyword_parser.hpp"

#include <list>
#include <map>
#include <string>
#include <boost/any.hpp>

using CMB::SamplerProgramException;
using CMB::SamplerParseException;
using namespace std;
using boost::format;
using boost::str;

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;
namespace spirit = boost::spirit;

typedef list<string> arg_list;
typedef map<string, arg_list> arg_container;

struct arg_struct
{
  string argname;
  arg_list parameters;
};

BOOST_FUSION_ADAPT_STRUCT(
			  arg_struct,
			  (std::string, argname)
			  (arg_list, parameters)
)


static void check_and_insert_arg(arg_container& c, arg_struct const& p)
{
    // check for duplicates and throw on error
  if (c.find(p.argname) != c.end())
    {
      cerr << format("Duplicated keyword %s.") % p.argname << endl;
      throw SamplerProgramException();
    }

  cerr << format("== Inserting argument %s -> %s.") % p.argname % (*p.parameters.begin())<< endl;

  c[p.argname] = p.parameters;
}

template<typename Iterator>
struct keyword_grammar : qi::grammar<Iterator, arg_container(), ascii::space_type>
{
  keyword_grammar() : keyword_grammar::base_type(start)
  {
    using qi::_1;
    using ascii::space;
    using phoenix::ref;
    using phoenix::push_back;
    using qi::char_;
    using qi::as;
    using ascii::alpha;
    using ascii::alnum;
    using qi::_val;
    using phoenix::at_c;
    using spirit::lexeme;
    using qi::eoi;
    using phoenix::bind;

    identifier %= as<std::string>()[+(alnum|char_('_')|char_('.')|char_('-')|char_('+'))];
    quoted_string %= lexeme['"' >> +(char_ - '"') >> '"'];

    parsed_arg_list = 
      ("{" >> ( (quoted_string[push_back(_val,_1)]) % ",") >> "}") |
      ("{" >> ( (identifier[push_back(_val,_1)]) % ",") >> "}") |
      (quoted_string[push_back(_val,_1)]) |
      (identifier[push_back(_val,_1)]);

    one_arg %= (identifier >> "=" >> parsed_arg_list );

    start = one_arg[bind(check_and_insert_arg, _val, _1)] % ",";
  }

  qi::rule<Iterator, std::string(), ascii::space_type> quoted_string;
  qi::rule<Iterator, std::string(), ascii::space_type> identifier;
  qi::rule<Iterator, arg_list(), ascii::space_type> parsed_arg_list;
  qi::rule<Iterator, arg_struct(), ascii::space_type> one_arg;
  qi::rule<Iterator, arg_container(), ascii::space_type> start;
};

template<typename Iterator>
static Iterator run_parser(Iterator first, Iterator last, arg_container& kw)
{
  using boost::spirit::ascii::space;
  keyword_grammar<Iterator> g;

  bool r = qi::phrase_parse(first, last, g, space, kw);

  if (!r)
    {
      cerr << format("Error while parsing. Stopped at '%s'.") % string(first, last) << endl;
      throw SamplerParseException();
    }
    
  return first;
}


CMB::KeywordMap CMB::parseKeywordOptions(const std::string& options)
{
  CMB::KeywordMap kwmap;
  arg_container ac;
  
  if (options.empty())
    return kwmap;
  
  if (run_parser(options.begin(), options.end(), ac) != options.end())
    {
       cerr << "Syntax error in " << options << endl;
       throw SamplerParseException();
    }
  
  for (arg_container::iterator i = ac.begin();
      i != ac.end();
      ++i)
    {
      if (i->second.size()==1)
       {
         cout << format("Copied kwmap[%s] = %s (type=%s)") % i->first % (*(i->second.begin())) % (typeid(*(i->second.begin())).name()) << endl;
         kwmap[i->first] = *(i->second.begin());
       }
      else
        kwmap[i->first] = i->second;
    }
   
  return kwmap;
}

