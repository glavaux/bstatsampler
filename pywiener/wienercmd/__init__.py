#+
# This is ABYSS (./pywiener/wienercmd/__init__.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
from .setup_channels import setupChannels
from .quit_command import quitWiener
from .setup_cls import setupCls
from .setup_data import setupData

QUIT_COMMAND = 0
SETUP_CHANNELS_COMMAND = 1
SETUP_CLS_COMMAND = 2
SETUP_DATA_COMMAND = 3
DO_WIENER_COMMAND = 4
MASK_READY = 5
BEAM_READY = 6
NOISE_READY = 7
CHANNEL_FINISHED = 8
DATA_READY = 9
CLS_READY = 10


OK_ACK=1
ERR_ACK=0

def _doWiener(w):
    lmax = w.recvInt()
    shm_name = w.recvString()
    try:
        a = w.dispatcher.doWiener(lmax)
    except:
        a = sys.exc_info()
        print("An exception has been raised");
        tb.print_exception(*a)
        w.sendError()
        return
    w.sendAlmArray(shm_name, a)

wiener_dispatch = {
  QUIT_COMMAND: quitWiener,
  SETUP_CHANNELS_COMMAND: setupChannels,
  SETUP_CLS_COMMAND: setupCls,
  SETUP_DATA_COMMAND: setupData,
  DO_WIENER_COMMAND: _doWiener,
}

