#+
# This is ABYSS (./pywiener/wienerloop.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import posix_ipc
import mmap
import sys
import os
import wienercmd as wc


class WienerCommand(object):

    def sendAck(self):
        pipe_out,pipe_in=self.pipe_out,self.pipe_in
        pipe_out.write(OK_ACK)
        pipe_out.flush()

    def sendError(self):
        self.pipe_out.write(ERR_ACK)
        self.pipe_out.flush()

    def recvInt(self):
        l = struct.unpack('i',pipe_in.read(4))[0]
        return l

    def recvString(self):
        pipe_out,pipe_in=self.pipe_out,self.pipe_in
        l = struct.unpack('h',pipe_in.read(2))[0]
        return pipe_in.read(l)

    def recvArray(dtype):
        shm_name = wpipe.recvString(state)
        sizeT,sizeA,readyType = struct.unpack('hlb',pipe_in.read(11))
        shm = posix_ipc.SharedMemory(shm_name)
        M = mmap.mmap(shm.fd, sizeA*sizeT)
        a = np.frombuffer(M,dtype=dtype).copy()
        M.close()
        shm.close_fd()
        return a,readyType

    def sendAlmArray(shm_name,alms):
        L= alms.size*alms.dtype.itemsize
        s = struct.pack('hl', alms.dtype.itemsize, alms.size)
        self.pipe_out.write(s)
        self.pipe_out.flush()
        shm = posix_ipc.SharedMemory(shm_name)
        M = mmap.mmap(shm.fd, L)
        M[:] = alms.tostring()
        M.close()
        shm.close_fd()
        self.pipe_out.write('\x01')
        self.pipe_out.flush()
        
    def quitWiener(self):
        self.sendAck()
        self.quit_loop = True
        pipe_out.close()
        pipe_in.close()
  
    def __init__(self):
        self.pipe_out = os.fdopen(int(sys.argv[1]), mode='w')
        self.pipe_in = os.fdopen(int(sys.argv[2]), mode='r')

    def run_wiener(self):
        self.quit_loop = False
        while not self.quit_loop:    
            cmd = struct.unpack('b',self.pipe_in.read(1))[0]      
            wc.wiener_dispatch[cmd](state)

    def setup_dispatch(self,target):
        self.dispatcher = target

