/*+
This is ABYSS (./tests/generate_KSZ.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include "generate_KSZ_conf.h"
#include <alm.h>
#include <xcomplex.h>

int main(int argc, char **argv)
{
  Healpix_Map<double> base_map, derived_map;
  Alm<xcomplex<double> > base_alms;
  gsl_rng *rng;
  int component;

  struct kszParams params;

  if (kszConf(argc, argv, &params))
    return 1;

  rng = gsl_rng_alloc(gsl_rng_env_setup());
  gsl_rng_set(rng, params.seed_arg);

  base_map.SetNside(params.Nside_arg, RING);
  base_alms.Set(params.Lmax_arg, params.Lmax_arg);

  component = params.component_arg;

  base_alms(0,0) = 0;
  base_alms(1,0) = base_alms(1,1) = 0;
  for (int l = 2; l <= params.Lmax_arg; l++)
    {
      double Cl = pow(l, params.power_arg)*params.norm_arg;
      double sqCl = sqrt(Cl);

      base_alms(l,0) = gsl_ran_gaussian(rng, sqCl);
      for (int m = 1; m <= l; m++)
	{
	  base_alms(l,m) = 
	    xcomplex<double>(gsl_ran_gaussian(rng, sqCl),
			     gsl_ran_gaussian(rng, sqCl));
	}
    }

  alm2map(base_alms, base_map);

  double calibration = (params.cmb_calibrated_flag ? 0.77e-6*2.72*0.04 : 1.0);

  for (int p = 0; p < base_map.Npix(); p++)
    {
      vec3 v = base_map.pix2vec(p);
      double xyz[3] = {v.x, v.y, v.z};
      double ur = (component == -1) ? 1 : xyz[component];

      base_map[p] = calibration*(exp(base_map[p]))*ur;
    }

  {
    fitshandle f;
    
    f.create("!outmap.fits");
    write_Healpix_map_to_fits(f, base_map, planckType<double>());
  }

  return 0;
}
