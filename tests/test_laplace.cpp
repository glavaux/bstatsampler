/*+
This is ABYSS (./tests/test_laplace.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <cmath>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <CosmoTool/miniargs.hpp>
#include "laplace_solver.hpp"

using namespace std;
using namespace CMB;
using namespace CosmoTool;

int main(int argc, char **argv)
{
  Healpix_Map<double> input_map, output;
  Healpix_Map<int> mask_map;
  char *inputMap, *inputMask;
  int Lmax, numKernels;
  double maxRadius;

  MiniArgDesc desc[] = {
    { "INPUT MAP", &inputMap, MINIARG_STRING },
    { "INPUT MASK", &inputMask, MINIARG_STRING },
    { "LMAX", &Lmax, MINIARG_INT },
    { "MAX RADIUS", &maxRadius, MINIARG_DOUBLE },
    { "NUM KERNELS", &numKernels, MINIARG_INT },
    { 0, 0, MINIARG_NULL }
  };

  if (!parseMiniArgs(argc, argv, desc))
    return 1;

  read_Healpix_map_from_fits(inputMap, input_map);
  read_Healpix_map_from_fits(inputMask, mask_map);

  if (mask_map.Scheme() == NEST)
    mask_map.swap_scheme();

  maxRadius *= M_PI/180;
    
  LaplaceSolver solver(Lmax, mask_map, maxRadius, numKernels);

  output.SetNside(input_map.Nside(), RING);
  solver.solve(input_map, output);

  {
    fitshandle f;
    f.create("!output.fits");
    write_Healpix_map_to_fits(f, output, planckType<double>());
  }

  return 0;
}
