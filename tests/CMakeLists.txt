add_executable(generateRandomMap generateRandomMap.cpp)


target_link_libraries(generateRandomMap ${HEALPIX_LIBRARIES} ${COSMOTOOL_LIBRARY} ${GSL_LIBRARIES})


if (GENGETOPT)
  set(genksz_SRCS generate_KSZ.cpp)

  add_genopt(genksz_SRCS
    generate_KSZ.ggo
    generate_KSZ_conf
    STRUCTNAME kszParams
    FUNCNAME kszConf
  )

  add_executable(genksz ${genksz_SRCS})

  target_link_libraries(genksz ${HEALPIX_LIBRARIES} ${GSL_LIBRARIES})
endif(GENGETOPT)

SET(basedir ${CMAKE_SOURCE_DIR}/libsampler)
include_directories(${basedir} ${basedir}/operators ${basedir}/samplers ${basedir}/samplers/smp ${basedir}/data ${basedir}/tools)

add_executable(test_laplace test_laplace.cpp)
target_link_libraries(test_laplace smp_sampler ${HEALPIX_LIBRARIES} ${COSMOTOOL_LIBRARY} ${GSL_LIBRARIES} ${NETCDFCPP_LIBRARY} ${NETCDF_LIBRARY} ${HDF5HL_LIBRARY} ${HDF5_LIBRARY} ${ZLIB_LIBRARY})
