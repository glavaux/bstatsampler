/*+
This is ABYSS (./tests/generateRandomMap.cpp) -- Copyright (C) Guilhem Lavaux (2009-2014)

guilhem.lavaux@gmail.com

This software is a computer program whose purpose is to provide to do full sky
bayesian analysis of random fields (e.g., non exhaustively,
wiener filtering, power spectra, lens reconstruction, template fitting).

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
+*/

#include <omp.h>
// STANDARD
#include <cstring>
#include <cassert>
#include <vector>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>

// HEALPIX
#include <vec3.h>
#include <planck_rng.h>
#include <xcomplex.h>
#include <arr.h>
#include <healpix_map.h>
#include <healpix_map_fitsio.h>
#include <alm_healpix_tools.h>
#include <alm_fitsio.h>
#include <alm.h>
#include <powspec.h>
#include <powspec_fitsio.h>
#include <alm_powspec_tools.h>
#include <fitshandle.h>

// COSMOTOOL
#include <CosmoTool/miniargs.hpp>

using namespace CosmoTool;
using namespace std;

int main(int argc, char **argv)
{
  Healpix_Map<float> cmb_map;
  int lmax = 50, lmaxphi = 3;
  double regul;
  int nside = 128;

  MiniArgDesc args[] = {
    { "LMAX", &lmax, MINIARG_INT },
    { "NSIDE", &nside, MINIARG_INT },
    { 0,0,MINIARG_NULL }
  };

  if (!parseMiniArgs(argc, argv, args))
    return 1;

  PowSpec powspec(1, lmax);
  int seed;
  Alm<xcomplex<float> > alm(lmax,lmax);

  seed = time(0);
  planck_rng rng(seed);

  read_powspec_from_fits("spectrum.fits", powspec, 1, lmax);
  create_alm(powspec, alm, rng);
  cout << "Generated alms..." << endl;

  PowSpec powspec2(1, lmax);
  fitshandle fh;
  extract_powspec(alm, powspec2);

  fh.create("!spectrum_effective.fits");
  write_powspec_to_fits(fh, powspec2, 1);

  cmb_map.SetNside(nside, RING);

  cout << "Generating maps..." << endl;
  alm2map(alm, cmb_map);
  cout << "Map RMS is " << cmb_map.rms() << endl;
  cout << "Power spectrum: N_Cls = " << powspec.tt().size() << endl;

  {
    fitshandle out;
    out.create("!ref.fits");    
    write_Healpix_map_to_fits(out, cmb_map, planckType<float>());
  }

  return 0;
}
