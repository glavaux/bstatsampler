#+
# This is ABYSS (./doc/exchange_algorithm/sample/gen_mod_data.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import numpy as np

N=10000
s=1.0

m_hat=np.zeros(N/2+1,dtype=np.complex128)

m_hat[0]=0
m_hat[1]=0.1
m_hat[2]=0.05+0.03*1j
m_hat[3]=0.4-0.01*1j

m=1+N*np.fft.irfft(m_hat)

x=np.random.randn(N)*s*m


np.save("data.npy", x)
