#+
# This is ABYSS (./doc/exchange_algorithm/sample/fit_modulate.py) -- Copyright (C) Guilhem Lavaux (2009-2014)
#
# guilhem.lavaux@gmail.com
#
# This software is a computer program whose purpose is to provide to do full sky
# bayesian analysis of random fields (e.g., non exhaustively,
# wiener filtering, power spectra, lens reconstruction, template fitting).
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#+
import numpy as np

S0=1
S=0.002

def compute_chi2(x,m):
    return ((x/m)**2).sum()

def gen_proposal(m_hat):

    Q=m_hat.size-1
    m_new_hat = m_hat.copy()
    m_new_hat[1:4].real += S*np.random.randn(3)
    m_new_hat[1:4].imag += S*np.random.randn(3)

    return m_new_hat

def chi2_proposal(m_old, m_new):
    return 0.5*(np.abs((m_old-m_new)/S)**2).sum()

def do_mcmc_step(m,m_hat,data):
    
    m_hat_new = gen_proposal(m_hat)

    m_new = data.size*np.fft.irfft(m_hat_new)+1
    
    fake = np.random.randn(data.size)*S0*m_new

    chi2_data_old = compute_chi2(data, m)
    chi2_data_new = compute_chi2(data, m_new)

    chi2_fake_old = compute_chi2(fake, m)
    chi2_fake_new = compute_chi2(fake, m_new)

    log_accept  = chi2_fake_old + chi2_data_new
    log_accept -= chi2_data_old + chi2_fake_new

    accept = np.exp(-0.5*max(0,log_accept))
    if np.random.uniform() < accept:
        return m_new,m_hat_new,chi2_data_new,log_accept
    else:
        return m,m_hat,chi2_data_old,log_accept

def run_mcmc(data,N):

    chain=[]
    chain_chi2=[]
    m_hat=np.zeros(data.size/2+1,dtype=np.complex128)
    m=1+np.fft.irfft(m_hat)
    while N>0:
        m,m_hat,chi2,accept=do_mcmc_step(m,m_hat,data)
        chain.append((m_hat[:4].copy()))
        chain_chi2.append((chi2,accept))
        N=N-1

    return chain,chain_chi2
